# encoding=utf-8
"""
scrape honda interactive network for vehicle status, invoices and incentives
5/2 status parse, invoice parse,
    incentive download
    incentive parse: fails when publish date has changed
5/8/20
    realized, the output file consists of the date and the class name (the local_target)
    tried to run honda incentives today, nothing ran, because the class name for nissan incentives is
    IncentivesDownload, the same as what i have named the classes here
    for this file, i will include the pipeline in the definition of local_target
    worked for IncetivesDownload, do it for all classes in this file

cd /home/jon/projects/luigi_1804_36 && /home/jon/Desktop/venv/luigi/bin/python3 -m luigi --module honda_csi ServiceCsiParse
cd /home/jon/projects/luigi_1804_36 && /home/jon/Desktop/venv/luigi/bin/python3 -m luigi --module honda_csi SalesCsiParse

"""

import utilities
import luigi
import datetime
from selenium import webdriver
import csv
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.by import By
import requests
import os
import time

pipeline = 'honda'
pg_server = '173'
ts_no_spaces = str(datetime.datetime.now()).replace(" ", "")
dealer_number = '207818'

user = '41d9ec15-5148-439d-938a-313a1fa91cc6'
passcode = '666'
username = None
password = None

# local_or_production = 'production'
local_or_production = 'local'


# # if these task are being called via run_all, that task will handle all of this logging
@luigi.Task.event_handler(luigi.Event.START)
def task_start(self):
    utilities.luigi_log_start(pg_server, self.task_id + '__' + ts_no_spaces, self.pipeline, self.__class__.__name__)


@luigi.Task.event_handler(luigi.Event.SUCCESS)
def task_success(self):
    utilities.luigi_log_pass(pg_server, self.task_id + '__' + ts_no_spaces, self.pipeline, self.__class__.__name__)
    try:  # WrapperTest throws an AttributeError, see /Notes/issues
        with self.output().open("w") as out_file:
            # write to the output file
            out_file.write('pass')
    except AttributeError:
        pass


class SalesCsiDownload(luigi.Task):
    """
    download vehicle status spreadsheet as VehicleStatus.csv
    """
    pipeline = pipeline

    if local_or_production == 'local':
        sales_csi_dir = '/home/jon/projects/luigi_1804_36/honda/sales_csi/'
        sales_csi_file_mtd = sales_csi_dir + 'sales_csi_mtd.csv'
        sales_csi_file_prev90 = sales_csi_dir + 'sales_csi_prev90.csv'
        # service_csi_file_mtd = csi_dir + 'service_csi_mtd.csv'
        # service_csi_file_prev90 = csi_dir + 'service_csi_prev90.csv'

    # elif local_or_production == 'production':
        # vehicle_status_dir = '/home/rydell/projects/luigi_1804_36/honda/vehicle_status/'
        # clean_vehicle_status_dir = '/home/rydell/projects/luigi_1804_36/honda/clean_vehicle_status/'

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                pipeline + '_' + self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    @staticmethod
    def get_secret(category, platform):
        modified_platform = platform.replace(' ', '+')
        url = ("https://beta.rydellvision.com:8888/vision/secret?platform={0}&category={1}".format
               (modified_platform, category))
        response = requests.get(url, auth=(user, passcode))
        return response.json()

    def setup_driver(self):
        # # Chromedriver path
        chromedriver_path = '/usr/bin/chromedriver'
        options = webdriver.ChromeOptions()
        options.add_argument('--user-agent="Mozilla/5.0 ((Windows NT 10.0; Win64; x64) AppleWebKit/537.36 '
                             '(KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36"')
        driver = webdriver.Chrome(executable_path=chromedriver_path, options=options)
        return driver

    def run(self):
        for filename in os.listdir(self.sales_csi_dir):
            os.unlink(self.sales_csi_dir + filename)
        driver = self.setup_driver()
        try:
            driver.get('https://www.in.honda.com/RRAAApps/login/asp/rraalog.asp')
            credentials = self.get_secret('Cartiva', 'Honda')
            resp_dict = credentials
            driver.find_element_by_xpath('//*[@id="txtDlrNo"]').send_keys(dealer_number)
            username_box = driver.find_element_by_xpath('//*[@id="txtLogonID"]')
            password_box = driver.find_element_by_xpath('//*[@id="txtPassword"]')
            login_btn = driver.find_element_by_xpath('//*[@id="btnLogon"]')
            username_box.send_keys(resp_dict.get('username'))
            password_box.send_keys(resp_dict.get('secret'))
            login_btn.click()
            # everything on the main page is in an iframe named Content
            WebDriverWait(driver, 30).until(
                ec.frame_to_be_available_and_switch_to_it((By.XPATH, '//*[@id="Content"]')))
            # Honda Sales Experience (HSE)
            WebDriverWait(driver, 30).until(ec.element_to_be_clickable(
                (By.XPATH, '//*[@id="SalesSatTelSurveySection1"]/table/tbody/tr[1]/th/a'))).click()
            driver.switch_to.window(driver.window_handles[1])

            # wait until page finishes loading
            # SALES MTD
            WebDriverWait(driver, 20).until(ec.presence_of_element_located(
                ('xpath', '//*[@id="pieChartInfoText"]/div[2]/a')))
            # the page opens by default on current month to date
            # 02/25/21 new "dashboardish" page, which has to be clicked to get to the data
            # DEALER NPS
            driver.find_element_by_xpath('//*[@id="pieChartInfoText"]/div[2]/a').click()
            WebDriverWait(driver, 20).until(ec.presence_of_element_located(
                ('xpath', '//*[@id="OrgGrid"]/tbody/tr[2]/td[3]')))
            store_nps_mtd = driver.find_element_by_xpath('//*[@id="OrgGrid"]/tbody/tr[5]/td[4]').text
            zone_nps_mtd = driver.find_element_by_xpath('//*[@id="OrgGrid"]/tbody/tr[3]/td[4]').text
            national_nps_mtd = driver.find_element_by_xpath('//*[@id="OrgGrid"]/tbody/tr[2]/td[4]').text
            store_dealer_rec_mtd = driver.find_element_by_xpath('//*[@id="OrgGrid"]/tbody/tr[5]/td[5]').text
            zone_dealer_rec_mtd = driver.find_element_by_xpath('//*[@id="OrgGrid"]/tbody/tr[3]/td[5]').text
            national_dealer_rec_mtd = driver.find_element_by_xpath('//*[@id="OrgGrid"]/tbody/tr[2]/td[5]').text
            sales_mtd = [store_nps_mtd, zone_nps_mtd, national_nps_mtd, store_dealer_rec_mtd,
                         zone_dealer_rec_mtd, national_dealer_rec_mtd]
            with open(self.sales_csi_file_mtd, 'w') as the_sales_mtd:
                the_writer = csv.writer(the_sales_mtd)
                the_writer.writerow(sales_mtd)
            # back to the dashboard to change time period
            # click the hamburger
            time.sleep(3)
            hamburger = driver.find_element_by_class_name('menu-wrapper')
            driver.execute_script("arguments[0].click();", hamburger)
            # click the back arrow
            time.sleep(3)
            back_arrow = driver.find_element_by_class_name('back-link')
            driver.execute_script("arguments[0].click();", back_arrow)
            # wait for the dashboard to fully load
            WebDriverWait(driver, 20).until(ec.presence_of_element_located(
                ('xpath', '//*[@id="pieChartInfoText"]/div[2]/a')))
            # change the time period to previous 90 days (R3M on the calendar)
            # expose the calendar
            calendar = driver.find_element_by_xpath('//*[@id="filterDesktop"]/li[5]/div[2]/div[1]')
            driver.execute_script("arguments[0].click();", calendar)
            # click R3M
            r3m = driver.find_element_by_xpath('//*[@id="Filter-1_datePeriod"]/li[4]/a')
            driver.execute_script("arguments[0].click();", r3m)
            # click Apply Filters
            apply_filter_button = driver.find_element_by_xpath(
                '//*[@id="filterDesktop"]/li[5]/div[2]/div[1]/div[2]/span[2]')
            driver.execute_script("arguments[0].click();", apply_filter_button)
            # click DEALER NPS
            time.sleep(10)  # added 3/14/21
            driver.find_element_by_xpath('//*[@id="pieChartInfoText"]/div[2]/a').click()
            WebDriverWait(driver, 30).until(ec.presence_of_element_located(
                ('xpath', '//*[@id="OrgGrid"]/tbody/tr[2]/td[3]')))
            # get the data
            store_nps_prev90 = driver.find_element_by_xpath('//*[@id="OrgGrid"]/tbody/tr[5]/td[4]').text
            zone_nps_prev90 = driver.find_element_by_xpath('//*[@id="OrgGrid"]/tbody/tr[3]/td[4]').text
            national_cse_index_prev90 = driver.find_element_by_xpath('//*[@id="OrgGrid"]/tbody/tr[2]/td[4]').text
            store_dealer_rec_prev90 = driver.find_element_by_xpath('//*[@id="OrgGrid"]/tbody/tr[5]/td[5]').text
            zone_dealer_rec_prev90 = driver.find_element_by_xpath('//*[@id="OrgGrid"]/tbody/tr[3]/td[5]').text
            national_dealer_rec_prev90 = driver.find_element_by_xpath('//*[@id="OrgGrid"]/tbody/tr[2]/td[5]').text
            sales_prev90 = [store_nps_prev90, zone_nps_prev90, national_cse_index_prev90,
                            store_dealer_rec_prev90, zone_dealer_rec_prev90, national_dealer_rec_prev90]
            with open(self.sales_csi_file_prev90, 'w') as the_sales_prev90:
                the_writer = csv.writer(the_sales_prev90)
                the_writer.writerow(sales_prev90)
            # close the Organization Summary window
            driver.close()
            # focus back to the original window
            driver.switch_to.window(driver.window_handles[0])
            assert len(os.listdir(self.sales_csi_dir)) == 2
        finally:
            driver.quit()


class SalesCsiParse(luigi.Task):
    """
    """
    pipeline = pipeline

    if local_or_production == 'local':
        sales_csi_dir = '/home/jon/projects/luigi_1804_36/honda/sales_csi/'
        sales_csi_file_mtd = sales_csi_dir + 'sales_csi_mtd.csv'
        sales_csi_file_prev90 = sales_csi_dir + 'sales_csi_prev90.csv'

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                pipeline + '_' + self.__class__.__name__ + ".txt")

    def requires(self):
        yield SalesCsiDownload()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute('truncate hn.mtd_hin_sales_make_csi_ext')
                with open(self.sales_csi_file_mtd, 'r') as io:
                    pg_cur.copy_expert("copy hn.mtd_hin_sales_make_csi_ext from stdin with csv encoding 'latin-1 '", io)
                pg_cur.execute('truncate hn.prev90_hin_sales_make_csi_ext')
                with open(self.sales_csi_file_prev90, 'r') as io:
                    pg_cur.copy_expert(
                        "copy hn.prev90_hin_sales_make_csi_ext from stdin with csv encoding 'latin-1 '", io)
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute('select hn.hin_sale_make_csi_update();')


class ServiceCsiDownload(luigi.Task):
    """
    """
    pipeline = pipeline

    if local_or_production == 'local':
        csi_dir = '/home/jon/projects/luigi_1804_36/honda/service_csi/'
        service_csi_file_mtd = csi_dir + 'service_csi_mtd.csv'
        service_csi_file_prev90 = csi_dir + 'service_csi_prev90.csv'
    elif local_or_production == 'production':
        vehicle_status_dir = '/home/rydell/projects/luigi_1804_36/honda/vehicle_status/'
        clean_vehicle_status_dir = '/home/rydell/projects/luigi_1804_36/honda/clean_vehicle_status/'

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                pipeline + '_' + self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    @staticmethod
    def get_secret(category, platform):
        modified_platform = platform.replace(' ', '+')
        url = ("https://beta.rydellvision.com:8888/vision/secret?platform={0}&category={1}".format
               (modified_platform, category))
        response = requests.get(url, auth=(user, passcode))
        return response.json()

    def setup_driver(self):
        # # Chromedriver path
        chromedriver_path = '/usr/bin/chromedriver'
        options = webdriver.ChromeOptions()
        options.add_argument('--user-agent="Mozilla/5.0 ((Windows NT 10.0; Win64; x64) AppleWebKit/537.36 '
                             '(KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36"')
        driver = webdriver.Chrome(executable_path=chromedriver_path, options=options)
        return driver

    def run(self):
        for filename in os.listdir(self.csi_dir):
            os.unlink(self.csi_dir + filename)
        driver = self.setup_driver()
        try:
            driver.get('https://www.in.honda.com/RRAAApps/login/asp/rraalog.asp')
            credentials = self.get_secret('Cartiva', 'Honda')
            resp_dict = credentials
            driver.find_element_by_xpath('//*[@id="txtDlrNo"]').send_keys(dealer_number)
            username_box = driver.find_element_by_xpath('//*[@id="txtLogonID"]')
            password_box = driver.find_element_by_xpath('//*[@id="txtPassword"]')
            login_btn = driver.find_element_by_xpath('//*[@id="btnLogon"]')
            username_box.send_keys(resp_dict.get('username'))
            password_box.send_keys(resp_dict.get('secret'))
            login_btn.click()

            # Now service csi
            # everything on the main page is in an iframe named Content
            WebDriverWait(driver, 30).until(
                ec.frame_to_be_available_and_switch_to_it((By.XPATH, '//*[@id="Content"]')))
            # click the Service tab
            driver.find_element_by_xpath('//*[@id="ulService"]').click()
            # # everything on the main page is in an iframe named Content
            # WebDriverWait(driver, 30).until(
            #     ec.frame_to_be_available_and_switch_to_it((By.XPATH, '//*[@id="Content"]')))
            # click CSE Daily Reports
            driver.find_element_by_xpath('//*[@id="CSEDailySection"]/table/tbody/tr[1]/th/a').click()
            driver.switch_to.window(driver.window_handles[1])
            # wait until page finishes loading
            WebDriverWait(driver, 20).until(ec.presence_of_element_located(
                ('xpath', '//*[@id="ctl00_ContentPlaceHolder1_ctl00_rptWidgets_ctl02_lblDealerIndex"]')))
            # the page opens by default on current month to date
            rydell_cse_index_mtd = driver.find_element_by_xpath(
                '//*[@id="ctl00_ContentPlaceHolder1_ctl00_rptWidgets_ctl02_lblDealerIndex"]').text
            zone_cse_index_mtd = driver.find_element_by_xpath(
                '//*[@id="ctl00_ContentPlaceHolder1_ctl00_rptWidgets_ctl02_lblZoneIndex"]').text
            national_cse_index_mtd = driver.find_element_by_xpath(
                '//*[@id="ctl00_ContentPlaceHolder1_ctl00_rptWidgets_ctl02_lblNationaltIndex"]').text
            service_mtd = [rydell_cse_index_mtd, zone_cse_index_mtd, national_cse_index_mtd]
            with open(self.service_csi_file_mtd, 'w') as the_service_mtd:
                the_writer = csv.writer(the_service_mtd)
                the_writer.writerow(service_mtd)
            # click the dates to enable the Date Range popup
            driver.find_element_by_xpath('//*[@id="ctl00_ContentPlaceHolder1_ctl00_lblDateRange"]').click()
            # click the Date Range drop down
            driver.find_element_by_xpath('//*[@id="ctl00_ContentPlaceHolder1_ctl00_ddlDateFilters"]').click()
            # select last 90 days
            driver.find_element_by_xpath('//*[@id="ctl00_ContentPlaceHolder1_ctl00_ddlDateFilters"]/option[5]').click()
            # click save
            driver.find_element_by_xpath('//*[@id="ctl00_ContentPlaceHolder1_ctl00_lnkDateRangeSet"]/div/img').click()
            # wait for the page to load
            time.sleep(20)
            rydell_cse_index_prev90 = driver.find_element_by_xpath(
                '//*[@id="ctl00_ContentPlaceHolder1_ctl00_rptWidgets_ctl02_lblDealerIndex"]').text
            zone_cse_index_prev90 = driver.find_element_by_xpath(
                '//*[@id="ctl00_ContentPlaceHolder1_ctl00_rptWidgets_ctl02_lblZoneIndex"]').text
            national_cse_index_prev90 = driver.find_element_by_xpath(
                '//*[@id="ctl00_ContentPlaceHolder1_ctl00_rptWidgets_ctl02_lblNationaltIndex"]').text
            service_prev90 = [rydell_cse_index_prev90, zone_cse_index_prev90, national_cse_index_prev90]
            with open(self.service_csi_file_prev90, 'w') as the_service_prev90:
                the_writer = csv.writer(the_service_prev90)
                the_writer.writerow(service_prev90)
        finally:
            driver.quit()


class ServiceCsiParse(luigi.Task):
    """
    """
    pipeline = pipeline

    if local_or_production == 'local':
        csi_dir = '/home/jon/projects/luigi_1804_36/honda/service_csi/'
        service_csi_file_mtd = csi_dir + 'service_csi_mtd.csv'
        service_csi_file_prev90 = csi_dir + 'service_csi_prev90.csv'

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                pipeline + '_' + self.__class__.__name__ + ".txt")

    def requires(self):
        yield ServiceCsiDownload()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute('truncate hn.mtd_hin_service_make_csi_ext')
                with open(self.service_csi_file_mtd, 'r') as io:
                    pg_cur.copy_expert("copy hn.mtd_hin_service_make_csi_ext from stdin with csv encoding 'latin-1 '", io)
                pg_cur.execute('truncate hn.prev90_hin_service_make_csi_ext')
                with open(self.service_csi_file_prev90, 'r') as io:
                    pg_cur.copy_expert(
                        "copy hn.prev90_hin_service_make_csi_ext from stdin with csv encoding 'latin-1 '", io)
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute('select hn.hin_service_make_csi_update();')


if __name__ == '__main__':
    luigi.run(["--workers=1"], main_task_cls=SalesCsiDownload)
# ServiceCsiParse  SalesCsiParse SalesCsiDownload