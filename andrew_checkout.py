# encoding=utf-8
"""

"""
import luigi
import utilities
import datetime
import smtplib
from email.message import EmailMessage
import xfm_arkona
import fact_repair_order

pipeline = 'andrew_checkout'
db2_server = 'report'
pg_server = '173'
ts_no_spaces = str(datetime.datetime.now()).replace(" ", "")

# # if these task are being called via run_all, that task will handle all of this logging
@luigi.Task.event_handler(luigi.Event.START)
def task_start(self):
    utilities.luigi_log_start(pg_server, self.task_id + '__' + ts_no_spaces, self.pipeline, self.__class__.__name__)


@luigi.Task.event_handler(luigi.Event.SUCCESS)
def task_success(self):
    utilities.luigi_log_pass(pg_server, self.task_id + '__' + ts_no_spaces, self.pipeline, self.__class__.__name__)
    try:  # WrapperTest throws an AttributeError, see /Notes/issues
        with self.output().open("w") as out_file:
            # write to the output file
            out_file.write('pass')
    except AttributeError:
        pass

class Ry1Checkout5Pm(luigi.Task):
    """
    """
    pipeline = pipeline
    body = ''
    # format date as Thursday 01/04/18
    subject = "Checkout data for " + (datetime.date.today()).strftime("%A %x") + " at 4:00 PM"

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        return None
        # yield xfm_arkona.XfmPypclockin()
        # yield fact_repair_order.FactRepairOrder()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = """
                    insert into dds.andrew_checkout
                    SELECT -- clockhours yesterday
                      current_date, current_time,
                      2 as seq, 'Tech Clock Hours', SUM(clock_hours) AS clock_hours
                    FROM arkona.xfm_pypclockin a
                    INNER JOIN (
                      SELECT b.pymast_employee_number
                      FROM dds.dim_tech a
                      inner JOIN arkona.ext_pymast b on a.employee_number = b.pymast_employee_number
                        AND b.active_code <> 'T'
                      WHERE a.active 
                        AND a.flag_department_code = 'mr'
                        AND a.current_row
                        AND a.store_code = 'RY1'
                        AND a.employee_number <> '1117960') aa on a.employee_number = aa.pymast_employee_number
                    INNER JOIN dds.dim_Date bb on a.the_date = bb.the_date
                      AND bb.the_date = current_date - 1
                    union
                    SELECT -- flaghours yesterday
                      current_date,
                      current_time,
                      CASE d.payment_type
                        WHEN 'customer pay' THEN 3
                        WHEN 'internal' THEN 4
                        WHEN 'warranty' THEN 5
                        WHEN 'service contract' THEN 6
                      END AS seq,
                      trim (d.payment_type) || ' Flag Hours', round(SUM(flag_hours), 1)
                    FROM dds.fact_repair_order a
                    INNER JOIN dds.dim_date b on a.close_date = b.the_date
                      AND b.date_type = 'DATE'
                      AND b.the_date = current_Date -1
                    INNER JOIN dds.dim_Service_type c on a.service_type_key = c.service_type_key
                      AND c.service_type_code IN ('AM','CM','EM','MN','MR','SV')
                    INNER JOIN dds.dim_payment_type d on a.payment_type_key = d.payment_type_key
                      AND d.payment_type_key IN (2,3,4,5)
                    where a.store_code = 'RY1'  
                    GROUP by d.payment_type;              
                """
                pg_cur.execute(sql)
                sql = """
                    SELECT metric, value
                    FROM dds.andrew_checkout
                    WHERE the_date = current_date
                      and extract(hour from the_time) > 12
                    ORDER BY seq;              
                """
                pg_cur.execute(sql)
                for t in pg_cur.fetchall():
                    self.body += t[0] + ': ' + str(t[1]) + '\n'
                self.body += '\n'


        msg = EmailMessage()
        msg.set_content(self.body)
        msg['Subject'] = self.subject
        msg['From'] = 'jandrews@cartiva.com'
        msg['To'] = 'jandrews@cartiva.com'
        s = smtplib.SMTP('mail.cartiva.com')
        s.send_message(msg)
        s.quit()


if __name__ == '__main__':
    luigi.run(["--workers=4"], main_task_cls=Ry1Checkout5Pm)