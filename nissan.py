# encoding=utf-8
"""
just like global connect, but for nissan
03/29/20
    don't need summary
05/08/28
    prefix local target name with pipeline (see comments in honda.py header)
"""

import utilities
import luigi
import datetime
import os
from selenium import webdriver
import time
import csv
import camelot
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.common.exceptions import NoSuchElementException
from pyvirtualdisplay import Display

pipeline = 'nissan'
pg_server = '173'
ts_no_spaces = str(datetime.datetime.now()).replace(" ", "")
user = 'XD163692'
password = '%6#@%!!@eDHN'


# # if these task are being called via run_all, that task will handle all of this logging
@luigi.Task.event_handler(luigi.Event.START)
def task_start(self):
    utilities.luigi_log_start(pg_server, self.task_id + '__' + ts_no_spaces, self.pipeline, self.__class__.__name__)


@luigi.Task.event_handler(luigi.Event.SUCCESS)
def task_success(self):
    utilities.luigi_log_pass(pg_server, self.task_id + '__' + ts_no_spaces, self.pipeline, self.__class__.__name__)
    try:  # WrapperTest throws an AttributeError, see /Notes/issues
        with self.output().open("w") as out_file:
            # write to the output file
            out_file.write('pass')
    except AttributeError:
        pass


class VehicleInventoryDetailDownload(luigi.Task):
    """
    04/06
    failed on production, elements not discoveralbe
    installed pyvirtualdisplay
    wrapped run in:
    with Display(visible=False, size=(1200,1500)):
    https://stackoverflow.com/questions/45520464/selenium-script-working-from-console-not-working-
        in-cron-geckodriver-error
    05/22: failing last couple of days, there are 109 vehicles and each time it seems to fail at or around
        101. mixed results on the actual error being generated. today (from production) it was:
          File "/home/rydell/projects/luigi_1804_36/nissan.py", line 260, in run
            WebDriverWait(driver, 20).until(ec.number_of_windows_to_be(2))
            raise TimeoutException(message, screen, stacktrace) selenium.common.exceptions.TimeoutException
        at seq 100 the fucking page resets to the default presentation (filttered location, 1 of 67)
            this makes no sense, can't implement a continue on fail, because the state of the page
            is no longer where it was
        try to stop at seq 100 and manually step thru
        ok, this is weird, stopped at 100, stepped thru, when got to
            WebDriverWait(driver, 20).until(ec.number_of_windows_to_be(2))
        the fucking page reset instead of opening the invoice window
        have to look and see if i can see what the click is actually doing
        forced idx = idx + 99
        stepped thru from 99 and it worked ok, including 100
    08/31/2020
        trying to eliminate the semi-frequent errors that only seem to occur in production
        Name: VehicleInventoryDetailDownload
          File "/home/rydell/projects/luigi_1804_36/nissan.py", line 258, in run
            driver.find_element_by_id(button_id).click()
        selenium.common.exceptions.ElementNotInteractableException: Message: element not interactable
        the (button_id).click() line is actually 279, one of these messages could be a red herring
        since the error ElementNotInteractableException
        the (button_id).click() is wrapped in a try except, added ElementNotInteractableException to the except clause
    11/23/2020
        detail download is failing quietly, there are currently 175 vehicles on the page after selecting
        Records per Page = All, but generating the count for enumerating it is 50, so after 50
        vehicles it gracefully proceeds, thus leaving out 125 vehicles
        All is orderly, all the identifiers work, the issue is the count only
        Changed the ieteration to be based on the row count total on the page
        That got me to 100, where once again it reset
    12/1/20
        struck out, reqached out to afton, she, of course, figured it out.  way way faster
        basically, scrape all the vins/orders, then generate the link for each vin/order, much faster, and works

    """
    pipeline = pipeline
    # # local
    # pdf_dir = '/home/jon/projects/luigi_1804_36/nissan/vehicle_inventory_detail/pdf_files/'

    # # production
    pdf_dir = '/home/rydell/projects/luigi_1804_36/nissan/vehicle_inventory_detail/pdf_files/'

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                pipeline + '_' + self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def setup_driver(self):
        # # Chromedriver path
        chromedriver_path = '/usr/bin/chromedriver'
        options = webdriver.ChromeOptions()
        profile = {"download.default_directory": self.pdf_dir}
        options.add_experimental_option("prefs", profile)
        options.add_argument('--user-agent="Mozilla/5.0 ((Windows NT 10.0; Win64; x64) AppleWebKit/537.36 '
                             '(KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36"')
        driver = webdriver.Chrome(executable_path=chromedriver_path, options=options)
        return driver

    def run(self):
        with Display(visible=False, size=(1200, 1500)):
            driver = self.setup_driver()
            try:
                driver.get('https://as.na.nissan.biz/SecureAuth71/')
                # SIGNING IN
                driver.find_element_by_class_name('txtUserid').send_keys(user)
                driver.find_element_by_class_name('tbxPassword').send_keys(password)
                driver.find_element_by_class_name('btn-secureauth').click()
                # Click MY LINKS
                WebDriverWait(driver, 10).until(ec.element_to_be_clickable(('id', '1a'))).click()
                # wait until page finishes loading
                WebDriverWait(driver, 10).until(ec.presence_of_element_located(
                    ('xpath', '//*[@id="linklisting-containerID"]/div[5]/div/ul/li[1]/a')))
                # load the Nissan DBS page
                driver.get('https://www.nnanet.com/content/nnanet/global/secure/viewlink.link001203.html')
                # load the inventory page
                driver.get('https://dbsapp.nnanet.com/dbs/vehicle/inventory/list?execution=e6s1')
                # select All Locations (statuses)_
                WebDriverWait(driver, 10).until(ec.presence_of_element_located(
                    ('id', 'f1:filterValueLocation'))).click()
                WebDriverWait(driver, 10).until(ec.presence_of_element_located(
                    ('xpath', '//*[@id="f1:filterValueLocation"]/option[1]'))).click()
                time.sleep(10)
                # show all on page
                driver.find_element_by_xpath('//*[@id="f1:selectPageNumber_newVehicleTable"]').click()
                driver.find_element_by_xpath('//*[@id="f1:selectPageNumber_newVehicleTable"]/option[6]').click()
                # get the row count from the bottom of the page
                row_count = int(driver.find_element_by_xpath('//*[@id="f1:newVehicleTable:newCount"]').text)
                # allow all rows to display
                # wait for last element to be visible
                WebDriverWait(driver, 50).until(ec.presence_of_element_located(
                    ('id', 'f1:newVehicleTable:%s_row_%s' % (row_count - 1, row_count - 1))))
                # empty the pdf_dir
                for filename in os.listdir(self.pdf_dir):
                    os.unlink(self.pdf_dir + filename)
                vin_array = []
                for idx in range(0, row_count):
                    try:
                        # find serial number a tag by id below
                        serial_number = driver.find_element_by_id('f1:newVehicleTable:%s:serialNum' % idx)
                        # split the onclick action to fetch the entire vin
                        full_vin = serial_number.get_attribute('onclick').split('Vin=')[1].split('&')[0]
                        # add the vin to the array
                        vin_array.append(full_vin)
                        # print(full_vin)
                    except NoSuchElementException:
                        # Could not find the serial number above - This means it is an order number.
                        # Change the serialNum to orderNum for the idea and it is basically the same otherwise
                        order_number = driver.find_element_by_id('f1:newVehicleTable:%s:orderNum' % idx)
                        temp_vin = order_number.get_attribute('onclick').split('Vin=')[1].split('&')[0]
                        # add the temp_vin to the array
                        vin_array.append(temp_vin)
                        # print(temp_vin)
                for idx, vin in enumerate(vin_array):
                    print(idx, vin)
                    driver.get(
                        'https://dbsapp.nnanet.com/dbs/vehicle/inventory/list/print?selectedVin=%s&detailFlag=1' % vin)
                    WebDriverWait(driver, 20).until(
                        ec.element_to_be_clickable(('xpath', '//*[@id="f2:comPrintIcon"]'))).click()
                # verify that all files downloaded
                # print(row_count)
                assert len(os.listdir(self.pdf_dir)) == row_count
            finally:
                driver.quit()


class VehicleInventoryDetailParse(luigi.Task):
    """

    """
    pipeline = pipeline

    # # local
    # pdf_dir = '/home/jon/projects/luigi_1804_36/nissan/vehicle_inventory_detail/pdf_files/'
    # csv_dir = '/home/jon/projects/luigi_1804_36/nissan/vehicle_inventory_detail/csv_files/'
    # clean_csv_dir = '/home/jon/projects/luigi_1804_36/nissan/vehicle_inventory_detail/clean_csv_files/'
    # bad_file_dir = '/home/jon/projects/luigi_1804_36/nissan/vehicle_inventory_detail/bad_files/'

    # # production
    pdf_dir = '/home/rydell/projects/luigi_1804_36/nissan/vehicle_inventory_detail/pdf_files/'
    csv_dir = '/home/rydell/projects/luigi_1804_36/nissan/vehicle_inventory_detail/csv_files/'
    clean_csv_dir = '/home/rydell/projects/luigi_1804_36/nissan/vehicle_inventory_detail/clean_csv_files/'
    bad_file_dir = '/home/rydell/projects/luigi_1804_36/nissan/vehicle_inventory_detail/bad_files/'

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                pipeline + '_' + self.__class__.__name__ + ".txt")

    def requires(self):
        yield VehicleInventoryDetailDownload()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute("truncate hn.stg_nna_vehicle_options")
                pg_con.commit()
                pg_cur.execute("truncate hn.stg_nna_vehicle_details cascade")
                pg_con.commit()
                # pg_cur.execute("truncate tem.nissan_vins")
                # empty the csv_dir
                for filename in os.listdir(self.csv_dir):
                    os.unlink(self.csv_dir + filename)
                # empty the bad_file_dir
                for filename in os.listdir(self.bad_file_dir):
                    os.unlink(self.bad_file_dir + filename)
                # convert pdf to csv
                for filename in os.listdir(self.pdf_dir):
                    # the separator between Code and Description gets missed in some conversions, resulting in 2 columns
                    # instead of 3, a setting of 50 works, all pdfs read cleanly
                    table = camelot.read_pdf(self.pdf_dir + filename, line_scale=50)
                    table[0].to_csv(self.csv_dir + os.path.splitext(filename)[0] + '.csv')
                # test number of pdf_files = number csv files
                # cleanup the csv files, remove embedded commas and line feeeds, removes all the quoting
                # this is what i am always looking for as edit in place
                # in the 12/04/20 kerfluffle, do the csv clean up in a single block
                # empty the clean_csv_dir
                for filename in os.listdir(self.clean_csv_dir):
                    os.unlink(self.clean_csv_dir + filename)
                # cleanup the csv files, remove embedded commas and line feeeds, removes all the quoting
                for filename in os.listdir(self.csv_dir):
                    # print(filename)
                    with open(self.csv_dir + filename, 'r') as infile, open(
                            self.clean_csv_dir + filename, 'w') as outfile:
                        reader = csv.reader(infile)
                        writer = csv.writer(outfile)
                        for row in reader:
                            writer.writerow(item.replace(",", "").replace('\n', ' ') for item in row)
                # test number of pdf_files = number of clean_csv_files
                assert len(os.listdir(self.pdf_dir)) == len(os.listdir(self.csv_dir))
                for filename in os.listdir(self.clean_csv_dir):
                    # print(filename)
                    # pg_cur.execute("insert into tem.nissan_vins values('{}')".format(filename))
                    with open(self.clean_csv_dir + filename, 'r') as io:
                        pg_cur.execute("truncate hn.ext_nna_vehicle_details")
                        pg_cur.copy_expert("""copy hn.ext_nna_vehicle_details from stdin with CSV
                                              encoding 'latin-1'""", io)
                        pg_con.commit()
                        pg_cur.execute("select hn.stg_nna_vehicles()")
                        pg_con.commit()
                # test for # of csv files = # of clean_csv_files, ie all were converted
                assert len(os.listdir(self.csv_dir)) == len(os.listdir(self.clean_csv_dir))
                pg_cur.execute("select count(*) from hn.stg_nna_vehicle_details")
                # test for number of records in stg_nna_vehicles = # of clean_csv_files
                assert pg_cur.fetchone()[0] == len(os.listdir(self.clean_csv_dir))
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute("select hn.update_nna_vehicle_details()")
                pg_cur.execute("select hn.update_nna_vehicle_options()")
                pg_cur.execute("select hn.update_nna_order_statuses()")


class IncentivesDownload(luigi.Task):
    """

    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                pipeline + '_' + self.__class__.__name__ + ".txt")

    def requires(self):
        yield VehicleInventoryDetailParse()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    @staticmethod
    def setup_driver():
        # # Chromedriver path
        chromedriver_path = '/usr/bin/chromedriver'
        options = webdriver.ChromeOptions()
        options.add_argument('--user-agent="Mozilla/5.0 ((Windows NT 10.0; Win64; x64) AppleWebKit/537.36 '
                             '(KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36"')
        driver = webdriver.Chrome(executable_path=chromedriver_path, options=options)
        return driver

    def run(self):
        with Display(visible=False, size=(1200, 1500)):
            driver = self.setup_driver()
            try:
                with utilities.pg(pg_server) as pg_con:
                    with pg_con.cursor() as pg_cur:
                        sql = """
                                select vin  
                                from hn.nna_vehicle_details  
                                where vin not like 'DBS%'
                                  and location_status = 'DLR-INV';
                            """
                        # sql = "select '3N1AB8CV1LY239140'"
                        pg_cur.execute(sql)
                        driver.get('https://as.na.nissan.biz/SecureAuth71/')
                        # SIGNING IN
                        driver.find_element_by_class_name('txtUserid').send_keys(user)
                        driver.find_element_by_class_name('tbxPassword').send_keys(password)
                        driver.find_element_by_class_name('btn-secureauth').click()
                        # Click MY LINKS
                        WebDriverWait(driver, 20).until(ec.presence_of_element_located(('id', '1a'))).click()
                        # wait until page finishes loading
                        WebDriverWait(driver, 10).until(ec.presence_of_element_located(
                            ('xpath', '//*[@id="linklisting-containerID"]/div[5]/div/ul/li[1]/a')))
                        # load the Nissan VIN/Zip Lookup (VIMS)
                        driver.get('https://www.nnanet.com/content/nnanet/global/secure/viewlink.link001287.html')
                        for row in pg_cur.fetchall():
                            vin = row[0]
                            # vin = '3N1AB8CV1LY294705'
                            zip_code = '58201'
                            today = datetime.date.today()
                            today_formatted = today.strftime('%m/%d/%Y')
                            url = ('https://vims.ndc.prd.nnanet.com/vims/qualification/'
                                   'retrieveVinGeographyDetails.html?stamp=1585169191&vin=%s&'
                                   'zipCode=%s&dealerCode=3071&preview=false&salesDate=%s' %
                                   (vin, zip_code, today_formatted))
                            driver.get(url)
                            result_from_nissan = driver.find_element_by_tag_name('pre').text
                            result_from_nissan = result_from_nissan.replace("'", "''")
                            sql = """
                                    insert into hn.ext_nna_incentives(vin,raw_incentives)
                                    values ('{0}','{1}');
                                """.format(vin, result_from_nissan)
                            pg_cur.execute(sql)
                            pg_con.commit()
            finally:
                driver.quit()


class IncentivesParse(luigi.Task):
    """

    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                pipeline + '_' + self.__class__.__name__ + ".txt")

    def requires(self):
        yield IncentivesDownload()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute("select hn.update_nna_incentives()")
                pg_cur.execute("select hn.update_nna_vehicle_incentives()")


# if __name__ == '__main__':
#     luigi.run(["--workers=1"], main_task_cls=IncentivesParse)
