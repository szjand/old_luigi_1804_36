# encoding=utf-8
"""

download reports from global connect and parse them into tables
requires a browser (chrome)
looks like at least one page on global connect is not available until 6:00AM
so, need to take this out of run_all, and make it a separate cron job, like homenet_new

8/8/19
    a bit weird, but it seems the structure for the login page thes routines invoke
    has changed, i think this is the stuff afton ran into with an invisible login
    page being invoked
    anyway, changed the identifiers for the login elements and seem to be all good

to run locally:
    don't forget to start the scheduler (luigid)

    change the download directory:
        # # local
        download_dir = '/home/jon/projects/luigi_1804_36/global_connect/dealer_constraints_xls/'
        # production
        # download_dir = '/home/rydell/projects/luigi_1804_36/global_connect/dealer_constraints_xls/'

    (luigi) jon@ubuntu-1804:~/projects/luigi_1804_36$ python3 -m luigi --module global_connect GlobalConnect

11/13/19
    all the global connect tasks failed
    it is reminiscent of an issue afton had long ago (in fact her only linux global connect task is failing today
    in the same way), the normal login page is hidden, and the login page comes up full screen.  fails on entering
    username and password, Element not interactable.  unable to locate and identify the actual input element.
    Afton and Greg huddled and came up with setting the user agent in the chromedriver.  AFton noticed that the
    in the failing instances, the html was for mobile
    That worked
    the only weirdness, on my local VM, the user agent comes up as:
        Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36
    which fails the same way
    afton used:
      Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36
    which works can't wait to see what happens on the production luigi machine.
    Asked Greg, of course he understood, user agent is relevant to the page being opened, global connect is no doubt
    coded for windows, ergo ..., should work fine on luigi production
    try it with invoices first, if that works will change all the other scripts
02/03/21
    a couple of weeks ago, these 2 reports BuildOut2020v3Parse() & StartUp2020v3Parse() disappeared from
    global connect.  neither taylor or cahalan can find out where they have moved or if they even still exist
    therefore, i am discontinuing these tasks today
02/22/21
    the links are back and are working
02/24/21
    but the pdf on the restored old links is structured differently, one less field
    so, Taylor sent me new links:
        2022MY S.U.
        https://view.highspot.com/viewer/602ab1a1628ba21bbfeebf27

        2021MY B.O. V2
        https://view.highspot.com/viewer/602ab1faa4dfa05a1b2e38bb
    that work great, so, major refactor to use these new pages

    also create new tables: nc.build_out_2021 & nc.start_up_2022

*** however, given how much GM changes stuff, here are the old (currently working) global connect
*** links, in case the new format goes away
*** login to: https://dealer.autopartners.net/portal/US/Order/Pages/default.aspx?appdeptid=4
*** get the forms at: https://dealer.autopartners.net/sites/usfleet/Order%20Management%20Reports/Forms/AllItems.aspx

"""

import utilities
import luigi
import datetime
import csv
from selenium import webdriver
import time
import os
import camelot
import xlrd
from bs4 import BeautifulSoup
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.common.exceptions import TimeoutException
import json
import requests

pipeline = 'global_connect'
pg_server = '173'
ts_no_spaces = str(datetime.datetime.now()).replace(" ", "")

user = '41d9ec15-5148-439d-938a-313a1fa91cc6'
passcode = '666'
username = None
password = None

local_or_production = 'production'
# local_or_production = 'local'


# # if these task are being called via run_all, that task will handle all of this logging
@luigi.Task.event_handler(luigi.Event.START)
def task_start(self):
    utilities.luigi_log_start(pg_server, self.task_id + '__' + ts_no_spaces, self.pipeline, self.__class__.__name__)


@luigi.Task.event_handler(luigi.Event.SUCCESS)
def task_success(self):
    utilities.luigi_log_pass(pg_server, self.task_id + '__' + ts_no_spaces, self.pipeline, self.__class__.__name__)
    try:  # WrapperTest throws an AttributeError, see /Notes/issues
        with self.output().open("w") as out_file:
            # write to the output file
            out_file.write('pass')
    except AttributeError:
        pass


class BuildOut2020v3Download(luigi.Task):
    """
    PDF from:
        order workbench
            reports & tools
                GMFleet.com Order Management Reports
                    2nd report in list: 2019 B-O  2020 S-U - V3.1 US.pdf
    variable pdf_dir proved to be an issue
    go get it to be recognized in setup_driver(), had to remove the @staticmethod decorator and then
    reference it with self. everywhere
    6/11/19 name of the report to download has changed, so a file does not get downloaded, but there is no check
        in this class to verify a file was downloaded, so the failure shows up in BuildOutParse as
        IndexError: list index out of range on the line pdf_file = os.listdir(self.pdf_dir)[0]
        Also, it is no longer a pdf, but a real spreadsheet (xlsx), with build out and start up on
        separate worksheets
    11/22/19
    script has stopped downloading the pdf both locally and in production
        to the rescue (modified profile in setup_driver()):
        https://stackoverflow.com/questions/53998690/downloading-a-pdf-using-selenium-chrome-and-python/54004678
        at least this worked locally, try next on production
    And if that is not enough, the document is now a pdf again
    02/14/20
        after countless days ignoring the erroring out, time to fix
    03/05/20
        GM has changed the format, the build out and start up are now separate links & pdfs
        so, lets make it to separate classes, build out and start up
        change class name from BuildOutDownload to BuildOut2020v3Download
        choosing to make this class very specific, thereby triggering failure as soon as GM changes stuff again1
    06/02/20
        GM has updated the report to Version 4, the only thing i am going to change is the report name to
        download
    08/03/2020
        GM has updated the report to Version 5.1, the only thing i am going to change is the report name to
        download
    10/05/20
        BuildOut2020v3Download & StartUp2021v3Download: link text changed to 2020MY B.O., holy shit,
            it is B.0. a fucking 0
    12/1/20
        BuildOut2020v3Download changed the 0 back to an O
        StartUp2021v3Download removed the last period from S.U.
    01/20/21
        There is no longer a report labeled as either build out or start up
    02/24/21
        no need to log in, no navigation, enter the url, hit the download button
    """
    pipeline = pipeline

    if local_or_production == 'local':
        pdf_dir = '/home/jon/projects/luigi_1804_36/global_connect/buildout_2020_v3_pdf/'
    elif local_or_production == 'production':
        pdf_dir = '/home/rydell/projects/luigi_1804_36/global_connect/buildout_2020_v3_pdf/'

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def setup_driver(self):
        # # Chromedriver path
        chromedriver_path = '/usr/bin/chromedriver'
        options = webdriver.ChromeOptions()
        profile = {"download.default_directory": self.pdf_dir,
                   "download.prompt_for_download": False,
                   "download.directory_upgrade": True,
                   "plugins.always_open_pdf_externally": True}

        options.add_experimental_option("prefs", profile)
        options.add_argument('--user-agent="Mozilla/5.0 ((Windows NT 10.0; Win64; x64) AppleWebKit/537.36 '
                             '(KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36"')
        driver = webdriver.Chrome(executable_path=chromedriver_path, options=options)
        return driver

    @staticmethod
    def download_pdf(driver):
        WebDriverWait(driver, 20).until(
            ec.element_to_be_clickable(('xpath', '//*[@id="hs-pv-download-item"]'))).click()

    def run(self):
        for filename in os.listdir(self.pdf_dir):
            os.unlink(self.pdf_dir + filename)
        driver = self.setup_driver()
        driver.get('https://view.highspot.com/viewer/602ab1faa4dfa05a1b2e38bb')
        # self.login(driver)
        self.download_pdf(driver)
        # let the file download
        time.sleep(10)
        driver.quit()
        assert len(os.listdir(self.pdf_dir)) == 1


class BuildOut2020v3Parse(luigi.Task):
    """
    had to reference all class variables with self.
    6/11/19 not a pdf anymore, but an xlsx
    11/22/19:
        not an xlsx anymore but a pdf, retrieved pdf code from bitbucket, commit 5c572cb
        had to alter tables nc.ext_build_out_2019 & nc.ext_start_up_2020, remove the first column: dummy_1
        and a refactoring of nc.xfm_model_year_build_out_start_up()
    02/14/20
        well, build out is now 2020 and start up is 2021
        there are still 2 tables, but, they are currently on one page, so the code that loops thru pages to get the
        tables is broken, changed the loop to the number of tables rather than pages
        Confused by the multiple allocation groups for some vehicles, eg: CLDREG / LDSILV, CLDDBL / LDSILV...
        the answer from GM was:
        That’s actually for Tier 2 (smaller) stores. Instead of breaking down allocation by Cab type, like you guys,
        Gateway, etc., they only earn LDSILV or LDSIER, because they receive much smaller numbers.
    """
    pipeline = pipeline

    if local_or_production == 'local':
        pdf_dir = '/home/jon/projects/luigi_1804_36/global_connect/buildout_2020_v3_pdf/'
        csv_dir = '/home/jon/projects/luigi_1804_36/global_connect/buildout_2020_v3_csv/'
    elif local_or_production == 'production':
        pdf_dir = '/home/rydell/projects/luigi_1804_36/global_connect/buildout_2020_v3_pdf/'
        csv_dir = '/home/rydell/projects/luigi_1804_36/global_connect/buildout_2020_v3_csv/'

    csv_file = 'build_out.csv'

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield BuildOut2020v3Download()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        # empty the csv_dir
        for filename in os.listdir(self.csv_dir):
            os.unlink(self.csv_dir + filename)
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute('truncate nc.ext_build_out_2020')
                # pg_cur.execute('truncate nc.ext_start_up_2021')
        # get the name of the pdf, it changes version in the filename over time
        pdf_file = os.listdir(self.pdf_dir)[0]  # only ever 1 file in directory
        table_count = camelot.read_pdf(self.pdf_dir + pdf_file, pages='1-end')
        i = table_count.n - 1  # the number of tables in the pdf, changed to subscriptable base 0
        while i > -1:
            for filename in os.listdir(self.csv_dir):  # only ever to be one file in directory
                os.unlink(self.csv_dir + filename)
            tables = camelot.read_pdf(self.pdf_dir + pdf_file, copy_text=['v'], strip_text='\n')
            tables[i].to_csv(self.csv_dir + self.csv_file)
            file = self. csv_dir + self.csv_file
            with utilities.pg(pg_server) as pg_con:
                with pg_con.cursor() as pg_cur:
                    with open(file, 'r') as io:
                        # if len(tables[i].cols) == 11:  # build out
                        pg_cur.copy_expert("""copy nc.ext_build_out_2020 from stdin with csv encoding 'latin-1 '""", io)
                        # if len(tables[i].cols) == 7:  # start up
                        #     pg_cur.copy_expert("""copy nc.ext_start_up_2021 from stdin
                        #                             with csv encoding 'latin-1 '""", io)
            i = i - 1
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute('select nc.xfm_model_year_build_out_2020()')


class StartUp2021v3Download(luigi.Task):
    """
    03/05/20
        GM has changed the format, the build out and start up are now separate links & pdfs
        this is the start up download
    06/02/20
        GM has updated to verion 4, the  only thing i am going to update is the report name to download
    02/24/21
        no need to log in, no navigation, enter the url, hit the download button
    """
    pipeline = pipeline

    if local_or_production == 'local':
        pdf_dir = '/home/jon/projects/luigi_1804_36/global_connect/startup_2021_v3_pdf/'
    elif local_or_production == 'production':
        pdf_dir = '/home/rydell/projects/luigi_1804_36/global_connect/startup_2021_v3_pdf/'

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def setup_driver(self):
        # # Chromedriver path
        chromedriver_path = '/usr/bin/chromedriver'
        options = webdriver.ChromeOptions()
        profile = {"download.default_directory": self.pdf_dir,
                   "download.prompt_for_download": False,
                   "download.directory_upgrade": True,
                   "plugins.always_open_pdf_externally": True}

        options.add_experimental_option("prefs", profile)
        options.add_argument('--user-agent="Mozilla/5.0 ((Windows NT 10.0; Win64; x64) AppleWebKit/537.36 '
                             '(KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36"')
        driver = webdriver.Chrome(executable_path=chromedriver_path, options=options)
        return driver

    @staticmethod
    def download_pdf(driver):
        WebDriverWait(driver, 20).until(
            ec.element_to_be_clickable(('xpath', '//*[@id="hs-pv-download-item"]'))).click()

    def run(self):
        for filename in os.listdir(self.pdf_dir):
            os.unlink(self.pdf_dir + filename)
        driver = self.setup_driver()
        driver.get('https://view.highspot.com/viewer/602ab1a1628ba21bbfeebf27')
        self.download_pdf(driver)
        # let the file download
        time.sleep(10)
        driver.quit()
        assert len(os.listdir(self.pdf_dir)) == 1


class StartUp2020v3Parse(luigi.Task):
    """
    """
    pipeline = pipeline

    if local_or_production == 'local':
        pdf_dir = '/home/jon/projects/luigi_1804_36/global_connect/startup_2021_v3_pdf/'
        csv_dir = '/home/jon/projects/luigi_1804_36/global_connect/startup_2021_v3_csv/'
    elif local_or_production == 'production':
        pdf_dir = '/home/rydell/projects/luigi_1804_36/global_connect/startup_2021_v3_pdf/'
        csv_dir = '/home/rydell/projects/luigi_1804_36/global_connect/startup_2021_v3_csv/'

    csv_file = 'start_up.csv'

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield StartUp2021v3Download()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        # empty the csv_dir
        for filename in os.listdir(self.csv_dir):
            os.unlink(self.csv_dir + filename)
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute('truncate nc.ext_start_up_2021')
        # get the name of the pdf, it changes version in the filename over time
        pdf_file = os.listdir(self.pdf_dir)[0]  # only ever 1 file in directory
        table_count = camelot.read_pdf(self.pdf_dir + pdf_file, pages='1-end')
        i = table_count.n - 1  # the number of tables in the pdf, changed to subscriptable base 0
        while i > -1:
            for filename in os.listdir(self.csv_dir):  # only ever to be one file in directory
                os.unlink(self.csv_dir + filename)
            tables = camelot.read_pdf(self.pdf_dir + pdf_file, copy_text=['v'], strip_text='\n')
            tables[i].to_csv(self.csv_dir + self.csv_file)
            file = self. csv_dir + self.csv_file
            with utilities.pg(pg_server) as pg_con:
                with pg_con.cursor() as pg_cur:
                    with open(file, 'r') as io:
                        pg_cur.copy_expert("""copy nc.ext_start_up_2021 from stdin with csv encoding 'latin-1 '""", io)
            i = i - 1
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute('select nc.xfm_model_year_start_up_2021()')


class DealerConstraintsDownload(luigi.Task):
    """
    PDF from:
        order workbench
            reports & tools
                Dealer Constraints Distribution Report
                    filename: VOM_PdfAction.xls
    though the file extention is .xls, it is an html file readable by excel, therefor BeautifulSoup
    """
    pipeline = pipeline

    if local_or_production == 'local':
        download_dir = '/home/jon/projects/luigi_1804_36/global_connect/dealer_constraints_xls/'
    elif local_or_production == 'production':
        download_dir = '/home/rydell/projects/luigi_1804_36/global_connect/dealer_constraints_xls/'

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def setup_driver(self):
        # # Chromedriver path
        chromedriver_path = '/usr/bin/chromedriver'
        options = webdriver.ChromeOptions()
        profile = {"download.default_directory": self.download_dir}
        options.add_experimental_option("prefs", profile)
        options.add_argument('--user-agent="Mozilla/5.0 ((Windows NT 10.0; Win64; x64) AppleWebKit/537.36 '
                             '(KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36"')
        driver = webdriver.Chrome(executable_path=chromedriver_path, options=options)
        return driver

    @staticmethod
    def get_secret(category, platform):
        modified_platform = platform.replace(' ', '+')
        url = ("https://beta.rydellvision.com:8888/vision/secret?platform={0}&category={1}".format
               (modified_platform, category))
        response = requests.get(url, auth=(user, passcode))
        return response.json()

    @staticmethod
    def login(self, driver):
        credentials = self.get_secret('Cartiva', 'Global Connect')
        resp_dict = credentials
        # get username and password fields
        username_box = driver.find_element_by_name('IDToken1')
        password_box = driver.find_element_by_name('IDToken2')
        login_btn = driver.find_element_by_class_name('login')
        username_box.send_keys(resp_dict.get('username'))
        password_box.send_keys(resp_dict.get('secret'))
        login_btn.click()
        # See if login worked
        login_fail_element = driver.find_elements_by_xpath('/html/body/table/tbody/tr[1]/td/table/tbody/tr[2]'
                                                           '/td[2]/table/tbody/tr/td/div/b')
        if len(login_fail_element) > 0:
            # this length should be 0 if login is successful
            raise AssertionError("Cannot login")

    @staticmethod
    def download_excel(driver):
        division_option_elem = driver.find_element_by_id('ConsOrg')
        division_options = division_option_elem.find_elements_by_tag_name('option')
        # Get the first option after "Select Division"
        all_divisions = division_options[1]
        all_divisions.click()
        date_options_elem = driver.find_element_by_id('RptDate')
        date_options = date_options_elem.find_elements_by_tag_name('option')
        # Get the first option after "Select Report Date"
        most_recent_date = date_options[1]
        most_recent_date.click()
        # Click Excel Button
        export_to_excel = driver.find_element_by_id('btnSGoExp')
        export_to_excel.click()
        # Wait for sheet to download
        time.sleep(5)

    def run(self):
        for filename in os.listdir(self.download_dir):
            os.unlink(self.download_dir + filename)
        driver = self.setup_driver()
        driver.get('https://www.autopartners.net/apps/naowb/naowb/reportstools/rt_02.do?modName=rt&ForceReport=Cstrnt'
                   'Dstrbn&USER_ACTION=rt_02_body&METHOD=doJspInit')
        self.login(self, driver)
        self.download_excel(driver)
        driver.quit()
        assert len(os.listdir(self.download_dir)) == 1


class DealerConstraintsParse(luigi.Task):
    """
    """
    pipeline = pipeline
    download_dir = 'global_connect/dealer_constraints_xls/'

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield DealerConstraintsDownload()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        # file = download_dir + 'VOM_PdfAction.xls'
        file = self.download_dir + os.listdir(self.download_dir)[0]
        soup = BeautifulSoup(open(file), 'lxml')
        rows = []
        for row in soup.find_all('tr'):
            rows.append([val.text.strip() for val in row.find_all('td')])
            # this eliminates the rows with no info
        with open(file, 'w') as f:
            writer = csv.writer(f)
            writer.writerows(row for row in rows if len(row) > 1)
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute("truncate nc.ext_dealer_constraints_1;")
                with open(file, 'r') as io:
                    pg_cur.copy_expert("""copy nc.ext_dealer_constraints_1 from stdin encoding 'latin-1 '""", io)
                sql = "select nc.update_dealer_constraints()"
                pg_cur.execute(sql)


class BuildoutConstraintsDownload(luigi.Task):
    """
    pdf from global connect
    order workbench
        reports & tools
            National Constraints Report
    filename: showconstraintsreport.pdf
    going to the fleet page for the xlsm, the fucking pdf is not parseable
    and it works
    order workbench
        reports & tools
            GMFleet.com Order Management Reports
            direct link: https://dealer.autopartners.net/sites/usfleet/Order%20Management%20Reports/Forms/AllItems.aspx
            file name: 5-3-2019 GMNA Weekly Dealer Constraints.xlsm
    02/14/20
        this has been failing forever
        only when run automated (selenium) as soon as the link was clicked, an alert appeared:
            Open sdx-open?
            Always open these types of links in the associated app
                    Cancel      Open xdg-open
        finally, today, this link pointed me in the right direction
        https://stackoverflow.com/questions/19003003/check-if-any-alert-exists-using-selenium-with-python
        EXCEPT it appears to be intermittent as hell, no idea why
    02/18/20
        not just intermittent, not fucking working at all
        turns out afton had the solution
        see the code in function download_xlsm(driver)
        basically, instead of clicking the link do driver.get(the link's href)
    """
    pipeline = pipeline

    if local_or_production == 'local':
        xlsm_dir = '/home/jon/projects/luigi_1804_36/global_connect/buildout_constraints_pdf/'
    elif local_or_production == 'production':
        xlsm_dir = '/home/rydell/projects/luigi_1804_36/global_connect/buildout_constraints_pdf/'

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def setup_driver(self):
        # # Chromedriver path
        chromedriver_path = '/usr/bin/chromedriver'
        options = webdriver.ChromeOptions()
        profile = {"download.default_directory": self.xlsm_dir}
        options.add_experimental_option("prefs", profile)
        options.add_argument('--user-agent="Mozilla/5.0 ((Windows NT 10.0; Win64; x64) AppleWebKit/537.36 '
                             '(KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36"')
        driver = webdriver.Chrome(executable_path=chromedriver_path, options=options)
        return driver

    @staticmethod
    def get_secret(category, platform):
        modified_platform = platform.replace(' ', '+')
        url = ("https://beta.rydellvision.com:8888/vision/secret?platform={0}&category={1}".format
               (modified_platform, category))
        response = requests.get(url, auth=(user, passcode))
        return response.json()

    @staticmethod
    def login(self, driver):
        credentials = self.get_secret('Cartiva', 'Global Connect')
        resp_dict = credentials
        # get username and password fields
        username_box = driver.find_element_by_name('IDToken1')
        password_box = driver.find_element_by_name('IDToken2')
        login_btn = driver.find_element_by_class_name('login')
        username_box.send_keys(resp_dict.get('username'))
        password_box.send_keys(resp_dict.get('secret'))
        login_btn.click()
        # See if login worked
        login_fail_element = driver.find_elements_by_xpath('/html/body/table/tbody/tr[1]/td/table/tbody/tr[2]'
                                                           '/td[2]/table/tbody/tr/td/div/b')
        if len(login_fail_element) > 0:
            # this length should be 0 if login is successful
            raise AssertionError("Cannot login")
        # Give it 5 seconds to download
        time.sleep(5)

    @staticmethod
    def download_xlsm(driver):
        # Since we have successfully logged in go to fleet link
        driver.get('https://dealer.autopartners.net/sites/usfleet/Order%20Management%20Reports/Forms/AllItems.aspx')
        # Get all a tags with class name
        links = driver.find_elements_by_class_name('ms-listlink')
        for link in links:
            download_url = link.get_attribute('href')
            if 'GMNA' in download_url:
                driver.get(download_url)

    def run(self):
        for filename in os.listdir(self.xlsm_dir):
            os.unlink(self.xlsm_dir + filename)
        driver = self.setup_driver()
        driver.get('https://dealer.autopartners.net/portal/US/Order/Pages/default.aspx?appdeptid=4')
        self.login(self, driver)
        self.download_xlsm(driver)
        driver.quit()
        assert len(os.listdir(self.xlsm_dir)) == 1


class BuildoutConstraintsParse(luigi.Task):
    """
    """
    pipeline = pipeline

    xlsm_dir = 'global_connect/buildout_constraints_pdf/'
    csv_dir = 'global_connect/buildout_constraints_csv/'

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield BuildoutConstraintsDownload()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        # empty the csv_dir
        for filename in os.listdir(self.csv_dir):
            os.unlink(self.csv_dir + filename)
        xlsm_file = os.listdir(self.xlsm_dir)[0]  # only ever 1 file in directory
        workbook = xlrd.open_workbook(self.xlsm_dir + xlsm_file)
        for sheet in workbook.sheets():
            if sheet.name == 'Build Out-US':
                with open(self.csv_dir + '{}.csv'.format(sheet.name), 'w') as f:
                    writer = csv.writer(f)
                    for row in range(sheet.nrows):
                        out = []
                        for cell in sheet.row_values(row):
                            out.append(cell)
                        writer.writerow(out)
            file = self.csv_dir + os.listdir(self.csv_dir)[0]
            with utilities.pg(pg_server) as pg_con:
                with pg_con.cursor() as pg_cur:
                    pg_cur.execute('truncate nc.ext_build_out_constraints')
                    with open(file, 'r') as io:
                        pg_cur.copy_expert("""copy nc.ext_build_out_constraints from stdin
                                            with csv encoding 'latin-1 '""", io)
            with utilities.pg(pg_server) as pg_con:
                with pg_con.cursor() as pg_cur:
                    pg_cur.execute('select nc.xfm_build_out_constraints()')


class EventCodesDownload(luigi.Task):
    """
     pdf from global connect: using a direct link to the pdf
     filename: vechicle_order_event_codes.pdf
     11/22/2019
        forever, the eventcodes pdf have not been downloading on the production server, though it worked locally,
        i could not get it to download on the production server (the pdf opened instead of downloading)
        trying the web driver profile setup up that i discovered today
        Also, needed to fully qualify path to pdf file
    """
    pipeline = pipeline

    if local_or_production == 'local':
        pdf_dir = '/home/jon/projects/luigi_1804_36/global_connect/event_codes_pdf/'
    elif local_or_production == 'production':
        pdf_dir = '/home/rydell/projects/luigi_1804_36/global_connect/event_codes_pdf/'

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def setup_driver(self):
        # # Chromedriver path
        chromedriver_path = '/usr/bin/chromedriver'
        options = webdriver.ChromeOptions()
        profile = {"download.default_directory": self.pdf_dir,
                   "download.prompt_for_download": False,
                   "download.directory_upgrade": True,
                   "plugins.always_open_pdf_externally": True}
        options.add_experimental_option("prefs", profile)
        options.add_argument('--user-agent="Mozilla/5.0 ((Windows NT 10.0; Win64; x64) AppleWebKit/537.36 '
                             '(KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36"')
        driver = webdriver.Chrome(executable_path=chromedriver_path, options=options)
        return driver

    @staticmethod
    def get_secret(category, platform):
        modified_platform = platform.replace(' ', '+')
        url = ("https://beta.rydellvision.com:8888/vision/secret?platform={0}&category={1}".format
               (modified_platform, category))
        response = requests.get(url, auth=(user, passcode))
        return response.json()

    @staticmethod
    def login(self, driver):
        credentials = self.get_secret('Cartiva', 'Global Connect')
        resp_dict = credentials
        # get username and password fields
        username_box = driver.find_element_by_name('IDToken1')
        password_box = driver.find_element_by_name('IDToken2')
        login_btn = driver.find_element_by_class_name('login')
        username_box.send_keys(resp_dict.get('username'))
        password_box.send_keys(resp_dict.get('secret'))
        login_btn.click()
        # See if login worked
        login_fail_element = driver.find_elements_by_xpath('/html/body/table/tbody/tr[1]/td/table/tbody/tr[2]'
                                                           '/td[2]/table/tbody/tr/td/div/b')
        if len(login_fail_element) > 0:
            # this length should be 0 if login is successful
            raise AssertionError("Cannot login")
        # Give it 5 seconds to download
        time.sleep(5)

    def run(self):
        for filename in os.listdir(self.pdf_dir):
            os.unlink(self.pdf_dir + filename)
        driver = self.setup_driver()
        # URL goes straight to National Constraints Report
        driver.get('https://www.autopartners.net/apps/naowb/naowb/help/us/en/Pdf/vehicle_order_event_codes.pdf')
        self.login(self, driver)
        driver.quit()
        assert len(os.listdir(self.pdf_dir)) == 1


class EventCodesParse(luigi.Task):
    """
    in the copy_expert of csv to postgres table, change with csv encoding 'latin_1'
    to with csv encoding 'utf-8'
    """
    pipeline = pipeline

    pdf_dir = 'global_connect/event_codes_pdf/'
    csv_dir = 'global_connect/event_codes_csv/'
    csv_file = 'vehicle_order_event_codes.csv'

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield EventCodesDownload()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        # empty the csv_dir
        for filename in os.listdir(self.csv_dir):
            os.unlink(self.csv_dir + filename)
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute('truncate gmgl.ext_vehicle_order_event_codes')
        pdf_file = os.listdir(self.pdf_dir)[0]  # only ever 1 file in directory
        tables = camelot.read_pdf(self.pdf_dir + pdf_file, pages='1-end', strip_text='\n')
        tables.export(self.csv_dir + self.csv_file, f='csv')
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                with os.scandir(self.csv_dir) as files:
                    for file in files:
                        with open(file, 'r') as io:
                            pg_cur.copy_expert("""copy gmgl.ext_vehicle_order_event_codes from stdin
                                                with csv encoding 'utf-8 '""", io)
            pg_con.commit()  # without this commit gmgl.ext_vehicle_order_event_codes does not get populated
            with pg_con.cursor() as pg_cur:
                pg_cur.execute('select gmgl.check_vehicle_order_event_codes()')


class VehicleOrders(luigi.Task):
    """
    adding the try...finally to run seems to take care of cleaning up chrome/chromedriver
        processes when this fails, either in finding elements in the page or in db functions
    if there are orders with current recalls, there is an additional field in the table: Field Action,
    which is why in create_data that we create 2 csv files
    9/2/19
        changed db schema to gmgl for deploying to luigi
    invoked in a separate cron job: 6:05 AM monday thru saturday
    01/09/21 this order workbench page no longer accessible on Saturday
    """
    pipeline = pipeline
    divisions = ["11,**,004", "12,**,006", "13,**,001", "48,**,012"]

    # this assignment satisfies the linters complaint: local variable 'csv_dir' might be referenced before assignment
    csv_dir = None

    if local_or_production == 'local':
        download_dir = '/home/jon/projects/luigi_1804_36/global_connect/orders_xls/'
        csv_dir = '/home/jon/projects/luigi_1804_36/global_connect/orders_csv/'
    elif local_or_production == 'production':
        download_dir = '/home/rydell/projects/luigi_1804_36/global_connect/orders_xls/'
        csv_dir = '/home/rydell/projects/luigi_1804_36/global_connect/orders_csv/'

    csv_file_field = csv_dir + 'ext_orders_field.csv'  # includes column Field Action
    csv_file = csv_dir + 'ext_orders.csv'  # does not includes column Field Action

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    @staticmethod
    def get_secret(category, platform):
        modified_platform = platform.replace(' ', '+')
        url = ("https://beta.rydellvision.com:8888/vision/secret?platform={0}&category={1}".format
               (modified_platform, category))
        response = requests.get(url, auth=(user, passcode))
        return response.json()

    def setup_driver(self):
        # # Chromedriver path
        chromedriver_path = '/usr/bin/chromedriver'
        options = webdriver.ChromeOptions()
        profile = {"download.default_directory": self.download_dir}
        options.add_experimental_option("prefs", profile)
        options.add_argument('--user-agent="Mozilla/5.0 ((Windows NT 10.0; Win64; x64) AppleWebKit/537.36 '
                             '(KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36"')
        driver = webdriver.Chrome(executable_path=chromedriver_path, options=options)
        return driver

    @staticmethod
    def login(self, driver):
        credentials = self.get_secret('Cartiva', 'Global Connect')
        resp_dict = credentials
        username_box = driver.find_element_by_id('IDToken1')
        password_box = driver.find_element_by_id('IDToken2')
        login_btn = driver.find_element_by_name('Login.Submit')
        username_box.send_keys(resp_dict.get('username'))
        password_box.send_keys(resp_dict.get('secret'))
        login_btn.click()
        WebDriverWait(driver, 20).until(
            ec.presence_of_element_located(('id', 'selModelYear')))

    @staticmethod
    def select_make(division, driver):
        year_dropdown = driver.find_element_by_xpath('//*[@id="selModelYear"]/option[2]')
        year_dropdown.click()
        division_element = driver.find_element_by_css_selector('option[value="%s"]' % division)
        division_name = division_element.get_attribute('innerHTML')
        division_element.click()
        get_data_button = driver.find_element_by_id('btnGetData')
        driver.execute_script('arguments[0].click()', get_data_button)
        time.sleep(2)
        alert = driver.switch_to.alert
        alert.accept()
        return division_name

    def download_file(self, driver):
        for filename in os.listdir(self.download_dir):
            os.unlink(self.download_dir + filename)
        driver.find_element_by_css_selector('area[title="Export"]').click()
        # switch focus to the download window
        driver.switch_to.window(driver.window_handles[1])
        time.sleep(5)
        # close the download window
        driver.close()
        # focus back to the original window
        driver.switch_to.window(driver.window_handles[0])
        assert len(os.listdir(self.download_dir)) == 1

    def create_data(self, division_name):
        with utilities.pg(pg_server) as database:
            with database.cursor() as pg_cursor:
                pg_cursor.execute("truncate gmgl.ext_ordered_vehicles")
                pg_cursor.execute("truncate gmgl.ext_ordered_vehicles_field")
                database.commit()
                soup = BeautifulSoup(open(self.download_dir + os.listdir(self.download_dir)[0]), 'lxml')
                rows = []
                for row in soup.find_all('tr'):
                    rows.append([val.text.strip() for val in row.find_all('td')])
                if 'Field Action' in (item for sublist in rows for item in sublist):
                    with open(self.csv_file_field, 'a') as f:
                        writer = csv.writer(f)
                        writer.writerows(row for row in rows if len(row) > 1)
                    with open(self.csv_file_field, 'r') as io:
                        pg_cursor.copy_expert("""copy gmgl.ext_ordered_vehicles_field from stdin 
                            with csv encoding 'latin-1 '""", io)
                    database.commit()
                else:
                    with open(self.csv_file, 'a') as f:
                        writer = csv.writer(f)
                        writer.writerows(row for row in rows if len(row) > 1)
                    with open(self.csv_file, 'r') as io:
                        pg_cursor.copy_expert("""copy gmgl.ext_ordered_vehicles from stdin 
                            with csv encoding 'latin-1 '""", io)
                    database.commit()
                sql = """select gmgl.ext_orders('{}')""".format(division_name)
                pg_cursor.execute(sql)

    def run(self):
        driver = self.setup_driver()
        try:
            for filename in os.listdir(self.csv_dir):
                os.unlink(self.csv_dir + filename)
            driver.get('https://www.autopartners.net/apps/naowb/naowb/manageinventory/mi_02.do?modName=mi'
                       '&VIEWTYPE=ALL&USER_ACTION=mi_02_body&METHOD=doTileInit')
            self.login(self, driver)
            for division in self.divisions:
                division_name = self.select_make(division, driver)
                self.download_file(driver)
                self.create_data(division_name)
        finally:
            driver.quit()


class FinalAllocationByWeekDownload(luigi.Task):
    """
    """
    pipeline = pipeline

    if local_or_production == 'local':
        download_dir = '/home/jon/projects/luigi_1804_36/global_connect/final_allocation_by_week/'
    elif local_or_production == 'production':
        download_dir = '/home/rydell/projects/luigi_1804_36/global_connect/final_allocation_by_week/'

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def setup_driver(self):
        # # Chromedriver path
        chromedriver_path = '/usr/bin/chromedriver'
        options = webdriver.ChromeOptions()
        profile = {"download.default_directory": self.download_dir}
        options.add_experimental_option("prefs", profile)
        options.add_argument('--user-agent="Mozilla/5.0 ((Windows NT 10.0; Win64; x64) AppleWebKit/537.36 '
                             '(KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36"')
        driver = webdriver.Chrome(executable_path=chromedriver_path, options=options)
        return driver

    @staticmethod
    def get_secret(category, platform):
        modified_platform = platform.replace(' ', '+')
        url = ("https://beta.rydellvision.com:8888/vision/secret?platform={0}&category={1}".format
               (modified_platform, category))
        response = requests.get(url, auth=(user, passcode))
        return response.json()

    @staticmethod
    def login(self, driver):
        credentials = self.get_secret('Cartiva', 'Global Connect')
        resp_dict = credentials
        # get username and password fields
        username_box = driver.find_element_by_name('IDToken1')
        password_box = driver.find_element_by_name('IDToken2')
        login_btn = driver.find_element_by_class_name('login')
        username_box.send_keys(resp_dict.get('username'))
        password_box.send_keys(resp_dict.get('secret'))
        login_btn.click()
        # See if login worked
        login_fail_element = driver.find_elements_by_xpath('/html/body/table/tbody/tr[1]/td/table/tbody/tr[2]'
                                                           '/td[2]/table/tbody/tr/td/div/b')
        if len(login_fail_element) > 0:
            # this length should be 0 if login is successful
            raise AssertionError("Cannot login")

    @staticmethod
    def download_excel(driver):
        select_report = driver.find_element_by_xpath('//*[@id="selRpt"]/option[4]')
        select_report.click()
        # Get the first option after "Select Division"
        all_divisions = driver.find_element_by_xpath('//*[@id="ConsOrg"]/option[2]')
        all_divisions.click()
        select_date = driver.find_element_by_xpath('//*[@id="ConsPrd"]/option[2]')
        select_date.click()
        # Click Excel Button
        export_to_excel = driver.find_element_by_id('btnSGoExp')
        export_to_excel.click()
        # Wait for sheet to download
        time.sleep(5)

    def run(self):
        for filename in os.listdir(self.download_dir):
            os.unlink(self.download_dir + filename)
        driver = self.setup_driver()
        driver.get('https://www.autopartners.net/apps/naowb/naowb/reportstools/rt_02.do?'
                   'modName=rt&USER_ACTION=rt_02_body&METHOD=doTileInit')
        self.login(self, driver)
        self.download_excel(driver)
        driver.quit()
        assert len(os.listdir(self.download_dir)) == 1


class FinalAllocationByWeekParse(luigi.Task):
    """
    """
    pipeline = pipeline

    download_dir = 'global_connect/final_allocation_by_week/'

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield FinalAllocationByWeekDownload()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        # changes the xls html file into a csv file
        file = self.download_dir + os.listdir(self.download_dir)[0]
        soup = BeautifulSoup(open(file), 'lxml')
        rows = []
        for row in soup.find_all('tr'):
            rows.append([val.text.strip() for val in row.find_all('td')])
        with open(file, 'w') as f:
            writer = csv.writer(f)
            writer.writerows(row for row in rows if len(row) > 1)
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute("truncate nc.ext_final_allocation_by_week_1;")
                with open(file, 'r') as io:
                    pg_cur.copy_expert("""copy nc.ext_final_allocation_by_week_1 from stdin encoding 'latin-1 '""", io)
                sql = "select nc.update_final_allocation_by_week()"
                pg_cur.execute(sql)


class WeeklyOrderPlacementDownload(luigi.Task):
    """
    """
    pipeline = pipeline

    if local_or_production == 'local':
        download_dir = '/home/jon/projects/luigi_1804_36/global_connect/weekly_order_placement/'
    elif local_or_production == 'production':
        download_dir = '/home/rydell/projects/luigi_1804_36/global_connect/weekly_order_placement/'

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def setup_driver(self):
        # # Chromedriver path
        chromedriver_path = '/usr/bin/chromedriver'
        options = webdriver.ChromeOptions()
        profile = {"download.default_directory": self.download_dir}
        options.add_experimental_option("prefs", profile)
        options.add_argument('--user-agent="Mozilla/5.0 ((Windows NT 10.0; Win64; x64) AppleWebKit/537.36 '
                             '(KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36"')
        driver = webdriver.Chrome(executable_path=chromedriver_path, options=options)
        return driver

    @staticmethod
    def get_secret(category, platform):
        modified_platform = platform.replace(' ', '+')
        url = ("https://beta.rydellvision.com:8888/vision/secret?platform={0}&category={1}".format
               (modified_platform, category))
        response = requests.get(url, auth=(user, passcode))
        return response.json()

    @staticmethod
    def login(self, driver):
        credentials = self.get_secret('Cartiva', 'Global Connect')
        resp_dict = credentials
        # get username and password fields
        username_box = driver.find_element_by_name('IDToken1')
        password_box = driver.find_element_by_name('IDToken2')
        login_btn = driver.find_element_by_class_name('login')
        username_box.send_keys(resp_dict.get('username'))
        password_box.send_keys(resp_dict.get('secret'))
        login_btn.click()
        # See if login worked
        login_fail_element = driver.find_elements_by_xpath('/html/body/table/tbody/tr[1]/td/table/tbody/tr[2]'
                                                           '/td[2]/table/tbody/tr/td/div/b')
        if len(login_fail_element) > 0:
            # this length should be 0 if login is successful
            raise AssertionError("Cannot login")

    @staticmethod
    def download_excel(driver):
        select_report = driver.find_element_by_xpath('//*[@id="selRpt"]/option[3]')
        select_report.click()
        # Get the first option after "Select Division"
        all_divisions = driver.find_element_by_xpath('//*[@id="ConsOrg"]/option[2]')
        all_divisions.click()
        # first report date option in the drop down after Select Report Date ........
        report_date = driver.find_element_by_xpath('//*[@id="RptDate"]/option[2]')
        report_date.click()
        # Click Excel Button
        export_to_excel = driver.find_element_by_id('btnSGoExp')
        export_to_excel.click()
        # Wait for sheet to download
        time.sleep(5)

    def run(self):
        for filename in os.listdir(self.download_dir):
            os.unlink(self.download_dir + filename)
        driver = self.setup_driver()
        driver.get('https://www.autopartners.net/apps/naowb/naowb/reportstools/rt_02.do?'
                   'modName=rt&USER_ACTION=rt_02_body&METHOD=doTileInit')
        self.login(self, driver)
        self.download_excel(driver)
        driver.quit()
        assert len(os.listdir(self.download_dir)) == 1


class WeeklyOrderPlacementParse(luigi.Task):
    """
    """
    pipeline = pipeline

    download_dir = 'global_connect/weekly_order_placement/'
    csv_file = download_dir + 'weekly_order_placement.csv'

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield WeeklyOrderPlacementDownload()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        # changes the xls html file into a csv file
        file = self.download_dir + os.listdir(self.download_dir)[0]
        soup = BeautifulSoup(open(file), 'lxml')
        rows = []
        for row in soup.find_all('tr'):
            rows.append([val.text.strip() for val in row.find_all('td')])
        with open(self.csv_file, 'w') as f:
            writer = csv.writer(f)
            writer.writerows(row for row in rows if len(row) > 1)
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute("truncate nc.ext_weekly_order_placement_1;")
                with open(self.csv_file, 'r') as io:
                    pg_cur.copy_expert("""copy nc.ext_weekly_order_placement_1 from stdin encoding 'latin-1 '""", io)
                sql = "select nc.update_weekly_order_placement()"
                pg_cur.execute(sql)


class VehicleInvoices(luigi.Task):
    """
    10/25/19: looks like bars url has changed, go ahead and move this from aftons scraper server (15) to luigi

    since this module implements these logging tasks:
        @luigi.Task.event_handler(luigi.Event.START)
        def task_start(self):
        @luigi.Task.event_handler(luigi.Event.SUCCESS)
        def task_success(self):
    VehicleInvoices is invoked in a separate cron job @ 3:00 AM daily

    i am not completely satisified with the task, the fact that i am closing & re-opening
        the chrome instance with every vehicle.  it's a trade off between persisting the query of vins for looping
        and surrounding the driver with a try .. finally that ensures chrome gets closed.
    10/28/19 afton is back, my confusion about her script has been clarified, what she does in her block:
              try:
                    driver.find_element_by_id('report').click()
                    tracking(90, 'found report')
                except NoSuchElementException:
                    print 'ERROR'
                    drop_down = driver.find_element_by_xpath("/html/body/form/table[1]/tbody/tr[2]/td[2]/select")
                    drop_down.click()
                    drop_down.find_element_by_css_selector('option[value="D1"]').click()
                    driver.find_element_by_xpath('/html/body/form/table[3]/tbody/tr[1]/td[9]/input').click()
                    driver.find_element_by_id('report').click()
        i was confused, so, if the vin doesn't return an invoice, i assumed she would be going to the next vin, which
        is why this block made no sense, where does she input the next vin?
        but that is not what she is doing, if the vin does not return an invoice, she select simply changes the
        Document drop down choice to Stock Vehicle Invoice, she alleges that she has never run into a situation
        where there was no invoice for an entered vin (how, she asks, can it be in inpmast if there is no invoice?)
        sigh
        so, lets give it a try and see what happens
        working on the script in ~/projects/global_connect_ordering/vehicle_invoices_jon.py
    07/24/20
        excludes 1GT59LE79LF277119:
            Yeah that is weird. Taylor emailed me yesterday to change it to show as retail because
            "This was a sold order ordered by another dealer. When their order got hung up due to Covid,
            we helped them out and let them sell the same truck we had on hand and just took their sold
            order when it got it"
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    @staticmethod
    def setup_driver():
        # # Chromedriver path
        chromedriver_path = '/usr/bin/chromedriver'
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--user-agent="Mozilla/5.0 ((Windows NT 10.0; Win64; x64) AppleWebKit/537.36 '
                                    '(KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36"')
        driver = webdriver.Chrome(executable_path=chromedriver_path, chrome_options=chrome_options)
        return driver

    @staticmethod
    def get_secret(category, platform):
        modified_platform = platform.replace(' ', '+')
        url = ("https://beta.rydellvision.com:8888/vision/secret?platform={0}&category={1}".format
               (modified_platform, category))
        response = requests.get(url, auth=(user, passcode))
        return response.json()

    @staticmethod
    def login(self, driver):
        credentials = self.get_secret('Cartiva', 'Global Connect')
        resp_dict = credentials
        username_box = driver.find_element_by_id('IDToken1')
        password_box = driver.find_element_by_id('IDToken2')
        login_btn = driver.find_element_by_name('Login.Submit')
        username_box.send_keys(resp_dict.get('username'))
        password_box.send_keys(resp_dict.get('secret'))
        login_btn.click()
        WebDriverWait(driver, 20).until(
            ec.presence_of_element_located(('id', 'div-vin')))

    def run(self):
        driver = self.setup_driver()
        driver.get('https://barsrr.autopartners.net/main')
        self.login(self, driver)
        try:
            with utilities.pg(pg_server) as pg_con:
                with pg_con.cursor() as pg_cursor:
                    # normal production query
                    # sql = """
                    #     select inpmast_vin
                    #     from arkona.ext_inpmast
                    #     where status = 'I'
                    #       and type_n_u = 'N'
                    #       and make in ('Chevrolet','GMC','Buick', 'Cadillac')
                    #       and inpmast_vin not in ('3GCUYGED6MG170976')
                    #       and inpmast_vin not in (
                    #         select vin
                    #         from gmgl.vehicle_invoices
                    #         where thru_date > now())
                    # """
                    # invoice not yet entered in dealertrack
                    sql = """
                        select a.vin
                        from gmgl.vehicle_orders a
                        join jo.vehicle_order_events b on a.order_number = b.order_number
                          and b.event_code = '4150'
                          and b.effective_date > '01/01/2020'
                        left join gmgl.vehicle_invoices c on a.vin = c.vin
                          and c.thru_date > current_date
                        where a.vin is not null
                          and c.vin is null
                          and a.vin not in ('1GT49REY3LF215108','1GT59LE79LF277119')
                    """
                    # sql = """
                    #     select '3GCPYCEF4LG172070' """
                    #     union
                    #     select '3GCPYFED9KG286838'
                    # """
                    # sql = "select '1GT49REY3LF215108'"
                    pg_cursor.execute(sql)
                    for the_vin in pg_cursor.fetchall():
                        # click Document -> Vehicle Invoice
                        driver.find_element_by_xpath(
                            '/html/body/br-root/br-main/div/br-search-criteria/'
                            'div/form/table/tr[2]/td[2]/select/option[10]').click()
                        vin_input = driver.find_element_by_xpath(
                            '/html/body/br-root/br-main/div/br-search-criteria/div/form/section[1]/div[1]/input')
                        # clear VIN text box
                        vin_input.clear()
                        # enter vin into VIN text box
                        vin_input.send_keys(the_vin)
                        # click search
                        driver.find_element_by_name('submit').click()
                        # need to pause while the search results refresh to the new vin
                        time.sleep(3)
                        # if the search returns a vehicle, click the select radio button
                        try:
                            WebDriverWait(driver, 3).until(ec.presence_of_element_located(('name', 'report')))
                        except TimeoutException:
                            # click Document -> Stock Vehicle Invoice
                            driver.find_element_by_xpath(
                                '/html/body/br-root/br-main/div/br-search-criteria/'
                                'div/form/table/tr[2]/td[2]/select/option[9]').click()
                            # click search
                            driver.find_element_by_name('submit').click()
                        WebDriverWait(driver, 3).until(ec.presence_of_element_located(('name', 'report')))
                        driver.find_element_by_name('report').click()
                        time.sleep(2)  # this pause is required
                        driver.switch_to.window(driver.window_handles[1])
                        driver.switch_to.default_content()
                        raw_text = driver.find_element_by_xpath('//*[@id="doc-detail"]')
                        invoice = raw_text.get_attribute('innerText')
                        driver.close()
                        driver.switch_to.window(driver.window_handles[0])
                        # driver.switch_to.default_content()
                        f = open('invoice_text.txt', 'w')
                        f.write(invoice)
                        print('waiting')
                        time.sleep(5)  # waiting to write
                        f.close()
                        testing_invoice = 'invoice_text.txt'
                        text = open(testing_invoice)
                        lines = open(testing_invoice, 'rt')
                        empinc = None
                        employ = None
                        supinc = None
                        supplr = None
                        total_model_msrp = None
                        total_model_invoice = None
                        total_msrp = None
                        total_invoice = None
                        act = None
                        hb = None
                        adv = None
                        exp = None
                        vin = None
                        vehicle_invoice = None
                        for line in text:
                            if 'EMPINC' in line:
                                empinc = line.split('EMPINC:', 1)[1].strip().replace('-', '')
                            if 'EMPLOY' in line:
                                employ = line.split('EMPLOY:', 1)[1].strip().replace('-', '')
                            if 'SUPINC' in line:
                                supinc = line.split('SUPINC:', 1)[1].strip().replace('-', '')
                            if 'SUPPLR' in line:
                                supplr = line.split('SUPPLR:', 1)[1].strip().replace('-', '')
                            if 'TOTAL MODEL &amp; OPTIONS' in line:
                                numbers = line.split('TOTAL MODEL &amp; OPTIONS')[1].split()
                                total_model_msrp = numbers[0].strip().replace('-', '')
                                total_model_invoice = numbers[1].strip().replace('-', '')
                            if 'TOTAL' in line:
                                if 'PAY' in line:
                                    numbers = line.split('TOTAL')[1].split()
                                    total_msrp = numbers[0].strip().replace('-', '')
                                    total_invoice = numbers[1].strip().replace('-', '')
                            if 'ACT' in line:
                                if 'TOTAL MODEL &amp; OPTIONS' in line:
                                    act = line.split('ACT')[1].split()[1].strip().replace('-', '')
                            if 'H/B' in line:
                                hb = line.split('H/B')[1].split()[1].strip().replace('-', '')
                            if 'ADV' in line:
                                if 'DEALER IMR' in line:
                                    adv = line.split('ADV')[1].split()[1].strip().replace('-', '')
                            if 'EXP' in line:
                                if 'LMA GROUP' in line:
                                    exp = line.split('EXP')[1].split()[1].strip().replace('-', '')
                            if 'VEHICLE INVOICE' in line:
                                vehicle_invoice = line.split('VEHICLE INVOICE')[1].strip()
                            if 'ADJUSTMENT INVOICE' in line:
                                vehicle_invoice = line.split('ADJUSTMENT INVOICE')[1].strip()
                            if 'VIN' in line:
                                if len(line.split('VIN')[1].strip()) == 17:
                                    vin = line.split('VIN')[1].strip().replace('-', '')
                                elif 'INVOICE' in line:
                                    vin = line.split('VIN')[1].split()[0] + line.split('VIN')[1].split()[1] \
                                          + line.split('VIN')[1].split()[2] + line.split('VIN')[1].split()[3]
                        parsed_invoice = dict(empinc=empinc, employ=employ, supinc=supinc, supplr=supplr,
                                              total_model_msrp=total_model_msrp,
                                              total_model_invoice=total_model_invoice, total_msrp=total_msrp,
                                              total_invoice=total_invoice, act=act, hb=hb, adv=adv, exp=exp,
                                              vin=vin, vehicle_invoice=vehicle_invoice)
                        with utilities.pg(pg_server) as pg_con_1:
                            with pg_con_1.cursor() as pg_cursor_1:
                                sql = """
                                    select gmgl.xfm_vehicle_invoice('%s','%s', '%s')""" % (
                                    vin, json.dumps(parsed_invoice),
                                    lines.read().replace("'", "''").replace('&amp;', '&'))
                                pg_cursor_1.execute(sql)
                        text.close()
                        lines.close()
                        os.remove('invoice_text.txt')
        finally:
            driver.quit()


class UpdateBtgVsOrdered(luigi.Task):
    """
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield FinalAllocationByWeekParse()
        yield WeeklyOrderPlacementParse()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute('select nc.update_btg_vs_ordered();')


class TradingPartnersDownload(luigi.Task):
    """
    https://stackoverflow.com/questions/51743859/navigating-through-pagination-with-selenium-in-python

    sales
        vehicle locator
            trading partners
    """
    pipeline = pipeline

    if local_or_production == 'local':
        download_dir = '/home/jon/projects/luigi_1804_36/global_connect/weekly_order_placement/'
    elif local_or_production == 'production':
        download_dir = '/home/rydell/projects/luigi_1804_36/global_connect/weekly_order_placement/'

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                pipeline + '_' + self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def setup_driver(self):
        # # Chromedriver path
        chromedriver_path = '/usr/bin/chromedriver'
        options = webdriver.ChromeOptions()
        profile = {"download.default_directory": self.download_dir}
        options.add_experimental_option("prefs", profile)
        options.add_argument('--user-agent="Mozilla/5.0 ((Windows NT 10.0; Win64; x64) AppleWebKit/537.36 '
                             '(KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36"')
        driver = webdriver.Chrome(executable_path=chromedriver_path, options=options)
        return driver

    @staticmethod
    def get_secret(category, platform):
        modified_platform = platform.replace(' ', '+')
        url = ("https://beta.rydellvision.com:8888/vision/secret?platform={0}&category={1}".format
               (modified_platform, category))
        response = requests.get(url, auth=(user, passcode))
        return response.json()

    @staticmethod
    def login(self, driver):
        credentials = self.get_secret('Cartiva', 'Global Connect')
        resp_dict = credentials
        # get username and password fields
        username_box = driver.find_element_by_name('IDToken1')
        password_box = driver.find_element_by_name('IDToken2')
        login_btn = driver.find_element_by_class_name('login')
        username_box.send_keys(resp_dict.get('username'))
        password_box.send_keys(resp_dict.get('secret'))
        login_btn.click()
        # See if login worked
        login_fail_element = driver.find_elements_by_xpath('/html/body/table/tbody/tr[1]/td/table/tbody/tr[2]'
                                                           '/td[2]/table/tbody/tr/td/div/b')
        if len(login_fail_element) > 0:
            # this length should be 0 if login is successful
            raise AssertionError("Cannot login")

    @staticmethod
    def download_excel(driver):
        select_report = driver.find_element_by_xpath('//*[@id="selRpt"]/option[3]')
        select_report.click()
        # Get the first option after "Select Division"
        all_divisions = driver.find_element_by_xpath('//*[@id="ConsOrg"]/option[2]')
        all_divisions.click()
        # first report date option in the drop down after Select Report Date ........
        report_date = driver.find_element_by_xpath('//*[@id="RptDate"]/option[2]')
        report_date.click()
        # Click Excel Button
        export_to_excel = driver.find_element_by_id('btnSGoExp')
        export_to_excel.click()
        # Wait for sheet to download
        time.sleep(5)

    def run(self):
        # for filename in os.listdir(self.download_dir):
        #     os.unlink(self.download_dir + filename)
        driver = self.setup_driver()
        driver.get('https://www.autopartners.net/apps/vls/vls/services/home/popup?source=tp#')
        self.login(self, driver)
        pages = WebDriverWait(driver, 20).until(
            ec.element_to_be_clickable(('xpath', '//*[@id="tbtext-1028"]')))
        pages = pages.text
        print(pages)
        # table = driver.find_elements_by_xpath('//*[@id="gridview-1021"]/table/tbody')
        # for row in table:
        #     row = driver.find_element_by_class_name('x-grid-row')
        #     print(row)

        driver.quit()
        # assert len(os.listdir(self.download_dir)) == 1


class GlobalConnect(luigi.WrapperTask):
    """
    invoked in a separate cron job: 6:10 AM monday thru friday
    """
    pipeline = pipeline

    def requires(self):
        yield BuildOut2020v3Parse()
        yield StartUp2020v3Parse()
        yield DealerConstraintsParse()
        yield BuildoutConstraintsParse()
        yield EventCodesParse()
        yield FinalAllocationByWeekParse()
        yield WeeklyOrderPlacementParse()
        yield UpdateBtgVsOrdered()


if __name__ == '__main__':
    luigi.run(["--workers=1"], main_task_cls=VehicleInvoices)
