# encoding=utf-8
"""
"""
import utilities
import luigi
import bs_est_pay
import sc_payroll_201803
import datetime
import used_cars
import ext_arkona
import xfm_arkona
import vendor_rules
import afton_tasks
import dataone
import clock_hour_tracking
import open_ros
import fact_fs
import misc
import full_shop
import fact_repair_order
import service_advisor_payroll
import team_pay

pg_server = '173'

ts_no_spaces = str(datetime.datetime.now()).replace(" ", "")


@luigi.Task.event_handler(luigi.Event.START)
def task_start(self):
    utilities.luigi_log_start(pg_server, self.task_id + '__' + ts_no_spaces, self.pipeline, self.__class__.__name__)


@luigi.Task.event_handler(luigi.Event.SUCCESS)
def task_success(self):
    utilities.luigi_log_pass(pg_server, self.task_id + '__' + ts_no_spaces, self.pipeline, self.__class__.__name__)
    try:  # WrapperTest throws an AttributeError, see /Notes/issues
        with self.output().open("w") as out_file:
            # write to the output file
            out_file.write('pass')
    except AttributeError:
        pass


class RunAll(luigi.WrapperTask):
    """
    """
    pipeline = 'run_all'

    def requires(self):
        yield fact_fs.FactFsWrapper()
        yield bs_est_pay.ClockHours()
        yield bs_est_pay.BsSalesDetail()
        yield sc_payroll_201803.ConsultantPayroll()
        yield xfm_arkona.XfmArkona()
        yield vendor_rules.RunVendorRules()
        yield used_cars.DailySnapshotBeta1()
        yield afton_tasks.BoardAccountingGross()
        yield dataone.Dataone()
        yield clock_hour_tracking.ClockHoursWrapper()
        yield open_ros.UpdateOpenRos()
        yield ext_arkona.TestExtInpmast()
        yield misc.MiscWrapper()
        yield ext_arkona.MiscArkonaTables()
        yield full_shop.FullShop()
        yield fact_repair_order.FactRepairOrder()
        yield service_advisor_payroll.ServiceAdvisorPayroll()
        yield team_pay.DataUpdateNightly()


if __name__ == '__main__':
    luigi.run(["--workers=6"], main_task_cls=RunAll)
