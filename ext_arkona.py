# encoding=utf-8
"""
expanded both to include 2015, 2016 for sales pto rates
pyhshdta
pyhscdta
pymast
inpmast
inpoptd
inpoptf
pypclockin
glpcust
pyactgr
added wrapper parts which includes ext only of pdpmrpl,pdppmex, pdpmast, pdpsgrp & pdptdet
09/16/20: added global variable ts_with_minute to enable ext_pymast to run multiple times a day
10/10/20: added ExtPypclkl - timeclock corrections
10/16/20: added ExtSdprdetHistory
11/05/20: ExtPymast: changed datetime element of target to target_date
11/05/20: ExtPymast: changed datetime element of target back to ts_with_minute to accomdate
            compli running every 20 minutes
todo: have to figure out how i am going to handle pymast being a common requirement for different
        "real time" tasks that run at different frequencies

this is a test to store the project on the E drive, modify it in pycharm in linux and git commit

"""
import luigi
import utilities
import csv
import datetime
from datetime import date, timedelta

pipeline = 'ext_arkona'
db2_server = 'report'
pg_server = '173'
ts_no_spaces = str(datetime.datetime.now()).replace(" ", "")
ts_with_minute = '{:%Y-%m-%d-%H-%M}'.format(datetime.datetime.now())
target_date = '{:%Y-%m-%d}'.format(datetime.datetime.now())

# # if these task are being called via run_all, that task will handle all of this logging
# @luigi.Task.event_handler(luigi.Event.START)
# def task_start(self):
#     utilities.luigi_log_start(pg_server, self.task_id + '__' + ts_no_spaces, self.pipeline, self.__class__.__name__)
#
#
# @luigi.Task.event_handler(luigi.Event.SUCCESS)
# def task_success(self):
#     utilities.luigi_log_pass(pg_server, self.task_id + '__' + ts_no_spaces, self.pipeline, self.__class__.__name__)
#     try:  # WrapperTest throws an AttributeError, see /Notes/issues
#         with self.output().open("w") as out_file:
#             # write to the output file
#             out_file.write('pass')
#     except AttributeError:
#         pass


class ExtPyhshdta(luigi.Task):
    """
    invoked by sc_payroll_201803.PaidByMonth
    """
    # TODO hard coded date in where clause
    extract_file = '../../extract_files/pyhshdta.csv'
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.arkona_luigi_27(db2_server) as cn:
            with cn.cursor() as cur:
                sql = """
                    select TRIM(COMPANY_NUMBER),PAYROLL_CEN_YEAR,PAYROLL_RUN_NUMBER,TRIM(EMPLOYEE_),TRIM(SEQ_VOID),
                      REFERENCE_CHECK_,PAYROLL_CENTURY,PAYROLL_ENDING_YEAR,PAYROLL_ENDING_MONTH,PAYROLL_ENDING_DAY,
                      CHECK_CENTURY,CHECK_YEAR,CHECK_MONTH,CHECK_DAY,BASE_PAY,TOTAL_GROSS_PAY,TOTAL_ADJ_GROSS,
                      CURR_FEDERAL_TAX,CURR_FICA,CURR_EMPLR_FICA,CURR_EMPLOYEE_MEDICARE,CURR_EMPLOYER_MEDICARE,
                      YTD_EIC_PAYMENTS,CURR_STATE_TAX,CURR_EMPLOYEE_SDI,CURR_EMPLOYER_SDI,CURR_COUNTY_TAX,CURR_CITY_TAX,
                      CURR_FUTA,CURR_SUTA,WORKMANS_COMP,EMPLY_CURR_RET,EMPLR_CURR_RET,OVERTIME_AMOUNT,TOTAL_DEDUCTION,
                      TOTAL_OTHER_PAY,CURRENT_NET,CURRENT_TAX_TOT,CURR_ADJ_FED,CURR_ADJ_SS,CURR_ADJ_FUTA,CURR_ADJ_STATE,
                      CURR_ADJ_CNTY,CURR_ADJ_CITY,CURR_ADJ_SUTA,CURR_ADJ_COMP,VACATION_TAKEN,HOLIDAY_TAKEN,
                      SICK_LEAVE_TAKEN,VACATION_ACC,HOLIDAY_ACC,SICK_LEAVE_ACC,REG_HOURS,OVERTIME_HOURS,ALT_PAY_HOURS,
                      DEF_VAC_HRS,DEF_HOL_HRS,DEF_SCK_HRS,TRIM(VAC),TRIM(SICK_LEAVE),TRIM(HOLIDAY),
                      TRIM(FEDERAL_TAX_EXEMPT),TRIM(FICA_EXEMPT),TRIM(STATE_TAX_EXEMPT),TRIM(COUNTY_TAX_EXEMPT),
                      TRIM(CITY_TAX_EXEMPT),HIRE_DATE,TRIM(MARITAL_STATUS),TRIM(PAY_PERIOD),TRIM(PAYROLL_CLASS),
                      TRIM(EXEMPT_FROM_FICA),FEDERAL_EXEMPTIONS,FEDERAL_ADD_ON_AMOUNT,TRIM(EIC_ELIGABLE),
                      TRIM(EIC_CLASS),STATE_EXEMPTIONS,STATE_FIXED_EXEMPTIONS,STATE_TAX_ADD_ON_AMOUNT,WKMAN_COMP_CODE,
                      TRIM(CUM_WAGE_WITHHLD),TRIM(STATE_TAX_TAB_CODE),TRIM(COUNTY_TAX_TAB_CODE),TRIM(CITY_TAX_TAB_CODE),
                      TRIM(STATE_TAX_TAB_CODE_YHSTCD),TRIM(COUNTY_TAX_TAB_CODE_YHCNCD),TRIM(CITY_TAX_TAB_CODE_YHMUCD),
                      BASE_SALARY,BASE_HRLY_RATE,ALT_SALARY,ALT_HRLY_RATE,TRIM(ELIG_FOR_RETIRE),RETIREMENT_,
                      RETIREMENT_FIXED_AMOUNT,DEFERRED_VAC_YEAR,DEFERRED_HOL_YEAR,DEFFERED_SIC_YEAR,
                      TRIM(EMPLOYEE_NAME),TRIM(DEPARTMENT_CODE),SECURITY_GROUP,SOC_SEC_NUMBER,TRIM(DISTRIB_CODE),
                      TRIM(A_R_CUSTOMER_),TRIM(VOIDED_FLAG),PAYROLL_CEN_YEAR_YHVCYY,PAYROLL_RUN_NUMBER_YHVNUM,
                      TRIM(SEQ_VOID_YHVSEQ),TRIM(RECORD_TYPE),ADJUSTED_CITY_WAGES,ADJUSTED_SDI_WAGES,
                      ADJUSTED_OTHER_WAGES,TRIM(DIRECT_DEPOSIT_INDICATOR),FEDERAL_ADDITIONAL_TAX_,
                      TRIM(SHOW_HOLIDAY_ON_CHECK),STATE_ADDITIONAL_TAX_,TRIM(FUTA_OVERRIDE),TRIM(ADDITIONAL_OVERRIDE),
                      TRIM(EMPLOYEE_RETIREMENT_OVERRIDE),TRIM(EMPLOYER_RETIREMENT_OVERRIDE),TRIM(SDI_EMPLOYEE_OVERRIDE),
                      TRIM(SDI_EMPLOYER_OVERRIDE),TRIM(SUTA_OVERRIDE),CURR_EMPLEE_SUTA,CURR_2ND_STATE_WHLD2,
                      CURR_2ND_CNTY_WHLD2,CURR_2ND_CITY_WHLD2,TRIM(FEDERAL_FILING_STATUS),TRIM(STATE_FILING_STATUS),
                      TRIM(COUNTY_FILING_STATUS),TRIM(CITY_FILING_STATUS),TRIM(STATE_FILING_STATUS_2),
                      TRIM(COUNTY_FILING_STATUS_2),TRIM(CITY_FILING_STATUS_2),TRIM(STATE_TAX_TAB_CODE2),
                      TRIM(COUNTY_TAX_TAB_CODE2),TRIM(CITY_TAX_TAB_CODE2),TRIM(SDI_STATE),TRIM(WRKR_COMP_STATE),
                      TRIM(REPORTING_CODE_1),TRIM(REPORTING_CODE_2),TRIM(EEOC),TRIM(OVERTIME_EXEMPT),
                      TRIM(HIGH_COMPENSATE_FLAG),TRIM(NOTUSED_1),TRIM(NOTUSED_2),TRIM(NOTUSED_3),TRIM(NOTUSED_4),
                      TRIM(NOTUSED_5),TRIM(NOTUSED_6),TRIM(NOTUSED_7),NOTUSED_8,NOTUSED_9
                    FROM  rydedata.pyhshdta a
                    where payroll_cen_year > 114
                """
                cur.execute(sql)
                with open(self.extract_file, 'w') as f:
                    csv.writer(f).writerows(cur)
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute("truncate arkona.ext_pyhshdta")
                with open(self.extract_file, 'r') as io:
                    pg_cur.copy_expert("""copy arkona.ext_pyhshdta from stdin with csv encoding 'latin-1 '""", io)


class ExtPyhscdta(luigi.Task):
    """
    invoked by sc_payroll_201803.PaidByMonth
    """
    # TODO hard coded date in where clause
    extract_file = '../../extract_files/pyhscdta.csv'
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.arkona_luigi_27(db2_server) as cn:
            with cn.cursor() as cur:
                sql = """
                select TRIM(COMPANY_NUMBER),PAYROLL_CEN_YEAR,PAYROLL_RUN_NUMBER,TRIM(EMPLOYEE_NUMBER),
                  TRIM(SEQ_VOID),TRIM(CODE_TYPE),CODE_SEQ_,TRIM(CODE_ID),TRIM(CODE_SW),TRIM(FIXED_DEDUCTION),
                  TRIM(FIXED_DEDUCTION_YHCCRT),TRIM(BENIFIT_ADJUST_SW),AMOUNT,ENTERED_AMOUNT,PAY_RATE,
                  TRIM(DESCRIPTION),TRIM(PERCNT_CODE),TRIM(LIMIT_REACHED),TRIM(DIST_OVERRIDE),
                  TRIM(DIST_ACCT),REFERENCE_CHECK_
                FROM  rydedata.pyhscdta a
                where payroll_cen_year > 114
                """
                cur.execute(sql)
                with open(self.extract_file, 'w') as f:
                    csv.writer(f).writerows(cur)
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute("truncate arkona.ext_pyhscdta")
                with open(self.extract_file, 'r') as io:
                    pg_cur.copy_expert("""copy arkona.ext_pyhscdta from stdin with csv encoding 'latin-1 '""", io)


class ExtPymast(luigi.Task):
    """
    This is required by compli which runs multiple times a day, so, this to update throughout the day, needs
    to have the local_target changed to be at the minute resolution rather than day
    we will see, contemplating changing the pattern to what is used in fact_repair_order_today, a separate
        output folder that is emptied at the end of the batch
    not sure how i am going to deal with this being a requirement for multiple real time tasks run at
        different frequencies, for now, just re-enable local_target with ts_with_minute to accomodate compli
    """
    extract_file = '../../extract_files/pymast.csv'
    pipeline = pipeline

    def local_target(self):
        # return '../../luigi_output_files/' + target_date + '_' + self.__class__.__name__ + ".txt"
        return '../../luigi_output_files/' + ts_with_minute + '_' + self.__class__.__name__ + ".txt"

    def requires(self):
        return None

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.arkona_luigi_27(db2_server) as cn:
            with cn.cursor() as cur:
                sql = """
                    select TRIM(PYMAST_COMPANY_NUMBER),TRIM(PYMAST_EMPLOYEE_NUMBER),TRIM(ACTIVE_CODE),
                      TRIM(DEPARTMENT_CODE),DEPARTMENT_EXT,SECURITY_GROUP,TRIM(STATE_CODE_UNEMPLOYMENT),
                      TRIM(COUNTY_CODE_UNEMPLOYMENT),TRIM(CITY_CODE_UNEMPLOYMENT),TRIM(EMPLOYEE_NAME),
                      TRIM(EMPLOYEE_FIRST_NAME),TRIM(EMPLOYEE_MIDDLE_NAME),TRIM(EMPLOYEE_LAST_NAME),RECORD_KEY,
                      TRIM(ADDRESS_1),TRIM(ADDRESS_2),TRIM(CITY_NAME),TRIM(STATE_CODE_ADDRESS_),ZIP_CODE,
                      TEL_AREA_CODE,TELEPHONE_NUMBER,CELL_PHONE_NUMBER,TRIM(CELL_PHONE_PROVIDER_CODE),
                      EMAIL_ADDRESS,SOC_SEC_NUMBER,TRIM(DRIVERS_LICENSE_),BIRTH_DATE,HIRE_DATE,ORG_HIRE_DATE,
                      LAST_RAISE_DATE,TERMINATION_DATE,TRIM(SEX),TRIM(FULL_TIME_PART_TIME),TRIM(MARITAL_STATUS),
                      TRIM(EXEMPT_FROM_FICA),TRIM(EXEMPT_FROM_FED),FEDERAL_EXEMPTIONS,FEDERAL_ADD_ON_AMOUNT,
                      FEDERAL_ADD_ON_PERCENT,TRIM(BENFIT_TYPE),TRIM(PAYROLL_CLASS),TRIM(EIC_ELIGABLE),
                      TRIM(EIC_CLASS),TRIM(PAY_PERIOD),BASE_SALARY,BASE_HRLY_RATE,ALT_SALARY,ALT_HRLY_RATE,
                      TRIM(DISTRIB_CODE),TRIM(ELIG_FOR_RETIRE),RETIREMENT_,RETIREMENT_FIXED_AMOUNT,
                      RETIREMENT_ANNUAL_MAX,WKMAN_COMP_CODE,TRIM(CUM_WAGE_WITHHLD),TRIM(STATE_TAX_TAB_CODE),
                      TRIM(COUNTY_TAX_TAB_CODE),TRIM(CITY_TAX_TAB_CODE),LAST_UPDATE,LAST_CHECK,TRIM(DECEASED_FLAG),
                      TRIM(EXEMPT_FROM_STATE),STATE_EXEMPTIONS,STATE_TAX_ADD_ON_AMOUNT,STATE_ADD_ON_PERCENT,
                      STATE_FIXED_EXEMPTIONS,TRIM(EXEMPT_FROM_COUNTY),COUNTY_TAX_ADD_ON_AMOUNT,COUNTY_ADD_ON_PERCENT,
                      TRIM(EXEMPT_FROM_LOCAL),LOCAL_TAX_ADD_ON_AMOUNT,LOCAL_ADD_ON_PERCENT,TRIM(BANK_INFO),
                      DEFERRED_VACATION,DEFERRED_HOLIDAYS,DEFFERED_SICKLEAVE,DEFERRED_VAC_YEAR,DEFERRED_HOL_YEAR,
                      DEFFERED_SIC_YEAR,VAC_HRLY_RATE,HOL_HRLY_RATE,SCK_HRLY_RATE,TRIM(A_R_CUSTOMER_),
                      TRIM(DRIVER_LIC_STATE_CODE),DRIVER_LIC_EXPIRE_DATE,TRIM(FEDERAL_FILING_STATUS),
                      TRIM(STATE_FILING_STATUS),TRIM(COUNTY_FILING_STATUS),TRIM(CITY_FILING_STATUS),
                      TRIM(STATE_FILING_STATUS_2),TRIM(COUNTY_FILING_STATUS_2),TRIM(CITY_FILING_STATUS_2),
                      TRIM(STATE_TAX_TAB_CODE2),TRIM(COUNTY_TAX_TAB_CODE2),TRIM(CITY_TAX_TAB_CODE2),
                      TRIM(SDI_STATE),TRIM(WRKR_COMP_STATE),TRIM(REPORTING_CODE_1),TRIM(REPORTING_CODE_2),
                      TRIM(EEOC),TRIM(OVERTIME_EXEMPT),TRIM(HIGH_COMPENSATE_FLAG),K_ELIGIBILITY_DATE01E,
                      K_MATCHING_DATE01M,TRIM(TEMP_ADD_1),TRIM(TEMP_ADD_2),TRIM(TEMP_ADD_3),TRIM(TEMP_ADD_4),
                      TRIM(TEMP_ADD_5),TRIM(TEMP_ADD_6),TRIM(TEMP_ADD_7),TEMP_ADD_9,TEMP_ADD10,UNION_ANNIVERSARY_DATE
                    from rydedata.pymast
                """
                cur.execute(sql)
                with open(self.extract_file, 'w') as f:
                    csv.writer(f).writerows(cur)
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute("truncate arkona.ext_pymast")
                with open(self.extract_file, 'r') as io:
                    pg_cur.copy_expert(
                        """copy arkona.ext_pymast from stdin with csv encoding 'latin-1 '""", io)


class ExtInpmast(luigi.Task):
    """
    9/11/17
    ref: Desktop/sql/inpmast.sql, exclude anomalies
    """
    extract_file = '../../extract_files/inpmast.csv'
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.arkona_luigi_27(db2_server) as cn:
            with cn.cursor() as cur:
                sql = """
                    SELECT TRIM(INPMAST_COMPANY_NUMBER),TRIM(INPMAST_VIN),TRIM(VIN_LAST_6),TRIM(INPMAST_STOCK_NUMBER),
                      TRIM(INPMAST_DOCUMENT_NUMBER),TRIM(STATUS),TRIM(G_L_APPLIED),TRIM(TYPE_N_U),
                      TRIM(BUS_OFF_FRAN_CODE),TRIM(SERVICE_FRAN_CODE),TRIM(MANUFACTURER_CODE),
                      TRIM(VEHICLE_CODE),YEAR,TRIM(MAKE),TRIM(MODEL_CODE),TRIM(MODEL),TRIM(BODY_STYLE),
                      TRIM(COLOR),TRIM(TRIM),TRIM(FUEL_TYPE),TRIM(MPG),CYLINDERS,TRIM(TRUCK),TRIM(WHEEL_DRIVE4WD),
                      TRIM(TURBO),TRIM(COLOR_CODE),TRIM(ENGINE_CODE),TRIM(TRANSMISSION_CODE),TRIM(IGNITION_KEY_CODE),
                      TRIM(TRUNK_KEY_CODE),TRIM(KEYLESS_CODE),TRIM(RADIO_CODE),TRIM(WHEEL_LOCK_CODE),
                      TRIM(DEALER_CODE),TRIM(LOCATION),ODOMETER,DATE_IN_INVENT,DATE_IN_SERVICE,DATE_DELIVERED,
                      DATE_ORDERED,TRIM(INPMAST_SALE_ACCOUNT),TRIM(INVENTORY_ACCOUNT),TRIM(DEMO_NAME),
                      WARRANTY_MONTHS,WARRANTY_MILES,WARRANTY_DEDUCT,LIST_PRICE,INPMAST_VEHICLE_COST,
                      TRIM(OPTION_PACKAGE),TRIM(LICENSE_NUMBER),GROSS_WEIGHT,WORK_IN_PROCESS,INSPECTION_MONTH,
                      TRIM(ODOMETER_ACTUAL),BOPNAME_KEY,TRIM(KEY_TO_CAP_EXPLOSION_DATA),TRIM(CO2_EMISSION_CODE2),
                      REGISTRATION_DATE1,FUNDING_EXPIRATION_DATE2,INSPECTION_DATE3,TRIM(DRIVERS_SIDE),
                      FREE_FLOORING_PERIOD,TRIM(ORDERED_STATUS),TRIM(PUBLISH_VEHICLE_INFO_TO_WEB),TRIM(SALE),
                      TRIM(CERTIFIED_USED_CAR),LAST_SERVICE_DATE4,NEXT_SERVICE_DATE5,DEALER_CODE_IMDLRCD,
                      COMMON_VEHICLE_ID,CHROME_STYLE_ID,CREATED_USER_CODE,UPDATED_USER_CODE,CREATED_TIMESTAMP,
                      UPDATED_TIMESTAMP,TRIM(STOCKED_COMPANY),ENGINE_HOURS
                    FROM  rydedata.inpmast
                    where inpmast_company_number = 'RY1'
                      and trim(inpmast_stock_number) not in ('18','2','K','U', '3532')
                      and inpmast_vin not in ('1FMCU0H98DUB23818','1GCEK19037Z506985')
                """
                cur.execute(sql)
                with open(self.extract_file, 'w') as f:
                    csv.writer(f).writerows(cur)
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute("truncate arkona.ext_inpmast")
                with open(self.extract_file, 'r') as io:
                    pg_cur.copy_expert(
                        """copy arkona.ext_inpmast from stdin with csv encoding 'latin-1 '""", io)


class ExtInpoptd(luigi.Task):
    """

    """
    extract_file = '../../extract_files/inpoptd.csv'
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield ExtInpmast()
        yield ExtInpoptf()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.arkona_luigi_27(db2_server) as cn:
            with cn.cursor() as cur:
                sql = """
                    SELECT TRIM(COMPANY_NUMBER),SEQ_NUMBER,TRIM(FIELD_DESCRIP),TRIM(FIELD_TYPE),
                      TRIM(REQ_ENTRY_INVENT),TRIM(REQ_ENTRY_TRADE),TRIM(REQ_ENTRY_USED),TRIM(SCHEDULE_BY),
                      DEF_VALUE_NEW,DEF_VALUE_USED,TRIM(ADD_TO_COST),TRIM(CONTROL_NUMBER),TRIM(CREATE_TRADE_GL)
                    FROM  rydedata.inpoptd
                """
                cur.execute(sql)
                with open(self.extract_file, 'w') as f:
                    csv.writer(f).writerows(cur)
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute("truncate arkona.ext_inpoptd")
                with open(self.extract_file, 'r') as io:
                    pg_cur.copy_expert(
                        """copy arkona.ext_inpoptd from stdin with csv encoding 'latin-1 '""", io)


class ExtInpoptf(luigi.Task):
    """

    """
    extract_file = '../../extract_files/inpoptf.csv'
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.arkona_luigi_27(db2_server) as cn:
            with cn.cursor() as cur:
                sql = """
                    SELECT
                      TRIM(COMPANY_NUMBER),TRIM(VIN_NUMBER),SEQ_NUMBER,TRIM(FIELD_TYPE),
                      TRIM(A_N_FIELD_VALUE),NUM_FIELD_VALUE,DATE_FIELD_VALUE,TRIM(ADD_TO_COST)
                      FROM  rydedata.inpoptf
                      where company_number in ('RY1','RY2')
                """
                cur.execute(sql)
                with open(self.extract_file, 'w') as f:
                    csv.writer(f).writerows(cur)
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute("truncate arkona.ext_inpoptf")
                with open(self.extract_file, 'r') as io:
                    pg_cur.copy_expert(
                        """copy arkona.ext_inpoptf from stdin with csv encoding 'latin-1 '""", io)


class ExtPypclockin(luigi.Task):
    """
    extracts a months worth of data from pypclockin, adds the data to arkona.ext_pypclockin,
    and truncates and populates arkona.ext_pypclockin_tmp
    """
    extract_file = '../../extract_files/pypclockin.csv'
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        from_date = date.today() - timedelta(days=100)
        with utilities.arkona_luigi_27(db2_server) as cn:
            with cn.cursor() as cur:
                sql = """
                    select
                      TRIM(YICO#),TRIM(PYMAST_EMPLOYEE_NUMBER),YICLKIND,
                      case when yiclkint = '24:00:00' then '23:59:59' else yiclkint end as yiclkint,
                      YICLKOUTD,
                      case when yiclkoutt = '24:00:00' then '23:59:59' else yiclkoutt end as YICLKOUTT,
                      TRIM(YISTAT),
                      TRIM(YIINFO),TRIM(YICODE),TRIM(YIUSRIDI),TRIM(YIWSIDI),TRIM(YIUSRIDO),TRIM(YIWSIDO)
                    from rydedata.pypclockin
                    where yico# in ('RY1','RY2')
                      and yiclkind >= '{0}'
                """.format(from_date)
                cur.execute(sql)
                with open(self.extract_file, 'w') as f:
                    csv.writer(f).writerows(cur)
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                # updates both arkona.ext_pypclockin andd arkona.ext_pypclockin_tmp
                sql = "delete from arkona.ext_pypclockin where yiclkind >= '{0}'".format(from_date)
                pg_cur.execute(sql)
                with open(self.extract_file, 'r') as io:
                    pg_cur.copy_expert(
                        """copy arkona.ext_pypclockin from stdin with csv encoding 'latin-1 '""", io)
                pg_con.commit()
                pg_cur.execute("truncate arkona.ext_pypclockin_tmp")
                with open(self.extract_file, 'r') as io:
                    pg_cur.copy_expert(
                        """copy arkona.ext_pypclockin_tmp from stdin with csv encoding 'latin-1 '""", io)


class ExtGlpcust(luigi.Task):
    """

    """
    extract_file = '../../extract_files/glpcust.csv'
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.arkona_luigi_27(db2_server) as cn:
            with cn.cursor() as cur:
                sql = """
                  SELECT TRIM(COMPANY_NUMBER),RECORD_KEY,TRIM(ACTIVE),TRIM(CUSTOMER_NUMBER),TRIM(VENDOR_NUMBER),
                    TRIM(SEARCH_NAME),TRIM(BILL_CONTACT),TRIM(BILL_ADDRESS_1),TRIM(BILL_ADDRESS_2),TRIM(BILL_CITY),
                    TRIM(BILL_STATE_CODE),BILL_ZIP_CODE,BILL_PHONE_NUMBER,BILL_PHONE_EXT,BILL_FAX_NUMBER,
                    TRIM(PYMT_CONTACT),TRIM(PYMT_ADDRESS_1),TRIM(PYMT_ADDRESS_2),TRIM(PYMT_CITY),
                    TRIM(PYMT_STATE_CODE),PYMT_ZIP_CODE,PYMT_PHONE_NUMBER,PYMT_PHONE_EXT,PYMT_FAX_NUMBER,
                    TRIM(FEDERAL_TAX_ID),SOC_SEC_,TRIM(PO_REQUIRED),CUSTOMER_TYPE,VENDOR_TYPE,AR_PAYMENT_TERMS,
                    AP_PAYMENT_TERMS,TAX_GROUP,CREDIT_LIMIT,TRIM(FINANCE_CHARGE),TRIM(CORPORATION_1099_),
                    TRIM(CHECK_STUB_DETAIL),CREATION_DATE,TRIM(OPEN_PO_),TRIM(CUSTOMER_ACCOUNT_),
                    LAST_INVOICE_DATE,LAST_INVOICE_AMT,LAST_PAYMENT_DATE,LAST_PAYMENT_AMT,LAST_STATMNT_DATE,
                    LAST_STATMENT_BAL,LAST_STMT_TRAN_NO,TRIM(DISPLAY_COMMENTS),TRIM(DIRECTED_ACCOUNT_),
                    TRIM(SECOND_NAME2),TRIM(INTERNATIONAL_BILL_ADDRESS_2),TRIM(INTERNATIONAL_BILL_ZIP_CODE),
                    TRIM(INTERNATIONAL_BILL_PHONE_),TRIM(INTERNATIONAL_BILL_PHONE_EXT),TRIM(INTERNATIONAL_BILL_FAX_),
                    TRIM(INTERNATIONAL_PYMT_ADDRESS_2),TRIM(INTERNATIONAL_PYMT_ZIP_CODE),
                    TRIM(INTERNATIONAL_PYMT_PHONE_),TRIM(INTERNATIONAL_PYMT_PHONE_EXT),TRIM(INTERNATIONAL_PYMT_FAX_),
                    TRIM(BANK_ACCOUNT_NO_),TRIM(BANK_ROUTING_NO_),TRIM(WORK1),TRIM(EMAIL_TEXT_APPROVAL)
                  from rydedata.glpcust
                """
                cur.execute(sql)
                with open(self.extract_file, 'w') as f:
                    csv.writer(f).writerows(cur)
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute("truncate arkona.ext_glpcust")
                with open(self.extract_file, 'r') as io:
                    pg_cur.copy_expert(
                        """copy arkona.ext_glpcust from stdin with csv encoding 'latin-1 '""", io)


class ExtPyactgr(luigi.Task):
    """

    """
    extract_file = '../../extract_files/pyactgr.csv'
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.arkona_luigi_27(db2_server) as cn:
            with cn.cursor() as cur:
                sql = """
                select
                  TRIM(COMPANY_NUMBER),TRIM(DIST_CODE),SEQ_NUMBER,TRIM(DESCRIPTION),TRIM(DIST_CO_),GROSS_DIST,
                  TRIM(GROSS_EXPENSE_ACT_),TRIM(OVERTIME_ACT_),TRIM(VACATION_EXPENSE_ACT_),
                  TRIM(HOLIDAY_EXPENSE_ACT_),TRIM(SICK_LEAVE_EXPENSE_ACT_),TRIM(RETIRE_EXPENSE_ACT_),
                  TRIM(EMPLR_FICA_EXPENSE),TRIM(EMPLR_MED_EXPENSE),TRIM(FEDERAL_UN_EMP_ACT_),TRIM(STATE_UNEMP_ACT_),
                  TRIM(STATE_COMP_ACT_),TRIM(STATE_SDI_ACT_),TRIM(EMPLR_CONTRIBUTIONS)
                from rydedata.pyactgr
                """
                cur.execute(sql)
                with open(self.extract_file, 'w') as f:
                    csv.writer(f).writerows(cur)
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute("truncate arkona.ext_pyactgr")
                with open(self.extract_file, 'r') as io:
                    pg_cur.copy_expert("""copy arkona.ext_pyactgr from stdin with csv encoding 'latin-1 '""", io)


class ExtBopname(luigi.Task):
    """
    """
    extract_file = '../../extract_files/bopname.csv'
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.arkona_luigi_27(db2_server) as cn:
            with cn.cursor() as cur:
                sql = """
                  SELECT TRIM(BOPNAME_COMPANY_NUMBER),BOPNAME_RECORD_KEY,TRIM(COMPANY_INDIVID),SOC_SEC_FED_ID_,
                    TRIM(BOPNAME_SEARCH_NAME),TRIM(LAST_COMPANY_NAME),TRIM(FIRST_NAME),TRIM(MIDDLE_INIT),
                    TRIM(SALUTATION),TRIM(GENDER),TRIM(LANGUAGE),TRIM(ADDRESS_1),TRIM(ADDRESS_2),TRIM(CITY),
                    TRIM(COUNTY),TRIM(STATE_CODE),ZIP_CODE,PHONE_NUMBER,BUSINESS_PHONE,BUSINESS_EXT,
                    FAX_NUMBER,BIRTH_DATE,TRIM(DRIVERS_LICENSE),TRIM(CONTACT),TRIM(PREFERRED_CONTACT),
                    TRIM(MAIL_CODE),TRIM(TAX_EXMPT_NO_),TRIM(ASSIGNED_SLSP),TRIM(CUSTOMER_TYPE),
                    TRIM(PREFERRED_PHONE),CELL_PHONE,PAGE_PHONE,OTHER_PHONE,TRIM(OTHER_PHONE_DESC),
                    TRIM(EMAIL_ADDRESS),TRIM(OPTIONAL_FIELD),TRIM(ALLOW_CONTACT_BY_POSTAL),
                    TRIM(ALLOW_CONTACT_BY_PHONE),TRIM(ALLOW_CONTACT_BY_EMAIL),TRIM(ADDRESS_LINE_3),
                    TRIM(BUSINESS_PHONE_EXTENSION),TRIM(INTERNATIONAL_BUSINESS_PHONE),LAST_CHANGE_DATE,
                    TRIM(INTERNATIONAL_CELL_PHONE),EXTERNAL_CROSS_REFERENCE_KEY,TRIM(SECOND_EMAIL_ADDRESS2),
                    TRIM(INTERNATIONAL_FAX_NUMBER),TRIM(INTERNATIONAL_OTHER_PHONE),
                    TRIM(INTERNATIONAL_HOME_PHONE),TRIM(CUSTOMER_PREFERRED_NAME),
                    TRIM(INTERNATIONAL_PAGER_PHONE),TRIM(PREFERRED_LANGUAGE),TRIM(INTERNATIONAL_ZIP_CODE),
                    TRIM(TOKEN_ID),TRIM(COMMON_CUSTOMER_ID),TRIM(SOUNDEX_FIRST_NAME),TRIM(SOUNDEX_LAST_NAME),
                    TRIM(SOUNDEX_ADDRESS1),TRIM(OPTIONAL_FIELD_2),TRIM(TAX_EXMPT_NO_2),TRIM(TAX_EXMPT_NO_3),
                    TRIM(TAX_EXMPT_NO_4),TRIM(GST_REGISTRANT_),PST_EXMPT_DATE,DEALER_CODE,CC_ID,
                    CUSTOMER_VERSION,ADDRESS_NUMBER,ADDRESS_VERSION,CREATED_BY_USER,TIMESTAMP_CREATED,
                    UPDATED_BY_USER,TIMESTAMP_UPDATED,TRIM(COUTRY_CODE),TRIM(DL_STATE),
                    TRIM(OPENTRACK_PARTNER_ID),TRIM(STATUS),TRIM(SHARE_INFO)
                  from rydedata.bopname
                  where TRIM(BOPNAME_COMPANY_NUMBER) in ('RY1','RY2')
                """
                cur.execute(sql)
                with open(self.extract_file, 'w') as f:
                    csv.writer(f).writerows(cur)
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute("truncate arkona.ext_bopname")
                with open(self.extract_file, 'r') as io:
                    pg_cur.copy_expert(
                        """copy arkona.ext_bopname from stdin with csv encoding 'latin-1 '""", io)


class ExtOpenRoHeaders(luigi.Task):
    """
    this is something a little different, not a raw scrape of an arkona table, but a partial scrape of a
    table that contains necessary attributes to populate the table jeri.open_ros which is
    the foundation for a vision application analyzing and tracking open ros
    this class will be a requirement for the luigi module open_ros.
    """
    extract_file = '../../extract_files/open_ro_headers.csv'
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.arkona_luigi_27(db2_server) as cn:
            with cn.cursor() as cur:
                sql = """
                    select a.company_number, trim(a.document_number) as ro, a.ptpkey, trim(a.cust_name) as customer, 
                        trim(a.service_writer_id) as service_writer_id,
                        case a.ro_status
                          when 'L' then 'G/L Error'
                          when 'S' then 'Cashier - Waiting for Special Order Part'
                          when '1' then 'Open'
                          when '2' then 'In Process'
                          when '3' then 'Approved by Parts'
                          when '4' then 'Cashier'
                          when '5' then 'Cashier, Delayed Close'
                          when '6' then 'Pre-Invoice'
                          when '7' then 'Odom Required'
                          when '8' then 'Waiting for Parts'
                          when '9' then 'Parts Approval Reqd'
                          else a.ro_status
                        end as ro_status,
                        cast(left(trim(open_tran_date),4)||'-'||substr(trim(open_tran_date),5,2) ||'-'||
                            right(trim(open_tran_date),2) as date) as open_date,
                        days(curdate()) - days(cast(left(trim(open_tran_date),4)||'-'||substr(trim(open_tran_date),5,2)
                                        ||'-'||right(trim(open_tran_date),2) as date)) as days_open,
                        0 as days_in_cashier, curdate()
                    from RYDEDATA.PDPPHDR a
                    where a.document_type = 'RO'
                        and a.document_number <> ''
                        and a.company_number in ('RY1','RY2')
                        and a.customer_key <> 0
                """
                cur.execute(sql)
                with open(self.extract_file, 'w') as f:
                    csv.writer(f).writerows(cur)
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute("truncate jeri.open_ro_headers")
                with open(self.extract_file, 'r') as io:
                    pg_cur.copy_expert(
                        """copy jeri.open_ro_headers from stdin with csv encoding 'latin-1 '""", io)


class ExtOpenRoDetails(luigi.Task):
    """
    this is something a little different, not a raw scrape of an arkona table, but a partial scrape of a
    table that contains necessary attributes to populate the table jeri.open_ros which is
    the foundation for a vision application analyzing and tracking open ros
    this class will be a requirement for the luigi module open_ros.
    """
    extract_file = '../../extract_files/open_ro_details.csv'
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.arkona_luigi_27(db2_server) as cn:
            with cn.cursor() as cur:
                sql = """
                    with 
                        headers as (
                            select ptpkey
                            from RYDEDATA.PDPPHDR a
                            where a.document_type = 'RO'
                                and a.document_number <> ''
                                and a.company_number in ('RY1','RY2')
                                and a.customer_key <> 0)
                    select ro, ptpkey, service_type, payment_type, sum(flag_hours) as flag_hours, 
                      sum(labor_sales) as labor_sales, sum(labor_cost) as labor_cost, 
                      sum(labor_gross) as labor_gross, sum(parts_gross) as parts_gross, curdate()
                    from ( -- and take the line out of the grouping
                        select trim(dd.document_number) as ro, aa.*, coalesce(bb.flag_hours,0) as flag_hours, 
                          coalesce(bb.labor_sales, 0) as labor_sales,
                          coalesce(bb.labor_cost, 0) as labor_cost, coalesce(bb.labor_gross, 0) as labor_gross,
                          coalesce(cc.parts_gross, 0) as parts_gross
                        from ( -- service type, payment type
                            select a.ptpkey, a.ptline, a.ptsvctyp as service_type, a.ptlpym as payment_type
                            from rydedata.pdppdet a
                            inner join headers b on a.ptpkey = b.ptpkey
                            where a.ptline < 900
                              and a.ptltyp = 'A'
                              and a.ptsvctyp <> ''
                              and a.ptlpym <> ''
                            group by a.ptpkey, a.ptline, a.ptsvctyp, a.ptlpym) aa
                        left join ( -- labor
                            select a.ptpkey, a.ptline, sum(coalesce(a.ptlhrs, 0)) as flag_hours, 
                              sum(coalesce(a.ptnet, 0)) as labor_sales, 
                              sum(coalesce(a.ptcost, 0)) as labor_cost, 
                              sum(coalesce(a.ptnet, 0) - coalesce(a.ptcost, 0)) as labor_gross
                            from rydedata.pdppdet a
                            inner join headers b on a.ptpkey = b.ptpkey
                            where a.ptline < 900
                              and a.ptcode = 'TT'
                            group by a.ptpkey, a.ptline) bb on aa.ptpkey = bb.ptpkey and aa.ptline = bb.ptline
                        left join (
                            select ptpkey, ptline, sum(parts_gross) as parts_gross
                            from ( -- parts
                                select a.ptpkey, a.ptline, a.ptqty * (a.ptnet - a.ptcost) as parts_gross
                                from rydedata.pdppdet a
                                inner join headers b on a.ptpkey = b.ptpkey
                                where a.ptline < 900
                                  and a.ptltyp = 'P'
                                  and a.ptcode in ('CP','SC','WS','IS')) c
                            group by ptpkey, ptline) cc on aa.ptpkey = cc.ptpkey and aa.ptline = cc.ptline
                        left join rydedata.pdpphdr dd on aa.ptpkey = dd.ptpkey) ee
                    group by ro, ptpkey, service_type, payment_type, curdate()
                """
                cur.execute(sql)
                with open(self.extract_file, 'w') as f:
                    csv.writer(f).writerows(cur)
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute("truncate jeri.open_ro_details")
                with open(self.extract_file, 'r') as io:
                    pg_cur.copy_expert(
                        """copy jeri.open_ro_details from stdin with csv encoding 'latin-1 '""", io)


class ExtBopmast(luigi.Task):
    """
    """
    extract_file = '../../extract_files/bopmast.csv'
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.arkona_luigi_27(db2_server) as cn:
            with cn.cursor() as cur:
                sql = """
                SELECT TRIM(BOPMAST_COMPANY_NUMBER),RECORD_KEY,TRIM(RECORD_STATUS),TRIM(RECORD_TYPE),
                  TRIM(VEHICLE_TYPE),TRIM(FRANCHISE_CODE),TRIM(SALE_TYPE),TRIM(BOPMAST_STOCK_NUMBER),
                  TRIM(BOPMAST_VIN),BUYER_NUMBER,CO_BUYER_NUMBER,TRIM(BOPMAST_SEARCH_NAME),RETAIL_PRICE,
                  DOWN_PAYMENT,TRADE_ALLOWANCE,TRADE_ACV,TRADE_PAYOFF,APR,TERM,INSURANCE_TERM,DAYS_TO_1ST_PAYMT,
                  TRIM(PAYMENT_FREQUENCY),PAYMENT,AMOUNT_FINANCED,ORIGINATION_DATE,DATE_1ST_PAYMENT,
                  DATE_LAST_PAYMENT,DATE_STICKER_EXP,TAX_GROUP,LENDING_SOURCE,LEASING_SOURCE,INSURANCE_SOURCE,
                  GAP_SOURCE,SERV_CONT_COMPANY,SERV_CONT_AMOUNT,SERV_CONT_DEDUCT,SERV_CONT_MILES,SERV_CONT_MONTHS,
                  SERV_CONT_COST,SERV_CONT_STR_DAT,SERV_CONT_EXP_DAT,TRIM(ADD_SERV_TO_CAP_C),CREDIT_LIFE_PREMIUM,
                  LEVEL_CL_PREMIUM,A_H_PREMIUM,GAP_PREMIUM,GAP_COST,TRIM(CREDIT_LIFE_CODE),TRIM(A_H_CODE),
                  TRIM(GAP_CODE),TAX_1,TAX_2,TAX_3,TAX_4,TAX_5,BUY_RATE,RESERVE_HOLDBACK_,NUMBER_OF_TRADES,
                  TRIM(TEMPORARYSTICKER_),TRIM(SERV_CONT_NUMBER),TRIM(CRED_LIFE_CERT_),TRIM(A_H_CERT_),
                  TRIM(GAP_CERT_),TRIM(FLOORING_VENDOR),TRIM(PD_INSURANCE_CO),TRIM(PD_INSUR_CO_ALIAS),
                  TRIM(PD_POLICY_NUMBER),PD_POLICY_EXP_DAT,TRIM(PD_AGENT_NAME),TRIM(PD_AGENT_ADDRESS),
                  TRIM(PD_AGENT_CITY),TRIM(PD_AGENT_STATE),PD_AGENT_ZIP,PD_AGENT_PHONE_NO,TRIM(PD_COLL_COVERAGE),
                  TRIM(PD_COMP_COVERAGE),TRIM(PD_FIRE_THEFT_COV),PD_COLL_DEDUCT,PD_COMP_DEDUCT,PD_FIRE_THEFT_DED,
                  TRIM(PD_VEHICLE_USE),PD_PREMIUM,AMO_TOTAL,FEE_TOTAL,TOTAL_OF_PAYMENTS,INTEREST_CHARGED,
                  VEHICLE_COST,INTERNAL_COST,ADDS_TO_COST_TOT,HOLD_BACK,PACK_AMOUNT,COMM_GROSS,INCENTIVE,
                  REBATE,ODOMETER_AT_SALE,PICKUP_AMOUNT,PICKUP_BEG_DATE,TRIM(PICKUP_FREQ),PICKUP_NO_PAY,
                  LIFE_RESERVE,A_H_RESERVE,GAP_RESERVE,PDI_RESERVE,AMO_RESERVE,SERV_RESERVE,FINANCE_RESERVE,
                  TOTAL_F_I_RESERVE,TRIM(PRIMARY_SALESPERS),TRIM(SECONDARY_SLSPERS2),DEF_DOWN_PAYMENT,
                  DEF_DOWN_DUE_DATE,LOAN_ORIG_FEE,TRIM(LOF_OVERRIDE),EFFECTIVE_APR,MSRP,LEASE_PRICE,
                  CAPITALIZED_COST,CAP_COST_REDUCT,APR_BMLAPR,LEASE_FACTOR,LEASE_TERM,RESIDUAL_,RESIDUAL_AMT,
                  NET_RESIDUAL,TOT_DEPRECIATION,MILES_PER_YEAR,MILES_YEAR_ALLOW,EXCESS_MILE_RATE,EXCESS_MILE_RATE2,
                  EXCESS_MILE_CHG,ACQUISITION_FEE,LEASE_PAYMENT,LEASE_PAYMT_TAX,LEASE_SALES_TAX,MO_LEASE_CHARGE,
                  CAP_REDUCTION_TAX,CAP_REDUCT_TAX_1,CAP_REDUCT_TAX_2,CAP_REDUCT_TAX_3,CAP_REDUCT_TAX_4,
                  CAP_REDUCT_TAX_6,SECURITY_DEPOSIT,DEPOSIT_RECEIVED,CASH_RECEIVED,FACTOR_BUY_RATE,
                  TOTAL_INITIAL_CHG,TRIM(CAP_LEASE_SLS_TAX),TRIM(SECUR_DEPOS_ORIDE),TRIM(ACQUI_FEE_ORIDE),
                  TRIM(FET_OPTIONS),TRIM(CAPITALIZE_SLS_TX),TRIM(ADV_PAYMENT_LEASE),ORIDE_TAX_RATE_1,
                  ORIDE_TAX_RATE_2,ORIDE_TAX_RATE_3,ORIDE_TAX_RATE_4,ORIDE_TAX_RATE_6,TRIM(ORIDE_EXC_TRADE_1),
                  TRIM(ORIDE_EXC_TRADE_2),TRIM(ORIDE_EXC_TRADE_3),TRIM(ORIDE_EXC_TRADE_4),TRIM(TAX_OVERRIDE_1),
                  TRIM(TAX_OVERRIDE_2),TRIM(TAX_OVERRIDE_3),TRIM(TAX_OVERRIDE_4),TRIM(TAX_OVERRIDE_6),
                  TRIM(FIN_RESERV_ORIDE),TRIM(COST_OVERRIDE),TRIM(HOLDBACK_OVERRIDE),TRIM(TAX_OVERRIDE),
                  TRIM(REBATE_DOWN_PAY),TRIM(CONTRACT_SELECTED),TRIM(CHG_INT_ON_1_PYMT),TRIM(SALE_ACCOUNT),
                  TRIM(UNWIND_FLAG),TRIM(DOCUMENT_NUMBER),BUYER_AGE,CO_BUYER_AGE,LIFE_COVERAGE,A_H_COVERAGE,
                  INSUR_TERM_DATE,LEASE_TAX_GROUP,CO_BUYER_LIFE_PREM,DATE_SAVED,DATE_TRADE_PAYOFF,
                  TRIM(CAPITALIZE_CR_TAX),REBATE_AS_DOWN,TOTAL_DOWN,TOT_CAP_REDUCTION,GROSS_CAP_COST,
                  DEALER_INCENTIVE,TRIM(FORMS_PRINTED),TRIM(NEW_VEH_DELIV_RPT),DATE_APPROVED,DATE_CAPPED,
                  EXCESS_MILEAGE_CHARGE_BALL,PENALTY_EXCESS_MILEAGE_CHARG,PREPAID_EXCESS_MILEAGE_CHARG,
                  MILES_PER_YEAR_BALLOON,MILES_PER_YEAR_ALLOWED_BAL, NET_RESIDUAL_BALLOON,RESIDUAL_AMOUNT_BALLOON,
                  RESIDUAL_PERCENT_BALLOON,AFTER_MARKET_TAX,EXEMPT_PAYMENTS,FULL_PAYMENTS,TRIM(FORD_PLAN_CODE),
                  A_H_TERM,LEASE_PAYMENT_TAX,TRIM(TRADE_FROM_LEASE),ORIG_NET_TRADE,NTAX_TRADE_EQUITY,
                  TRIM(PD_POLICY_NUMBER_B9PDP#),PART_EXEMPT_PYMT,PART_EXEMPT_TAX,SERVICE_CONT_TAX,
                  TOTAL_A_H_COVERAG,TAX_BASE_AMOUNT,WORK_IN_PROCESS,DATE_LAST_LEASE_PAYMENT,
                  AFTERMARKET_TAX_RATE5,SERVICE_CONTRACT_TAX_RATE7,ODD_LAST_PAYMENT,ORIGINAL_CONTRACT_DATE,
                  TRIM(LEVEL_LIFE_OPTIONAL),REG_FEE_TAX_1,REG_FEE_TAX_2,PYMT_EXCLUDING_INS,PAYBACK_RATE,
                  TRIM(BUYER_RED_RT_IND),TRIM(CO_BUYER_RED_RT_IND),TRIM(BUYER_GENDER),TRIM(CO_BUYER_GENDER),
                  TRIM(CRIT_ILL_CODE),CRIT_ILL_PREMIUM,TRIM(LOSS_OF_EMPLOYMENT_CODE),LOSS_OF_EMPLOYMENT_PREMIUM,
                  UPFRONT_FEE_TAX_1,UPFRONT_FEE_TAX_2,TRIM(RESIDUAL_INSURANCE),TRIM(DISABILITY_ELIG),
                  TRIM(REM_INS_CODE),PREMIUM_PERCENT,GAP_TAX_RATE8,DELIVERY_DATE,INITIAL_MILES,INITIAL_MILE_RATE,
                  INITIAL_MILE_CHG,LEASE_PMTS_REMAINING,F_I_GROSS,HOUSE_GROSS,MONTHLY_GAP_PREMIUM,
                  TRIM(INITIAL_MILES_CHG_FLAG),INITIAL_MILES_THRESHOLD,GAP_TAX,PRO_RATA_AMOUNT,PRO_RATA_TAX_1,
                  PRO_RATA_TAX_2,TRIM(OPENTRACK_VENDOR),TRIM(F_I_MANAGER),TRIM(SALES_MANAGER),TRADE_UPFRONT,
                  TRADE_CAPITALIZED,REBATE_UPFRONT,REBATE_CAPITALIZED,CASH_UPFRONT,CASH_CAPITALIZED,
                  SIGN_AND_DRIVE,TRIM(ROLL_PRICE_FLAG),SAVED_PRICE,CRITICAL_ILLNESS_TERM,LOSS_OF_EMPLOYMENT_TERM,
                  CREDIT_LIFE_SOURCE,A_H_SOURCE,CRITICAL_ILLNESS_SOURCE,LOSS_OF_EMPLOYMENT_SOURCE,
                  A_H_COVERAGE_BMAHCOV,LEVEL_CI_PREMIUM,TRIM(CRITICAL_ILLNES_CERT_),
                  TRIM(LOSS_OF_EMPLOYMENT_CERT_),CRITICAL_ILLNESS_RESERVE,LOSS_OF_EMPLOYMENT_RESERVE,
                  VW_EFFECTIVE_APR,UNREALIZED_REBATE,A_H_INSURANCE_TERM_DATE,CI_INSURANCE_TERM_DATE,
                  LE_INSURANCE_TERM_DATE,TRIM(FIRST_PAYMENT_WAIVER),
                  TRADE_TAX_CREDIT,ESTIMATED_FEES_AND_TAXES,TRIM(MAPPED_TAX_ZIP_CODE),MAPPED_TAX_CITY,
                  MAPPED_TAX_LOCATION_ID
                from rydedata.bopmast
                where TRIM(BOPMAST_COMPANY_NUMBER) <> 'RY5'
                """
                cur.execute(sql)
                with open(self.extract_file, 'w') as f:
                    csv.writer(f).writerows(cur)
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute("truncate arkona.ext_bopmast")
                with open(self.extract_file, 'r') as io:
                    pg_cur.copy_expert(
                        """copy arkona.ext_bopmast from stdin with csv encoding 'latin-1 '""", io)


class TestExtInpmast(luigi.Task):
    """
    tests arkona.ext_inpmast for vins that exist in arkona.xfm_inpmast that no longer exist in
    arkona.ext_inpmast
    see python projects/ext_arkona/sql/inpmast_vins_changed.sql
    no dependencies on this, don't want anything to fail, but want to be notified when a vin "disappears"
    Unlike the other tasks in this file which are all required by something else, this will have to be
    called directly from run_all.py
    If this pattern expands, may break the "testing" classes out into a separate module
    4/6/19
    in an attempt to clean up after themselves, someone deleted 2HKRM4H73EH722710 from inpmast
    but they also deleted valid history of stocknumber H9675A, so i exempted it
    07/11/20
        someone deleted vin 5FNYF6H59HB103175 from inpmast, looks suspicious
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                # test for disappeared vins
                sql = """
                    select count(inpmast_vin)
                    from arkona.xfm_inpmast a
                    where inpmast_vin not in ('2HKRM4H73EH722710','5FNYF6H59HB103175')
                      and not exists (
                        select *
                        from arkona.ext_inpmast
                        where inpmast_vin = a.inpmast_vin)              
                """
                pg_cur.execute(sql)
                if pg_cur.fetchone()[0] != 0:
                    raise ValueError('arkona.xfm_inpmast includes vin(s) no longer in inpmast')


class ExtPdpmrpl(luigi.Task):
    """

    """
    extract_file = '../../extract_files/pdpmrpl.csv'
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.arkona_luigi_27(db2_server) as cn:
            with cn.cursor() as cur:
                sql = """
                    select
                      TRIM(COMPANY_NUMBER),TRIM(MANUFACTURER),TRIM(OLD_PART_NUMBER),
                      TRIM(REPLACE_TYPE),TRIM(ACTIVE_PART_NO_)
                    from rydedata.pdpmrpl
                    where company_number in ('RY1','RY2')
                 """
                cur.execute(sql)
                with open(self.extract_file, 'w') as f:
                    csv.writer(f).writerows(cur)
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute("truncate arkona.ext_pdpmrpl")
                with open(self.extract_file, 'r') as io:
                    pg_cur.copy_expert(
                        """copy arkona.ext_pdpmrpl from stdin with csv encoding 'latin-1 '""", io)


class ExtPdppmex(luigi.Task):
    """

    """
    extract_file = '../../extract_files/pdppmex.csv'
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.arkona_luigi_27(db2_server) as cn:
            with cn.cursor() as cur:
                sql = """
                    select
                      TRIM(COMPANY_NUMBER),TRIM(MANUFACTURER),TRIM(PART_NUMBER),TRIM(STOCK_CODE),STOCK_LEVEL,
                      MFR_REORDER_LEVEL,MFR_REORDER_MAX,TRIM(MFR_AUTO_REPLEN),TRIM(MFR_ASS_CLASS),
                      TRIM(MFR_ASS_CLASS_PMASSSAP),MFR_FORECAST,TRIM(EXCLUDE_REASON)
                    from rydedata.pdppmex
                    where company_number in ('RY1','RY2')
                 """
                cur.execute(sql)
                with open(self.extract_file, 'w') as f:
                    csv.writer(f).writerows(cur)
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute("truncate arkona.ext_pdppmex")
                with open(self.extract_file, 'r') as io:
                    pg_cur.copy_expert(
                        """copy arkona.ext_pdppmex from stdin with csv encoding 'latin-1 '""", io)


class ExtPdpsgrp(luigi.Task):
    """

    """
    extract_file = '../../extract_files/pdpsgrp.csv'
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.arkona_luigi_27(db2_server) as cn:
            with cn.cursor() as cur:
                sql = """
                    select
                      TRIM(COMPANY_NUMBER),TRIM(STOCK_GROUP),TRIM(DESCRIPTION),TRIM(MANUFACTURER),TRADE_DEFAULT_PCT,
                      LIST_DEFAULT_PCT,TRIM(ADD_PI_TO_STK_ORD),REG_ORDER_FREQ,LEAD_TIME,RESERVE_STOCK,STOCK_LEVEL_ADJ_,
                      OF_RECENT_DEMND,RECENT_DAYS,OF_PREV_DEMAND,PREVIOUS_DAYS,TRIM(PREV_DAYS_TYPE),FORCST_NOTIFY_PCT,
                      TRIM(ORD_ZERO_ON_HAND),TRIM(STOCK_ORDER_BASIS),DAYS_BEFORE_EXCES,DAYS_BEFORE_UNDER,DAYS_IN_PERIOD,
                      PHASE_IN_DEMAND,PI_NO_OF_PERIODS,PI_NUMBER_OF_DAYS,PO_NO_OF_PERIODS,PO_NUMBER_OF_DAYS,
                      PO_HIGH_MOD_YEAR,EOQ_COST_01,EOQ_COST_02,EOQ_COST_03,EOQ_COST_04,EOQ_COST_05,EOQ_COST_06,
                      EOQ_COST_07,EOQ_COST_08,EOQ_COST_09,EOQ_COST_10,EOQ_COST_11,EOQ_COST_12,EOQ_COST_13,EOQ_COST_14,
                      EOQ_COST_15,EOQ_COST_16,EOQ_COST_17,EOQ_COST_18,EOQ_COST_19,EOQ_COST_20,EOQ_COST_21,EOQ_COST_22,
                      EOQ_COST_23,EOQ_COST_24,EOQ_COST_25,EOQ_COST_26,EOQ_ADJ_DAYS_01,EOQ_ADJ_DAYS_02,EOQ_ADJ_DAYS_03,
                      EOQ_ADJ_DAYS_04,EOQ_ADJ_DAYS_05,EOQ_ADJ_DAYS_06,EOQ_ADJ_DAYS_07,EOQ_ADJ_DAYS_08,EOQ_ADJ_DAYS_09,
                      EOQ_ADJ_DAYS_10,EOQ_ADJ_DAYS_11,EOQ_ADJ_DAYS_12,EOQ_ADJ_DAYS_13,EOQ_ADJ_DAYS_14,EOQ_ADJ_DAYS_15,
                      EOQ_ADJ_DAYS_16,EOQ_ADJ_DAYS_17,EOQ_ADJ_DAYS_18,EOQ_ADJ_DAYS_19,EOQ_ADJ_DAYS_20,EOQ_ADJ_DAYS_21,
                      EOQ_ADJ_DAYS_22,EOQ_ADJ_DAYS_23,EOQ_ADJ_DAYS_24,EOQ_ADJ_DAYS_25,EOQ_ADJ_DAYS_26,TRIM(INT_SALE_ACCT),
                      TRIM(CTR_SALE_TAXABLE),TRIM(CTR_SALE_NON_TAX),TRIM(WHOLESALE_TAXABLE),TRIM(WHOLESALE_NON_TAX),
                      TRIM(SERVICE_SALE_ACCT),TRIM(BODY_SHOP_SALE),TRIM(FACT_CLAIM_SALE),TRIM(INT_SALE_ACCT_PGISAD),
                      TRIM(CTR_SALE_TAXABLE_PGCSTD),TRIM(CTR_SALE_NON_TAX_PGCSND),TRIM(WHOLESALE_TAXABLE_PGWHTD),
                      TRIM(WHOLESALE_NON_TAX_PGWHND),TRIM(SERVICE_SALE_ACCT_PGSVSD),TRIM(BODY_SHOP_SALE_PGBSSD),
                      TRIM(FACT_CLAIM_SALE_PGFCSD),DAYS_SUPPLY_BASE,DLY_DMND_TO_01,DLY_DMND_TO_02,DLY_DMND_TO_03,
                      DLY_DMND_TO_04,DLY_DMND_TO_05,DLY_DMND_TO_06,DLY_DMND_TO_07,DLY_DMND_TO_08,DLY_DMND_TO_09,
                      DLY_DMND_TO_10,DAYS_SUPPLY_01,DAYS_SUPPLY_02,DAYS_SUPPLY_03,DAYS_SUPPLY_04,DAYS_SUPPLY_05,
                      DAYS_SUPPLY_06,DAYS_SUPPLY_07,DAYS_SUPPLY_08,DAYS_SUPPLY_09,DAYS_SUPPLY_10,REORDER_POINT_01,
                      REORDER_POINT_02,REORDER_POINT_03,REORDER_POINT_04,REORDER_POINT_05,REORDER_POINT_06,
                      REORDER_POINT_07,REORDER_POINT_08,REORDER_POINT_09,REORDER_POINT_10,FACTORY_COST_RATE
                    from rydedata.pdpsgrp
                    where company_number in ('RY1','RY2')
                      and description <> 'Default'
                 """
                cur.execute(sql)
                with open(self.extract_file, 'w') as f:
                    csv.writer(f).writerows(cur)
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute("truncate arkona.ext_pdpsgrp")
                with open(self.extract_file, 'r') as io:
                    pg_cur.copy_expert(
                        """copy arkona.ext_pdpsgrp from stdin with csv encoding 'latin-1 '""", io)


class ExtPdpmast(luigi.Task):
    """

    """
    extract_file = '../../extract_files/pdpmast.csv'
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.arkona_luigi_27(db2_server) as cn:
            with cn.cursor() as cur:
                sql = """
                    SELECT TRIM(COMPANY_NUMBER),TRIM(MANUFACTURER),TRIM(PART_NUMBER),TRIM(PART_DESCRIPTION),
                        TRIM(STOCKING_GROUP),TRIM(STATUS),TRIM(OBSOLETE),TRIM(RETURN_CODE),TRIM(ARRIVAL_CODE),
                        TRIM(STOCK_PROMO_CODE),TRIM(SPEC_ORDER_CODE),TRIM(PRICE_UPDATE),TRIM(KIT_PART),
                        TRIM(COMPONENT_PART),TRIM(ASSOC_WITH_PART),TRIM(ALTERNATE_PART),TRIM(IS_CORE_PART),
                        TRIM(MERCH_CODE),TRIM(PRICE_SYMBOL),TRIM(PROD_CODE_CLASS),TRIM(FLEET_ALLOWANCE),
                        TRIM(GROUP_CODE),TRIM(BIN_LOCATION),TRIM(SHELF_LOCATIION),QTY_ON_HAND,QTY_RESERVED,
                        QTY_ON_ORDER,QTY_ON_BACK_ORDER,QTY_ON_SPEC_ORDER,PLACE_ON_ORDER,PLACE_ON_SPEC_ORD,
                        PACK_QTY,MIN_SALES_QTY,COST,LIST_PRICE,TRADE_PRICE,WHLSE_COMP,WHLSE_COMP_FLEET,
                        FLAT_PRICE,MIN_MAX_PACK_ADJ,DYNAM_DAYS_SUPPLY,MINIMUM_ON_HAND,MAXIMUM_ON_HAND,
                        LOW_MODEL_YEAR,HIGH_MODEL_YEAR,DATE_IN_INVENTORY,DATE_PHASED_OUT,DATE_LAST_SOLD,
                        INVENTORY_TURNS,BEST_STOCK_LEVEL,STOCK_STATUS,RECENT_DEMAND,PRIOR_DEMAND,
                        RECENT_WORK_DAYS,PRIOR_WORK_DAYS,WEIGHTED_DLY_AVG,SALES_DEMAND,LOST_SALES_DEMAND,
                        SPEC_ORDER_DEMAND,NON_STOCK_DEMAND,RETURN_DEMAND,INVENTORY_VALUE,TOT_QTY_SOLD,
                        TOT_COST_SALES,STOCK_PURCHASES,TRIM(DISPLAY_PART_NO_),TRIM(SORT_PART_NUMBER),
                        TRIM(OLD_PART_NUMBER),TRIM(NEW_PART_NUMBER),TRIM(CORE_PART_NUMBER),TRIM(REMARKS),
                        TRIM(DISPLAY_REMARKS),BULK_ITEM_ACTUAL_WEIGHT,TRIM(SECONDARY_BIN_LOCATION2),
                        TRIM(CLASS_CODE),PART_DEALER_TO_DEALER_PRICE,PART_SUBSIDIARY_PRICE,
                        TRIM(PART_DEALER_TO_DEALER_CODE),EMERGENCY_REPAIR_PACKAGE_QUA,
                        TRIM(PART_LSG_CODE),TRIM(BRAND_CODE),TRIM(PART_COMMOND_CODE),
                        TRIM(PART_CROSS_SHIP_CODE),TRIM(PART_DISCOUNT_CODE),TRIM(PART_DIRECT_SHIP_CODE),
                        TRIM(PART_HAZARD_CODE),TRIM(PART_INTERCHANGE_SUBSTITUTIO),
                        TRIM(PART_LARGE_QUANTITY_CODE),PART_MAXIMUM_ORDER_QUANTITY,
                        PART_MINIMUM_ORDER_QUANTITY,TRIM(PART_OBSOLETE_STATUS),
                        TRIM(PROGRAM_PART_ALLOWANCE_CODE),PART_PRICING_DISCOUNT_PERCEN,
                        TRIM(PROGRAM_PART_RETURNABLE_CODE),TRIM(PART_SIZE_CODE),TRIM(PART_TYPE),
                        TRIM(PART_VOLUME_DISCOUNT_FLAG),TRIM(SECONDARY_SHELF2),TRIM(WEIGHT_CODE),
                        UNIT_OF_MEASURE,LAST_RECEIPT_DATE,FIRST_RECEIPT_DATE,TRIM(PART_ZONE),
                        SPIFF_BONUS_AMOUNT,TRIM(ALIAS),HAZ_MAT_DISPOSAL_FEE,TRIM(MANUAL_ORDER_PART),
                        LAST_CHANGE_TIMESTAMP,TRIM(LAST_UPDATE_USER),PART_WEIGHT,SHIPPING_HEIGHT,
                        SHIPPING_LENGTH,SHIPPING_DEPTH,TRIM(TURNOVER_CODE)
                    from rydedata.pdpmast
                    where company_number in ('RY1','RY2')
                      and left(part_number, 1) <> ' '
                 """
                cur.execute(sql)
                with open(self.extract_file, 'w') as f:
                    csv.writer(f).writerows(cur)
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute("truncate arkona.ext_pdpmast")
                with open(self.extract_file, 'r') as io:
                    pg_cur.copy_expert(
                        """copy arkona.ext_pdpmast from stdin with csv encoding 'latin-1 '""", io)


class ExtPdptdet(luigi.Task):
    """

    """
    extract_file = '../../extract_files/pdptdet.csv'
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        return luigi.LocalTarget(self.local_target())

    def run(self):
        # create db2 friendly integer format dates, eg; 20190607
        start_date = datetime.datetime.now() - timedelta(days=60)
        start_date = 100 * (100 * start_date.year + start_date.month) + start_date.day
        end_date = datetime.datetime.now()
        end_date = 100 * (100 * end_date.year + end_date.month) + end_date.day
        with utilities.arkona_luigi_27(db2_server) as cn:
            with cn.cursor() as cur:
                sql = """
                    select TRIM(PTCO#),TRIM(PTINV#),PTLINE,PTSEQ#,TRIM(PTTGRP),
                      TRIM(PTCODE),TRIM(PTSOEP),PTDATE,PTCDATE,TRIM(PTCPID),
                      TRIM(PTMANF),TRIM(PTPART),TRIM(PTSGRP),PTQTY,PTCOST,
                      PTLIST,PTNET,PTEPCDIFF,TRIM(PTSPCD),TRIM(PTORSO),
                      TRIM(PTPOVR),TRIM(PTGPRC),TRIM(PTXCLD),TRIM(PTFPRT),TRIM(PTRTRN),
                      PTOHAT,TRIM(PTVATCODE),PTVATAMT,PTTIME,PTQOHCHG,
                      PTLASTCHG,TRIM(PTUPDUSER),PTIDENTITY,PTKTXAMT,TRIM(PTLNTAXE),
                      TRIM(PTDUPDATE),TRIM(PTRIM)
                    FROM  rydedata.pdptdet a
                    where ptdate between {0} and {1}
                """.format(start_date, end_date)
                cur.execute(sql)
                with open(self.extract_file, 'w') as f:
                    csv.writer(f).writerows(cur)
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = """
                    delete
                    from arkona.ext_pdptdet
                    where ptdate between {0} and {1}
                """.format(start_date, end_date)
                pg_cur.execute(sql)
                with open(self.extract_file, 'r') as io:
                    pg_cur.copy_expert(
                        """copy arkona.ext_pdptdet from stdin with csv encoding 'latin-1 '""", io)


class ExtSdpxtim(luigi.Task):
    """
    required for honda main shop flat rate flag hour adjustments
    """
    extract_file = '../../extract_files/sdpxtim.csv'
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.arkona_luigi_27(db2_server) as cn:
            with cn.cursor() as cur:
                sql = """
                    select
                      TRIM(COMPANY_NUMBER),PENDING_KEY,TRIM(RO_NUMBER),SERVICE_LINE_NO,TRANS_DATE,COST,
                      TRIM(LINE_PAYMT_METHOD),TRIM(TECHNICIAN_ID),LABOR_HOURS,TABLE_SEQUENCE
                    from rydedata.sdpxtim
                 """
                cur.execute(sql)
                with open(self.extract_file, 'w') as f:
                    csv.writer(f).writerows(cur)
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute("truncate arkona.ext_sdpxtim")
                with open(self.extract_file, 'r') as io:
                    pg_cur.copy_expert(
                        """copy arkona.ext_sdpxtim from stdin with csv encoding 'latin-1 '""", io)


class ExtInpcmnt(luigi.Task):
    """
    required to estimate date ctp units are returned to inventory
    """
    extract_file = '../../extract_files/inpcmnt.csv'
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.arkona_luigi_27(db2_server) as cn:
            with cn.cursor() as cur:
                sql = """
                    select
                      TRIM(COMPANY_NUMBER),TRIM(VIN),TRANSACTION_DATE,TRANSACTION_TIME,SEQUENCE_NUMBER,TRIM(COMMENT)
                    from rydedata.inpcmnt
                 """
                cur.execute(sql)
                with open(self.extract_file, 'w') as f:
                    csv.writer(f).writerows(cur)
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute("truncate arkona.ext_inpcmnt")
                with open(self.extract_file, 'r') as io:
                    pg_cur.copy_expert(
                        """copy arkona.ext_inpcmnt from stdin with csv encoding 'latin-1 '""", io)


class ExtPypcodes(luigi.Task):
    """
    required to estimate date ctp units are returned to inventory
    """
    extract_file = '../../extract_files/pypcodes.csv'
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.arkona_luigi_27(db2_server) as cn:
            with cn.cursor() as cur:
                sql = """
                    select
                      TRIM(COMPANY_NUMBER),TRIM(CODE_TYPE),TRIM(DED_PAY_CODE),TRIM(DESCRIPTION),TRIM(ACCOUNT_NUMBER),
                      TRIM(EXEMPT_1_FED_TAX_),TRIM(EXEMPT_2_FICA_),TRIM(EXEMPT_3_FUTA_),TRIM(EXEMPT_4_STATE_TAX_),
                      TRIM(EXEMPT_5_CITY_TAX_),TRIM(EXEMPT_6_MEDC_TAX_),TRIM(EXEMPT_7_SUTA_),
                      TRIM(EXEMPT_8_WORKMEN_COMP8),
                      TRIM(EXEMPT_9_COUNTY_TAX_),TRIM(EXEMPT_10_SDI_TAX_),TRIM(EXEMPT_11_NOT_ASSIGNED_),
                      TRIM(EXEMPT_FIXED_DED),TRIM(PAY_AMT_HOURS),TRIM(FRINGE_BENEFIT),TRIM(EXEMPT_FROM_GROSS),
                      TRIM(INCENTIVE_PAY),TRIM(DED_CONTROL_NUMBER),TRIM(PRINT_EMPLOYEE_NAME),TRIM(W2_CODE2C),
                      TRIM(W2_BOX2B),TRIM(DISIBILITY_SWITCHES),DEDUCTION_PRIORITY_SEQ,TRIM(RD_PARTY_PAY3RD),
                      TRIM(EXEMPT_PAY),TRIM(PAY_CODE_IN_ATTENDANCE),TRIM(DEDUCTION_CODE_LINK),TRIM(GARNISHMENT_DED),
                      TRIM(DEFERRED_COMP_DED),TRIM(K_LIKE_DED401),TRIM(OVERTIME_PAY)
                    from rydedata.pypcodes
                 """
                cur.execute(sql)
                with open(self.extract_file, 'w') as f:
                    csv.writer(f).writerows(cur)
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute("truncate arkona.ext_pypcodes")
                with open(self.extract_file, 'r') as io:
                    pg_cur.copy_expert(
                        """copy arkona.ext_pypcodes from stdin with csv encoding 'latin-1 '""", io)


class ExtBopvref(luigi.Task):
    """
    required to estimate date ctp units are returned to inventory
    """
    extract_file = '../../extract_files/bopvref.csv'
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.arkona_luigi_27(db2_server) as cn:
            with cn.cursor() as cur:
                sql = """
                    SELECT TRIM(COMPANY_NUMBER),CUSTOMER_KEY,TRIM(VIN),START_DATE,END_DATE,TRIM(TYPE_CODE),
                        TRIM(SALESPERSON),TRIM(DEALER_NUMBER),TRIM(LIEN_HOLDER)
                    from rydedata.bopvref
                 """
                cur.execute(sql)
                with open(self.extract_file, 'w') as f:
                    csv.writer(f).writerows(cur)
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute("truncate arkona.ext_bopvref")
                with open(self.extract_file, 'r') as io:
                    pg_cur.copy_expert(
                        """copy arkona.ext_bopvref from stdin with csv encoding 'latin-1 '""", io)


class ExtPydeduct(luigi.Task):
    """
    """
    extract_file = '../../extract_files/pydeduct.csv'
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.arkona_luigi_27(db2_server) as cn:
            with cn.cursor() as cur:
                sql = """
                    select TRIM(COMPANY_NUMBER),TRIM(EMPLOYEE_NUMBER),TRIM(CODE_TYPE),TRIM(DED_PAY_CODE),SEQUENCE,
                        FIXED_DED_AMT,TRIM(VARIABLE_AMOUNT),TRIM(FIXED_DED_PCT),DED_LIMIT,TRIM(DED_FREQ),TOTAL,
                        TRIM(TAXING_UNIT_ID_NUMBER),TRIM(TAXING_UNIT_TYPE),BANK_ROUTING_,BANK_ACCOUNT,
                        CODE_BEGINNING_DATE,CODE_ENDING_DATE,FIXED_DED_AMT_2,TRIM(VARIABLE_AMOUNT_2),
                        TRIM(FIXED_DED_PCT_2),TRIM(EMP_LEVEL_CTL_),TRIM(PLAN_POLICY_OF_GARN),TRIM(FUTURE_1),
                        TRIM(FUTURE_2),YEAR_FOR_USE,DED_PRIORITY_SEQ
                    from rydedata.pydeduct
                 """
                cur.execute(sql)
                with open(self.extract_file, 'w') as f:
                    csv.writer(f).writerows(cur)
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute("truncate arkona.ext_pydeduct")
                with open(self.extract_file, 'r') as io:
                    pg_cur.copy_expert(
                        """copy arkona.ext_pydeduct from stdin with csv encoding 'latin-1 '""", io)


class ExtPyprhead(luigi.Task):
    """
    """
    extract_file = '../../extract_files/pyprhead.csv'
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.arkona_luigi_27(db2_server) as cn:
            with cn.cursor() as cur:
                sql = """
                    select
                      TRIM(COMPANY_NUMBER),TRIM(EMPLOYEE_NUMBER),TRIM(EMPLOYEE_NUMBER_YRMGRN),TRIM(JOB_DESCRIPTION),
                      TRIM(JOB_LEVEL),HIRE_DATE,ORG_HIRE_DATE,TERMINATION_DATE,LAST_REVIEW,NEXT_REVIEW,LAST_CONTACT,
                      BASE_SALARY01,BASE_SALARY02,BASE_SALARY03,BASE_SALARY04,BASE_SALARY05,BASE_SALARY06,BASE_SALARY07,
                      BASE_SALARY08,BASE_SALARY09,BASE_SALARY10,BASE_SALARY11,BASE_SALARY12,BASE_SALARY13,BASE_SALARY14,
                      BASE_SALARY15,BASE_SALARY16,RAISE_DATE01,RAISE_DATE02,RAISE_DATE03,RAISE_DATE04,RAISE_DATE05,
                      RAISE_DATE06,RAISE_DATE07,RAISE_DATE08,RAISE_DATE09,RAISE_DATE10,RAISE_DATE11,RAISE_DATE12,
                      RAISE_DATE13,RAISE_DATE14,RAISE_DATE15,RAISE_DATE16
                    from rydedata.pyprhead
                 """
                cur.execute(sql)
                with open(self.extract_file, 'w') as f:
                    csv.writer(f).writerows(cur)
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute("truncate arkona.ext_pyprhead")
                with open(self.extract_file, 'r') as io:
                    pg_cur.copy_expert(
                        """copy arkona.ext_pyprhead from stdin with csv encoding 'latin-1 '""", io)


class ExtPyprjobd(luigi.Task):
    """
    """
    extract_file = '../../extract_files/pyprjobd.csv'
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.arkona_luigi_27(db2_server) as cn:
            with cn.cursor() as cur:
                sql = """
                    select
                      TRIM(COMPANY_NUMBER),TRIM(JOB_DESCRIPTION),TRIM(JOB_LEVEL),SEQ_,TRIM(PERFORMANCE_RATING),TRIM(DATA)
                    from rydedata.pyprjobd
                  """
                cur.execute(sql)
                with open(self.extract_file, 'w') as f:
                    csv.writer(f).writerows(cur)
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute("truncate arkona.ext_pyprjobd")
                with open(self.extract_file, 'r') as io:
                    pg_cur.copy_expert(
                        """copy arkona.ext_pyprjobd from stdin with csv encoding 'latin-1 '""", io)


class ExtPdphist(luigi.Task):
    """
    initial scrape was only from 01/01/2019 on
    subsequent scrapes are 35 days worth
    08/31/20
        fails with null manufacturers code, looking at db2 data, only 1 instance, 3 with null part numbers,
        filter these out in the where clause
    10/15/20
        previous fix took care of null part numbers, now we have 2 rows with non null part number but null
            manufacturer code, add manufacturer_code <> '' to where clause
    """
    extract_file = '../../extract_files/pdphist.csv'
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.arkona_luigi_27(db2_server) as cn:
            with cn.cursor() as cur:
                sql = """
                    select
                        TRIM(COMPANY_NUMBER),TRIM(MANUFACTURER_CODE),TRIM(PART_NUMBER),TRANSACTION_TIME,
                        PRE_QUANTITY_ON_HAND,QUANTITY_ON_HAND_ADJUSTMENT,POST_QUANTITY_ON_HAND,
                        QUANTITY_RESERVE_ADJUSTMENT,QUANTITY_ON_STOCK_ORDER_ADJUSTMENT,
                        QUANTITY_BACK_ORDER_ADJUSTMENT,QUANTITY_ON_SPECIAL_ORDER_ADJUSTMENT,
                        QUANTITY_PLACE_ON_ORDER_ADJUSTMENT,QUANTITY_PLACE_ON_SPECIAL_ORDER_ADJUSTMENT,
                        QUANTITY_LOST_SALE,COST_AMOUNT,TRIM(BIN_LOCATION_CODE),TRIM(SHELF_LOCATION_CODE),
                        TRIM(TRANSACTION_TYPE),TRIM(TRANSACTION_SUB_TYPE),TRIM(PAYOR_TYPE),TRIM(ORIGINAL_PART_NUMBER),
                        TRIM(SOURCE_DOCUMENT_TYPE),TRIM(SOURCE_DOCUMENT_NUMBER),SOURCE_DOCUMENT_LINE_NUMBER,
                        SOURCE_DOCUMENT_LINE_SEQ_NUMBER,TRIM(SOURCE_RECORD_TYPE),TRIM(POST_TO_INVENTORY_FLAG),
                        TRIM(RETURN_TO_INVENTORY_FLAG),TRIM(SOURCE_COUNTERPERSON),TRIM(PRICE_OVERRIDE),
                        TRIM(CORE_STATUS),TRIM(STOCKING_GROUP),TRIM(PART_STATUS),TRIM(SALE_TYPE),TRIM(UNUSUAL_SALE),
                        TRIM(ORIGINAL_ORDER_SO),TRIM(KIT_PART),TRIM(PROGRAM_SOURCE_CODE),TRIM(USER_MODIFIED),
                        CUSTOMER_KEY,TRIM(STOCK_ORDER_TYPE),TRIM(FLAG01),TRIM(FLAG02),TRIM(FLAG03),TRIM(FLAG04),
                        TRIM(FLAG05),QUANTITY_NOT_ON_HAND_ADJUSTMENT
                    from rydedata.pdphist
                    where date(transaction_time) between curdate() - 35 days and curdate()
                      and trim(part_number) <> ''
                      and trim(manufacturer_code) <> ''
                  """
                cur.execute(sql)
                with open(self.extract_file, 'w') as f:
                    csv.writer(f).writerows(cur)
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = """
                    delete 
                    from arkona.ext_pdphist 
                    where transaction_time::date between current_date - 35 and current_date
                """
                pg_cur.execute(sql)
                with open(self.extract_file, 'r') as io:
                    pg_cur.copy_expert(
                        """copy arkona.ext_pdphist from stdin with csv encoding 'latin-1 '""", io)


class PdpmastSnapshot(luigi.Task):
    """
    creates a table named for the current date, eg arkona_cdc.pdpmast_20200207, and populates it with
    the current days scrape of pdpmast
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield ExtPdpmast()

    def output(self):
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = " select arkona_cdc.create_pdpmast_snapshot() "
                pg_cur.execute(sql)


class ExtSdptech(luigi.Task):
    """
    technicians
    invoked from fact_repair_order.py
    """
    extract_file = '../../extract_files/sdptech.csv'
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.arkona_luigi_27(db2_server) as cn:
            with cn.cursor() as cur:
                sql = """
                    select
                        TRIM(COMPANY_NUMBER),TRIM(TECHNICIAN_ID),TRIM(NAME),TRIM(EMPLOYEE_),TRIM(USE_TIME_LOG),
                        LABOR_RATE_A,LABOR_RATE_B,LABOR_RATE_C,LABOR_RATE_D,TRIM(PASSWORD),LOGOFF_TIME,LUNCH_DURATION,
                        MON_START_TIME,TUE_START_TIME,WED_START_TIME,THU_START_TIME,FRI_START_TIME,SAT_START_TIME,
                        SUN_START_TIME,MON_LUNCH_TIME,TUE_LUNCH_TIME,WED_LUNCH_TIME,THU_LUNCH_TIME,FRI_LUNCH_TIME,
                        SAT_LUNCH_TIME,SUN_LUNCH_TIME,MON_END_TIME,TUE_END_TIME,WED_END_TIME,THU_END_TIME,
                        FRI_END_TIME,SAT_END_TIME,SUN_END_TIME,TRIM(ASSIGNED_SERV_WTR),TRIM(PAYROLL_EMP_),
                        TRIM(DISPLAY_ASSIGNMNT),APPROVAL_HOLD,PARTS_HOLD,OTHER_HOLD,COMMENT_HOLD,RETURNS_TO_DISPATCH,
                        TRIM(TECHNICIAN_ACTIVE),TRIM(STATE_CERTIFICATION_NUMBER),TRIM(DEFAULT_SERVICE_TYPE),
                        WARRANTY_LBR_RATE,WARRANTY_LBR_RATE_STWARLBRB,WARRANTY_LBR_RATE_STWARLBRC,
                        WARRANTY_LBR_RATE_STWARLBRD,INTERNAL_LBR_RATE,INTERNAL_LBR_RATE_STINTLBRB,
                        INTERNAL_LBR_RATE_STINTLBRC,INTERNAL_LBR_RATE_STINTLBRD,SVCCONTR_LBR_RATE,
                        SVCCONTR_LBR_RATE_STSVCLBRB,SVCCONTR_LBR_RATE_STSVCLBRC,SVCCONTR_LBR_RATE_STSVCLBRD,
                        TRIM(TECHNICIAN_PREFERENCE),NO_OF_SERVICE_BAY
                    from rydedata.sdptech
                  """
                cur.execute(sql)
                with open(self.extract_file, 'w') as f:
                    csv.writer(f).writerows(cur)
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = """
                    truncate arkona.ext_sdptech
                """
                pg_cur.execute(sql)
                with open(self.extract_file, 'r') as io:
                    pg_cur.copy_expert(
                        """copy arkona.ext_sdptech from stdin with csv encoding 'latin-1 '""", io)


class ExtSdpswtr(luigi.Task):
    """
    service writers
    invoked from fact_repair_order.py
    """
    extract_file = '../../extract_files/sdpswtr.csv'
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.arkona_luigi_27(db2_server) as cn:
            with cn.cursor() as cur:
                sql = """
                    select
                        TRIM(COMPANY_NUMBER),TRIM(ID),TRIM(NAME),TRIM(INITIALS),TRIM(PASSWORD),TRIM(EMPLOYEE_),
                        TRIM(TECH_TIME_ENTRY01),TRIM(CHG_RO_SERV_WTR02),TRIM(APPROVE_RO03),TRIM(SUBLET_ENTRY04),
                        TRIM(PRE_INVOICE05),TRIM(UNLOCK_RO06),TRIM(CLOSE07),TRIM(PARTS_ENTRY08),
                        TRIM(ACCEPT_C_L_EXCEED09),TRIM(CLS_CUST_PAY_ONLY10),TRIM(COUPONS11),TRIM(REVIEW_SPEC_ORDER12),
                        TRIM(BODY_SHOP_SWTR13),TRIM(OVERIDE_DEPOSIT14),TRIM(DISP_GROSS_PROFIT15),
                        TRIM(OVERIDE_SKILL_LEV16),TRIM(WARRANTY_CLERK17),TRIM(DELETE_LABPR_OP18),
                        TRIM(DELETE_SHOP_SUPP19),TRIM(OPEN_ADDITIONL_RO20),TRIM(CHANGE_INTER_ACCT21),
                        TRIM(AVAILABLE_TO_USE22),TRIM(AVAILABLE_TO_USE23),TRIM(AVAILABLE_TO_USE24),
                        TRIM(AVAILABLE_TO_USE25),TRIM(AVAILABLE_TO_USE26),TRIM(AVAILABLE_TO_USE27),
                        TRIM(AVAILABLE_TO_USE28),TRIM(AVAILABLE_TO_USE29),TRIM(AVAILABLE_TO_USE30),
                        TRIM(SEARCH_TYPE),TRIM(SORT_BY),TRIM(SERV_WRITER_SELEC),TRIM(STATUS_SELECTION),
                        TRIM(ACTIVE),TRIM(MISC_FIELDS)
                    from rydedata.sdpswtr
                  """
                cur.execute(sql)
                with open(self.extract_file, 'w') as f:
                    csv.writer(f).writerows(cur)
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = """
                    truncate arkona.ext_sdpswtr;
                """
                pg_cur.execute(sql)
                with open(self.extract_file, 'r') as io:
                    pg_cur.copy_expert(
                        """copy arkona.ext_sdpswtr from stdin with csv encoding 'latin-1 '""", io)


class ExtSdpsvct(luigi.Task):
    """
    Service Types
    invoked from fact_repair_order
    """
    extract_file = '../../extract_files/sdpsvct.csv'
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.arkona_luigi_27(db2_server) as cn:
            with cn.cursor() as cur:
                sql = """
                    select
                        TRIM(COMPANY_NUMBER),TRIM(SERVICE_TYPE),TRIM(DESCRIPTION),TRIM(DELETE_CAPABLE),
                        TRIM(QUICK_ENTRY_CODE),TRIM(CAR_SUBLET_ACCOUNT),TRIM(TRUCK_SUBLET_ACCOUNT),
                        TRIM(HAZARDOUS_MAT_ACCT),TRIM(PAINT_MATERIAL_ACCT),TRIM(MISC_CHARGES_ACCT),
                        TRIM(SHOP_SUPPLIES_ACCT),SHOP_SUPPLIES_PERCNT,SHOP_SUPPLIES_LIMIT,
                        TRIM(PAINT_MATRL_TAX_FLAG),DFT_SUBLET_LBR_MRKUP,DFT_SUBLET_PRT_MRKUP,
                        TRIM(DFT_SHPSUP_BASIS_SBL),TRIM(DFT_SHPSUP_BASIS_LBR),
                        TRIM(DFT_SHPSUP_BASIS_PRT),TRIM(ACTIVE_FLAG),DFT_TAX_GROUP
                    from rydedata.sdpsvct
                    where TRIM(COMPANY_NUMBER) in ('RY1','RY2')
                  """
                cur.execute(sql)
                with open(self.extract_file, 'w') as f:
                    csv.writer(f).writerows(cur)
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = """
                    truncate arkona.ext_sdpsvct;
                """
                pg_cur.execute(sql)
                with open(self.extract_file, 'r') as io:
                    pg_cur.copy_expert(
                        """copy arkona.ext_sdpsvct from stdin with csv encoding 'latin-1 '""", io)


class ExtSdprtxt(luigi.Task):
    """
    Repair Order History Text
    invoked by fact_repair_order
    used for constructing dim_ccc
    extract based on 45 days of new/changed ros, based on open, close, final_close or transaction dates
    currently no full scrape of sdprtxt
    """
    extract_file = '../../extract_files/sdprtxt.csv'
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.arkona_luigi_27(db2_server) as cn:
            with cn.cursor() as cur:
                sql = """
                    select TRIM(SXCO#),TRIM(SXRO#),SXLINE,TRIM(SXLTYP),SXSEQ#,TRIM(SXCODE),TRIM(SXTEXT)
                    from rydedata.sdprtxt a
                    join (
                        select trim(ro_number) as ro_number
                        from rydedata.sdprhdr 
                        where final_close_date <> 0
                          and company_number in ('RY1','RY2')
                          and (
                            (to_date(cast(open_date as varchar(8)), 'YYYYMMDD') > curdate() -  45 days)  
                            or
                            (to_date(cast(close_date as varchar(8)), 'YYYYMMDD') > curdate() -  45 days)
                            or
                            (to_date(cast(final_close_date as varchar(8)), 'YYYYMMDD') > curdate() -  45 days))
                            
                        union   
                        select trim(ro_number) as ro_number
                        from rydedata.sdprdet
                        where transaction_date <> 0
                          and company_number in ('RY1','RY2')
                          and to_date(cast(transaction_date as varchar(8)), 'YYYYMMDD') > curdate() - 45 days) b
                                on trim(a.SXRO#) = trim(b.ro_number)
                  """
                cur.execute(sql)
                with open(self.extract_file, 'w') as f:
                    csv.writer(f).writerows(cur)
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = """
                    truncate arkona.ext_sdprtxt;
                """
                pg_cur.execute(sql)
                with open(self.extract_file, 'r') as io:
                    pg_cur.copy_expert(
                        """copy arkona.ext_sdprtxt from stdin with csv encoding 'latin-1 '""", io)


class ExtSdprhdr(luigi.Task):
    """
    45 days worth of data for RY1 & RY2 only for generating fact repair order
    invoked by fact_repair_order
    """
    extract_file = '../../extract_files/sdprhdr.csv'
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.arkona_luigi_27(db2_server) as cn:
            with cn.cursor() as cur:
                sql = """
                    select 
                        TRIM(COMPANY_NUMBER),TRIM(a.RO_NUMBER),TRIM(RO_TYPE),WARRANTY_RO_,TRIM(SERVICE_WRITER_ID),
                        TRIM(RO_TECHNICIAN_ID),CUSTOMER_KEY,TRIM(CUST_NAME),A_R_CUST_KEY,TRIM(PAYMENT_METHOD),
                        OPEN_DATE,CLOSE_DATE,FINAL_CLOSE_DATE,TRIM(VIN),TOTAL_ESTIMATE,SERV_CONT_COMP,
                        SERV_CONT_COMP_2,SERV_CONT_DEDUCT,SERV_CONT_DEDUCT2,WARRANTY_DEDUCT,TRIM(FRANCHISE_CODE),
                        ODOMETER_IN,ODOMETER_OUT,TRIM(CHECK_NUMBER),TRIM(PURCHASE_ORDER_),RECEIPT_NUMBER,PARTS_TOTAL,
                        LABOR_TOTAL,SUBLET_TOTAL,SC_DEDUCT_PAID,SERV_CONT_TOTAL,SPEC_ORD_DEPOS,CUST_PAY_HZRD_MAT,
                        CUST_PAY_SALE_TAX,CUST_PAY_SALE_TAX_2,CUST_PAY_SALE_TAX_3,CUST_PAY_SALE_TAX_4,
                        WARRANTY_SALE_TAX,WARRANTY_SALE_TAX_2,WARRANTY_SALE_TAX_3,WARRANTY_SALE_TAX_4,
                        INTERNAL_SALE_TAX,INTERNAL_SALE_TAX_2,INTERNAL_SALE_TAX_3,INTERNAL_SALE_TAX_4,
                        SVC_CONT_SALE_TAX,SVC_CONT_SALE_TAX_2,SVC_CONT_SALE_TAX_3,SVC_CONT_SALE_TAX_4,
                        CUST_PAY_SHOP_SUP,WARRANTY_SHOP_SUP,INTERNAL_SHOP_SUP,SVC_CONT_SHOP_SUP,COUPON_NUMBER,
                        COUPON_DISCOUNT,TOTAL_COUPON_DISC,TOTAL_SALE,TOTAL_GROSS,TRIM(INTERNAL_AUTH_BY),
                        TRIM(TAG_NUMBER),DATE_TIME_OF_APPOINTMENT,DATE_TIME_LAST_LINE_COMPLETE,DATE_TIME_PRE_INVOICED,
                        TRIM(TOWED_IN_INDICATOR),DOC_CREATE_TIMESTAMP,TRIM(DISPATCH_TEAM),TAX_GROUP_OVERRIDE,
                        DISCOUNT_TAXABLE_AMOUNT,PARTS_DISCOUNT_ALLOCATION_AMOUNT,LABOR_DISCOUNT_ALLOCATION_AMOUNT
                    from rydedata.sdprhdr a
                    join (
                        select trim(ro_number) as ro_number
                        from rydedata.sdprhdr 
                        where final_close_date <> 0
                          and (
                            (to_date(cast(open_date as varchar(8)), 'YYYYMMDD') > curdate() -  45 days)  
                            or
                            (to_date(cast(close_date as varchar(8)), 'YYYYMMDD') > curdate() -  45 days)
                            or
                            (to_date(cast(final_close_date as varchar(8)), 'YYYYMMDD') > curdate() -  45 days))
                            
                        union   
                        select trim(ro_number) as ro_number
                        from rydedata.sdprdet
                        where transaction_date <> 0
                          and to_date(cast(transaction_date as varchar(8)), 'YYYYMMDD') > curdate() - 45 days) b
                                on trim(a.ro_number) = trim(b.ro_number)
                    where TRIM(COMPANY_NUMBER) in ('RY1','RY2')
                  """
                cur.execute(sql)
                with open(self.extract_file, 'w') as f:
                    csv.writer(f).writerows(cur)
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = """
                    truncate arkona.ext_sdprhdr;
                """
                pg_cur.execute(sql)
                with open(self.extract_file, 'r') as io:
                    pg_cur.copy_expert(
                        """copy arkona.ext_sdprhdr from stdin with csv encoding 'latin-1 '""", io)


class ExtSdprdet(luigi.Task):
    """
    45 days worth of data for RY1 & RY2 only for generating fact repair order
    invoked by fact_repair_order
    """
    extract_file = '../../extract_files/sdprdet.csv'
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.arkona_luigi_27(db2_server) as cn:
            with cn.cursor() as cur:
                sql = """
                    select 
                        TRIM(a.COMPANY_NUMBER),TRIM(a.RO_NUMBER),a.LINE_NUMBER,TRIM(a.LINE_TYPE),a.SEQUENCE_NUMBER,
                        TRIM(a.TRANSACTION_CODE),a.TRANSACTION_DATE,TRIM(a.LINE_PAYMENT_METHOD),TRIM(a.SERVICE_TYPE),
                        TRIM(a.POLICY_ADJUSTMENT),TRIM(a.TECHNICIAN_ID),TRIM(a.LABOR_OPERATION_CODE),
                        TRIM(a.CORRECTION_CODE),a.LABOR_HOURS,a.LABOR_AMOUNT,a.COST,TRIM(a.ACTUAL_RETAIL_FLAG),
                        TRIM(a.FAILURE_CODE),TRIM(a.SUBLET_VENDOR_NUMBER),TRIM(a.SUBLET_INVOICE_NUMBER),
                        a.DISCOUNT_KEY,TRIM(a.DISCOUNT_BASIS),TRIM(a.VAT_CODE),a.VAT_AMOUNT,
                        TRIM(a.DISPATCH_PRIORITY_1),TRIM(a.DISPATCH_PRIORITY_2),a.DISPATCH_RANK_1,
                        a.DISPATCH_RANK_2,a.POLICY_TAX_AMOUNT,a.KIT_TAXABLE_AMOUNT
                    from rydedata.sdprdet a
                    join (
                        select trim(ro_number) as ro_number
                        from rydedata.sdprhdr 
                        where final_close_date <> 0
                          and (
                            (to_date(cast(open_date as varchar(8)), 'YYYYMMDD') > curdate() -  45 days)  
                            or
                            (to_date(cast(close_date as varchar(8)), 'YYYYMMDD') > curdate() -  45 days)
                            or
                            (to_date(cast(final_close_date as varchar(8)), 'YYYYMMDD') > curdate() -  45 days))
                            
                        union   
                        select trim(ro_number) as ro_number
                        from rydedata.sdprdet
                        where transaction_date <> 0
                          and to_date(cast(transaction_date as varchar(8)), 'YYYYMMDD') > curdate() - 45 days) b
                                on trim(a.ro_number) = trim(b.ro_number)
                    where TRIM(COMPANY_NUMBER) in ('RY1','RY2')
                   """
                cur.execute(sql)
                with open(self.extract_file, 'w') as f:
                    csv.writer(f).writerows(cur)
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = """
                    truncate arkona.ext_sdprdet;
                """
                pg_cur.execute(sql)
                with open(self.extract_file, 'r') as io:
                    pg_cur.copy_expert(
                        """copy arkona.ext_sdprdet from stdin with csv encoding 'latin-1 '""", io)


class ExtPdpphdr(luigi.Task):
    """
    invoked by MiscArkonaTables
    used by fact repair order
    """
    extract_file = '../../extract_files/pdpphdr.csv'
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.arkona_luigi_27(db2_server) as cn:
            with cn.cursor() as cur:
                sql = """
                    select
                      TRIM(COMPANY_NUMBER),PENDING_KEY,TRIM(COUNTER_PERSON_ID),TRIM(RECORD_STATUS),TRIM(DOCUMENT_TYPE),
                      TRIM(TRANSACTION_TYPE),OPEN_TRAN_DATE,TRIM(DOCUMENT_NUMBER),TRIM(CUSTOMER_NUMBER),CUSTOMER_KEY,
                      TRIM(CUST_NAME),CUST_PHONE_NO,SHIP_TO_KEY,TRIM(PAYMENT_METHOD),TRIM(SALE_TYPE),PRICE_LEVEL,
                      TRIM(TAX_EXEMPT),PARTS_TOTAL,SHIPPING_TOTAL,SPEC_ORD_DEPOSIT,SPEC_ORD_DEPOS_CR,SPEC_ORD_DEP_PCT,
                      TRIM(SP_ORD_DEP_ORIDE),TRIM(SPEC_ORD_ON_HOLD),PARTS_SALES_TAX_1,PARTS_SALES_TAX_2,
                      PARTS_SALES_TAX_3,PARTS_SALES_TAX_4,TRIM(CTRPERSON_NAME),TRIM(SORT_KEY),TRIM(AUTHOR_CTRP_ID),
                      TRIM(CRED_CARD_AR_CUS_),TRIM(PURCHASE_ORDER_),RECEIPT_NUMBER,TRIM(ORIG_DOC_NUMBER),
                      TRIM(INTERNAL_ACCT_NO),TRIM(RO_TYPE),TRIM(RO_STATUS),TRIM(WARRANTY_RO),TRIM(PARTS_APPROVED),
                      TRIM(SERVICE_APPROVED),TRIM(SERVICE_WRITER_ID),TRIM(RO_TECHNICIAN_ID),TOTAL_ESTIMATE,
                      SERV_CONT_COMP,SERV_CONT_COMP_2,SERV_CONT_DEDUCT,SERV_CONT_DEDUCT2,WARRANTY_DEDUCT,TRIM(VIN),
                      TRIM(STOCK_NUMBER),TRIM(TRUCK),TRIM(FRANCHISE_CODE),ODOMETER_IN,ODOMETER_OUT,TIME_IN,
                      PROMISED_DATE_TIME,REMAINING_TIME,TRIM(TAG_NUMBER),TRIM(CHECK_NUMBER),TRIM(HEADER_COMMMENTS),
                      TRIM(SHOP_SUPPLY_ORIDE),TRIM(HAZD_MATER_ORIDE),LABOR_TOTAL,SUBLET_TOTAL,SC_DEDUCT_PAID,
                      CUST_PAY_HZRD_MAT,CUST_PAY_TAX_1,CUST_PAY_TAX_2,CUST_PAY_TAX_3,CUST_PAY_TAX_4,WARRANTY_TAX_1,
                      WARRANTY_TAX_2,WARRANTY_TAX_3,WARRANTY_TAX_4,INTERNAL_TAX_1,INTERNAL_TAX_2,INTERNAL_TAX_3,
                      INTERNAL_TAX_4,SVC_CONT_TAX_1,SVC_CONT_TAX_2,SVC_CONT_TAX_3,SVC_CONT_TAX_4,CUST_PAY_SHOP_SUP,
                      WARRANTY_SHOP_SUP,INTERNAL_SHOP_SUP,SVC_CONT_SHOP_SUP,COUPON_NUMBER,COUPON_DISCOUNT,
                      TOTAL_COUPON_DISC,PAID_BY_MANUF,PAID_BY_DEALER,CUST_PAY_TOT_DUE,TRIM(ORDER_STATUS),
                      DATE_TO_ARRIVE,A_R_CUST_KEY,WARRANTY_RO_,TRIM(WORK_STATION_ID),TRIM(INTERNAL_AUTH_BY),
                      APPOINTMENT_DATE_TIME,DATE_TIME_LAST_LINE_COMPLETE,DATE_TIME_PRE_INVOICED,TRIM(INTERNAL_PAY_TYPE),
                      TRIM(REPAIR_ORDER_PRIORITY),DAILY_RENTAL_AGREEMENT_NUMBE,TRIM(TOWED_IN_INDICATOR),
                      DOC_CREATE_TIMESTAMP,CT_HOLD_TIMESTAMP,TRIM(INVOICE_LIMIT_AUTH_),TRIM(LIMIT_OVERRIDE_USER),
                      DELIVERY_METHOD,MULTI_SHIP_SEQ_,TRIM(DISPATCH_TEAM),TAX_GROUP_OVERRIDE,DISCOUNT_TAXABLE_AMT,
                      TRIM(UPDATED_BY_OBJECT),PARTS_DISCOUNT_ALLOCATION_AMOUNT,LABOR_DISCOUNT_ALLOCATION_AMOUNT,
                      TRIM(CUSTOMER_ORDER_NUMBER),CUSTOMER_ORDER_SEQUENCE_NUMBER,SPECIAL_ORDER_PARTS_TOTAL,
                      SPECIAL_ORDER_PARTS_TAX,TRIM(APPOINTMENT_DOCUMENT_ID)
                    from rydedata.pdpphdr
                  """
                cur.execute(sql)
                with open(self.extract_file, 'w') as f:
                    csv.writer(f).writerows(cur)
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute("truncate arkona.ext_pdpphdr")
                with open(self.extract_file, 'r') as io:
                    pg_cur.copy_expert(
                        """copy arkona.ext_pdpphdr from stdin with csv encoding 'latin-1 '""", io)


class ExtPdppdet(luigi.Task):
    """
    invoked by fact_repair_order
    """
    extract_file = '../../extract_files/pdppdet.csv'
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.arkona_luigi_27(db2_server) as cn:
            with cn.cursor() as cur:
                sql = """
                    select 
                        TRIM(COMPANY_NUMBER),PENDING_KEY,LINE_NUMBER,TRIM(LINE_TYPE),SEQUENCE_NUMBER,
                        TRIM(COUNTER_PERSON_ID),TRIM(TRANSACTION_CODE),TRIM(ORDER_TYPE),TRANSACTION_DATE,
                        TRIM(MANUFACTURER),TRIM(PART_NUMBER),PART_NUMBER_LENGTH,TRIM(STOCKING_GROUP),
                        TRIM(BIN_LOCATION),QUANTITY,COST,LIST_PRICE,TRADE_PRICE,FLAT_PRICE,NET_PRICE,BOOK_LIST,
                        TRIM(PRICE_METHOD),PRICE_PERCENT,TRIM(PRICE_BASE),TRIM(SPECIAL_CODE),TRIM(PRICE_OVERRIDE),
                        TRIM(GROUP_PRICE),TRIM(IS_CORE_PART),TRIM(ADD_NONSTOCK_PART),SPECIAL_ORDER_PRIORITY,
                        TRIM(ORIGINAL_SPECIAL_ORDER),TRIM(EXCLUDE_FROM_HISTORY),TRIM(COMMENTS),TRIM(LINE_STATUS),
                        TRIM(SERVICE_TYPE),TRIM(LINE_PAYMENT_METHOD),TRIM(POLICY_ADJUSTMENT),TRIM(TECHNICIAN_ID),
                        TRIM(LABOR_OPERATION_CODE),TRIM(CORRECTION_CODE),LABOR_HOURS,LABOR_COST_HOURS,
                        ACTUAL_RETAIL_AMOUNT,TRIM(FAILURE_CODE),PRICE_LEVEL,PRICE_STRATEGY_ASSIGNMENT,
                        TRIM(SUBLET_VENDOR_NUMBER),TRIM(SUBLET_INVOICE_NUMBER),DISCOUNT_KEY,ACTUAL_LABOR_HOURS,
                        TRIM(AUTHORIZATION_CODE),TRIM(BATTERY_WARRANTY_CODE),DATETIME_LINE_COMPLETED,
                        TRIM(INTERNAL_ACCOUNT),TRIM(AUTHORIZER),NET_ITEM_TOTAL,ADMIN_LABOR_AMOUNT,ADMIN_LABOR_HOURS,
                        PAINT_MATERIALS_COST,PAINT_MATERIALS_RETAIL,TRIM(REPEAT_REPAIR_FLAG),
                        TRIM(TAXABLE_INTERNAL_LINE),TRIM(UPSELL_FLAG),TRIM(WARRANTY_CLAIM_TYPE),
                        TRIM(VAT_CODE),VAT_AMOUNT,TRIM(RETURN_TO_INVENTORY),TRIM(SKILL_LEVEL),TRIM(OEM_REPLACEMENT),
                        COST_DIFFERENTIAL,OPTION_SEQUENCE,DISPATCH_START_TIME,TOTAL_DISCOUNT_LABOR,
                        TOTAL_DISCOUNT_PARTS,TRIM(PICKING_ZONE),TRIM(DISPATCH_PRIORITY_1),TRIM(DISPATCH_PRIORITY_2),
                        DISPATCH_RANK_1,DISPATCH_RANK_2,AUTO_ASSIGN_DATE,AUTO_ASSIGN_TIME,MANUAL_ASSIGN_DATE,
                        MANUAL_ASSIGN_TIME,POLICY_TAX_AMOUNT,POLICY_TAX_GROUP,KIT_TAXABLE_AMOUNT,TRIM(UPDATED_BY_OBJECT)
                    from rydedata.pdppdet a
                   """
                cur.execute(sql)
                with open(self.extract_file, 'w') as f:
                    csv.writer(f).writerows(cur)
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = """
                    truncate arkona.ext_pdppdet;
                """
                pg_cur.execute(sql)
                with open(self.extract_file, 'r') as io:
                    pg_cur.copy_expert(
                        """copy arkona.ext_pdppdet from stdin with csv encoding 'latin-1 '""", io)


class ExtSdplopc(luigi.Task):
    """
    invoked by fact_repair_order
    """
    extract_file = '../../extract_files/sdplopc.csv'
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.arkona_luigi_27(db2_server) as cn:
            with cn.cursor() as cur:
                sql = """
                    select TRIM(COMPANY_NUMBER),TRIM(OPERACTION_CODE),TRIM(DESCRIPTION_1),TRIM(DESCRIPTION_2),
                      TRIM(DESCRIPTION_3),TRIM(DESCRIPTION_4),TRIM(MAJOR_CODE),TRIM(MINOR_CODE),TRIM(MANUFACTURER),
                      TRIM(MAKE),TRIM(MODEL),TRIM(ENGINE),TRIM(YEAR),TRIM(DEF_LINE_PYM_METH),TRIM(CORRECTION_LOP),
                      TRIM(TYPE),TRIM(EXCLUDE_DISCOUNT),TRIM(A_B_C_OR_D),TRIM(TAXABLE),HAZARD_MATER_CHG,
                      TRIM(SHOP_SUPPLY_CHG),TRIM(RETAIL_METHOD),RETAIL_AMOUNT,TRIM(COST_METHOD),COST_AMOUNT,
                      TRIM(PARTS_METHOD),PARTS_AMOUNT,TRIM(ESTIMATE),ESTIMATED_HRS,TRIM(WAITER_FLAG),
                      TRIM(PREDEFINED_CAUSE_DESCRIPTION1),TRIM(ADDITIONAL_DESCRIPTION_LINE5),
                      TRIM(ADDITIONAL_DESCRIPTION_LINE6),TRIM(PREDEFINED_COMPLAINT_DESCRIP1),TRIM(DEFAULT_SERVICE_TYPE)
                    from rydedata.sdplopc
                    where TRIM(COMPANY_NUMBER) in ('RY1','RY2')
                   """
                cur.execute(sql)
                with open(self.extract_file, 'w') as f:
                    csv.writer(f).writerows(cur)
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = """
                    truncate arkona.ext_sdplopc;
                """
                pg_cur.execute(sql)
                with open(self.extract_file, 'r') as io:
                    pg_cur.copy_expert(
                        """copy arkona.ext_sdplopc from stdin with csv encoding 'latin-1 '""", io)


class ExtVoidRos(luigi.Task):
    """
    invoked by fact_repair_order
    """
    extract_file = '../../extract_files/void_ros.csv'
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.arkona_luigi_27(db2_server) as cn:
            with cn.cursor() as cur:
                sql = """
                    select trim(ro_number)
                    from rydedata.sdprhdr
                    where cust_name = '*VOIDED REPAIR ORDER*'
                    union
                    select trim(document_number)
                    from rydedata.pdpphdr
                    where cust_name = '*VOIDED REPAIR ORDER*'
                """
                cur.execute(sql)
                with open(self.extract_file, 'w') as f:
                    csv.writer(f).writerows(cur)
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = """
                    truncate arkona.ext_void_ros;
                """
                pg_cur.execute(sql)
                with open(self.extract_file, 'r') as io:
                    pg_cur.copy_expert(
                        """copy arkona.ext_void_ros from stdin with csv encoding 'latin-1 '""", io)


class ExtSdprhdrCloseDates(luigi.Task):
    """
    invoked by fact_repair_order
    """
    extract_file = '../../extract_files/sdprhdr_close_dates.csv'
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.arkona_luigi_27(db2_server) as cn:
            with cn.cursor() as cur:
                sql = """
                  SELECT TRIM(RO_NUMBER),OPEN_DATE,CLOSE_DATE,FINAL_CLOSE_DATE
                  from rydedata.sdprhdr
                  where days(DATE( TIMESTAMP_FORMAT(cast(open_date as varchar(8)), 'YYYYMMDD'))) > days(curdate()) -365
                """
                cur.execute(sql)
                with open(self.extract_file, 'w') as f:
                    csv.writer(f).writerows(cur)
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = """
                    truncate arkona.ext_sdprhdr_close_dates_ext;
                """
                pg_cur.execute(sql)
                with open(self.extract_file, 'r') as io:
                    pg_cur.copy_expert(
                        """copy arkona.ext_sdprhdr_close_dates_ext from stdin with csv encoding 'latin-1 '""", io)


class ExtSdprdetHistory(luigi.Task):
    """
    invoked by MiscArkonaTables in this module
    """
    extract_file = '../../extract_files/sdprdet_history.csv'
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.arkona_luigi_27(db2_server) as cn:
            with cn.cursor() as cur:
                sql = """
                  select 
                      TRIM(a.COMPANY_NUMBER),TRIM(a.RO_NUMBER),a.LINE_NUMBER,TRIM(a.LINE_TYPE),a.SEQUENCE_NUMBER,
                      TRIM(a.TRANSACTION_CODE),a.TRANSACTION_DATE,TRIM(a.LINE_PAYMENT_METHOD),TRIM(a.SERVICE_TYPE),
                      TRIM(a.POLICY_ADJUSTMENT),TRIM(a.TECHNICIAN_ID),TRIM(a.LABOR_OPERATION_CODE),
                      TRIM(a.CORRECTION_CODE),a.LABOR_HOURS,a.LABOR_AMOUNT,a.COST,TRIM(a.ACTUAL_RETAIL_FLAG),
                      TRIM(a.FAILURE_CODE),TRIM(a.SUBLET_VENDOR_NUMBER),TRIM(a.SUBLET_INVOICE_NUMBER),
                      a.DISCOUNT_KEY,TRIM(a.DISCOUNT_BASIS),TRIM(a.VAT_CODE),a.VAT_AMOUNT,
                      TRIM(a.DISPATCH_PRIORITY_1),TRIM(a.DISPATCH_PRIORITY_2),a.DISPATCH_RANK_1,
                      a.DISPATCH_RANK_2,a.POLICY_TAX_AMOUNT,a.KIT_TAXABLE_AMOUNT
                  from rydedata.sdprdet a
                  join rydedata.sdprhdr b on trim(a.ro_number) = trim(b.ro_number)
                    and days(DATE(TIMESTAMP_FORMAT(cast(open_date as varchar(8)), 'YYYYMMDD'))) > days(curdate()) - 180
                  where a.company_number in ('RY1','RY2')
                """
                cur.execute(sql)
                with open(self.extract_file, 'w') as f:
                    csv.writer(f).writerows(cur)
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = "truncate arkona.ext_sdprdet_history_tmp;"
                pg_cur.execute(sql)
                with open(self.extract_file, 'r') as io:
                    pg_cur.copy_expert(
                        """copy arkona.ext_sdprdet_history_tmp from stdin with csv encoding 'latin-1 '""", io)
                sql = "select arkona.ext_sdprdet_history_update()"
                pg_cur.execute(sql)


class ExtPypclkl(luigi.Task):
    """
    invoked by fact_repair_order
    """
    extract_file = '../../extract_files/pypclkl.csv'
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.arkona_luigi_27(db2_server) as cn:
            with cn.cursor() as cur:
                sql = """
                    select TRIM(YLCCO#),TRIM(PYMAST_EMPLOYEE_NUMBER),YLCLKIND,
                        case 
                            when YLCLKINT = '24:00:00' then '00:00:01'
                            else YLCLKINT
                        end as ylclkint,  
                         YLCLKOUTD,                   
                        case 
                            when YLCLKOUTT = '24:00:00' then '23:59:59'
                            else YLCLKOUTT
                        end as ylclkoutt,
                        TRIM(YLCODE),TRIM(YLUSER),TRIM(YLWSID),TRIM(YLCHGC),YLCHGD,YLCHGT
                    from rydedata.pypclkl
                    where year(ylchgd) > 2018
                   """
                cur.execute(sql)
                with open(self.extract_file, 'w') as f:
                    csv.writer(f).writerows(cur)
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = """
                    truncate arkona.ext_PYPCLKL;
                """
                pg_cur.execute(sql)
                with open(self.extract_file, 'r') as io:
                    pg_cur.copy_expert(
                        """copy arkona.ext_PYPCLKL from stdin with csv encoding 'latin-1 '""", io)


class MiscArkonaTables(luigi.WrapperTask):
    """
    a wrapper task that invokes all the parts extract classes
    this task is susequently listed as a requirement in run_all
    added sdpxtim, required for honda main shop flat rate, but nowhere else in luigi,
        so, even though it is not a parts table, it needs to be run nightly, so,
        why not here ...
    6/29/19: added inpcmnt to support r units  not in accounting
    9/18/19: renamed class from PartsTables to MiscArkonaTables
        these are arkona tables required for misc functions/reports that require a fresh
        scrape of these extract tables, none of which are currently required for
        another luigi class
        added pypcodes, bopvref
    9/25/19 added pydeduct for payroll master_deduction_list
    12/20/19 added pyprhead, pyprjobd
    01/24/2020 add pdpphdr for full shop bdr appointments
    02/07/90 add pdphist, parts transaction history, only since 01/01/2019
    10/16/20 add ExtSdprdetHistory
    """
    pipeline = pipeline

    def requires(self):
        yield ExtPdpmrpl()
        yield ExtPdppmex()
        yield ExtPdpsgrp()
        yield ExtPdpmast()
        yield ExtPdptdet()
        yield ExtSdpxtim()
        yield ExtInpcmnt()
        yield ExtPypcodes()
        yield ExtBopvref()
        yield ExtPydeduct()
        yield ExtPyprhead()
        yield ExtPyprjobd()
        yield ExtPdpphdr()
        yield ExtPdphist()
        yield PdpmastSnapshot()
        yield ExtPypclkl()
        yield ExtSdprdetHistory()


# if __name__ == '__main__':
#     luigi.run(["--workers=1"], main_task_cls=ExtPypclkl)
