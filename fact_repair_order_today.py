# encoding=utf-8
"""
target_date_with_minute: used for generating the output files, include hours & minutes to enable
running a task multiple times a day
but need to be able to verify each "fact repair order batch" as a group, thinking make a separated
folder for these targets, at the end, empty the folder, seems to work ok

TODO: deployment: create target directory,
"""
import utilities
import luigi
import datetime
import csv
import os

pipeline = 'fact_repair_order_today'
db2_server = 'report'
pg_server = '173'
ts_no_spaces = str(datetime.datetime.now()).replace(" ", "")
target_directory = '../../luigi_output_fact_repair_order_today/'
target_date = '{:%Y-%m-%d}'.format(datetime.datetime.now())
target_date_with_minute = '{:%Y-%m-%d-%H-%M}'.format(datetime.datetime.now())


# # if these task are being called via run_all, that task will handle all of this logging
@luigi.Task.event_handler(luigi.Event.START)
def task_start(self):
    utilities.luigi_log_start(pg_server, self.task_id + '__' + ts_no_spaces, self.pipeline, self.__class__.__name__)


@luigi.Task.event_handler(luigi.Event.SUCCESS)
def task_success(self):
    utilities.luigi_log_pass(pg_server, self.task_id + '__' + ts_no_spaces, self.pipeline, self.__class__.__name__)
    try:  # WrapperTest throws an AttributeError, see /Notes/issues
        with self.output().open("w") as out_file:
            # write to the output file
            out_file.write('pass')
    except AttributeError:
        pass


class ExtPdpphdrToday(luigi.Task):
    """
    any pdpphdr row with an open date today, or with an associated (pending_key) row in pdppdet with a
    transaction date today
    """
    extract_file = '../../extract_files/pdpphdr_today.csv'
    pipeline = pipeline

    def local_target(self):
        return target_directory + target_date + '_' + self.__class__.__name__ + '.txt'

    def requires(self):
        return None

    def output(self):
        return luigi.LocalTarget(self.local_target())

    def run(self):
        print('starting ' + self.__class__.__name__ + ' at ' + '{:%Y-%m-%d-%H-%M}'.format(datetime.datetime.now()))
        with utilities.arkona_luigi_27(db2_server) as cn:
            with cn.cursor() as cur:
                sql = """
                      select
                          TRIM(COMPANY_NUMBER),PENDING_KEY,TRIM(COUNTER_PERSON_ID),TRIM(RECORD_STATUS),
                          TRIM(DOCUMENT_TYPE),TRIM(TRANSACTION_TYPE),OPEN_TRAN_DATE,TRIM(DOCUMENT_NUMBER),
                          TRIM(CUSTOMER_NUMBER),CUSTOMER_KEY,TRIM(CUST_NAME),CUST_PHONE_NO,SHIP_TO_KEY,
                          TRIM(PAYMENT_METHOD),TRIM(SALE_TYPE),PRICE_LEVEL,TRIM(TAX_EXEMPT),PARTS_TOTAL,
                          SHIPPING_TOTAL,SPEC_ORD_DEPOSIT,SPEC_ORD_DEPOS_CR,SPEC_ORD_DEP_PCT,
                          TRIM(SP_ORD_DEP_ORIDE),TRIM(SPEC_ORD_ON_HOLD),PARTS_SALES_TAX_1,PARTS_SALES_TAX_2,
                          PARTS_SALES_TAX_3,PARTS_SALES_TAX_4,TRIM(CTRPERSON_NAME),TRIM(SORT_KEY),TRIM(AUTHOR_CTRP_ID),
                          TRIM(CRED_CARD_AR_CUS_),TRIM(PURCHASE_ORDER_),RECEIPT_NUMBER,TRIM(ORIG_DOC_NUMBER),
                          TRIM(INTERNAL_ACCT_NO),TRIM(RO_TYPE),TRIM(RO_STATUS),TRIM(WARRANTY_RO),TRIM(PARTS_APPROVED),
                          TRIM(SERVICE_APPROVED),TRIM(SERVICE_WRITER_ID),TRIM(RO_TECHNICIAN_ID),TOTAL_ESTIMATE,
                          SERV_CONT_COMP,SERV_CONT_COMP_2,SERV_CONT_DEDUCT,SERV_CONT_DEDUCT2,WARRANTY_DEDUCT,TRIM(VIN),
                          TRIM(STOCK_NUMBER),TRIM(TRUCK),TRIM(FRANCHISE_CODE),ODOMETER_IN,ODOMETER_OUT,TIME_IN,
                          PROMISED_DATE_TIME,REMAINING_TIME,TRIM(TAG_NUMBER),TRIM(CHECK_NUMBER),TRIM(HEADER_COMMMENTS),
                          TRIM(SHOP_SUPPLY_ORIDE),TRIM(HAZD_MATER_ORIDE),LABOR_TOTAL,SUBLET_TOTAL,SC_DEDUCT_PAID,
                          CUST_PAY_HZRD_MAT,CUST_PAY_TAX_1,CUST_PAY_TAX_2,CUST_PAY_TAX_3,CUST_PAY_TAX_4,WARRANTY_TAX_1,
                          WARRANTY_TAX_2,WARRANTY_TAX_3,WARRANTY_TAX_4,INTERNAL_TAX_1,INTERNAL_TAX_2,INTERNAL_TAX_3,
                          INTERNAL_TAX_4,SVC_CONT_TAX_1,SVC_CONT_TAX_2,SVC_CONT_TAX_3,SVC_CONT_TAX_4,CUST_PAY_SHOP_SUP,
                          WARRANTY_SHOP_SUP,INTERNAL_SHOP_SUP,SVC_CONT_SHOP_SUP,COUPON_NUMBER,COUPON_DISCOUNT,
                          TOTAL_COUPON_DISC,PAID_BY_MANUF,PAID_BY_DEALER,CUST_PAY_TOT_DUE,TRIM(ORDER_STATUS),
                          DATE_TO_ARRIVE,A_R_CUST_KEY,WARRANTY_RO_,TRIM(WORK_STATION_ID),TRIM(INTERNAL_AUTH_BY),
                          APPOINTMENT_DATE_TIME,DATE_TIME_LAST_LINE_COMPLETE,DATE_TIME_PRE_INVOICED,
                          TRIM(INTERNAL_PAY_TYPE),TRIM(REPAIR_ORDER_PRIORITY),DAILY_RENTAL_AGREEMENT_NUMBE,
                          TRIM(TOWED_IN_INDICATOR),DOC_CREATE_TIMESTAMP,CT_HOLD_TIMESTAMP,TRIM(INVOICE_LIMIT_AUTH_),
                          TRIM(LIMIT_OVERRIDE_USER),DELIVERY_METHOD,MULTI_SHIP_SEQ_,TRIM(DISPATCH_TEAM),
                          TAX_GROUP_OVERRIDE,DISCOUNT_TAXABLE_AMT,TRIM(UPDATED_BY_OBJECT),
                          PARTS_DISCOUNT_ALLOCATION_AMOUNT,LABOR_DISCOUNT_ALLOCATION_AMOUNT,
                          TRIM(CUSTOMER_ORDER_NUMBER),CUSTOMER_ORDER_SEQUENCE_NUMBER,SPECIAL_ORDER_PARTS_TOTAL,
                          SPECIAL_ORDER_PARTS_TAX,TRIM(APPOINTMENT_DOCUMENT_ID)
                      from rydedata.pdpphdr
                      where pending_key in (
                        select distinct pending_key
                        from rydedata.pdpphdr
                        where document_type = 'RO'
                          and document_number <> ''
                          and to_date(cast(open_tran_date as varchar(8)), 'YYYYMMDD') = curdate()
                        union 
                        select distinct a.pending_key
                        from rydedata.pdppdet a
                        inner join rydedata.pdpphdr b on a.pending_key = b.pending_key
                          and b.document_type = 'RO'
                          and b.document_number <> ''
                        where a.transaction_date <> 0 
                          and to_date(cast(a.transaction_date as varchar(8)), 'YYYYMMDD') = curdate())
                  """
                cur.execute(sql)
                with open(self.extract_file, 'w') as f:
                    csv.writer(f).writerows(cur)
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute("truncate arkona.ext_pdpphdr_today")
                with open(self.extract_file, 'r') as io:
                    pg_cur.copy_expert(
                        """copy arkona.ext_pdpphdr_today from stdin with csv encoding 'latin-1 '""", io)
        print('ending ' + self.__class__.__name__ + ' at ' + '{:%Y-%m-%d-%H-%M}'.format(datetime.datetime.now()))


class ExtPdppdetToday(luigi.Task):
    """
    any pdppdet row with a transaction date today, or with an associated (pending_key) row in pdphdr with an
    open date today
    """
    extract_file = '../../extract_files/pdppdet_today.csv'
    pipeline = pipeline

    def local_target(self):
        return target_directory + target_date + '_' + self.__class__.__name__ + '.txt'

    def requires(self):
        return None

    def output(self):
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.arkona_luigi_27(db2_server) as cn:
            with cn.cursor() as cur:
                sql = """
                    select 
                        TRIM(COMPANY_NUMBER),PENDING_KEY,LINE_NUMBER,TRIM(LINE_TYPE),SEQUENCE_NUMBER,
                        TRIM(COUNTER_PERSON_ID),TRIM(TRANSACTION_CODE),TRIM(ORDER_TYPE),TRANSACTION_DATE,
                        TRIM(MANUFACTURER),TRIM(PART_NUMBER),PART_NUMBER_LENGTH,TRIM(STOCKING_GROUP),
                        TRIM(BIN_LOCATION),QUANTITY,COST,LIST_PRICE,TRADE_PRICE,FLAT_PRICE,NET_PRICE,BOOK_LIST,
                        TRIM(PRICE_METHOD),PRICE_PERCENT,TRIM(PRICE_BASE),TRIM(SPECIAL_CODE),TRIM(PRICE_OVERRIDE),
                        TRIM(GROUP_PRICE),TRIM(IS_CORE_PART),TRIM(ADD_NONSTOCK_PART),SPECIAL_ORDER_PRIORITY,
                        TRIM(ORIGINAL_SPECIAL_ORDER),TRIM(EXCLUDE_FROM_HISTORY),TRIM(COMMENTS),TRIM(LINE_STATUS),
                        TRIM(SERVICE_TYPE),TRIM(LINE_PAYMENT_METHOD),TRIM(POLICY_ADJUSTMENT),TRIM(TECHNICIAN_ID),
                        TRIM(LABOR_OPERATION_CODE),TRIM(CORRECTION_CODE),LABOR_HOURS,LABOR_COST_HOURS,
                        ACTUAL_RETAIL_AMOUNT,TRIM(FAILURE_CODE),PRICE_LEVEL,PRICE_STRATEGY_ASSIGNMENT,
                        TRIM(SUBLET_VENDOR_NUMBER),TRIM(SUBLET_INVOICE_NUMBER),DISCOUNT_KEY,ACTUAL_LABOR_HOURS,
                        TRIM(AUTHORIZATION_CODE),TRIM(BATTERY_WARRANTY_CODE),DATETIME_LINE_COMPLETED,
                        TRIM(INTERNAL_ACCOUNT),TRIM(AUTHORIZER),NET_ITEM_TOTAL,ADMIN_LABOR_AMOUNT,ADMIN_LABOR_HOURS,
                        PAINT_MATERIALS_COST,PAINT_MATERIALS_RETAIL,TRIM(REPEAT_REPAIR_FLAG),
                        TRIM(TAXABLE_INTERNAL_LINE),TRIM(UPSELL_FLAG),TRIM(WARRANTY_CLAIM_TYPE),
                        TRIM(VAT_CODE),VAT_AMOUNT,TRIM(RETURN_TO_INVENTORY),TRIM(SKILL_LEVEL),TRIM(OEM_REPLACEMENT),
                        COST_DIFFERENTIAL,OPTION_SEQUENCE,DISPATCH_START_TIME,TOTAL_DISCOUNT_LABOR,
                        TOTAL_DISCOUNT_PARTS,TRIM(PICKING_ZONE),TRIM(DISPATCH_PRIORITY_1),TRIM(DISPATCH_PRIORITY_2),
                        DISPATCH_RANK_1,DISPATCH_RANK_2,AUTO_ASSIGN_DATE,AUTO_ASSIGN_TIME,MANUAL_ASSIGN_DATE,
                        MANUAL_ASSIGN_TIME,POLICY_TAX_AMOUNT,POLICY_TAX_GROUP,KIT_TAXABLE_AMOUNT,TRIM(UPDATED_BY_OBJECT)
                    from rydedata.pdppdet 
                    where pending_key in (
                        select distinct pending_key
                        from rydedata.pdpphdr
                        where document_type = 'RO'
                            and document_number <> ''
                            and to_date(cast(open_tran_date as varchar(8)), 'YYYYMMDD') = curdate()
                        union
                        select distinct a.pending_key
                        from rydedata.pdppdet a
                        inner join rydedata.pdpphdr b on a.pending_key = b.pending_key
                            and b.document_type = 'RO'
                            and b.document_number <> ''
                         where a.transaction_date <> 0 
                             and to_date(cast(a.transaction_date as varchar(8)), 'YYYYMMDD') = curdate())
                   """
                cur.execute(sql)
                with open(self.extract_file, 'w') as f:
                    csv.writer(f).writerows(cur)
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = """
                    truncate arkona.ext_pdppdet_today;
                """
                pg_cur.execute(sql)
                with open(self.extract_file, 'r') as io:
                    pg_cur.copy_expert(
                        """copy arkona.ext_pdppdet_today from stdin with csv encoding 'latin-1 '""", io)


class ExtSdprhdrToday(luigi.Task):
    """
    any sdprhdr row with an open,close or final close date today, or with an associated (ro_number) row in sdprdet
    with a transaction date today
    """
    extract_file = '../../extract_files/sdprhdr_today.csv'
    pipeline = pipeline

    def local_target(self):
        return target_directory + target_date + '_' + self.__class__.__name__ + '.txt'

    def requires(self):
        return None

    def output(self):
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.arkona_luigi_27(db2_server) as cn:
            with cn.cursor() as cur:
                sql = """
                     select 
                         TRIM(COMPANY_NUMBER),TRIM(a.RO_NUMBER),TRIM(RO_TYPE),WARRANTY_RO_,TRIM(SERVICE_WRITER_ID),
                         TRIM(RO_TECHNICIAN_ID),CUSTOMER_KEY,TRIM(CUST_NAME),A_R_CUST_KEY,TRIM(PAYMENT_METHOD),
                         OPEN_DATE,CLOSE_DATE,FINAL_CLOSE_DATE,TRIM(VIN),TOTAL_ESTIMATE,SERV_CONT_COMP,
                         SERV_CONT_COMP_2,SERV_CONT_DEDUCT,SERV_CONT_DEDUCT2,WARRANTY_DEDUCT,TRIM(FRANCHISE_CODE),
                         ODOMETER_IN,ODOMETER_OUT,TRIM(CHECK_NUMBER),TRIM(PURCHASE_ORDER_),RECEIPT_NUMBER,PARTS_TOTAL,
                         LABOR_TOTAL,SUBLET_TOTAL,SC_DEDUCT_PAID,SERV_CONT_TOTAL,SPEC_ORD_DEPOS,CUST_PAY_HZRD_MAT,
                         CUST_PAY_SALE_TAX,CUST_PAY_SALE_TAX_2,CUST_PAY_SALE_TAX_3,CUST_PAY_SALE_TAX_4,
                         WARRANTY_SALE_TAX,WARRANTY_SALE_TAX_2,WARRANTY_SALE_TAX_3,WARRANTY_SALE_TAX_4,
                         INTERNAL_SALE_TAX,INTERNAL_SALE_TAX_2,INTERNAL_SALE_TAX_3,INTERNAL_SALE_TAX_4,
                         SVC_CONT_SALE_TAX,SVC_CONT_SALE_TAX_2,SVC_CONT_SALE_TAX_3,SVC_CONT_SALE_TAX_4,
                         CUST_PAY_SHOP_SUP,WARRANTY_SHOP_SUP,INTERNAL_SHOP_SUP,SVC_CONT_SHOP_SUP,COUPON_NUMBER,
                         COUPON_DISCOUNT,TOTAL_COUPON_DISC,TOTAL_SALE,TOTAL_GROSS,TRIM(INTERNAL_AUTH_BY),
                         TRIM(TAG_NUMBER),DATE_TIME_OF_APPOINTMENT,DATE_TIME_LAST_LINE_COMPLETE,DATE_TIME_PRE_INVOICED,
                         TRIM(TOWED_IN_INDICATOR),DOC_CREATE_TIMESTAMP,TRIM(DISPATCH_TEAM),TAX_GROUP_OVERRIDE,
                         DISCOUNT_TAXABLE_AMOUNT,PARTS_DISCOUNT_ALLOCATION_AMOUNT,LABOR_DISCOUNT_ALLOCATION_AMOUNT
                     from rydedata.sdprhdr a
                     join (
                         select trim(ro_number) as ro_number
                         from rydedata.sdprhdr 
                         where final_close_date <> 0
                             and close_date <> 0
                             and (
                               (to_date(cast(open_date as varchar(8)), 'YYYYMMDD') = curdate())  
                               or
                               (to_date(cast(close_date as varchar(8)), 'YYYYMMDD') = curdate())
                               or
                               (to_date(cast(final_close_date as varchar(8)), 'YYYYMMDD') = curdate()))
                         group by trim(ro_number)
                         union   
                         select trim(ro_number) as ro_number
                         from rydedata.sdprdet
                         where transaction_date <> 0
                             and to_date(cast(transaction_date as varchar(8)), 'YYYYMMDD') = curdate()
                         group by trim(ro_number)) b on trim(a.ro_number) = b.ro_number
                     where TRIM(a.COMPANY_NUMBER) in ('RY1','RY2')
                   """
                cur.execute(sql)
                with open(self.extract_file, 'w') as f:
                    csv.writer(f).writerows(cur)
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = """
                     truncate arkona.ext_sdprhdr_today;
                 """
                pg_cur.execute(sql)
                with open(self.extract_file, 'r') as io:
                    pg_cur.copy_expert(
                        """copy arkona.ext_sdprhdr_today from stdin with csv encoding 'latin-1 '""", io)


class ExtSdprdetToday(luigi.Task):
    """
    any sdprdet row with a transaction date today, or with an associated (ro_number) row in sdprhdr with an
    open,close or final close date today
    """
    extract_file = '../../extract_files/sdprdet_today.csv'
    pipeline = pipeline

    def local_target(self):
        return target_directory + target_date + '_' + self.__class__.__name__ + '.txt.'

    def requires(self):
        return None

    def output(self):
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.arkona_luigi_27(db2_server) as cn:
            with cn.cursor() as cur:
                sql = """
                    select 
                        TRIM(a.COMPANY_NUMBER),TRIM(a.RO_NUMBER),a.LINE_NUMBER,TRIM(a.LINE_TYPE),a.SEQUENCE_NUMBER,
                        TRIM(a.TRANSACTION_CODE),a.TRANSACTION_DATE,TRIM(a.LINE_PAYMENT_METHOD),TRIM(a.SERVICE_TYPE),
                        TRIM(a.POLICY_ADJUSTMENT),TRIM(a.TECHNICIAN_ID),TRIM(a.LABOR_OPERATION_CODE),
                        TRIM(a.CORRECTION_CODE),a.LABOR_HOURS,a.LABOR_AMOUNT,a.COST,TRIM(a.ACTUAL_RETAIL_FLAG),
                        TRIM(a.FAILURE_CODE),TRIM(a.SUBLET_VENDOR_NUMBER),TRIM(a.SUBLET_INVOICE_NUMBER),
                        a.DISCOUNT_KEY,TRIM(a.DISCOUNT_BASIS),TRIM(a.VAT_CODE),a.VAT_AMOUNT,
                        TRIM(a.DISPATCH_PRIORITY_1),TRIM(a.DISPATCH_PRIORITY_2),a.DISPATCH_RANK_1,
                        a.DISPATCH_RANK_2,a.POLICY_TAX_AMOUNT,a.KIT_TAXABLE_AMOUNT
                    from rydedata.sdprdet a
                    join (
                        select trim(ro_number) as ro_number
                        from rydedata.sdprhdr 
                        where final_close_date <> 0
                            and close_date <> 0
                            and (
                              (to_date(cast(open_date as varchar(8)), 'YYYYMMDD') = curdate())  
                              or
                              (to_date(cast(close_date as varchar(8)), 'YYYYMMDD') = curdate())
                              or
                              (to_date(cast(final_close_date as varchar(8)), 'YYYYMMDD') = curdate()))
                        group by trim(ro_number)
                        union   
                        select trim(ro_number) as ro_number
                        from rydedata.sdprdet
                        where transaction_date <> 0
                            and to_date(cast(transaction_date as varchar(8)), 'YYYYMMDD') = curdate()
                        group by trim(ro_number)) b on trim(a.ro_number) = b.ro_number
                    where TRIM(a.COMPANY_NUMBER) in ('RY1','RY2')
                   """
                cur.execute(sql)
                with open(self.extract_file, 'w') as f:
                    csv.writer(f).writerows(cur)
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = """
                    truncate arkona.ext_sdprdet_today;
                """
                pg_cur.execute(sql)
                with open(self.extract_file, 'r') as io:
                    pg_cur.copy_expert(
                        """copy arkona.ext_sdprdet_today from stdin with csv encoding 'latin-1 '""", io)


class ExtInpmastToday(luigi.Task):
    """
    any row in inpmast for a vehicle from pdpphdr or sdprhdr today
    """
    extract_file = '../../extract_files/inpmast_today.csv'
    pipeline = pipeline

    def local_target(self):
        return target_directory + target_date + '_' + self.__class__.__name__ + '.txt'

    def requires(self):
        return None

    def output(self):
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.arkona_luigi_27(db2_server) as cn:
            with cn.cursor() as cur:
                sql = """
                    SELECT TRIM(INPMAST_COMPANY_NUMBER),TRIM(INPMAST_VIN),TRIM(VIN_LAST_6),TRIM(INPMAST_STOCK_NUMBER),
                      TRIM(INPMAST_DOCUMENT_NUMBER),TRIM(STATUS),TRIM(G_L_APPLIED),TRIM(TYPE_N_U),
                      TRIM(BUS_OFF_FRAN_CODE),TRIM(SERVICE_FRAN_CODE),TRIM(MANUFACTURER_CODE),
                      TRIM(VEHICLE_CODE),YEAR,TRIM(MAKE),TRIM(MODEL_CODE),TRIM(MODEL),TRIM(BODY_STYLE),
                      TRIM(COLOR),TRIM(TRIM),TRIM(FUEL_TYPE),TRIM(MPG),CYLINDERS,TRIM(TRUCK),TRIM(WHEEL_DRIVE4WD),
                      TRIM(TURBO),TRIM(COLOR_CODE),TRIM(ENGINE_CODE),TRIM(TRANSMISSION_CODE),TRIM(IGNITION_KEY_CODE),
                      TRIM(TRUNK_KEY_CODE),TRIM(KEYLESS_CODE),TRIM(RADIO_CODE),TRIM(WHEEL_LOCK_CODE),
                      TRIM(DEALER_CODE),TRIM(LOCATION),ODOMETER,DATE_IN_INVENT,DATE_IN_SERVICE,DATE_DELIVERED,
                      DATE_ORDERED,TRIM(INPMAST_SALE_ACCOUNT),TRIM(INVENTORY_ACCOUNT),TRIM(DEMO_NAME),
                      WARRANTY_MONTHS,WARRANTY_MILES,WARRANTY_DEDUCT,LIST_PRICE,INPMAST_VEHICLE_COST,
                      TRIM(OPTION_PACKAGE),TRIM(LICENSE_NUMBER),GROSS_WEIGHT,WORK_IN_PROCESS,INSPECTION_MONTH,
                      TRIM(ODOMETER_ACTUAL),BOPNAME_KEY,TRIM(KEY_TO_CAP_EXPLOSION_DATA),TRIM(CO2_EMISSION_CODE2),
                      REGISTRATION_DATE1,FUNDING_EXPIRATION_DATE2,INSPECTION_DATE3,TRIM(DRIVERS_SIDE),
                      FREE_FLOORING_PERIOD,TRIM(ORDERED_STATUS),TRIM(PUBLISH_VEHICLE_INFO_TO_WEB),TRIM(SALE),
                      TRIM(CERTIFIED_USED_CAR),LAST_SERVICE_DATE4,NEXT_SERVICE_DATE5,DEALER_CODE_IMDLRCD,
                      COMMON_VEHICLE_ID,CHROME_STYLE_ID,CREATED_USER_CODE,UPDATED_USER_CODE,CREATED_TIMESTAMP,
                      UPDATED_TIMESTAMP,TRIM(STOCKED_COMPANY),ENGINE_HOURS
                    FROM  rydedata.inpmast
                    where inpmast_company_number = 'RY1'
                        and trim(inpmast_vin) in (
                            select trim(vin)
                            from rydedata.pdpphdr
                            where document_type = 'RO'
                                and document_number <> ''
                                and open_tran_date <> 0 
                                and to_date(cast(open_tran_date as varchar(8)), 'YYYYMMDD') = curdate()
                            union
                            select trim(vin) as vin
                            from rydedata.sdprhdr 
                            where final_close_date <> 0
                                and close_date <> 0
                                and (
                                    (to_date(cast(open_date as varchar(8)), 'YYYYMMDD') = curdate())  
                                    or
                                    (to_date(cast(close_date as varchar(8)), 'YYYYMMDD') = curdate())
                                    or
                                    (to_date(cast(final_close_date as varchar(8)), 'YYYYMMDD') = curdate()))
                            group by trim(vin)) 
                """
                cur.execute(sql)
                with open(self.extract_file, 'w') as f:
                    csv.writer(f).writerows(cur)
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute("truncate arkona.ext_inpmast_today")
                with open(self.extract_file, 'r') as io:
                    pg_cur.copy_expert(
                        """copy arkona.ext_inpmast_today from stdin with csv encoding 'latin-1 '""", io)


class DimVehicleUpdateToday(luigi.Task):
    """
    adds vehicles to dds.dim_vehicle from arkona.ext_inpmast_today
    """
    pipeline = pipeline

    def local_target(self):
        return target_directory + target_date + '_' + self.__class__.__name__ + '.txt'

    def requires(self):
        yield ExtInpmastToday()

    def output(self):
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = """ select dds.dim_vehicle_update_today(); """
                pg_cur.execute(sql)


class ExtBopnameToday(luigi.Task):
    """
    any row in bopname with timestamp_updated today
    """
    extract_file = '../../extract_files/bopname_today.csv'
    pipeline = pipeline

    def local_target(self):
        return target_directory + target_date + '_' + self.__class__.__name__ + '.txt'

    def requires(self):
        return None

    def output(self):
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.arkona_luigi_27(db2_server) as cn:
            with cn.cursor() as cur:
                sql = """
                    SELECT 
                        TRIM(BOPNAME_COMPANY_NUMBER),BOPNAME_RECORD_KEY,TRIM(COMPANY_INDIVID),SOC_SEC_FED_ID_,
                        TRIM(BOPNAME_SEARCH_NAME),TRIM(LAST_COMPANY_NAME),TRIM(FIRST_NAME),TRIM(MIDDLE_INIT),
                        TRIM(SALUTATION),TRIM(GENDER),TRIM(LANGUAGE),TRIM(ADDRESS_1),TRIM(ADDRESS_2),TRIM(CITY),
                        TRIM(COUNTY),TRIM(STATE_CODE),ZIP_CODE,PHONE_NUMBER,BUSINESS_PHONE,BUSINESS_EXT,
                        FAX_NUMBER,BIRTH_DATE,TRIM(DRIVERS_LICENSE),TRIM(CONTACT),TRIM(PREFERRED_CONTACT),
                        TRIM(MAIL_CODE),TRIM(TAX_EXMPT_NO_),TRIM(ASSIGNED_SLSP),TRIM(CUSTOMER_TYPE),
                        TRIM(PREFERRED_PHONE),CELL_PHONE,PAGE_PHONE,OTHER_PHONE,TRIM(OTHER_PHONE_DESC),
                        TRIM(EMAIL_ADDRESS),TRIM(OPTIONAL_FIELD),TRIM(ALLOW_CONTACT_BY_POSTAL),
                        TRIM(ALLOW_CONTACT_BY_PHONE),TRIM(ALLOW_CONTACT_BY_EMAIL),TRIM(ADDRESS_LINE_3),
                        TRIM(BUSINESS_PHONE_EXTENSION),TRIM(INTERNATIONAL_BUSINESS_PHONE),LAST_CHANGE_DATE,
                        TRIM(INTERNATIONAL_CELL_PHONE),EXTERNAL_CROSS_REFERENCE_KEY,TRIM(SECOND_EMAIL_ADDRESS2),
                        TRIM(INTERNATIONAL_FAX_NUMBER),TRIM(INTERNATIONAL_OTHER_PHONE),
                        TRIM(INTERNATIONAL_HOME_PHONE),TRIM(CUSTOMER_PREFERRED_NAME),
                        TRIM(INTERNATIONAL_PAGER_PHONE),TRIM(PREFERRED_LANGUAGE),TRIM(INTERNATIONAL_ZIP_CODE),
                        TRIM(TOKEN_ID),TRIM(COMMON_CUSTOMER_ID),TRIM(SOUNDEX_FIRST_NAME),TRIM(SOUNDEX_LAST_NAME),
                        TRIM(SOUNDEX_ADDRESS1),TRIM(OPTIONAL_FIELD_2),TRIM(TAX_EXMPT_NO_2),TRIM(TAX_EXMPT_NO_3),
                        TRIM(TAX_EXMPT_NO_4),TRIM(GST_REGISTRANT_),PST_EXMPT_DATE,DEALER_CODE,CC_ID,
                        CUSTOMER_VERSION,ADDRESS_NUMBER,ADDRESS_VERSION,CREATED_BY_USER,TIMESTAMP_CREATED,
                        UPDATED_BY_USER,TIMESTAMP_UPDATED,TRIM(COUTRY_CODE),TRIM(DL_STATE),
                        TRIM(OPENTRACK_PARTNER_ID),TRIM(STATUS),TRIM(SHARE_INFO)
                    from rydedata.bopname
                    where TRIM(BOPNAME_COMPANY_NUMBER) in ('RY1','RY2')
                        and date(timestamp_updated) = curdate()
                """
                cur.execute(sql)
                with open(self.extract_file, 'w') as f:
                    csv.writer(f).writerows(cur)
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute("truncate arkona.ext_bopname_today")
                with open(self.extract_file, 'r') as io:
                    pg_cur.copy_expert(
                        """copy arkona.ext_bopname_today from stdin with csv encoding 'latin-1 '""", io)


class DimCustomerUpdateToday(luigi.Task):
    """
    adds rows to dds.dim_customer from arkona.ext_bopname_today
    """
    pipeline = pipeline

    def local_target(self):
        return target_directory + target_date + '_' + self.__class__.__name__ + '.txt'

    def requires(self):
        yield ExtBopnameToday()

    def output(self):
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = """ select dds.dim_customer_update_today(); """
                pg_cur.execute(sql)


class FactRepairOrderToday(luigi.Task):
    """
    """
    pipeline = pipeline

    def local_target(self):
        return target_directory + target_date_with_minute + '_' + self.__class__.__name__ + '.txt'

    def requires(self):
        yield ExtSdprhdrToday()
        yield ExtSdprdetToday()
        yield ExtPdpphdrToday()
        yield ExtPdppdetToday()
        yield DimVehicleUpdateToday()
        yield DimCustomerUpdateToday()

    def output(self):
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute("truncate dds.fact_repair_order_today")
                sql = "select dds.fact_repair_order_today_update()"
                pg_cur.execute(sql)
        for filename in os.listdir(target_directory):
            os.unlink(target_directory + filename)


if __name__ == '__main__':
    luigi.run(["--workers=6"], main_task_cls=FactRepairOrderToday)
