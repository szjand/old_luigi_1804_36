# encoding=utf-8
"""
"""
# todo UcXfmInpmast is not idempotent, first complete run updates changes based on diff between ext and xfm,
# todo after some time passes, and changes are made to inpmast, second can generate changes for some of the
# todo same vehicles which would result in attempting to set row_thru_date of multiple instances
# todo of the same vin to current_date - 1
import utilities
import luigi
import datetime
import fact_gl

pipeline = 'used_cars'
db2_server = 'report'
pg_server = '173'
ts_no_spaces = str(datetime.datetime.now()).replace(" ", "")


class UcMakesModels(luigi.Task):
    """

    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                # ads requirement
                pg_cur.execute("select ops.check_for_ads_extract('uc_inventory_ext_make_model_classifications')")
                if not pg_cur.fetchone()[0]:
                    raise ValueError('The ads requirement: uc_inventory_ext_make_model_classifications is missing')
                pg_cur.execute("select greg.update_uc_makes_models()")


class UcBaseVehicles(luigi.Task):
    """

    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        # define output filename and pathr
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                # ads requirement
                pg_cur.execute("select ops.check_for_ads_extract('uc_inventory_ext_vehicle_items')")
                if not pg_cur.fetchone()[0]:
                    raise ValueError('The ads requirement: uc_inventory_ext_vehicle_items is missing')
                pg_cur.execute("select ops.check_for_ads_extract('uc_inventory_ext_vehicle_inventory_items')")
                if not pg_cur.fetchone()[0]:
                    raise ValueError('The ads requirement: uc_inventory_ext_vehicle_inventory_items is missing')
                pg_cur.execute("select ops.check_for_ads_extract('uc_inventory_ext_vehicle_sales')")
                if not pg_cur.fetchone()[0]:
                    raise ValueError('The ads requirement: uc_inventory_ext_vehicle_sales is missing')
                pg_cur.execute("select greg.update_uc_base_vehicles()")


class UcBaseVehicleDays(luigi.Task):
    """

    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield UcBaseVehicles()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute("select greg.update_uc_base_vehicle_days()")


class UcBaseVehiclePrices(luigi.Task):
    """

    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield UcBaseVehicles()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                # ads requirement
                pg_cur.execute("select ops.check_for_ads_extract('uc_inventory_ext_vehicle_pricings')")
                if not pg_cur.fetchone()[0]:
                    raise ValueError('The ads requirement: uc_inventory_ext_vehicle_pricings is missing')
                pg_cur.execute("select ops.check_for_ads_extract('uc_inventory_ext_vehicle_pricing_details')")
                if not pg_cur.fetchone()[0]:
                    raise ValueError('The ads requirement: uc_inventory_ext_vehicle_pricing_details is missing')
                pg_cur.execute("select greg.update_uc_base_vehicle_prices()")


class UcBaseVehicleMiles(luigi.Task):
    """

    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield UcBaseVehicles()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                # ads requirement
                pg_cur.execute("select ops.check_for_ads_extract('uc_inventory_ext_vehicle_item_mileages')")
                if not pg_cur.fetchone()[0]:
                    raise ValueError('The ads requirement: uc_inventory_ext_vehicle_item_mileages is missing')
                pg_cur.execute("select greg.update_uc_base_vehicle_miles()")


class UcBaseVehicleStatuses(luigi.Task):
    """

    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield UcBaseVehicleDays()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                # ads requirement
                pg_cur.execute("select ops.check_for_ads_extract('uc_inventory_ext_vehicle_inventory_item_statuses')")
                if not pg_cur.fetchone()[0]:
                    raise ValueError('The ads requirement: uc_inventory_ext_vehicle_inventory_item_statuses is missing')
                pg_cur.execute("select greg.update_uc_base_vehicle_statuses()")


class UcBaseVehicleAccounting(luigi.Task):
    """

    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield fact_gl.FactGl()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute("select greg.update_uc_base_vehicle_accounting()")


class UcReconRos(luigi.Task):
    """

    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield fact_gl.FactGl()
        yield UcBaseVehicles()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute("select greg.update_uc_recon_ros()")


class UcReconDetails(luigi.Task):
    """

    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield UcReconRos()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute("select greg.update_uc_recon_details()")


class UcReconByDay(luigi.Task):
    """

    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield UcReconDetails()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute("select greg.update_uc_recon_by_day()")


class DailySnapshotBeta1(luigi.Task):
    """
    replaces the wrapper task UsedCars in the sense of the requires
    all classes in this module have to pass for this one to run
    called from run_all
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield UcMakesModels()
        yield UcBaseVehiclePrices()
        yield UcBaseVehicleMiles()
        yield UcBaseVehicleStatuses()
        yield UcBaseVehicleAccounting()
        yield UcReconByDay()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute("select greg.update_daily_snapshot_beta_1()")
