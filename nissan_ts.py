# encoding=utf-8
"""
just like global connect, but for nissan
03/29/20
    don't need summary
05/08/28
    prefix local target name with pipeline (see comments in honda.py header)
"""

import utilities
import luigi
import datetime
import os
from selenium import webdriver
import time
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.common.exceptions import ElementNotVisibleException, TimeoutException, ElementNotInteractableException


pipeline = 'nissan'
pg_server = '173'
ts_no_spaces = str(datetime.datetime.now()).replace(" ", "")
user = 'XD163692'
password = '%6#@%!!@eDHN'


# # if these task are being called via run_all, that task will handle all of this logging
@luigi.Task.event_handler(luigi.Event.START)
def task_start(self):
    utilities.luigi_log_start(pg_server, self.task_id + '__' + ts_no_spaces, self.pipeline, self.__class__.__name__)


@luigi.Task.event_handler(luigi.Event.SUCCESS)
def task_success(self):
    utilities.luigi_log_pass(pg_server, self.task_id + '__' + ts_no_spaces, self.pipeline, self.__class__.__name__)
    try:  # WrapperTest throws an AttributeError, see /Notes/issues
        with self.output().open("w") as out_file:
            # write to the output file
            out_file.write('pass')
    except AttributeError:
        pass


class VehicleInventoryDetailDownload(luigi.Task):
    """
    04/06
    failed on production, elements not discoveralbe
    installed pyvirtualdisplay
    wrapped run in:
    with Display(visible=False, size=(1200,1500)):
    https://stackoverflow.com/questions/45520464/selenium-script-working-from-console-not-working-
        in-cron-geckodriver-error
    05/22: failing last couple of days, there are 109 vehicles and each time it seems to fail at or around
        101. mixed results on the actual error being generated. today (from production) it was:
          File "/home/rydell/projects/luigi_1804_36/nissan.py", line 260, in run
            WebDriverWait(driver, 20).until(ec.number_of_windows_to_be(2))
            raise TimeoutException(message, screen, stacktrace) selenium.common.exceptions.TimeoutException
        at seq 100 the fucking page resets to the default presentation (filttered location, 1 of 67)
            this makes no sense, can't implement a continue on fail, because the state of the page
            is no longer where it was
        try to stop at seq 100 and manually step thru
        ok, this is weird, stopped at 100, stepped thru, when got to
            WebDriverWait(driver, 20).until(ec.number_of_windows_to_be(2))
        the fucking page reset instead of opening the invoice window
        have to look and see if i can see what the click is actually doing
        forced idx = idx + 99
        stepped thru from 99 and it worked ok, including 100
    08/31/2020
        trying to eliminate the semi-frequent errors that only seem to occur in production
        Name: VehicleInventoryDetailDownload
          File "/home/rydell/projects/luigi_1804_36/nissan.py", line 258, in run
            driver.find_element_by_id(button_id).click()
        selenium.common.exceptions.ElementNotInteractableException: Message: element not interactable
        the (button_id).click() line is actually 279, one of these messages could be a red herring
        since the error ElementNotInteractableException
        the (button_id).click() is wrapped in a try except, added ElementNotInteractableException to the except clause
    11/23/2020
        detail download is failing quietly, there are currently 175 vehicles on the page after selecting
        Records per Page = All, but generating the count for enumerating it is 50, so after 50
        vehicles it gracefully proceeds, thus leaving out 125 vehicles
        All is orderly, all the identifiers work, the issue is the count only
        Changed the ieteration to be based on the row count total on the page
        That got me to 100, where once again it reset


    """
    pipeline = pipeline
    # # local
    pdf_dir = '/home/jon/projects/luigi_1804_36/nissan/vehicle_inventory_detail/pdf_files/'

    # # production
    # pdf_dir = '/home/rydell/projects/luigi_1804_36/nissan/vehicle_inventory_detail/pdf_files/'

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                pipeline + '_' + self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def setup_driver(self):
        # # Chromedriver path
        chromedriver_path = '/usr/bin/chromedriver'
        options = webdriver.ChromeOptions()
        profile = {"download.default_directory": self.pdf_dir}
        options.add_experimental_option("prefs", profile)
        options.add_argument('--user-agent="Mozilla/5.0 ((Windows NT 10.0; Win64; x64) AppleWebKit/537.36 '
                             '(KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36"')
        driver = webdriver.Chrome(executable_path=chromedriver_path, options=options)
        return driver

    def run(self):
        # with Display(visible=False, size=(1200, 1500)):
        driver = self.setup_driver()
        try:
            driver.get('https://as.na.nissan.biz/SecureAuth71/')
            # SIGNING IN
            driver.find_element_by_class_name('txtUserid').send_keys(user)
            driver.find_element_by_class_name('tbxPassword').send_keys(password)
            driver.find_element_by_class_name('btn-secureauth').click()
            # Click MY LINKS
            WebDriverWait(driver, 10).until(ec.element_to_be_clickable(('id', '1a'))).click()
            # wait until page finishes loading
            WebDriverWait(driver, 10).until(ec.presence_of_element_located(
                ('xpath', '//*[@id="linklisting-containerID"]/div[5]/div/ul/li[1]/a')))
            # load the Nissan DBS page
            driver.get('https://www.nnanet.com/content/nnanet/global/secure/viewlink.link001203.html')
            # load the inventory page
            driver.get('https://dbsapp.nnanet.com/dbs/vehicle/inventory/list?execution=e6s1')
            # select All Locations (statuses)
            WebDriverWait(driver, 10).until(ec.presence_of_element_located(
                ('id', 'f1:filterValueLocation'))).click()
            WebDriverWait(driver, 10).until(ec.presence_of_element_located(
                ('xpath', '//*[@id="f1:filterValueLocation"]/option[1]'))).click()
            time.sleep(10)
            # 100 records per page
            driver.find_element_by_xpath('//*[@id="f1:selectPageNumber_newVehicleTable"]').click()
            driver.find_element_by_xpath('//*[@id="f1:selectPageNumber_newVehicleTable"]/option[3]').click()
            # allow all rows to display
            time.sleep(10)
            # empty the pdf_dir
            for filename in os.listdir(self.pdf_dir):
                os.unlink(self.pdf_dir + filename)
            # total number from bottom left of page
            vehicle_count = int(driver.find_element_by_xpath('//*[@id="f1:newVehicleTable:newCount"]').text)
            print(str(vehicle_count) + ': vehicles')
            pages = 0
            rows = 0
            if vehicle_count < 101:
                pages = 1
            elif vehicle_count < 201:
                pages = 2
            elif vehicle_count < 301:
                pages = 3
            # be explicit about range: loops thru min value thru max value - 1
            page_range = pages + 1
            for page in range(1, page_range):
                if page == 1 and pages == 1:
                    rows = vehicle_count
                if page == 1 and pages > 1:
                    rows = 100
                if page == 2 and pages == 2:
                    rows = vehicle_count - 100
                if page == 2 and pages > 2:
                    rows = 100
                if page == 3:
                    rows = vehicle_count - 200
                if page != 1:
                    # damnit, in the wild when it reset page number it also reset status, so, display all then page #
                    # select All Locations (statuses)
                    WebDriverWait(driver, 10).until(ec.presence_of_element_located(
                        ('id', 'f1:filterValueLocation'))).click()
                    WebDriverWait(driver, 10).until(ec.presence_of_element_located(
                        ('xpath', '//*[@id="f1:filterValueLocation"]/option[1]'))).click()
                    time.sleep(10)
                    # the above did not stop it from changing back AGAIN
                    # so go thru the whole sequence, select all statuses, 100 items, then change page number
                    # it might be the fucking page number changing the statuses ????
                    # FUCK in production, this appears to be the case
                    # 100 records per page
                    driver.find_element_by_xpath('//*[@id="f1:selectPageNumber_newVehicleTable"]').click()
                    driver.find_element_by_xpath('//*[@id="f1:selectPageNumber_newVehicleTable"]/option[3]').click()
                    # allow all rows to display
                    time.sleep(10)
                    # enter go to page
                    driver.find_element_by_xpath('//*[@id="f1:inputPage"]').send_keys(page)
                    # click Go button
                    WebDriverWait(driver, 10).until(ec.element_to_be_clickable(
                        ('xpath', '//*[@id="f1:j_id501"]'))).click()
                    # refreshing the page after change the page number takes an indeterminately long time
                    time.sleep(20)
                min_value = 0
                # # min_value proved to be helpful in troubleshooting
                # if page == 1:
                #     min_value = 98
                # elif page == 2:
                #     min_value = 0
                # # the indexing on the page for rows is 0 based and therefore matches range functionality
                # # eg, 75 rows is sequenced as 0 thru 74
                for idx in range(min_value, rows):
                    print(str(page) + ' of ' + str(pages) + ' pages')
                    print(str(idx + 1) + ' of ' + str(rows) + ' rows')
                    print(datetime.datetime.now())
                    td_id = 'f1:newVehicleTable:%s:actionMenu' % idx
                    td = driver.find_element_by_id(td_id)
                    td.click()
                    button_id = 'f1:newVehicleTable:%s:showVIDetailActionLink' % idx
                    # click on Vehicle Details
                    try:
                        driver.find_element_by_id(button_id).click()
                    except ElementNotVisibleException or ElementNotInteractableException:
                        continue
                    # wait for detail window to open
                    try:
                        WebDriverWait(driver, 20).until(ec.number_of_windows_to_be(2))
                    except TimeoutException:
                        continue
                    driver.switch_to.window(driver.window_handles[1])
                    # click Print, downloads the PDF
                    driver.find_element_by_xpath('//*[@id="f2:comPrintIcon"]').click()
                    driver.close()
                    driver.switch_to.window(driver.window_handles[0])
            # verify that all files downloaded
            assert len(os.listdir(self.pdf_dir)) == vehicle_count
        finally:
            driver.quit()


if __name__ == '__main__':
    luigi.run(["--workers=1"], main_task_cls=VehicleInventoryDetailDownload)
