# encoding=utf-8
"""
jo.vehicle_orders_unavailable: orders that are no longer accessible thru global connect
1/26/21: scope (indentation) was wrong on the call to select jo.update_vehicle_order_events(), so it
    probably never ran

"""
from selenium import webdriver
import os
import utilities
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import TimeoutException
import datetime
import time
import csv
import luigi
from pathlib import Path
from bs4 import BeautifulSoup
import requests


pipeline = 'global_connect'
pg_server = '173'
ts_no_spaces = str(datetime.datetime.now()).replace(" ", "")

user = '41d9ec15-5148-439d-938a-313a1fa91cc6'
passcode = '666'
username = None
password = None


# # if these task are being called via run_all, that task will handle all of this logging
@luigi.Task.event_handler(luigi.Event.START)
def task_start(self):
    utilities.luigi_log_start(pg_server, self.task_id + '__' + ts_no_spaces, self.pipeline, self.__class__.__name__)


@luigi.Task.event_handler(luigi.Event.SUCCESS)
def task_success(self):
    utilities.luigi_log_pass(pg_server, self.task_id + '__' + ts_no_spaces, self.pipeline, self.__class__.__name__)
    try:  # WrapperTest throws an AttributeError, see /Notes/issues
        with self.output().open("w") as out_file:
            # write to the output file
            out_file.write('pass')
    except AttributeError:
        pass


class Orders1(luigi.Task):
    """
    it appears that this script is generating access denied from global connect across the dealership
    insert a delay before each request
    03/07/20 start with 2 sec
    03/13/20 changed to 1 sec
    """
    pipeline = pipeline
    html_path = '/home/jon/projects/luigi_1804_36/global_connect/html_files/'
    orders_not_found_path = '/home/jon/projects/luigi_1804_36/global_connect/orders_not_found/'

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    @staticmethod
    def setup_driver():
        # # Chromedriver path
        chromedriver_path = '/usr/bin/chromedriver'
        options = webdriver.ChromeOptions()
        options.add_argument('--user-agent="Mozilla/5.0 ((Windows NT 10.0; Win64; x64) AppleWebKit/537.36 '
                             '(KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36"')
        driver = webdriver.Chrome(executable_path=chromedriver_path, options=options)
        return driver

    @staticmethod
    def get_secret(category, platform):
        modified_platform = platform.replace(' ', '+')
        url = ("https://beta.rydellvision.com:8888/vision/secret?platform={0}&category={1}".format
               (modified_platform, category))
        response = requests.get(url, auth=(user, passcode))
        return response.json()

    @staticmethod
    def login(self, driver):
        credentials = self.get_secret('Cartiva', 'Global Connect')
        resp_dict = credentials
        username_box = driver.find_element_by_id('IDToken1')
        password_box = driver.find_element_by_id('IDToken2')
        login_btn = driver.find_element_by_name('Login.Submit')
        username_box.send_keys(resp_dict.get('username'))
        password_box.send_keys(resp_dict.get('secret'))
        login_btn.click()
        WebDriverWait(driver, 5).until(ec.presence_of_element_located(('id', 'sbut')))

    def get_files(self, driver):
        with utilities.pg(pg_server) as database:
            with database.cursor() as order_cursor:
                # update jo.vehicle_orders with data from gmgl.vehicle_orders
                sql = """
                    insert into jo.vehicle_orders
                    select *
                    from gmgl.vehicle_orders a  
                    where  not exists (-- exclude orders_unavailable
                      select 1
                      from jo.vehicle_orders_unavailable
                      where order_number = a.order_number)
                    on conflict (order_number)
                    do update
                      set (order_type,msrp_dfc,tpw,vin,age_of_inventory,model_year,alloc_group,
                            vehicle_trim,color,make,model_code,trade,assigned,gm_config_id,
                            dan,est_delivery_date,secondary_color,changed,stock_number,peg,
                            primary_fan,end_user_fan,po_number,ship_to_bac,ship_to_bfc,
                            request_id,customer_name,gm_stored_config_description)
                        = (excluded.order_type,excluded.msrp_dfc,
                            coalesce(excluded.tpw, vehicle_orders.tpw),
                            excluded.vin,
                            excluded.age_of_inventory,excluded.model_year,excluded.alloc_group,
                            excluded.vehicle_trim,excluded.color,excluded.make,excluded.model_code,
                            excluded.trade,excluded.assigned,excluded.gm_config_id,excluded.dan,
                            excluded.est_delivery_date,excluded.secondary_color,excluded.changed,
                            excluded.stock_number,excluded.peg,excluded.primary_fan,
                            excluded.end_user_fan,excluded.po_number,excluded.ship_to_bac,
                            excluded.ship_to_bfc,excluded.request_id,excluded.customer_name,
                            excluded.gm_stored_config_description);                
                """
                order_cursor.execute(sql)
                database.commit()
                # sql = "select 'XQKR4C' union select 'wbvw15'"
                sql = """
                    select distinct a.order_number
                    from jo.vehicle_orders a
                    left join jo.vehicle_orders_unavailable b on a.order_number = b.order_number
                    left join nc.vehicle_acquisitions c on a.vin = c.vin
                    where b.order_number is null
                      and c.vin is null
                    union
                    select distinct a.order_number
                    from jo.vehicle_orders a
                    left join jo.vehicle_orders_unavailable b on a.order_number = b.order_number
                    where a.order_number in (
                      select distinct order_number  
                      from jo.vehicle_order_events a
                      where not exists (
                        select 1
                        from jo.vehicle_order_events
                        where order_number = a.order_number
                          and left(event_code, 1) in ('6')))
                    and b.order_number is null
                """
                order_cursor.execute(sql)
                for order_number in order_cursor:
                    order_number = str(order_number[0])
                    print(order_number)
                    time.sleep(1)
                    #  bypass the order search page, go straight to Order Detail page
                    driver.get('https://www.autopartners.net/apps/naowb/naowb/manageinventory/'
                               'mi_05.do?method=getOrderList&pageName=mi_07&modName=mi&ORDNUM=' + order_number +
                               '&searchByValue=VehicleOrderNumber&orderNums=' + order_number)
                    try:
                        history_link = WebDriverWait(driver, 2).until(
                            ec.presence_of_element_located(('xpath', '//*[@id="orderDetailForm"]/table/tbody/tr[7]/'
                                                                     'td[2]/table[2]/tbody/tr[3]/td[1]/table/'
                                                                     'tbody/tr/td[4]/a')))
                    except NoSuchElementException:
                        continue
                    except TimeoutException:
                        try:
                            WebDriverWait(driver, 5).until(
                                ec.presence_of_element_located
                                (('xpath', '//*[@id="bodyRow"]/td/table[2]/tbody/tr[5]/td[2]/table')))
                        except TimeoutException:
                            continue
                        with open(self.orders_not_found_path + order_number + '.txt', 'w') as f:
                            f.write(order_number + '.txt')
                        continue
                    history_link.click()
                    try:
                        table = WebDriverWait(driver, 5).until(
                            ec.presence_of_element_located(('xpath', '//*[@id="bodyRow"]/td/table/tbody/tr[9]/td[2]/'
                                                            'table[2]/tbody')))
                    except NoSuchElementException:
                        continue
                    except TimeoutException:
                        continue
                    with open(self.html_path + order_number + '.html', 'w') as f:
                        f.write(table.get_attribute('innerHTML'))

    def run(self):
        #  empty html and not found orders directories
        for filename in os.listdir(self.orders_not_found_path):
            os.unlink(self.orders_not_found_path + filename)
        for filename in os.listdir(self.html_path):
            os.unlink(self.html_path + filename)
        driver = self.setup_driver()
        try:
            # change login to order main page
            driver.get('https://www.autopartners.net/apps/naowb/naowb/globalelements/login.do')
            self.login(self, driver)
            self.get_files(driver)
        finally:
            driver.quit()


class VehicleOrderEvents(luigi.Task):
    """

    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield Orders1()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        html_path = Path.home() / 'projects' / 'luigi_1804_36' / 'global_connect' / 'html_files'
        not_found_path = Path.home() / 'projects' / 'luigi_1804_36' / 'global_connect' / 'orders_not_found'
        html_to_csv = Path.home() / 'projects' / 'luigi_1804_36' / 'global_connect' / 'html_to_csv' / 'html.csv'
        html_files = (entry for entry in html_path.iterdir() if entry.is_file())
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                for file in html_files:
                    order_number = file.stem
                    page = open(str(file))
                    soup = BeautifulSoup(page.read(), "lxml")
                    new_rows = []
                    # only those rows with a class of greengray or white, eg data
                    for row in soup.find_all('tr', {'class': ["greengray", "white"]}):
                        # new_rows.append([val.text.strip() for val in row.find_all('td')])
                        # exclude the up and down arrow columns, width = 4
                        # to skip the first empty column had to tinker with [1:9] doesn't make sense but it works
                        # strip = trim
                        # select was required instead of find or findAll, has something to do with Soup Sieve
                        new_rows.append([val.text.strip() for val in row.select('td[width!="4"]')[1:9]])
                    for row in new_rows:
                        #  insert the order_number as the first element in the list
                        row.insert(0, order_number)
                    with html_to_csv.open('w') as f:
                        writer = csv.writer(f)
                        writer.writerows(row for row in new_rows if len(row) > 1)
                    with html_to_csv.open('r') as io:
                        pg_cur.copy_expert(
                            """copy jo.ext_vehicle_order_events from stdin with csv encoding 'latin-1 '""", io)
                not_found_files = (entry for entry in not_found_path.iterdir() if entry.is_file())
                for file in not_found_files:
                    sql = """
                        insert into jo.ext_vehicle_orders_unavailable(order_number)
                        values ('{}');
                        
                    """.format(file.stem)
                    pg_cur.execute(sql)
                    pg_con.commit()
                    sql = """
                        select jo.update_vehicle_orders_unavailable();
                    """
                    pg_cur.execute(sql)
                sql = """
                    select jo.update_vehicle_order_events();
                """
                pg_cur.execute(sql)


if __name__ == '__main__':
    luigi.run(["--workers=4"], main_task_cls=VehicleOrderEvents)
