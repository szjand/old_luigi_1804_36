# encoding=utf-8
"""
this is developing into the page where i will download the history
uhoh, for August it selected July 26 - 31 same link text, different class

09/03/20
finally able to click the correct date when the same date from 2 months show on the calendar
https://www.lambdatest.com/blog/how-to-automate-calendar-using-selenium-webdriver-for-testing/
            elem_date = driver.find_element_by_xpath(
                "//td[not(contains(@class,'ui-datepicker-other-month'))]/a[text()='" + self.the_day + "']")
            elem_date.click()

incorporate the download and parsing all in one function
one date at a time, beginning to end
"""

import utilities
import luigi
import datetime
from selenium import webdriver
import time
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.by import By
import requests
import xlrd
import csv
import os
from datetime import date, timedelta


pipeline = 'dfx'
pg_server = '173'
ts_no_spaces = str(datetime.datetime.now()).replace(" ", "")
ts_with_minute = '{:%Y-%m-%d-%H-%M}'.format(datetime.datetime.now())
user = '41d9ec15-5148-439d-938a-313a1fa91cc6'
passcode = '666'
username = None
password = None

# local_or_production = 'production'
local_or_production = 'local'


# # if these task are being called via run_all, that task will handle all of this logging
@luigi.Task.event_handler(luigi.Event.START)
def task_start(self):
    utilities.luigi_log_start(pg_server, self.task_id + '__' + ts_no_spaces, self.pipeline, self.__class__.__name__)


@luigi.Task.event_handler(luigi.Event.SUCCESS)
def task_success(self):
    utilities.luigi_log_pass(pg_server, self.task_id + '__' + ts_no_spaces, self.pipeline, self.__class__.__name__)
    try:  # WrapperTest throws an AttributeError, see /Notes/issues
        with self.output().open("w") as out_file:
            # write to the output file
            out_file.write('pass')
    except AttributeError:
        pass


class DynamicMPIUsageReportHistory(luigi.Task):
    """
    https://stackoverflow.com/questions/49747034/select-from-a-datepicker-in-python
    """
    pipeline = pipeline

    if local_or_production == 'local':
        download_dir = '/home/jon/projects/luigi_1804_36/dealerfx/download/dynamic_mpi_usage_report/'
    elif local_or_production == 'production':
        download_dir = '/home/rydell/projects/luigi_1804_36/dealerfx/download/dynamic_mpi_usage_report/'


    start_date = date(2020, 9, 3)
    end_date = date(2020, 9, 3)
    delta = timedelta(days=1)
    the_year = None
    the_month = None
    the_day = None

    def local_target(self):
        # using target name with minute allows frequent rerunning
        return '../../luigi_output_files/' + ts_with_minute + '_' + self.__class__.__name__ + ".txt"

    def requires(self):
        return None

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    @staticmethod
    def get_secret(category, platform):
        modified_platform = platform.replace(' ', '+')
        url = ("https://beta.rydellvision.com:8888/vision/secret?platform={0}&category={1}".format
               (modified_platform, category))
        response = requests.get(url, auth=(user, passcode))
        return response.json()

    def setup_driver(self):
        # # Chromedriver path
        chromedriver_path = '/usr/bin/chromedriver'
        options = webdriver.ChromeOptions()
        profile = {"download.default_directory": self.download_dir}
        options.add_experimental_option("prefs", profile)
        options.add_argument('--user-agent="Mozilla/5.0 ((Windows NT 10.0; Win64; x64) AppleWebKit/537.36 '
                             '(KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36"')
        driver = webdriver.Chrome(executable_path=chromedriver_path, options=options)
        return driver

    @staticmethod
    def login(self, driver):
        # get username and password fields
        credentials = self.get_secret('Cartiva', 'DealerFx')
        resp_dict = credentials
        username_box = driver.find_element_by_name('txtFrameworkUserName')
        password_box = driver.find_element_by_name('txtFrameworkPassword')
        login_btn = driver.find_element_by_name('btnDoLogin')
        username_box.send_keys(resp_dict.get('username'))
        password_box.send_keys(resp_dict.get('secret'))
        login_btn.click()
        # See if login worked
        login_fail_element = driver.find_elements_by_xpath('/html/body/table/tbody/tr/td/img')
        if len(login_fail_element) > 0:
            # this length should be 0 if login is successful
            raise AssertionError("Cannot login")

    def get_files(self, driver):
        # switch to relevant frame
        WebDriverWait(driver, 5).until(
            ec.frame_to_be_available_and_switch_to_it((By.XPATH, '//*[@id="contents"]')))
        # click Reporting
        WebDriverWait(driver, 10).until(ec.element_to_be_clickable(
            (By.ID, "upx_GroupHeader3_LeftIcon"))).click()
        # click Reports
        WebDriverWait(driver, 10).until(ec.element_to_be_clickable(
            (By.XPATH, '//*[@id="upx_GroupHeader3_Item1"]'))).click()
        # switch to the On-demand Reports window
        WebDriverWait(driver, 10).until(ec.number_of_windows_to_be(2))
        driver.switch_to.window(driver.window_handles[1])
        # in spite of all the WebDriverWait in place, this one still intermittently fails with
        # Message: unknown error: cannot determine loading status from no such execution context
        # watching it, it seems to react too quickly, so, let's make it wait a bit
        time.sleep(3)
        # click Reports: dropdown
        WebDriverWait(driver, 10).until(ec.element_to_be_clickable(
            (By.XPATH, '//*[@id="ctl00_ContentPlaceHolder1_ddlReports"]'))).click()
        # click Dynamic MPI - Usage Report option
        WebDriverWait(driver, 10).until(ec.element_to_be_clickable(
            (By.XPATH, '//*[@id="ctl00_ContentPlaceHolder1_ddlReports"]/option[12]'))).click()

        while self.start_date <= self.end_date:
            self.the_year = self.start_date.strftime('%Y')
            self.the_month = self.start_date.strftime('%b')
            self.the_day = self.start_date.strftime('%-d')
            time.sleep(2)

            # enable from datepicker
            WebDriverWait(driver, 10).until(ec.element_to_be_clickable(
                (By.XPATH, '//*[@id="ctl00_ContentPlaceHolder1_tbFromDate"]'))).click()

            # select march
            select_month = driver.find_element_by_xpath('//select[@class="ui-datepicker-month"]')
            for option in select_month.find_elements_by_tag_name('option'):
                if option.text == self.the_month:
                    option.click()
                    break
            # select 2020
            select_year = driver.find_element_by_xpath('//select[@class="ui-datepicker-year"]')
            for option in select_year.find_elements_by_tag_name('option'):
                if option.text == self.the_year:
                    option.click()
                    break
            # pick the day
            elem_date = driver.find_element_by_xpath(
                "//td[not(contains(@class,'ui-datepicker-other-month'))]/a[text()='" + self.the_day + "']")
            elem_date.click()

            # pick the same TO date
            WebDriverWait(driver, 10).until(ec.element_to_be_clickable(
                (By.XPATH, '//*[@id="ctl00_ContentPlaceHolder1_tbToDate"]'))).click()
            select_month = driver.find_element_by_xpath('//select[@class="ui-datepicker-month"]')
            for option in select_month.find_elements_by_tag_name('option'):
                if option.text == self.the_month:
                    option.click()
                    break
            select_year = driver.find_element_by_xpath('//select[@class="ui-datepicker-year"]')
            for option in select_year.find_elements_by_tag_name('option'):
                if option.text == self.the_year:
                    option.click()
                    break
            elem_date = driver.find_element_by_xpath(
                "//td[not(contains(@class,'ui-datepicker-other-month'))]/a[text()='" + self.the_day + "']")
            elem_date.click()

            # click Generate
            WebDriverWait(driver, 10).until(ec.element_to_be_clickable(
                (By.XPATH, '//*[@id="btnGenerate"]'))).click()
            # click Download dropdown image
            # again this doesn't make immediate sense, it as if the dropdown image is not clickable until
            # the onscreen report is complete, but the WebDriverWait does not seem to respect that, so
            time.sleep(3)
            WebDriverWait(driver, 10).until(ec.element_to_be_clickable(
                (By.XPATH, '//*[@id="ctl00_ContentPlaceHolder1_ReportViewer_ctl05_ctl04_ctl00"]'))).click()
            # click Excel
            WebDriverWait(driver, 10).until(ec.element_to_be_clickable(
                (By.XPATH, '//*[@id="ctl00_ContentPlaceHolder1_ReportViewer_ctl05_ctl04_ctl00_Menu"]/div[1]'))).click()
            # let the file download
            time.sleep(3)
            # rename the file
            for filename in os.listdir(self.download_dir):
                os.rename(self.download_dir + filename, self.download_dir + 'dynamic_mpi_usage_report.xls')
            xls_file_name = self.download_dir + 'dynamic_mpi_usage_report.xls'
            csv_file_name = self.download_dir + 'dynamic_mpi_usage_report.csv'
            # Convert the xls to csv
            with xlrd.open_workbook(xls_file_name) as wb:
                # the second sheet
                sh = wb.sheet_by_index(1)
                with open(csv_file_name, 'w', newline="") as f:
                    col = csv.writer(f, quoting=csv.QUOTE_NONNUMERIC)
                    for row in range(sh.nrows):
                        col.writerow(sh.row_values(row))
                assert len(os.listdir(self.download_dir)) == 2
                with utilities.pg(pg_server) as pg_con:
                    with pg_con.cursor() as pg_cur:
                        pg_cur.execute("truncate dfx.dynamic_mpi_usage_report_ext")
                        with open(csv_file_name, 'r') as io:
                            pg_cur.copy_expert("copy dfx.dynamic_mpi_usage_report_ext from stdin "
                                               "with csv encoding 'latin-1 '", io)
                        sql = "select dfx.dynamic_mpi_usage_report_update('{}');".format(self.start_date)
                        pg_cur.execute(sql)
            for filename in os.listdir(self.download_dir):
                os.unlink(self.download_dir + filename)
            self.start_date += self.delta

    def run(self):
        driver = self.setup_driver()
        try:
            for filename in os.listdir(self.download_dir):
                os.unlink(self.download_dir + filename)
            driver.get('https://service.dealer-fx.com/logins/Login.2.aspx')
            self.login(self, driver)
            time.sleep(3)
            self.get_files(driver)
        finally:
            driver.quit()


class AdisorCheckinUsageReportHistory(luigi.Task):
    """
    https://stackoverflow.com/questions/49747034/select-from-a-datepicker-in-python
    """
    pipeline = pipeline

    if local_or_production == 'local':
        download_dir = '/home/jon/projects/luigi_1804_36/dealerfx/download/advisor_checkin_usage_report/'
    elif local_or_production == 'production':
        download_dir = '/home/rydell/projects/luigi_1804_36/dealerfx/download/advisor_checkin_usage_report/'


    start_date = date(2020, 4, 3)
    end_date = date(2020, 9, 3)
    delta = timedelta(days=1)
    the_year = None
    the_month = None
    the_day = None

    def local_target(self):
        # using target name with minute allows frequent rerunning
        return '../../luigi_output_files/' + ts_with_minute + '_' + self.__class__.__name__ + ".txt"

    def requires(self):
        return None

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    @staticmethod
    def get_secret(category, platform):
        modified_platform = platform.replace(' ', '+')
        url = ("https://beta.rydellvision.com:8888/vision/secret?platform={0}&category={1}".format
               (modified_platform, category))
        response = requests.get(url, auth=(user, passcode))
        return response.json()

    def setup_driver(self):
        # # Chromedriver path
        chromedriver_path = '/usr/bin/chromedriver'
        options = webdriver.ChromeOptions()
        profile = {"download.default_directory": self.download_dir}
        options.add_experimental_option("prefs", profile)
        options.add_argument('--user-agent="Mozilla/5.0 ((Windows NT 10.0; Win64; x64) AppleWebKit/537.36 '
                             '(KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36"')
        driver = webdriver.Chrome(executable_path=chromedriver_path, options=options)
        return driver

    @staticmethod
    def login(self, driver):
        # get username and password fields
        credentials = self.get_secret('Cartiva', 'DealerFx')
        resp_dict = credentials
        username_box = driver.find_element_by_name('txtFrameworkUserName')
        password_box = driver.find_element_by_name('txtFrameworkPassword')
        login_btn = driver.find_element_by_name('btnDoLogin')
        username_box.send_keys(resp_dict.get('username'))
        password_box.send_keys(resp_dict.get('secret'))
        login_btn.click()
        # See if login worked
        login_fail_element = driver.find_elements_by_xpath('/html/body/table/tbody/tr/td/img')
        if len(login_fail_element) > 0:
            # this length should be 0 if login is successful
            raise AssertionError("Cannot login")

    def get_files(self, driver):
        # switch to relevant frame
        WebDriverWait(driver, 5).until(
            ec.frame_to_be_available_and_switch_to_it((By.XPATH, '//*[@id="contents"]')))
        # click Reporting
        WebDriverWait(driver, 10).until(ec.element_to_be_clickable(
            (By.ID, "upx_GroupHeader3_LeftIcon"))).click()
        # click Reports
        WebDriverWait(driver, 10).until(ec.element_to_be_clickable(
            (By.XPATH, '//*[@id="upx_GroupHeader3_Item1"]'))).click()
        # switch to the On-demand Reports window
        WebDriverWait(driver, 10).until(ec.number_of_windows_to_be(2))
        driver.switch_to.window(driver.window_handles[1])
        # in spite of all the WebDriverWait in place, this one still intermittently fails with
        # Message: unknown error: cannot determine loading status from no such execution context
        # watching it, it seems to react too quickly, so, let's make it wait a bit
        time.sleep(3)
        # click Reports: dropdown
        WebDriverWait(driver, 10).until(ec.element_to_be_clickable(
            (By.XPATH, '//*[@id="ctl00_ContentPlaceHolder1_ddlReports"]'))).click()
        # click Advisor Check-In Usage Report option
        WebDriverWait(driver, 10).until(ec.element_to_be_clickable(
            (By.XPATH, '//*[@id="ctl00_ContentPlaceHolder1_ddlReports"]/option[2]'))).click()

        if self.start_date.weekday == 6:
            self.start_date += self.delta
        while self.start_date <= self.end_date:
            self.the_year = self.start_date.strftime('%Y')
            self.the_month = self.start_date.strftime('%b')
            self.the_day = self.start_date.strftime('%-d')
            time.sleep(2)

            # enable from datepicker
            WebDriverWait(driver, 10).until(ec.element_to_be_clickable(
                (By.XPATH, '//*[@id="ctl00_ContentPlaceHolder1_tbFromDate"]'))).click()

            # select month
            select_month = driver.find_element_by_xpath('//select[@class="ui-datepicker-month"]')
            for option in select_month.find_elements_by_tag_name('option'):
                if option.text == self.the_month:
                    option.click()
                    break
            # select year
            select_year = driver.find_element_by_xpath('//select[@class="ui-datepicker-year"]')
            for option in select_year.find_elements_by_tag_name('option'):
                if option.text == self.the_year:
                    option.click()
                    break
            # pick the day
            elem_date = driver.find_element_by_xpath(
                "//td[not(contains(@class,'ui-datepicker-other-month'))]/a[text()='" + self.the_day + "']")
            elem_date.click()

            # pick the same TO date
            WebDriverWait(driver, 10).until(ec.element_to_be_clickable(
                (By.XPATH, '//*[@id="ctl00_ContentPlaceHolder1_tbToDate"]'))).click()
            select_month = driver.find_element_by_xpath('//select[@class="ui-datepicker-month"]')
            for option in select_month.find_elements_by_tag_name('option'):
                if option.text == self.the_month:
                    option.click()
                    break
            select_year = driver.find_element_by_xpath('//select[@class="ui-datepicker-year"]')
            for option in select_year.find_elements_by_tag_name('option'):
                if option.text == self.the_year:
                    option.click()
                    break
            elem_date = driver.find_element_by_xpath(
                "//td[not(contains(@class,'ui-datepicker-other-month'))]/a[text()='" + self.the_day + "']")
            elem_date.click()

            # click Generate
            WebDriverWait(driver, 10).until(ec.element_to_be_clickable(
                (By.XPATH, '//*[@id="btnGenerate"]'))).click()
            # click Download dropdown image
            # again this doesn't make immediate sense, it as if the dropdown image is not clickable until
            # the onscreen report is complete, but the WebDriverWait does not seem to respect that, so
            time.sleep(3)
            WebDriverWait(driver, 10).until(ec.element_to_be_clickable(
                (By.XPATH, '//*[@id="ctl00_ContentPlaceHolder1_ReportViewer_ctl05_ctl04_ctl00"]'))).click()
            # click Excel
            WebDriverWait(driver, 10).until(ec.element_to_be_clickable(
                (By.XPATH, '//*[@id="ctl00_ContentPlaceHolder1_ReportViewer_ctl05_ctl04_ctl00_Menu"]/div[1]'))).click()
            # let the file download
            time.sleep(3)
            # rename the file
            for filename in os.listdir(self.download_dir):
                os.rename(self.download_dir + filename, self.download_dir + 'advisor_checkin_usage_report.xls')
            xls_file_name = self.download_dir + 'advisor_checkin_usage_report.xls'
            csv_file_name = self.download_dir + 'advisor_checkin_usage_report.csv'
            # Convert the xls to csv
            with xlrd.open_workbook(xls_file_name) as wb:
                sh = wb.sheet_by_index(0)
                # if there is no data, the fucking spreadsheet has 24 columns, with data, 17, skip the no data dates
                if sh.ncols == 17:
                    with open(csv_file_name, 'w', newline="") as f:
                        col = csv.writer(f, quoting=csv.QUOTE_NONNUMERIC)
                        for row in range(sh.nrows):
                            col.writerow(sh.row_values(row))
                    assert len(os.listdir(self.download_dir)) == 2
                    with utilities.pg(pg_server) as pg_con:
                        with pg_con.cursor() as pg_cur:
                            pg_cur.execute("truncate dfx.advisor_checkin_usage_report_ext")
                            with open(csv_file_name, 'r') as io:
                                pg_cur.copy_expert("copy dfx.advisor_checkin_usage_report_ext from stdin "
                                                   "with csv encoding 'latin-1 '", io)
                            sql = "select dfx.advisor_checkin_usage_report_update('{}');".format(self.start_date)
                            pg_cur.execute(sql)
            for filename in os.listdir(self.download_dir):
                os.unlink(self.download_dir + filename)
            self.start_date += self.delta

    def run(self):
        driver = self.setup_driver()
        try:
            for filename in os.listdir(self.download_dir):
                os.unlink(self.download_dir + filename)
            driver.get('https://service.dealer-fx.com/logins/Login.2.aspx')
            self.login(self, driver)
            time.sleep(3)
            self.get_files(driver)
        finally:
            driver.quit()


if __name__ == '__main__':
    luigi.run(["--workers=1"], main_task_cls=DynamicMPIUsageReportHistory)
