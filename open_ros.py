# encoding=utf-8
"""
7/15/18
requires ext_arkona.ExtOpenRoHeaders() & ext_arkona.ExtOpenRoDetails()
followed by a call to jeri.update_open_ros(), which updates jeri.open_ros
"""
import luigi
import utilities
import datetime
import ext_arkona

pipeline = 'ext_arkona'
db2_server = 'report'
pg_server = '173'
ts_no_spaces = str(datetime.datetime.now()).replace(" ", "")


class UpdateOpenRos(luigi.Task):
    """

    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield ext_arkona.ExtOpenRoHeaders()
        yield ext_arkona.ExtOpenRoDetails()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute("select jeri.update_open_ros()")
