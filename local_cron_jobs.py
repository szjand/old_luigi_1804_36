# encoding=utf-8
"""
03/01/2021
    the idea here is, there are several touchy scraping jobs that i am having to run locally every day
    one of the side effects of the touchiness is that that most of the will sporadically fail, but run succcessfully
    after one or more attempts to rerun.
    i have managed to get the rerunning to work (Notes->retry), but i have not been able to limit which jobs get
    rerun, at this point it seems like the retry setting is in effect at the server (luigid) level
    another glitch in the retry is that the number of retries is not consistent, sometimes resulting in the
    scheduler refusing additional work
    i do not want the production server to retry large and otherwise stable jobs in the case of a failure
    so
    i want to accomplish a couple of things
    1. get these jobs off my manual list
    2. better understand how to implement retry at the class level
    3. better understand the options for retry
    so a pseudo solution is to run the cron jobs on my local machine, where the retry for all jobs is ok and, as time
    and motivation allow, understand retry better

first concern
    all of these scraping jobs are run in the context of xvfb-run, will that work calling the actual
    task from this wrapper? or sill the jobs have to be run from individual cron jobs?

first run (success):
    ===== Luigi Execution Summary =====

    Scheduled 5 tasks of which:
    * 5 ran successfully:
        - 1 LocalCronJobs()
        - 1 SalesCsiDownload()
        - 1 SalesCsiParse()
        - 1 ServiceCsiDownload()
        - 1 ServiceCsiParse()

    This progress looks :) because there were failed tasks but they all succeeded in a retry

    ===== Luigi Execution Summary =====

second run (mess):
included nissan and all 3 gm all called from wrapper gask
nissan paased, but never got a pass notification
gm sales make passed, no notification
servce make & service advisor both failed multiple times, service make got disabled
manually restarted luigid, not sure whats happening right now

try it again (3:23) wtf, they all finished
    ===== Luigi Execution Summary =====

    Scheduled 8 tasks of which:
    * 2 complete ones were encountered:
        - 1 CsiParse()
        - 1 SalesMakeCSIDownload()
    * 6 ran successfully:
        - 1 LocalCronJobs()
        - 1 SalesMakeCSIParse()
        - 1 ServiceAdvisorCSIDownload()
        - 1 ServiceAdvisorCSIParse()
        - 1 ServiceMakeCSIDownload()
        ...

    This progress looks :) because there were no failed tasks or missing dependencies

    ===== Luigi Execution Summary =====

maybe i can take off the retry, and just run the cron job like 5 times
if they have all passed on the first run, no problem, luigi just says, nothing to do.


"""
import utilities
import luigi
import datetime
import honda_csi
import nissan_csi
import gm_csi


pg_server = '173'

ts_no_spaces = str(datetime.datetime.now()).replace(" ", "")


@luigi.Task.event_handler(luigi.Event.START)
def task_start(self):
    utilities.luigi_log_start(pg_server, self.task_id + '__' + ts_no_spaces, self.pipeline, self.__class__.__name__)


@luigi.Task.event_handler(luigi.Event.SUCCESS)
def task_success(self):
    utilities.luigi_log_pass(pg_server, self.task_id + '__' + ts_no_spaces, self.pipeline, self.__class__.__name__)
    try:  # WrapperTest throws an AttributeError, see /Notes/issues
        with self.output().open("w") as out_file:
            # write to the output file
            out_file.write('pass')
    except AttributeError:
        pass


class LocalCronJobs(luigi.WrapperTask):
    """
    """
    pipeline = 'run_all'

    def requires(self):
        yield honda_csi.SalesCsiParse()
        yield honda_csi.ServiceCsiParse()
        yield nissan_csi.CsiParse()
        yield gm_csi.SalesMakeCSIParse()
        yield gm_csi.ServiceMakeCSIParse()
        yield gm_csi.ServiceAdvisorCSIParse()


if __name__ == '__main__':
    luigi.run(["--workers=6"], main_task_cls=LocalCronJobs)
