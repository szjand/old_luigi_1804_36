# encoding=utf-8
"""
patterns: extract/tmp

populates fin.fact_gl, fin.dim_journal, fin.dim_account

Tasks: ExtGlpdept, ExtGlpjrnd
"""
# TODO should all these queries be stored procs where possible (ads, pg)?
# TODO change extract days in XfmGlptrns
import utilities
import luigi
import csv
import datetime

pipeline = 'fact_gl'
db2_server = 'report'
# pg_server = 'local'
pg_server = '173'


class ExtGlpdept(luigi.Task):
    """
    ETL Pattern 1: very slow changing, compare extract to archive
    """
    source_table = 'rydedata.glpdept'
    extract_file = '../../extract_files/glpdept.csv'
    extract_table = 'arkona.ext_glpdept_tmp'
    archive_table = 'arkona.ext_glpdept'
    pipeline = pipeline

    def sql_extract(self):
        return """select TRIM(COMPANY_NUMBER),TRIM(DEPARTMENT_CODE),TRIM(DEPT_DESCRIPTION)
                  from {}""".format(self.source_table)

    def sql_compare(self):
        return """
                select count (*)
                from (
                    select * from {1}
                    except
                    select * from {0}
                union all (
                    select * from {0}
                    except
                    select * from {1})) x
            """.format(self.extract_table, self.archive_table)

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    # -------------------------------- change nothing below this line -------------------------------------------------#

    def requires(self):
        return None

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.arkona_luigi_27(db2_server) as cn:
            with cn.cursor() as cur:
                cur.execute(self.sql_extract())
                with open(self.extract_file, 'w') as f:
                    csv.writer(f).writerows(cur)
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = """truncate {}""".format(self.extract_table)
                pg_cur.execute(sql)
                with open(self.extract_file, 'r') as io:
                    # copy_string = """copy {} from stdin with csv encoding 'latin-1 '""".format(self.extract_table)
                    # pg_cur.copy_expert(copy_string, io)
                    pg_cur.copy_expert("""copy {} from stdin with csv encoding 'latin-1 '""".format(self.extract_table),
                                       io)
                pg_con.commit()
                pg_cur.execute(self.sql_compare())
                if pg_cur.fetchone()[0] != 0:
                    raise ValueError('There are differences between the extract and archive tables')


class ExtGlpjrnd(luigi.Task):
    """
    ETL Pattern 1: very slow changing, compare extract to archive
    """
    source_table = 'rydedata.glpjrnd'
    extract_file = '../../extract_files/glpjrnd.csv'
    extract_table = 'arkona.ext_glpjrnd_tmp'
    archive_table = 'arkona.ext_glpjrnd'
    pipeline = pipeline

    def sql_extract(self):
        return """
            select TRIM(COMPANY_NUMBER),TRIM(JOURNAL_CODE),TRIM(JOURNAL_DESC),TRIM(CONTROL_PROMPT),
            TRIM(DOCUMENT_PROMPT),TRIM(OVERRIDE_PROMPT),TRIM(TRANSACTION_DESC),TRIM(COST_REQUIRED),
            COST_RELATION_PCT,TRIM(COUNT_SALES_UNITS),TRIM(COUNT_ASSET_UNITS),TRIM(UPD_DATE_LAST_CHG),
            TRIM(UPD_DATE_LAST_PAY),TRIM(ALLOW_SUSP_TRANS),TRIM(PROCESS_BY_BATCH),
            TRIM(REPLACE_CTL_W_OVR),TRIM(COST_CODE)
            from {}
        """.format(self.source_table)

    def sql_compare(self):
        return """
                select count (*)
                from (
                    select * from {1}
                    except
                    select * from {0}
                union all (
                    select * from {0}
                    except
                    select * from {1})) x
            """.format(self.extract_table, self.archive_table)

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")
    # -------------------------------- change nothing below this line ----------------------------------------------#

    def requires(self):
        return None

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.arkona_luigi_27(db2_server) as db2_con:
            with db2_con.cursor() as db2_cur:
                db2_cur.execute(self.sql_extract())
                with open(self.extract_file, 'w') as f:
                    csv.writer(f).writerows(db2_cur)
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = """truncate {}""".format(self.extract_table)
                pg_cur.execute(sql)
                with open(self.extract_file, 'r') as io:
                    pg_cur.copy_expert("""copy {} from stdin with csv encoding 'latin-1 '""".format(self.extract_table),
                                       io)
                pg_con.commit()
                pg_cur.execute(self.sql_compare())
                if pg_cur.fetchone()[0] != 0:
                    raise ValueError('There are differences between the extract and archive tables')


class DimAccount(luigi.Task):
    """
    ETL Pattern 2: full extract -> xfm_table -> type 2 dimension
    7/8/19 added persisting account balances from glpmast
    """
    source_table = 'rydedata.glpmast'
    extract_file = '../../extract_files/glpmast.csv'
    extract_table = 'arkona.ext_glpmast'
    xfm_table = 'arkona.xfm_glpmast'
    dimension_table = 'fin.dim_account'
    pipeline = pipeline

    def sql_extract(self):
        return """
            select TRIM(COMPANY_NUMBER),TRIM(FISCAL_ANNUAL),YEAR,TRIM(ACCOUNT_NUMBER),TRIM(ACCOUNT_TYPE),
              TRIM(ACCOUNT_DESC),TRIM(ACCOUNT_SUB_TYPE),TRIM(ACCOUNT_CTL_TYPE),TRIM(DEPARTMENT),
              TRIM(TYPICAL_BALANCE),TRIM(DEBIT_OFFSET_ACCT),TRIM(CRED_OFFSET_ACCT),OFFSET_PERCENT,
              AUTO_CLEAR_AMOUNT,TRIM(WRITE_OFF_ACCOUNT),TRIM(SCHEDULE_BY),TRIM(RECONCILE_BY),
              TRIM(COUNT_UNITS),TRIM(CONTROL_DESC1),TRIM(VALIDATE_STOCK_NUMBER),TRIM(OPTION_3),
              TRIM(OPTION_4),TRIM(OPTION_5),BEGINNING_BALANCE,JAN_BALANCE01,FEB_BALANCE02,MAR_BALANCE03,
              APR_BALANCE04,MAY_BALANCE05,JUN_BALANCE06,JUL_BALANCE07,AUG_BALANCE08,SEP_BALANCE09,
              OCT_BALANCE10,NOV_BALANCE11,DEC_BALANCE12,ADJ_BALANCE13,UNITS_BEG_BALANCE,JAN_UNITS01,
              FEB_UNITS02,MAR_UNITS03,APR_UNITS04,MAY_UNITS05,JUN_UNITS06,JUL_UNITS07,AUG_UNITS08,
              SEP_UNITS09,OCT_UNITS10,NOV_UNITS11,DEC_UNITS12,ADJ_UNITS13,TRIM(ACTIVE)
            from {}
        """.format(self.source_table)

    def sql_xfm(self):
        return """
            insert into {0}
            select account_number, account_type, account_desc,
              case account_sub_type
                when 'A' then 'RY1'
                when 'B' then 'RY2'
                else 'XXX'
              end,
              department,
              case typical_balance
                when 'D' then 'Debit'
                when 'C' then 'Credit'
                else 'X'
              end
            from {1}
            where account_sub_type <> 'C' -- crookston accounts
            group by account_number, account_type, account_desc, account_sub_type,
              department, typical_balance
        """.format(self.xfm_table, self.extract_table)

    @staticmethod
    def sql_new_row():
        """

        """
        return """
            insert into fin.dim_account (account, account_type_code, account_type, description,
              store_code, store, department_code, department, typical_balance, current_row,
              row_from_date, row_reason)
            select a.account, a.account_type_code, c.account_type,
              a.description, a.store_code,
              case a.store_code
                when 'RY1' then 'Rydell GM'
                when 'RY2' then 'Honda Nissan'
              end,
              a.department_code, 
              case
                when a.store_code = 'RY2' and a.department_code = 'RE' then 'Detail'
                else b.dept_description
              end,
              a.typical_balance,
              true,
              (select first_of_month from dds.dim_date where the_date = current_date - 1),
              'new account created by controller'
            from (
              select account, account_type_code, description, store_code, department_code, typical_balance
              from arkona.xfm_glpmast z
              where not exists (
                select 1
                from fin.dim_account
                where account = z.account)) a          
            left join arkona.ext_glpdept b on a.department_code = b.department_code
              and a.store_code = b.company_number
            left join dds.lkp_account_types c on a.account_type_code = c.account_type_code; 
        """

    @staticmethod
    def sql_type_2():
        return """
            with upd as (
              update fin.dim_account
              set current_row = false,
                  row_thru_date = current_date - 1
              where account in ( -- the account(s) that have changed
                select c.account
                from ( -- changed accounts, any attribute
                  select account, md5(a::text) as hash
                  from arkona.xfm_glpmast a) c
                inner join (
                  select account, md5(a::text) as hash
                  from (
                    select account,account_type_code,description,store_code,department_code,typical_balance
                    from fin.dim_account
                    where current_row = true) a) d on c.account = d.account and c.hash <> d.hash)
              and current_row = true
              returning *)
            insert into fin.dim_account (account, account_type_code, account_type, description,
              store_code, store, department_code, department, typical_balance, current_row,
              row_from_date, row_reason)
            select a.account, a.account_type_code, c.account_type,
              a.description, a.store_code,
              case a.store_code
                when 'RY1' then 'Rydell GM'
                when 'RY2' then 'Honda Nissan'
              end,
              a.department_code, b.dept_description,
              a.typical_balance,
              true, current_date, 'type 2 update'
            from (
              select z.account, z.account_type_code, z.description, z.store_code, z.department_code, z.typical_balance
              from arkona.xfm_glpmast z
              inner join upd on z.account = upd.account)a
            left join arkona.ext_glpdept b on a.department_code = b.department_code
              and a.store_code = b.company_number
            left join dds.lkp_account_types c on a.account_type_code = c.account_type_code;
        """

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    # -------------------------------- change nothing below this line ----------------------------------------------#

    def requires(self):
        # return ExtGlpdept()
        yield ExtGlpdept()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.arkona_luigi_27(db2_server) as db2_con:
            with db2_con.cursor() as db2_cur:
                db2_cur.execute(self.sql_extract())
                with open(self.extract_file, 'w') as f:
                    csv.writer(f).writerows(db2_cur)
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = """truncate {}""".format(self.extract_table)
                pg_cur.execute(sql)
                with open(self.extract_file, 'r') as io:
                    pg_cur.copy_expert("""copy {} from stdin with csv encoding 'latin-1 '""".format(self.extract_table),
                                       io)
                # empty xfm table
                sql = """truncate {}""".format(self.xfm_table)
                pg_cur.execute(sql)
                # populate xfm_table
                pg_cur.execute(self.sql_xfm())
                pg_cur.execute(self.sql_new_row())
                pg_cur.execute(self.sql_type_2())
                # persist inventory account balances from arkona.ext_glpmast
                pg_cur.execute("select jeri.update_inventory_balances()")


class XfmGlptrns (luigi.Task):
    """
    ETL Pattern: 3: partial extract -> xfm_table
    """
    source_table = 'rydedata.glptrns'
    extract_file = '../../extract_files/glptrns.csv'
    extract_table = 'arkona.ext_glptrns_tmp'
    xfm_table = 'arkona.xfm_glptrns'
    pipeline = pipeline

    def sql_extract(self):
        return """
            select
              gtco#, gttrn#, gtseq#, gtdtyp, gttype, trim(gtpost), gtrsts, gtadjust, gtpsel,
              trim(gtjrnl), gtdate, gtrdate, gtsdate, trim(gtacct), trim(gtctl#), trim(gtdoc#),
              trim(gtrdoc#), gtrdtyp, trim(gtodoc#), trim(gtref#), trim(gtvnd#),
              trim(upper(gtdesc)), gttamt, gtcost, gtctlo,gtoco#
            from {}
            where gtdate between current_date - 45 day and current_date
            -- gtadjust are 13th month entries
              and gtadjust <> 'Y'
        """.format(self.source_table)

    def sql_xfm(self):
        return """
            insert into {0}
            select a.*, md5(a::text) as hash
            from (
              select gttrn_, gtseq_, gtdtyp, gtpost, coalesce(gtjrnl, 'none') as gtjrnl, gtdate, gtacct,
                coalesce(gtctl_,'none'), coalesce(gtdoc_,'none'), coalesce(gtref_,'none'), 
                gttamt::numeric(12,2), coalesce(gtdesc, 'NONE')
              from {1}
              where gtco_ = 'RY1'
                and gtpost in ('Y','V')
                and left(gtacct, 1) <> '3'
              group by gttrn_, gtseq_, gtdtyp, gtpost, gtjrnl, gtdate, gtacct, gtctl_,
                gtdoc_, gtref_, gttamt, gtdesc) a;
        """.format(self.xfm_table, self.extract_table)

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    # -------------------------------- change nothing below this line ----------------------------------------------#

    def requires(self):
        return None

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.arkona_luigi_27(db2_server) as db2_con:
            with db2_con.cursor() as db2_cur:
                db2_cur.execute(self.sql_extract())
                with open(self.extract_file, 'w') as f:
                    csv.writer(f).writerows(db2_cur)
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = """truncate {}""".format(self.extract_table)
                pg_cur.execute(sql)
                with open(self.extract_file, 'r') as io:
                    pg_cur.copy_expert("""copy {} from stdin with csv encoding 'latin-1 '""".format(self.extract_table),
                                       io)
                # empty xfm table
                sql = """truncate {}""".format(self.xfm_table)
                pg_cur.execute(sql)
                # populate xfm_table
                pg_cur.execute(self.sql_xfm())


class DimGlDescription(luigi.Task):
    """
    ETL Pattern 4: xfm table -> type 1 dimension
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    # -------------------------------- change nothing below this line ----------------------------------------------#
    def requires(self):
        # return XfmGlptrns()
        yield XfmGlptrns()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = """
                    Insert into fin.dim_gl_description (description, row_from_date, current_row, row_reason)
                    select distinct description, current_date, true, 'added from nightly etl'
                    from arkona.xfm_glptrns a
                    where not exists (
                      select 1
                      from fin.dim_gl_description
                      where description = a.description);
                """
                pg_cur.execute(sql)


class FactGl(luigi.Task):
    """
    ETL Pattern 5: populate fact table
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    # -------------------------------- change nothing below this line ----------------------------------------------#
    def requires(self):
        # return [ExtGlpjrnd(), DimAccount(), DimGlDescription()]
        yield ExtGlpjrnd()
        yield DimAccount()
        yield DimGlDescription()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                # insert new rows
                sql = """
                    insert into fin.fact_gl (trans,seq,doc_type_key,post_Status,journal_key,date_key,
                      account_key,control,doc,ref,amount,gl_description_key)
                    select e.trans, e.seq, coalesce(f.doc_type_key, ff.doc_type_key),
                      e.post_status, g.journal_key, h.datekey,
                      coalesce(i.account_key, j.account_key) as account_key,
                      e.control, e.doc, coalesce(e.ref, ''), e.amount, m.gl_description_key
                    from arkona.xfm_glptrns e
                    left join fin.fact_gl ee on e.trans = ee.trans
                      and e.seq = ee.seq
                      -- and e.post_status = ee.post_status
                    left join fin.dim_doc_type f on e.doc_type_code = f.doc_type_code
                      and e.the_date between f.row_from_date and f.row_thru_date
                    left join fin.dim_doc_type ff on 1 = 1
                      and ff.doc_type_code = 'none'
                    left join fin.dim_journal g on e.journal_code = g.journal_code
                      and e.the_date between g.row_from_date and g.row_thru_date
                    left join dds.day h on e.the_date = h.thedate
                    left join fin.dim_account i on e.account = i.account
                      and e.the_date between i.row_from_date and i.row_thru_date
                    left join fin.dim_account j on 1 = 1
                      and j.account = 'none'
                    --left join fin.xfm_gl_description k on e.trans = k.trans
                    --  and e.seq = k.seq
                    --  and e.post_status = k.post_status
                    left join fin.dim_gl_description m on e.description = m.description
                    where ee.trans is null;
                """
                pg_cur.execute(sql)
                # update the voids in fin.fact_gl
                sql = """
                    update fin.fact_gl a
                    set post_status = x.post_status
                    from (-- these are the voids to be updated
                      select b.trans, b.seq, b.post_status
                      from fin.fact_gl a
                      inner join arkona.xfm_glptrns b on a.trans = b.trans
                          and a.seq = b.seq
                          and a.post_status = 'Y'
                          and b.post_status <> 'Y') x
                    where a.trans = x.trans
                      and a.seq = x.seq;
                """
                pg_cur.execute(sql)

                # # write output to file, see wtf is happening
                # sql = """
                #     select e.*
                #     from arkona.xfm_glptrns e
                #     inner join (
                #       select 'fact', trans, seq, post_status, md5(a::text) as hash
                #       from (
                #         select a.trans, a.seq, b.doc_type_code, a.post_status, c.journal_code,
                #           d.thedate, e.account, a.control, a.doc, a.ref, a.amount, ee.description
                #         from fin.fact_gl a
                #         inner join arkona.xfm_glptrns aa on a.trans = aa.trans
                #           and a.seq = aa.seq
                #           and a.post_status = aa.post_status
                #         inner join fin.dim_doc_type b on a.doc_type_key = b.doc_type_key
                #         inner join fin.dim_journal c on a.journal_key = c.journal_key
                #         inner join dds.day d on a.date_key = d.datekey
                #         inner join fin.dim_account e on a.account_key = e.account_key
                #         inner join fin.dim_gl_description ee on a.gl_description_key = ee.gl_description_key) a) f
                #           on e.trans = f.trans
                #             and e.seq = f.seq
                #             and e.post_status = f.post_status
                #             and e.hash <> f.hash
                # """
                # pg_cur.execute(sql)
                # with open('../../luigi_output_files/ts_output.csv', 'w') as f:
                #     csv.writer(f).writerows(pg_cur)

                # then check for other changes, and now post_status needs to be equal
                # if there are changes, fail and send email
                sql = """
                    select count(*)
                    from arkona.xfm_glptrns e
                    inner join (
                      select 'fact', trans, seq, post_status, md5(a::text) as hash
                      from (
                        select a.trans, a.seq, b.doc_type_code, a.post_status, c.journal_code,
                          d.thedate, e.account, a.control, a.doc, a.ref, a.amount, ee.description
                        from fin.fact_gl a
                        inner join arkona.xfm_glptrns aa on a.trans = aa.trans
                          and a.seq = aa.seq
                          and a.post_status = aa.post_status
                        inner join fin.dim_doc_type b on a.doc_type_key = b.doc_type_key
                        inner join fin.dim_journal c on a.journal_key = c.journal_key
                        inner join dds.day d on a.date_key = d.datekey
                        inner join fin.dim_account e on a.account_key = e.account_key
                        inner join fin.dim_gl_description ee on a.gl_description_key = ee.gl_description_key) a) f
                          on e.trans = f.trans
                            and e.seq = f.seq
                            and e.post_status = f.post_status
                            and e.hash <> f.hash
                """
                pg_cur.execute(sql)
                the_count = pg_cur.fetchone()[0]
                if the_count != 0:
                    raise Exception('fact_gl has some changed rows ' + str(the_count))


# class WrapperTest(luigi.WrapperTask):
#     """
#
#     """
#
#     def requires(self):
#         return [ExtGlpjrnd(), DimAccount(), DimGlDescription()]


if __name__ == '__main__':
    luigi.run(["--workers=4"], main_task_cls=FactGl)
