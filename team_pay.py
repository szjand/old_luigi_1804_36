# encoding=utf-8
"""
target_date_with_minute: used for generating the output files, include hours & minutes to enable
running a task multiple times a day
but need to be able to verify each "data_today" as a group, thinking make a separated
folder for these targets, at the end, empty the folder, seems to work ok
11/07/20
Removed ExtPypclockinToday and disabled event handling in this module
DataUpdateNightly is called by run_all which implements luigi.Event.START & luigi.Event.SUCCESS,
having that implentation also in this module resulted in double logging for every task invoked by run_all
"""
import utilities
import luigi
import datetime
import ext_arkona
import xfm_arkona
import fact_repair_order

pipeline = 'tp_data_nightly'
db2_server = 'report'
pg_server = '173'
ts_no_spaces = str(datetime.datetime.now()).replace(" ", "")
target_directory = '../../luigi_output_team_pay_today/'
target_date = '{:%Y-%m-%d}'.format(datetime.datetime.now())
target_date_with_minute = '{:%Y-%m-%d-%H-%M}'.format(datetime.datetime.now())


# # if these task are being called via run_all, that task will handle all of this logging
# @luigi.Task.event_handler(luigi.Event.START)
# def task_start(self):
#     utilities.luigi_log_start(pg_server, self.task_id + '__' + ts_no_spaces, self.pipeline, self.__class__.__name__)
#
#
# @luigi.Task.event_handler(luigi.Event.SUCCESS)
# def task_success(self):
#     utilities.luigi_log_pass(pg_server, self.task_id + '__' + ts_no_spaces, self.pipeline, self.__class__.__name__)
#     try:  # WrapperTest throws an AttributeError, see /Notes/issues
#         with self.output().open("w") as out_file:
#             # write to the output file
#             out_file.write('pass')
#     except AttributeError:
#         pass


# class ExtPypclockinToday(luigi.Task):
#     """
#     extracts todays data from pypclockin, updated every 10 minutes throughout the day
#     populates arkona.xfm_pypclockin_today
#     """
#     extract_file = '../../extract_files/pypclockin_today.csv'
#     pipeline = pipeline
#
#     def local_target(self):
#         return target_directory + target_date_with_minute + '_' + self.__class__.__name__ + '.txt'
#
#     def requires(self):
#         return None
#
#     def output(self):
#         # define output filename and path
#         return luigi.LocalTarget(self.local_target())
#
#     def run(self):
#         with utilities.arkona_luigi_27(db2_server) as cn:
#             with cn.cursor() as cur:
#                 sql = """
#                     select
#                       TRIM(YICO#),TRIM(PYMAST_EMPLOYEE_NUMBER),YICLKIND,
#                       case when yiclkint = '24:00:00' then '23:59:59' else yiclkint end as yiclkint,
#                       YICLKOUTD,
#                       case when yiclkoutt = '24:00:00' then '23:59:59' else yiclkoutt end as YICLKOUTT,
#                       TRIM(YISTAT),
#                       TRIM(YIINFO),TRIM(YICODE),TRIM(YIUSRIDI),TRIM(YIWSIDI),TRIM(YIUSRIDO),TRIM(YIWSIDO)
#                     from rydedata.pypclockin
#                     where yico# in ('RY1','RY2')
#                       and yiclkind = curdate()
#                 """
#                 cur.execute(sql)
#                 with open(self.extract_file, 'w') as f:
#                     csv.writer(f).writerows(cur)
#         with utilities.pg(pg_server) as pg_con:
#             with pg_con.cursor() as pg_cur:
#                 pg_cur.execute("truncate arkona.ext_pypclockin_today")
#                 with open(self.extract_file, 'r') as io:
#                     pg_cur.copy_expert(
#                         """copy arkona.ext_pypclockin_today from stdin with csv encoding 'latin-1 '""", io)
#                     pg_cur.execute("select arkona.xfm_pypclockin_today();")


class DataUpdateNightly(luigi.Task):
    """
    """

    pipeline = pipeline

    def local_target(self):
        return target_directory + target_date + '_' + self.__class__.__name__ + '.txt'

    def requires(self):
        yield ext_arkona.ExtSdpxtim()
        yield ext_arkona.ExtPymast()
        yield xfm_arkona.XfmPypclockin()
        yield fact_repair_order.FactRepairOrder()

    def output(self):
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = "select tp.data_update_nightly()"
                pg_cur.execute(sql)


# if __name__ == '__main__':
#     luigi.run(["--workers=6"], main_task_cls=ExtPypclockinToday)
