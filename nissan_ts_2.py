# encoding=utf-8
"""
just like global connect, but for nissan
03/29/20
    don't need summary
05/08/28
    prefix local target name with pipeline (see comments in honda.py header)
"""

import utilities
import luigi
import datetime
import os
from selenium import webdriver
import time
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.common.exceptions import ElementNotVisibleException, TimeoutException, ElementNotInteractableException
from selenium.webdriver.common.by import By

pipeline = 'nissan'
pg_server = '173'
ts_no_spaces = str(datetime.datetime.now()).replace(" ", "")
user = 'XD163692'
password = '%6#@%!!@eDHN'


# # if these task are being called via run_all, that task will handle all of this logging
@luigi.Task.event_handler(luigi.Event.START)
def task_start(self):
    utilities.luigi_log_start(pg_server, self.task_id + '__' + ts_no_spaces, self.pipeline, self.__class__.__name__)


@luigi.Task.event_handler(luigi.Event.SUCCESS)
def task_success(self):
    utilities.luigi_log_pass(pg_server, self.task_id + '__' + ts_no_spaces, self.pipeline, self.__class__.__name__)
    try:  # WrapperTest throws an AttributeError, see /Notes/issues
        with self.output().open("w") as out_file:
            # write to the output file
            out_file.write('pass')
    except AttributeError:
        pass


class VehicleInventoryDetailDownload(luigi.Task):
    """
        try to click on serial
    """

    pipeline = pipeline
    # # local
    pdf_dir = '/home/jon/projects/luigi_1804_36/nissan/vehicle_inventory_detail/pdf_files/'

    # # production
    # pdf_dir = '/home/rydell/projects/luigi_1804_36/nissan/vehicle_inventory_detail/pdf_files/'

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                pipeline + '_' + self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def setup_driver(self):
        # # Chromedriver path
        chromedriver_path = '/usr/bin/chromedriver'
        options = webdriver.ChromeOptions()
        profile = {"download.default_directory": self.pdf_dir}
        options.add_experimental_option("prefs", profile)
        options.add_argument('--user-agent="Mozilla/5.0 ((Windows NT 10.0; Win64; x64) AppleWebKit/537.36 '
                             '(KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36"')
        driver = webdriver.Chrome(executable_path=chromedriver_path, options=options)
        return driver

    def run(self):
        # with Display(visible=False, size=(1200, 1500)):
        driver = self.setup_driver()
        try:
            driver.get('https://as.na.nissan.biz/SecureAuth71/')
            # SIGNING IN
            driver.find_element_by_class_name('txtUserid').send_keys(user)
            driver.find_element_by_class_name('tbxPassword').send_keys(password)
            driver.find_element_by_class_name('btn-secureauth').click()
            # Click MY LINKS
            WebDriverWait(driver, 10).until(ec.element_to_be_clickable(('id', '1a'))).click()
            # wait until page finishes loading
            WebDriverWait(driver, 10).until(ec.presence_of_element_located(
                ('xpath', '//*[@id="linklisting-containerID"]/div[5]/div/ul/li[1]/a')))
            # load the Nissan DBS page
            driver.get('https://www.nnanet.com/content/nnanet/global/secure/viewlink.link001203.html')
            # load the inventory page
            driver.get('https://dbsapp.nnanet.com/dbs/vehicle/inventory/list?execution=e6s1')
            # select All Locations (statuses)_
            WebDriverWait(driver, 10).until(ec.presence_of_element_located(
                ('id', 'f1:filterValueLocation'))).click()
            WebDriverWait(driver, 10).until(ec.presence_of_element_located(
                ('xpath', '//*[@id="f1:filterValueLocation"]/option[1]'))).click()
            time.sleep(10)
            # show all on page
            driver.find_element_by_xpath('//*[@id="f1:selectPageNumber_newVehicleTable"]').click()
            driver.find_element_by_xpath('//*[@id="f1:selectPageNumber_newVehicleTable"]/option[6]').click()
            # allow all rows to display
            time.sleep(20)
            row_count = int(driver.find_element_by_xpath('//*[@id="f1:newVehicleTable:newCount"]').text)
            # # empty the pdf_dir
            # for filename in os.listdir(self.pdf_dir):
            #     os.unlink(self.pdf_dir + filename)
            for idx in range(0, row_count):
                print(idx)
                print(str(idx + 1) + ' of ' + str(row_count) + ' rows')
                print(datetime.datetime.now())
                # if the serial is the last 6 of the vin the serial link name is ...:serialNum
                # if the serial is an order number, length is 7, then the serial link name is ...:orderNum
                # the text property of the the td containing the serial should be what i need
                # the xpath of the that td is named consistently thru all rows
                # uh oh failing at idx 101
                serial_td_xpath = '//*[@id="f1:newVehicleTable:{}:serialNumber"]'.format(idx)
                # print('serial_td_path: ' + serial_td_xpath)
                # td_serial = driver.find_element_by_xpath(serial_td_xpath)
                # time.sleep(3)
                td_serial = WebDriverWait(driver, 20).until(ec.presence_of_element_located(('xpath', serial_td_xpath)))
                the_serial = td_serial.text
                # print('the_serial:' + the_serial)
                print(the_serial)
                # print(len(the_serial))
                serial_name = ''
                if len(the_serial) == 7:
                    serial_name = 'f1:newVehicleTable:%s:orderNum' % idx
                elif len(the_serial) == 6:
                    serial_name = 'f1:newVehicleTable:%s:serialNum' % idx
                # print('serial_name' + serial_name)
                # the_link = driver.find_element_by_name(serial_name)
                # the_link.click()
                # time.sleep(3)
                WebDriverWait(driver, 20).until(ec.element_to_be_clickable(('name', serial_name))).click()
                # wait for detail window to open
                try:
                    WebDriverWait(driver, 20).until(ec.number_of_windows_to_be(2))
                except TimeoutException:
                    continue
                driver.switch_to.window(driver.window_handles[1])
                # click Print, downloads the PDF
                driver.find_element_by_xpath('//*[@id="f2:comPrintIcon"]').click()
                driver.close()
                driver.switch_to.window(driver.window_handles[0])
        finally:
            driver.quit()


if __name__ == '__main__':
    luigi.run(["--workers=1"], main_task_cls=VehicleInventoryDetailDownload)
