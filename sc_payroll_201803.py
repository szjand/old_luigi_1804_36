# encoding=utf-8
"""
201709:
  tmpConsultants: add digital role to join on sls.personnel_roles
  ConsultantPayroll:  qualified from 200 -> 250
    when payplan = 'executive' then (250 * qual_count) + (280 * non_qual_count)
20180201
  uh oh PaidByMonth has hard coded year of 117
201803
  new pay plans
4/1/18 renamed from sales_consultant_payroll to sc_payroll_201803

"""
# TODO AccountsRoutes factory_financial_year is hard coded

import luigi
import utilities
import datetime
import csv
import car_deals
import fact_gl
import ext_arkona

pipeline = 'sales_consultant_payroll'
db2_server = 'report'
pg_server = '173'


class DealsByMonth(luigi.Task):
    """
    6/28/17: delete current month deals every night and reload
    04/03/18: replace sql in script with call to function
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    # -------------------------------- change nothing below this line -------------------------------------------------#
    def requires(self):
        yield car_deals.DealsChangedRows()
        # yield AccountsRoutes()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                # ads requirement
                sql = """
                    select sum(the_count)
                    from (
                      select count(*) as the_count
                      from ops.ads_extract
                      where the_date = current_date
                        and task = 'ads_ext_dim_car_deal_info'
                        and complete = TRUE
                      union all
                      select count(*)
                      from ops.ads_extract
                      where the_date = current_date
                        and task = 'ads_ext_dim_vehicle'
                        and complete = TRUE
                      union all
                      select count(*)
                      from ops.ads_extract
                      where the_date = current_date
                        and task = 'ads_ext_dim_customer'
                        and complete = TRUE) a;
                """
                pg_cur.execute(sql)
                if pg_cur.fetchone()[0] != 3:
                    raise ValueError('The ads requirement is not there')
                sql = """select * from sls.update_deals_by_month()"""
                pg_cur.execute(sql)


class AccountsRoutes(luigi.Task):
    """
    3/20/18: include page 7 lines 3, 13 - chargebacks
    """
    # TODO hard coded year in select from eisglobal.sypffxmst
    extract_file = '../../extract_files/accounts_routes.csv'
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    # -------------------------------- change nothing below this line -------------------------------------------------#
    def requires(self):
        return None

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.arkona_luigi_27(db2_server) as cn:
            with cn.cursor() as cur:
                sql = """
                    select distinct case b.consolidation_grp when '2' then 'RY2' else 'RY1' end as store_code,
                      a.fxmpge as the_page, INTEGER(a.fxmlne) as line, trim(b.g_l_acct_number) as gl_account
                    from eisglobal.sypffxmst a
                    inner join rydedata.ffpxrefdta b on a.fxmact = b.factory_account
                      and a.fxmcyy = b.factory_financial_year
                    where a.fxmcyy = 2018
                      and trim(a.fxmcde) = 'GM'
                      and b. consolidation_grp <> '3'
                      and b.factory_code = 'GM'
                      and trim(b.factory_account) <> '331A'
                      and b.company_number = 'RY1'
                      and b.g_l_acct_number <> ''
                      and (
                        (a.fxmpge between 5 and 15) or
                        (a.fxmpge = 16 and a.fxmlne between 1 and 14) or
                        -- exclude fin comp 
                        (a.fxmpge = 17 and a.fxmlne in (1,2,3,6,7,11,12,13,16,17)))
                """
                cur.execute(sql)
                with open(self.extract_file, 'w') as f:
                    csv.writer(f).writerows(cur)
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute("truncate sls.deals_accounts_routes")
                with open(self.extract_file, 'r') as io:
                    pg_cur.copy_expert("""copy sls.deals_accounts_routes from stdin with csv encoding 'latin-1 '""", io)


class DealsAccountingDetail(luigi.Task):
    """
    replaced sql in code with call to function
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    # -------------------------------- change nothing below this line -------------------------------------------------#
    def requires(self):
        yield fact_gl.FactGl()
        yield AccountsRoutes()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = """select * from sls.update_deals_accounting_detail()"""
                pg_cur.execute(sql)


class DealsGrossByMonth(luigi.Task):
    """
    4/3/18: replace sql in script with call to function
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    # -------------------------------- change nothing below this line -------------------------------------------------#
    def requires(self):
        yield DealsAccountingDetail()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = """select * from sls.update_deals_gross_by_month()"""
                pg_cur.execute(sql)


class FiChargebacks(luigi.Task):
    """
    requires: used DealsByMonth because that requires both sls.deals & ads.ext_ads_dim_customer
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    # -------------------------------- change nothing below this line -------------------------------------------------#
    def requires(self):
        yield DealsAccountingDetail()
        yield DealsByMonth()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = """select * from sls.update_fi_chargebacks()"""
                pg_cur.execute(sql)


class ClockHoursByMonth(luigi.Task):
    """
    6/5/17 added the populating of sls.folks
    6/28/17: eliminated table sls.folks
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    # -------------------------------- change nothing below this line -------------------------------------------------#
    def requires(self):
        return None

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                # ads requirement
                sql = """
                  select count(*) as the_count
                  from ops.ads_extract
                  where the_date = current_date
                    and task = 'sls_xfm_fact_clock_hours'
                    and complete = TRUE;
                """
                pg_cur.execute(sql)
                if pg_cur.fetchone()[0] != 1:
                    raise ValueError('The ads requirement is not there')
                # delete rows for open month
                sql = """
                    delete
                    from sls.clock_hours_by_month
                    where year_month = (select year_month from sls.months where open_closed = 'open');
                """
                pg_cur.execute(sql)
                # repopulate for open month
                sql = """
                    insert into sls.clock_hours_by_month
                    select a.employee_number, c.year_month, sum(clock_hours) as clock_hours,
                      sum(regular_hours) as regular_hours, sum(b.overtime_hours) as overtime_hours,
                      sum(b.vacation_hours + b.pto_hours) as pto_hours,
                      sum(b.holiday_hours) as holiday_hours
                    from (
                        select c.team, a.employee_number, a.last_name, a.first_name,
                          a.ry1_id, a.ry2_id, a.fi_id, d.payplan, a.is_senior
                        from sls.personnel a
                        inner join sls.team_personnel c on a.employee_number = c.employee_number
                          and c.from_Date < (select first_day_of_next_month from sls.months
                            where open_closed = 'open')
                          and c.thru_date > (select last_day_of_previous_month from sls.months
                            where open_closed = 'open')
                        inner join sls.personnel_payplans d  on a.employee_number = d.employee_number
                          and d.from_Date < (select first_day_of_next_month from sls.months where open_closed = 'open')
                          and d.thru_date > (select last_day_of_previous_month from sls.months
                            where open_closed = 'open')) a
                    inner join sls.xfm_fact_clock_hours b on a.employee_number = b.employee_number
                    inner join dds.dim_date c on b.the_date = c.the_date
                    where c.year_month = (select year_month from sls.months where open_closed = 'open')
                      and b.clock_hours + b.vacation_hours + b.pto_hours + b.holiday_hours <> 0
                    group by c.year_month, a.employee_number;
                """
                pg_cur.execute(sql)


class PaidByMonth(luigi.Task):
    """
    for draw, and comparison reporting between vision and arkona payroll
    3/20/18 replaced sql with call to function
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

# -------------------------------- change nothing below this line -------------------------------------------------#
    def requires(self):
        yield ext_arkona.ExtPyhscdta()
        yield ext_arkona.ExtPyhshdta()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = "select * from sls.update_paid_by_month()"
                pg_cur.execute(sql)


class PtoIntervalsDates(luigi.Task):
    """

    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

# -------------------------------- change nothing below this line -------------------------------------------------#
    def requires(self):
        yield ext_arkona.ExtPyhshdta()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                # employees in without a row in pto_intervals for current month
                sql = """
                    select count(*)
                    from ( --c: personnel assigned to teams in open month
                      select a.*
                      from sls.personnel a
                      inner join sls.team_personnel b  on a.employee_number = b.employee_number
                      where b.from_Date < (select first_day_of_next_month from sls.months where open_closed = 'open')
                        and thru_date >
                            (select last_day_of_previous_month from sls.months where open_closed = 'open')) c
                    left join sls.pto_intervals d on c.employee_number = d.employee_number
                      and d.year_month = (select year_month from sls.months where open_closed = 'open')
                    where d.employee_number is null;
                """
                pg_cur.execute(sql)
                if pg_cur.fetchone()[0] == 0:
                    return
                pg_cur.execute("truncate sls.pto_interval_dates")
                sql = """
                    insert into sls.pto_interval_dates
                    select year_month, employee_number, anniversary_date, most_recent_anniversary,
                      case
                        when anniversary_date = most_recent_anniversary then anniversary_date
                      else (
                        select first_of_month
                        from sls.months
                        WHERE seq = (
                          select seq - 12
                          from sls.months
                          where v.most_recent_anniversary between first_of_month and last_of_month))
                      end as pto_period_from,
                      case
                        when anniversary_date = most_recent_anniversary then anniversary_date
                      else (
                        select last_of_month
                        from sls.months
                        WHERE seq = (
                          select seq -1
                          from sls.months
                          where v.most_recent_anniversary between first_of_month and last_of_month))
                      END as pto_period_thru
                    from ( -- v: 1 row per emp with most recent anniversary and no row in pto_intervals for open month
                      select (select year_month from sls.months where open_closed = 'open'),
                        last_name, first_name, employee_number, anniversary_date,
                        case -- most recent anniversary
                          when anniversary_date = to_date then anniversary_date
                          when to_date < current_date then to_date
                          when to_date > current_date then (to_date - interval '1 year'):: date
                        end as most_recent_anniversary
                      from (-- u: 1 row per emp with anniv date for cur year,  no row in pto_intervals for open month
                        select c.*,
                          to_date(extract(year from current_date)::text ||'-'||
                            extract(month from c.anniversary_date)::text
                            ||'-'|| extract(day from c.anniversary_date)::text, 'YYYY MM DD' ) -- current yr anniv date
                        from ( --c: personnel assigned to teams in open month
                          select a.*
                          from sls.personnel a
                          inner join sls.team_personnel b  on a.employee_number = b.employee_number
                          where b.from_Date <
                            (select first_day_of_next_month from sls.months where open_closed = 'open')
                            and thru_date >
                                (select last_day_of_previous_month from sls.months where open_closed = 'open')) c
                        left join sls.pto_intervals d on c.employee_number = d.employee_number
                          and d.year_month = (select year_month from sls.months where open_closed = 'open')
                        where d.employee_number is null) u) v;
                """
                pg_cur.execute(sql)


class PtoIntervals(luigi.Task):
    """

    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    # -------------------------------- change nothing below this line -------------------------------------------------#

    def requires(self):
        yield PtoIntervalsDates()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = """
                    insert into sls.pto_intervals (employee_number,year_month,from_date,thru_date,
                        most_recent_anniv,pto_rate)
                    select c.employee_number, c.year_month, c.from_date, c.thru_date, c.most_recent_anniversary,
                      coalesce(x.pto_rate, 0) as pto_rate
                    from sls.pto_interval_dates c
                    left join sls.pto_intervals d on c.employee_number = d.employee_number
                      and d.year_month = (select year_month from sls.months where open_closed = 'open')
                    left join (
                      select employee_number, year_month, total_gross,
                        -- 4.333 weeks/month, 5 working days/month, 8 hrs/day
                        round(total_gross/(4.333 * adj_count/2)/5/8, 2) as pto_rate, adj_count
                      from  (
                        select a.employee_number, a.year_month, sum(b.total_gross_pay) as total_gross,
                          count(*),
                          case
                            when count(*) >= 24 then 24
                            else count(*)
                          end as adj_count
                        from sls.pto_interval_dates a
                        left join arkona.ext_pyhshdta b on a.employee_number = b.employee_
                          and (b.check_month || '-' || b.check_day || '-' || (2000 + b.check_year))::date
                            between a.from_date and a.thru_date
                        where a.from_date <> a.thru_date
                          and b.seq_void = '00'
                        group by a.employee_number, a.year_month) e) x on  c.employee_number = x.employee_number
                    where d.employee_number is null;
                """
                pg_cur.execute(sql)


class ConsultantPayroll(luigi.Task):
    """

    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    # -------------------------------- change nothing below this line -------------------------------------------------#
    def requires(self):
        yield FiChargebacks()
        yield DealsGrossByMonth()
        yield ClockHoursByMonth()
        yield PaidByMonth()
        yield PtoIntervals()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = """select * from sls.update_consultant_payroll()"""
                pg_cur.execute(sql)


if __name__ == '__main__':
    luigi.run(["--workers=4"], main_task_cls=ConsultantPayroll)
# class ConsultantDeals(luigi.WrapperTask):
#     """
#
#     """
#     pipeline = 'run_all'
#
#     def requires(self):
#         yield PtoIntervals()
#         yield PaidByMonth()
#         yield car_deals.DealsChangedRows()
#         yield ext_arkona.ExtPymast()
#         yield ClockHoursByMonth()
#         yield DealsGrossByMonth()
#         yield DealsByMonth()
