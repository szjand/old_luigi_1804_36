# encoding=utf-8
"""

"""
import luigi
import utilities
import datetime
import ext_arkona
import smtplib
from email.message import EmailMessage

pipeline = 'xfm_arkona'
db2_server = 'report'
pg_server = '173'
ts_no_spaces = str(datetime.datetime.now()).replace(" ", "")


class XfmPymast(luigi.Task):
    """
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield ext_arkona.ExtPymast()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = "select arkona.xfm_pymast()"
                pg_cur.execute(sql)


class XfmPyactgr(luigi.Task):
    """
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield ext_arkona.ExtPyactgr()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = "select arkona.xfm_pyactgr()"
                pg_cur.execute(sql)


class XfmPypclockin(luigi.Task):
    """
    populates table arkona.xfm_pypclockin, which is a pseudo-fact for clock hours
    has employee numbers rather than employee keys, no employee dim yet
    09/30/20: this is now called by XfmPypclockinEmail which tests for anomalies
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield ext_arkona.ExtPypclockin()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = "select * from arkona.xfm_pypclockin()"
                pg_cur.execute(sql)


class XfmPypclockinEmail(luigi.Task):
    """
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield XfmPypclockin()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = "select arkona.xfm_pypclockin_email();"
                pg_cur.execute(sql)
                if pg_cur.fetchone()[0] != 0:
                    msg = EmailMessage()
                    msg.set_content('xfm_pypclockin requires attention')
                    msg['Subject'] = 'xfm_pypclockin requires attention'
                    msg['From'] = 'jandrews@cartiva.com'
                    msg['To'] = 'jandrews@cartiva.com'
                    s = smtplib.SMTP('mail.cartiva.com')
                    s.send_message(msg)
                    s.quit()


class XfmInpmast(luigi.Task):
    """
    full blown type 2, any change in any attribute in inpmast generates a new row
    started persisting history in sep 2017
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield ext_arkona.ExtInpoptd()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute("select arkona.xfm_inpmast()")


class XfmBopname(luigi.Task):
    """
    full blown type 2, any change in any attribute except updated_by_user and timestamp_updated
    in inpmast generates a new row
    exclude those 2 attributes because they get updated if the record is just touched and not changed
    started persisting history in 06/13/2018
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield ext_arkona.ExtBopname()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute("select arkona.xfm_bopname()")


class XfmBopmast(luigi.Task):
    """
    full blown type 2, any change in any attribute
    started history with 1/1/2019, from ubuntu cdc
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield ext_arkona.ExtBopmast()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute("select arkona.xfm_bopmast()")


class XfmArkona(luigi.WrapperTask):
    """
    a wrapper task that invokes all the xfm classes in this module
    this task is susequently listed as a requirement in run_all
    """
    pipeline = pipeline

    def requires(self):
        yield XfmInpmast()
        yield XfmPymast()
        yield XfmPyactgr()
        yield XfmPypclockinEmail()
        yield XfmBopname()
        yield XfmBopmast()


# if __name__ == '__main__':
#     luigi.run(["--workers=4"], main_task_cls=XfmArkona)
