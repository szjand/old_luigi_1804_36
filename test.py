# encoding=utf-8
"""
"""
import luigi
import datetime
import utilities


pipeline = 'test'
pg_server = '173'


class Test(luigi.Task):
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):

        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = "select arkona.xfm_pyactgr()"
                pg_cur.execute(sql)


if __name__ == '__main__':
    luigi.run(["--workers=4"], main_task_cls=Test)
