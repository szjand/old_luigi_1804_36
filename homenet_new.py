# encoding=utf-8
"""
cronjob: 7 past the hour between 9AN and 9PM mon - sat
ts_with_minute: used for generating the output files, include hours & minutes to enable
running a task multiple times a day
7/12/19:
    need to exclude vehicles with cost < $10K, currently they got to homenet with price coming soon flag set
    to true, but according to yem, the actual numbers go to dealer teamwork
    whatever
    the best place to filter out vehicle based on current cost is in ExtNewData (inpmast)
12/03/20:
    changed the GenerateFile query for December
"""
import luigi
import utilities
import csv
import datetime
import ftplib
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
import os
import smtplib
import opentrack


pipeline = 'internet_feeds'
db2_server = 'report'
pg_server = '173'
ts_no_spaces = str(datetime.datetime.now()).replace(" ", "")
ts_with_minute = '{:%Y-%m-%d-%H-%M}'.format(datetime.datetime.now())


# # if these task are being called via run_all, that task will handle all of this logging
@luigi.Task.event_handler(luigi.Event.START)
def task_start(self):
    utilities.luigi_log_start(pg_server, self.task_id + '__' + ts_no_spaces, self.pipeline, self.__class__.__name__)


@luigi.Task.event_handler(luigi.Event.SUCCESS)
def task_success(self):
    utilities.luigi_log_pass(pg_server, self.task_id + '__' + ts_no_spaces, self.pipeline, self.__class__.__name__)
    try:  # WrapperTest throws an AttributeError, see /Notes/issues
        with self.output().open("w") as out_file:
            # write to the output file
            out_file.write('pass')
    except AttributeError:
        pass


class ExtNewData(luigi.Task):
    """
    9/25/19 added best_price to ifd.ext_new_data
    """
    extract_file = '../../extract_files/ext_new_data.csv'
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + ts_with_minute + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        return opentrack.UpdateNewVehiclePrices()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.arkona_luigi_27(db2_server) as cn:
            with cn.cursor() as cur:
                sql = """
                    SELECT trim(inpmast_vin), trim(inpmast_stock_number), 
                      'NEW', year, trim(make), trim(model), trim(model_code), trim(replace(body_Style, '"', '')), 
                      odometer,
                      trim(color) as EXTCOLOR, 
                      trim(trim) as INTCOLOR,
                      e.num_field_value, --invoice
                      list_price, -- msrp
                      b.num_field_value, -- internet price
                      trim(color_code), 
                      g.num_field_value -- best price
                    FROM rydedata.INPMAST a
                    left join rydedata.inpoptf b on a.inpmast_vin = b.vin_number
                    join rydedata.inpoptd c on b.company_number = c.company_number
                      and b.seq_number = c.seq_number
                      and c.field_descrip = 'Internet Price'
                    left join rydedata.inpoptf e on a.inpmast_vin = e.vin_number
                    join rydedata.inpoptd f on e.company_number = f.company_number
                      and e.seq_number = f.seq_number
                      and f.field_descrip = 'Flooring/Payoff'  
                    left join rydedata.inpoptf g on a.inpmast_vin = g.vin_number
                    join rydedata.inpoptd h on g.company_number = h.company_number
                      and g.seq_number = h.seq_number
                      and h.field_descrip = 'Best Price'                      
                    where a.status = 'I'
                      and a.type_n_u = 'N'  
                      and a.inpmast_vehicle_cost > 10000      
                      and a.inpmast_company_number = 'RY1'                         
                """
                cur.execute(sql)
                with open(self.extract_file, 'w') as f:
                    csv.writer(f).writerows(cur)
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute("truncate ifd.ext_new_data")
                with open(self.extract_file, 'r') as io:
                    pg_cur.copy_expert(
                        """copy ifd.ext_new_data from stdin with csv encoding 'latin-1 '""", io)


class GenerateFile(luigi.Task):
    """
    writes a header line
    then appends the data lines
    added field to ifd.homenet_new: price_before_incentives
    9/11/19
    case: when internet_price = 0 then msrp as price_before_incentives
    12/4/2020
        emp inc has changed this month, added join to gmgl.get_vehicle_prices() (dd), if dd.has_emp_inc, then
        price before incentives = dd.testing_price
    01/11/2021
        per yem, cadillac price before incentive should = internet price
    """
    extract_file = '../../extract_files/RydellNew.txt'
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + ts_with_minute + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield ExtNewData()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as cn:
            with cn.cursor() as cur:
                sql = """ 
                    select replace(string_agg(upper(column_name), '|'), '_','-')
                    from information_schema.columns
                    where table_schema = 'ifd'
                      and table_name = 'homenet_new'                
                """
                cur.execute(sql)
                # headers
                with open(self.extract_file, 'w') as f:
                    csv.writer(f).writerows(cur)
                cur.execute('truncate ifd.homenet_new')
                sql = """
                    insert into ifd.homenet_new (dealershipname,vin,stock,new_used,year,make,model,
                      modelnumber,body,miles,extcolor,intcolor,invoice,msrp,sellingprice,color_code,
                      photourls,intransit,price_coming_soon,price_before_incentives)
                    SELECT 'Rydell GM Auto Center', aa.vin, aa.stock_number, 
                      'NEW', aa.model_year, aa.make, aa.model, aa.model_code, 
                      replace(aa.body_Style, '"', ''), aa.odometer,
                      aa.ext_color as EXTCOLOR, aa.int_color as INTCOLOR,
                      aa.invoice, aa.msrp, aa.internet_price, aa.color_code,
                      case 
                        when exists (select 1 from nrv.new_vehicle_pictures where vin = aa.vin) then
                          (select string_agg(url, ',')
                          from (
                            select *
                            from nrv.new_vehicle_hero_cards
                            union       
                            select url, sequence_number
                            from nrv.new_vehicle_pictures 
                            where vin = aa.vin 
                              and thru_ts > now() order by sequence_number) z) 
                        else null
                        end as "photoURLs",
                        case when bb.vin is null then 'intransit' else 'onlot' end,
                        case when aa.internet_price = 0 then true else false end,
                        case
                          when aa.make = 'cadillac' then aa.internet_price::integer
                          when aa.internet_price = 0 then aa.msrp::integer
                          when dd.has_emp_inc then dd.testing_price
                          else (aa.internet_price + coalesce(cc.total_cash, 0))::integer 
                        end as price_before_incentives  
                    FROM ifd.ext_new_data  aa
                    left join nc.vehicle_acquisitions bb on aa.vin = bb.vin 
                      and bb.thru_date > current_date
                    left join gmgl.vin_incentive_cash cc on aa.vin = cc.vin
                      and cc.thru_ts > now()
                      and cc.is_best_incentive
                    left join gmgl.get_vehicle_prices() dd on aa.stock_number = dd.stock_number
                      and aa.vin = dd.vin
                      and dd.has_emp_inc
                    where aa.vin not in ( --exclude list, pending board, sold orders, fleet orders
                      select vin
                      from gmgl.daily_inventory
                      where is_fleet = true
                        or is_sold = true
                        or stock_number in (
                          select stock_number
                          from board.pending_boards
                          where is_delivered = false
                            and is_deleted = false));                                      
                """
                cur.execute(sql)
                cur.execute('select * from ifd.homenet_new')
                # data
                with open(self.extract_file, 'a') as f:
                    csv.writer(f, delimiter='|', quotechar='"', quoting=csv.QUOTE_NONE).writerows(cur)


class FtpFile(luigi.Task):
    """
    """
    extract_file = '../../extract_files/RydellNew.txt'
    file_name = 'RydellNew.txt'
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + ts_with_minute + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield GenerateFile()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        # server = 'download.cartiva.com'
        # username = 'download'
        # password = 'th!s!sc@rt!v@ftp'
        # # homenet
        server = 'iol.homenetinc.com'
        username = 'hndatafeed'
        password = 'gx8m6'
        ftp = ftplib.FTP(server)
        ftp.login(username, password)
        ftp.storbinary('STOR '+self.file_name, open(self.extract_file, 'rb'))
        ftp.quit()


class EmailFile(luigi.Task):
    """
    based on python projects/open_ros/email_test.py
    mods:
        a single attachment
        smtp throws: AttributeError: SMTP instance has no attribute '__exit__'
            when invoked in a context manager, maybe a python 2 issue
    """
    extract_file = '../../extract_files/RydellNew.txt'
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + ts_with_minute + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield FtpFile()

    def output(self):
        return luigi.LocalTarget(self.local_target())

    def run(self):
        comma_space = ', '
        sender = 'jandrews@cartiva.com'
        # recipients = ['jandrews@cartiva.com', 'test@cartiva.com']
        # # homenet
        recipients = ['jandrews@cartiva.com', 'mdelohery@rydellcars.com',
                      'myem@rydellcars.com', 'abruggeman@cartiva.com']
        outer = MIMEMultipart()
        outer['Subject'] = 'Homenet New Vehicle Feed'
        outer['To'] = comma_space.join(recipients)
        outer['From'] = sender
        with open(self.extract_file, 'rb') as fp:
            msg = MIMEBase('application', "octet'stream")
            msg.set_payload(fp.read())
            msg.add_header('Content-Disposition', 'attachment', filename=os.path.basename(self.extract_file))
            outer.attach(msg)
        composed = outer.as_string()
        s = smtplib.SMTP('mail.cartiva.com')
        s.sendmail(sender, recipients, composed)
        s.close()


# if __name__ == '__main__':
#     luigi.run(["--workers=2"], main_task_cls=EmailFile)
