﻿-- based on work done in e:\python projects\honda_sales\honda_incentives.sql

------------------------------------------------------------------
--< 06/02/20 new incentives for june
------------------------------------------------------------------
-- 1. update hn.stg_hin_incentives
truncate hn.stg_hin_incentives;
insert into hn.stg_hin_incentives
select a.*,
  md5(a::text) as hash
from (  
select split_part(model,E'\n', 1), split_part(model, E'\n', 2), split_part(model, E'\n', 3) ||  split_part(model, E'\n', 4), -- split_part(model, E'\n', 3),
  split_part(dealer_finance_cash, E'\n', 1) as program_id, split_part(dealer_finance_cash, E'\n', 2), split_part(dealer_finance_cash, E'\n', 3),
  split_part(dealer_finance_cash, E'\n', 4), split_part(dealer_finance_cash, E'\n', 5), split_part(dealer_finance_cash, E'\n', 6),
  split_part(dealer_finance_cash, E'\n', 7), split_part(dealer_finance_cash, E'\n', 8), split_part(dealer_finance_cash, E'\n', 9) as field_7,
  split_part(dealer_finance_cash, E'\n', 10) as field_8, split_part(dealer_finance_cash, E'\n', 11), split_part(dealer_finance_cash, E'\n', 12)
from hn.ext_hin_incentives
where left(model, 4) = '2020' 
  and split_part(dealer_finance_cash, E'\n', 1) <> 'N/A') a;

-- 2. insert the new incentives
insert into hn.hin_incentives values
('HP-X80','2020 Fit Dealer, Lease, Finance Cash',500, null, '06/02/2020','07/06/2020'),  
('HP-X91','2020 Accord Dealer, Lease, Finance Cash',500, null, '06/02/2020','07/06/2020'),  
('HP-X93','2020 Insight Dealer, Lease, Finance Cash',500, null, '06/02/2020','07/06/2020'), 
('HP-Y03','2020 HR-V Dealer, Lease, Finance Cash',500, null, '06/02/2020','07/06/2020'), 
('HP-Y04','2020 HR-V Conquest',750, null, '06/02/2020','07/06/2020'), 
('HP-Y11','2020 CR-V Dealer, Lease, Finance Cash',500, 'AWD only excluding Hybrid', '06/02/2020','07/06/2020'),
('HP-Y14','2020 Pilot Dealer, Lease, Finance Cash',1750, null, '06/02/2020','07/06/2020'),  
('HP-Y15','2020 Pilot Loyalty',1000, null, '06/02/2020','07/06/2020'), 
('HP-Y16','2020 Pilot Conquest',1000, null, '06/02/2020','07/06/2020'), 
('HP-Y26','2020 Odyssey Dealer, Lease, Finance Cash', 750, null, '06/02/2020','07/06/2020'),
('HP-Y27','2020 Odyssey Loyalty', 1000, null, '06/02/2020','07/06/2020');  

------------------------------------------------------------------
--/> 06/02/20 new incentives for june
------------------------------------------------------------------