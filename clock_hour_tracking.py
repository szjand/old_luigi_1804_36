# encoding=utf-8
"""

"""
import luigi
import utilities
import datetime
import xfm_arkona

pipeline = 'clock_hours'
db2_server = 'report'
pg_server = '173'


class HourlyEmployees(luigi.Task):
    """

    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                # ads requirement
                pg_cur.execute("select ops.check_for_ads_extract('ads_ext_edw_employee_dim')")
                if not pg_cur.fetchone()[0]:
                    raise ValueError('The ads requirement: ads_ext_edw_employee_dim is missing')
                sql = "truncate jeri.hourly_employees"
                pg_cur.execute(sql)
                sql = """
                    insert into jeri.hourly_employees  
                    select a.storecode, a.pydept, a.distcode, a.distribution, a.name, a.employeenumber, 
                      a.hourlyrate, a.termdate, a.employeekeyfromdate, a.employeekeythrudate
                    from ads.ext_edw_employee_dim a
                    where a.payrollClassCode = 'H'
                      and a.storecode in ('RY1','RY2')
                      and a.termdate > '01/01/2017'
                      and a.employeekeythrudate > '12/31/2016'
                    order by a.storecode, a.pydept, a.distcode, a.distribution, a.name;                
                """
                pg_cur.execute(sql)


class ClockHourTracking(luigi.Task):
    """

    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield HourlyEmployees()
        yield xfm_arkona.XfmPypclockin()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = "truncate jeri.clock_hour_tracking"
                pg_cur.execute(sql)
                sql = """
                    insert into jeri.clock_hour_tracking
                    select b.store_Code, b.department, b.dist_code, b.dist_code_description, 
                      aa.the_year, aa.year_month, max(b.employee_name), b.employee_number,
                      sum(a.reg_hours) as reg_hours, sum(a.ot_hours) as ot_hours, 
                      sum(ot_hours * 1.5 * hourly_rate) as ot_comp
                    from arkona.xfm_pypclockin a
                    inner join dds.dim_date aa on a.the_date = aa.the_date
                    inner join jeri.hourly_employees b on a.employee_number = b.employee_number
                      and a.the_date between b.ek_from_date and b.ek_thru_date
                      -- 7/7 add term date
                      and a.the_date <= b.term_date
                    where a.the_date between '01/01/2017' and current_date
                    group by  b.store_Code, b.department, b.dist_code, b.dist_code_description, 
                      aa.the_year, aa.year_month, /*b.employee_name,*/ b.employee_number             
                """
                pg_cur.execute(sql)


class ClockHourTrackingDaily(luigi.Task):
    """

    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield xfm_arkona.XfmPypclockin()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = """
                    insert into jeri.clock_hour_tracking_daily
                    select current_date as run_date, a.the_year, a.year_month, a.the_date, b.employee_number, 
                      b.reg_hours, b.ot_hours,  round(1.5 * ot_hours * hourlyrate, 2) as ot_comp,
                      c.name, c.storecode, c.pydept, c.distcode, c.distribution, 
                      c.payrollclasscode, c.hourlyrate
                    from dds.dim_date a
                    inner join arkona.xfm_pypclockin b on a.the_date = b.the_date
                    inner join ads.ext_edw_employee_dim c on b.employee_number = c.employeenumber
                      and a.the_date between c.employeekeyfromdate and c.employeekeythrudate
                    where a.the_date between '01/01/2020' and current_date - 1           
                """
                pg_cur.execute(sql)


class ClockHoursWrapper(luigi.WrapperTask):
    """
    a wrapper task that invokes all the classes in this module
    this task is susequently listed as a requirement in run_all
    that way i don't need to construct atrificial dependencies just to ensure everything gets run
    """
    pipeline = pipeline

    def requires(self):
        yield ClockHourTracking()
        yield ClockHourTrackingDaily()


# if __name__ == '__main__':
#     luigi.run(["--workers=4"], main_task_cls=ClockHours)


