# encoding=utf-8
"""
6/15/18
first task for afton's stuff to be integrated into luigi
it came up because it requires fact_gl to have been run and we discovered that her scheduled task was being
run before fact_gl was and so every day it appeared that her task had not run because the data it was using was a
day old
perfect fit for luigi
"""
import luigi
import utilities
import datetime
import fact_gl

pipeline = 'afton_tasks'
db2_server = 'report'
pg_server = '173'
ts_no_spaces = str(datetime.datetime.now()).replace(" ", "")


class BoardAccountingGross(luigi.Task):
    """
    updates board.gross_reviews
    inserts new rows in board.daily_accounting_gross
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield fact_gl.FactGl()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute("select board.daily_deal_accounting_gross_pop()")


class AftonTask(luigi.WrapperTask):
    """

    """
    pipeline = pipeline

    def requires(self):
        yield BoardAccountingGross()
