# encoding=utf-8
"""
cd /home/jon/projects/luigi_1804_36 && /home/jon/Desktop/venv/luigi/bin/python3 -m luigi --module nissan_csi CsiParse

02/07/21
    i am confused. i could sswear that previous to today this ran fine without pyvirtualdisplay
    but today, it could not click MY LINKS without it
"""

import utilities
import luigi
import datetime
import os
from selenium import webdriver
import time
import csv
import requests
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from pyvirtualdisplay import Display

pipeline = 'nissan_csi'
pg_server = '173'
ts_no_spaces = str(datetime.datetime.now()).replace(" ", "")
# user = 'XD163692'
# password = '%6#@%!!@eDHN'
user = '41d9ec15-5148-439d-938a-313a1fa91cc6'
passcode = '666'
username = None
password = None

# local_or_production = 'production'
local_or_production = 'local'


# # if these task are being called via run_all, that task will handle all of this logging
@luigi.Task.event_handler(luigi.Event.START)
def task_start(self):
    utilities.luigi_log_start(pg_server, self.task_id + '__' + ts_no_spaces, self.pipeline, self.__class__.__name__)


@luigi.Task.event_handler(luigi.Event.SUCCESS)
def task_success(self):
    utilities.luigi_log_pass(pg_server, self.task_id + '__' + ts_no_spaces, self.pipeline, self.__class__.__name__)
    try:  # WrapperTest throws an AttributeError, see /Notes/issues
        with self.output().open("w") as out_file:
            # write to the output file
            out_file.write('pass')
    except AttributeError:
        pass


class CsiDownload(luigi.Task):
    """
    04/06
    failed on production, elements not discoveralbe
    installed pyvirtualdisplay
    wrapped run in:
    with Display(visible=False, size=(1200,1500)):
    https://stackoverflow.com/questions/45520464/selenium-script-working-from-console-not-working-
        in-cron-geckodriver-error


    """
    pipeline = pipeline
    # # local
    # pdf_dir = '/home/jon/projects/luigi_1804_36/nissan/vehicle_inventory_detail/pdf_files/'
    sales_csi_dir = '/home/jon/projects/luigi_1804_36/nissan/sales_csi/'
    service_csi_dir = '/home/jon/projects/luigi_1804_36/nissan/service_csi/'
    sales_csi_file = sales_csi_dir + 'sales_csi.csv'
    service_csi_file = service_csi_dir + 'service_csi.csv'

    # # production
    # pdf_dir = '/home/rydell/projects/luigi_1804_36/nissan/vehicle_inventory_detail/pdf_files/'

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                pipeline + '_' + self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    @staticmethod
    def get_secret(category, platform):
        modified_platform = platform.replace(' ', '+')
        url = ("https://beta.rydellvision.com:8888/vision/secret?platform={0}&category={1}".format
               (modified_platform, category))
        response = requests.get(url, auth=(user, passcode))
        return response.json()

    def setup_driver(self):
        # # Chromedriver path
        chromedriver_path = '/usr/bin/chromedriver'
        options = webdriver.ChromeOptions()
        # profile = {"download.default_directory": self.pdf_dir}
        # options.add_experimental_option("prefs", profile)
        options.add_argument('--user-agent="Mozilla/5.0 ((Windows NT 10.0; Win64; x64) AppleWebKit/537.36 '
                             '(KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36"')
        driver = webdriver.Chrome(executable_path=chromedriver_path, options=options)
        return driver

    def run(self):
        with Display(visible=False, size=(1200, 1500)):
            for filename in os.listdir(self.sales_csi_dir):
                os.unlink(self.sales_csi_dir + filename)
            for filename in os.listdir(self.service_csi_dir):
                os.unlink(self.service_csi_dir + filename)
            driver = self.setup_driver()
            try:
                driver.get('https://as.na.nissan.biz/SecureAuth71/')
                # SIGNING IN
                credentials = self.get_secret('Cartiva', 'Nissan NNAnet')
                resp_dict = credentials
                username_box = driver.find_element_by_class_name('txtUserid')
                password_box = driver.find_element_by_class_name('tbxPassword')
                login_btn = driver.find_element_by_class_name('btn-secureauth')
                username_box.send_keys(resp_dict.get('username'))
                password_box.send_keys(resp_dict.get('secret'))
                login_btn.click()

                # Click MY LINKS
                WebDriverWait(driver, 20).until(ec.element_to_be_clickable(('id', '1a'))).click()
                # wait until page finishes loading
                WebDriverWait(driver, 10).until(ec.presence_of_element_located(
                    ('xpath', '//*[@id="linklisting-containerID"]/div[5]/div/ul/li[1]/a')))
                # load the Nissan Customer Experience page
                WebDriverWait(driver, 10).until(ec.presence_of_element_located(
                    ('xpath', '//*[@id="accordion"]/div/div[13]/h4/a'))).click()
                # Owner First Survey
                WebDriverWait(driver, 10).until(ec.element_to_be_clickable(
                    ('xpath', '//*[@id="6"]/div/ul[8]/li/a'))).click()
                time.sleep(10)
                # switch to the new window, not sure if a new tab constitues a new window?
                driver.switch_to.window(driver.window_handles[1])
                # wait until page finishes loading
                WebDriverWait(driver, 10).until(ec.presence_of_element_located(
                    ('xpath', '//*[@id="dealersales"]/div/table[1]/tbody/tr/td[2]/a')))
                time.sleep(10)
                # sales csi
                sales_mtd = driver.find_element_by_xpath('//*[@id="dealersales"]/div/table[1]/tbody/tr/td[2]/a').text
                sales_district_mtd = driver.find_element_by_xpath(
                    '//*[@id="dealersales"]/div/table[1]/tbody/tr/td[4]/span').text
                sales_qtd = driver.find_element_by_xpath('//*[@id="dealersales"]/div/table[2]/tbody/tr/td[2]/a').text
                sales_tier_qtd = driver.find_element_by_xpath(
                    '//*[@id="dealersales"]/div/table[2]/tbody/tr/td[4]/span').text
                sales_3mtd = driver.find_element_by_xpath('//*[@id="dealersales"]/div/table[3]/tbody/tr/td[2]/a').text
                sales_district_3mtd = driver.find_element_by_xpath(
                    '//*[@id="dealersales"]/div/table[3]/tbody/tr/td[4]/span').text
                the_sales = [sales_mtd, sales_district_mtd, sales_qtd, sales_tier_qtd, sales_3mtd, sales_district_3mtd]
                # print(the_sales)
                with open(self.sales_csi_file, 'w') as sales_csi:
                    the_writer = csv.writer(sales_csi)
                    the_writer.writerow(the_sales)
                assert len(os.listdir(self.sales_csi_dir)) == 1
                # service_csi
                service_mtd = driver.find_element_by_xpath(
                    '//*[@id="dealerservice"]/div/table[1]/tbody/tr/td[2]/a').text
                service_district_mtd = driver.find_element_by_xpath(
                    '//*[@id="dealerservice"]/div/table[1]/tbody/tr/td[4]/span').text
                service_qtd = driver.find_element_by_xpath(
                    '//*[@id="dealerservice"]/div/table[2]/tbody/tr/td[2]/a').text
                service_tier_qtd = driver.find_element_by_xpath(
                    '//*[@id="dealerservice"]/div/table[2]/tbody/tr/td[4]/span').text
                service_3mtd = driver.find_element_by_xpath(
                    '//*[@id="dealerservice"]/div/table[3]/tbody/tr/td[2]/a').text
                service_district_3mtd = driver.find_element_by_xpath(
                    '//*[@id="dealerservice"]/div/table[3]/tbody/tr/td[4]/span').text
                the_service = [service_mtd, service_district_mtd, service_qtd, service_tier_qtd,
                               service_3mtd, service_district_3mtd]
                # print(the_service)
                with open(self.service_csi_file, 'w') as service_csi:
                    the_writer = csv.writer(service_csi)
                    the_writer.writerow(the_service)
                assert len(os.listdir(self.service_csi_dir)) == 1
            finally:
                driver.quit()


class CsiParse(luigi.Task):
    """
    """
    pipeline = pipeline

    if local_or_production == 'local':
        sales_csi_dir = '/home/jon/projects/luigi_1804_36/nissan/sales_csi/'
        service_csi_dir = '/home/jon/projects/luigi_1804_36/nissan/service_csi/'
        sales_csi_file = sales_csi_dir + 'sales_csi.csv'
        service_csi_file = service_csi_dir + 'service_csi.csv'
    # elif local_or_production == 'production':
    #     download_dir = '/home/rydell/projects/luigi_1804_36/global_connect/sales_make_csi/download/'
    #     final_csv_dir = '/home/rydell/projects/luigi_1804_36/global_connect/sales_make_csi/final_csv/'

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                pipeline + '_' + self.__class__.__name__ + ".txt")

    def requires(self):
        yield CsiDownload()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute('truncate hn.nna_sales_make_csi_ext')
                pg_cur.execute('truncate hn.nna_service_make_csi_ext')
                with open(self.sales_csi_file, 'r') as io:
                    pg_cur.copy_expert("copy hn.nna_sales_make_csi_ext from stdin with csv encoding 'latin-1 '", io)
                with open(self.service_csi_file, 'r') as io:
                    pg_cur.copy_expert("copy hn.nna_service_make_csi_ext from stdin with csv encoding 'latin-1 '", io)
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute('select hn.nna_sales_make_csi_update();')
                pg_cur.execute('select hn.nna_service_make_csi_update();')


if __name__ == '__main__':
    luigi.run(["--workers=1"], main_task_cls=CsiParse)
