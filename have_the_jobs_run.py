# encoding=utf-8
"""
run every 5 minutes
generate an email if the task in question has not passed in the proscribed time period
luigi generates an email if a task fails
don't want to be bothered with Cron Daemon emails for every task that passes, could be hundreds a day
but i want some level of confidence that the damned jobs are running as expected

so, upon realizing there are multiple relevant parameters to determine  the nature of the query to test for
a job having run, i got bogged down into trying to design a table to hold all the relevant attributes
so
fuck, just hard code a bunch of tasks to check, let's see what turns out to be necessary
sql file in /tem/have_the_jobs_run/configure_tests.sql
1. complireports: every 20 minutes between 7AM and 7PM every day
2. homenet new feed monday thru saturday @ 7 past the hour from 9AM to 5PM
"""

import luigi
import utilities
import datetime
import smtplib
from email.message import EmailMessage
import os

pipeline = 'job_check'
pg_server = '173'
ts_no_spaces = str(datetime.datetime.now()).replace(" ", "")
target_datetime = '{:%Y-%m-%d-%H-%M}'.format(datetime.datetime.now())


# if these task are being called via run_all, that task will handle all of this logging
@luigi.Task.event_handler(luigi.Event.START)
def task_start(self):
    utilities.luigi_log_start(pg_server, self.task_id + '__' + ts_no_spaces, self.pipeline, self.__class__.__name__)


@luigi.Task.event_handler(luigi.Event.SUCCESS)
def task_success(self):
    utilities.luigi_log_pass(pg_server, self.task_id + '__' + ts_no_spaces, self.pipeline, self.__class__.__name__)
    try:  # WrapperTest throws an AttributeError, see /Notes/issues
        with self.output().open("w") as out_file:
            # write to the output file
            out_file.write('pass')
    except AttributeError:
        pass


class JobCheck(luigi.Task):
    """
    """
    pipeline = pipeline

    def local_target(self):
        return '../../luigi_output_files/' + target_datetime + '_' + self.__class__.__name__ + ".txt"

    def requires(self):
        return None

    def output(self):
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            # 1. complireports
            with pg_con.cursor() as pg_cur:
                sql = """
                    select 
                      case when now()::time between '07:41:00' and '19:00:00' then  
                        case
                          when extract (epoch from (now() - coalesce(max(from_ts), 
                            (now() - interval '1 day'))))/60 > 39 then 1
                        end
                        else 0
                      end as failed
                    from luigi.luigi_log
                    where task = 'CompliReports';            
                """
                pg_cur.execute(sql)
                if pg_cur.fetchone()[0] != 0:
                    msg = EmailMessage()
                    msg.set_content('compli.CompliReports has not passed in more than 21 minutes')
                    msg['Subject'] = 'compli.CompliReports has not passed in more than 21 minutes'
                    msg['From'] = 'jandrews@cartiva.com'
                    msg['To'] = 'jandrews@cartiva.com'
                    s = smtplib.SMTP('mail.cartiva.com')
                    s.send_message(msg)
                    s.quit()
        # clean up the /var/mail/rydell file
        os.system('> /var/mail/rydell')
