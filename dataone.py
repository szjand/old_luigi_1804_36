# encoding=utf-8
"""
ftp & zip: overwrite existing file in target directory
the copy string for dao.LKP_VEH_OEM_OPTION (file has quoted empty string for null):
    copy dao.LKP_VEH_OEM_OPTION from  stdin with (FORMAT csv, header, FORCE_NULL(oem_option_msrp,oem_option_invoice))
the copy string for dao.DEF_OEM_OPTION (file has " as inches):
    copy dao.DEF_OEM_OPTION from stdin with (FORMAT csv, header, escape '\\')
"""
import luigi
import datetime
import utilities
import ftplib
import zipfile

pipeline = 'dataone'
pg_server = '173'


class Dataone(luigi.Task):
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        server = 'ftp.dataonesoftware.com'
        username = 'cartiva'
        password = 'Yn9bAsKiNI6psAyG'
        file_name = 'DataOne_US_LDV_Data.zip'
        ftp_target = '../../dataone_files/'
        ftp = ftplib.FTP(server)
        ftp.login(username, password)
        with open(ftp_target + file_name, 'wb') as fhandle:
            ftp.retrbinary('RETR ' + file_name, fhandle.write)
        ftp.quit()
        zipfile.ZipFile(ftp_target + file_name).extractall(ftp_target)

        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as trunc_seq:
                sql = """
                    select table_name, file_name
                    from dao.maint_load_order
                    order by truncate_seq
                """
                trunc_seq.execute(sql)
                for rows in trunc_seq:
                    with pg_con.cursor() as trunc:
                        sql = """
                            truncate {0} cascade;
                        """.format(rows[0])
                        trunc.execute(sql)
            with pg_con.cursor() as load_seq:
                sql = """
                    select table_name, file_name
                    from dao.maint_load_order
                    order by load_seq
                """
                load_seq.execute(sql)
                for rows in load_seq:
                    file_name = ftp_target + rows[1]
                    with open(file_name, 'r') as io:
                        with pg_con.cursor() as load:
                            print(file_name)
                            if rows[1] == 'DEF_OEM_OPTION.csv':
                                load.copy_expert("""copy dao.DEF_OEM_OPTION from stdin
                                    with (FORMAT csv, header, escape '\\')""", io)
                            if rows[1] == 'LKP_VEH_OEM_OPTION.csv':
                                load.copy_expert("""copy dao.LKP_VEH_OEM_OPTION from stdin
                                    with (FORMAT csv, header, FORCE_NULL(oem_option_msrp,oem_option_invoice))""", io)
                            load.copy_expert("""copy {0} from stdin with csv header""".format(rows[0]), io)

if __name__ == '__main__':
    luigi.run(["--workers=4"], main_task_cls=Dataone)