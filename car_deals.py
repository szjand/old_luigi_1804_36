# encoding=utf-8
"""
6/23/17: type_2_b: generate year_month from xfm_sales.gl_date
8/4/17: ExtAccountingDeals: added truncate to sls.ext_accounting_base
8/30/17: ExtDeals: expand extract of bopmast to all rows
            limit input to ext_deals to origination_date > 20170000
"""
import luigi
import utilities
import datetime
import csv
import fact_gl

pipeline = 'car_deals'
db2_server = 'report'
pg_server = '173'


class ExtDeals(luigi.Task):
    """
    6/28/17: H10275P RY2 FRANCHISE_CODE = UC, s/b OUC, fix in insert into sls.ext_deals
    """
    extract_file = '../../extract_files/bopmast_partial.csv'
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    # -------------------------------- change nothing below this line -------------------------------------------------#
    def requires(self):
        return None

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.arkona_luigi_27(db2_server) as cn:
            with cn.cursor() as cur:
                sql = """
                    select TRIM(BOPMAST_COMPANY_NUMBER),RECORD_KEY,TRIM(RECORD_STATUS),TRIM(RECORD_TYPE),
                      TRIM(SALE_TYPE),TRIM(FRANCHISE_CODE),TRIM(VEHICLE_TYPE),TRIM(BOPMAST_STOCK_NUMBER),
                      TRIM(BOPMAST_VIN),ODOMETER_AT_SALE,BUYER_NUMBER,CO_BUYER_NUMBER,
                      TRIM(PRIMARY_SALESPERS),TRIM(SECONDARY_SLSPERS2),trim(LEFT(PD_POLICY_NUMBER, 3)),
                      ORIGINATION_DATE,DATE_APPROVED,DATE_CAPPED,DELIVERY_DATE,
                      GAP_PREMIUM,SERV_CONT_AMOUNT,AMO_TOTAL,coalesce(TRADE_ALLOWANCE, 0),
                      coalesce(TRADE_ACV, 0), coalesce(TRADE_ALLOWANCE, 0) - coalesce(TRADE_ACV, 0),
                      leasing_source, lending_source
                    from rydedata.bopmast
                    -- where origination_date > 20170000
                """
                cur.execute(sql)
                with open(self.extract_file, 'w') as f:
                    csv.writer(f).writerows(cur)
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute("truncate sls.ext_bopmast_partial")
                with open(self.extract_file, 'r') as io:
                    pg_cur.copy_expert("""copy sls.ext_bopmast_partial from stdin with csv encoding 'latin-1 '""", io)
                pg_cur.execute("truncate sls.ext_deals")
                sql = """
                    insert into sls.ext_deals (run_date,store_code,bopmast_id,deal_status,deal_type,sale_type,
                      sale_group,vehicle_type,stock_number,vin,odometer_at_sale,buyer_bopname_id,
                      cobuyer_bopname_id,primary_sc,secondary_sc,fi_manager,origination_date,approved_date,
                      capped_date,delivery_date,gap,service_contract,total_care)
                    select current_date, a.store,a.bopmast_id,coalesce(a.deal_status, 'none'),a.deal_type,a.sale_type,
                      case
                        when a.stock_number = 'H10275P' then 'OUC'
                        else coalesce(a.sale_group,'none')
                      end, a.vehicle_type,a.stock_number,a.vin,a.odometer_at_sale,
                      a.buyer_bopname_id, coalesce(a.cobuyer_bopname_id, -1),
                      case
                        when coalesce(a.primary_sc, 'none') = 'AMA' then 'HAN'
                        when coalesce(a.primary_sc, 'none') = 'NLA' then 'NIL'
                        when coalesce(a.primary_sc, 'none') = 'HAL' and a.store = 'RY1' then 'JHA'
                        when coalesce(a.primary_sc, 'none') = 'WWW' and a.store = 'RY2' then 'WHE'
                        when coalesce(a.primary_sc, 'none') = 'JKO' and a.store = 'RY1' then 'JOL'
                        when coalesce(a.primary_sc, 'none') = 'CBJ' and a.store = 'RY2' then 'CHB'
                        when coalesce(a.primary_sc, 'none') = 'SHI' and a.store = 'RY2' then 'NIC'
                        when coalesce(a.primary_sc, 'none') = 'NDO' and a.store = 'RY1' then 'DOC'
                        when coalesce(a.primary_sc, 'none') = 'FDC' and a.store = 'RY1' then 'FDE'
                        when coalesce(a.primary_sc, 'none') = 'STD' and a.store = 'RY2' then 'STA'
                        when coalesce(a.primary_sc, 'none') = 'DST' and a.store = 'RY1' then 'DAS'
                        when coalesce(a.primary_sc, 'none') = 'TRO' and a.store = 'RY2' then 'THR'
                        when coalesce(a.primary_sc, 'none') = 'BDA' and a.store = 'RY2' then 'BED'
                        when coalesce(a.primary_sc, 'none') = 'AHA' and a.store = 'RY1' then 'ALH'
                        when coalesce(a.primary_sc, 'none') = 'DDU' and a.store = 'RY2' then 'DED'
                        when coalesce(a.primary_sc, 'none') = 'CCA' and a.store = 'RY1' then 'BAL'
                        when coalesce(a.primary_sc, 'none') = 'BDA' and a.store = 'RY2' then 'BED'
                        when coalesce(a.primary_sc, 'none') = 'NNE' and a.store = 'RY2' then 'NIN'
                        else coalesce(a.primary_sc, 'none')
                      end,
                      case
                        when coalesce(a.secondary_sc, 'none') = 'AMA' then 'HAN'
                        when coalesce(a.secondary_sc, 'none') = 'NLA' then 'NIL'
                        when coalesce(a.secondary_sc, 'none') = 'HAL' and a.store = 'RY1' then 'JHA'
                        -- when coalesce(a.secondary_sc, 'none') = 'ENT' and a.store = 'RY1' then 'AJO'
                        when coalesce(a.secondary_sc, 'none') = 'WWW' and a.store = 'RY2' then 'WHE'
                        when coalesce(a.secondary_sc, 'none') = 'JKO' and a.store = 'RY1' then 'JOL'
                        when coalesce(a.secondary_sc, 'none') = 'CBJ' and a.store = 'RY2' then 'CHB'
                        when coalesce(a.secondary_sc, 'none') = 'SHI' and a.store = 'RY2' then 'NIC'
                        when coalesce(a.secondary_sc, 'none') = 'NDO' and a.store = 'RY1' then 'DOC'
                        when coalesce(a.secondary_sc, 'none') = 'FDC' and a.store = 'RY1' then 'FDE'
                        when coalesce(a.secondary_sc, 'none') = 'BDA' and a.store = 'RY2' then 'BED'
                        when coalesce(a.secondary_sc, 'none') = 'NNE' and a.store = 'RY2' then 'NIN'
                        else coalesce(a.secondary_sc, 'none')
                      end,
                      coalesce(a.fi_manager,'none'),
                      (select dds.db2_integer_to_date(a.origination_date)),a.approved_date,
                      a.capped_date,a.delivery_date,a.gap,a.service_contract,a.total_care
                    from sls.ext_bopmast_partial a
                    where a.origination_date > 20170000
                """
                pg_cur.execute(sql)


class ExtAccountingDeals(luigi.Task):
    """

    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    # -------------------------------- change nothing below this line -------------------------------------------------#
    def requires(self):
        yield fact_gl.FactGl()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute("truncate sls.ext_accounting_base")
                sql = """
                    insert into sls.ext_accounting_base
                    select d.the_date, a.control, amount,
                      b.account, b.description as account_description, e.description
                    from fin.fact_gl a
                    inner join fin.dim_account b on a.account_key = b.account_key
                      and b.current_row = true
                      and b.account <> '144500'
                    inner join fin.dim_journal c on a.journal_key = c.journal_key
                    inner join dds.dim_date d on a.date_key = d.date_key
                    inner join fin.dim_gl_description e on a.gl_description_key = e.gl_description_key
                    where b.account_type = 'sale'
                      and b.department_code in ('nc','uc','ao')
                      and c.journal_code in ('vsn','vsu')
                      and a.post_status = 'Y'
                      and
                        case
                          when a.control = 'H12469A' then b.account = '244800'
                          when a.control = 'H12485TA' then b.account = '245200'
                          else 1 = 1
                        end                      
                      and d.the_date between '02/01/2017' and current_date;
                    """
                pg_cur.execute(sql)
                sql = """
                    insert into sls.ext_accounting_deals
                    select current_date, the_date, control, amount, unit_count,
                      account, account_description, gl_description
                    from (  -- ccc all rows
                      select aa.the_date, aa.control, sum(aa.amount) as amount, aa.account,
                        aa.account_description, max(aa.description) as gl_description,
                        sum(case when aa.amount < 0 then 1 else -1 end) as unit_count
                      from sls.ext_accounting_base aa
                      left join (-- multiple rows per date to exclude
                        select x.control, x.the_date
                        from ( -- assign unit count to rows w/multiple rows per date
                          select a.control, a.the_date,
                            case when a.amount < 0 then 1 else -1 end as the_count
                          from sls.ext_accounting_base a
                          inner join ( -- multiple rows per date
                            select control, the_date
                            from sls.ext_accounting_base
                            group by control, the_date
                            having count(*) > 1) b on a.control = b.control and a.the_date = b.the_date) x
                        group by x.control, x.the_date
                        having sum(the_count) = 0) bb on aa.the_date = bb.the_date and aa.control = bb.control
                      where bb.control is null -- this is the line that excludes the multiple entry vehicles
                      group by aa.the_date, aa.control, aa.account, aa.account_description) ccc
                    where not exists (
                      select 1
                      from sls.ext_accounting_deals
                      where control = ccc.control
                        and gl_date = ccc.the_date);
                    """
                pg_cur.execute(sql)


class XfmDealsNewRows(luigi.Task):
    """

    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    # -------------------------------- change nothing below this line -------------------------------------------------#
    def requires(self):
        yield ExtDeals()
        yield ExtAccountingDeals()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = """
                    insert into sls.xfm_deals (run_date, store_code, bopmast_id, deal_status,
                      deal_type, sale_type, sale_group, vehicle_type, stock_number, vin,
                      odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
                      primary_sc, secondary_sc, fi_manager, origination_Date, approved_date,
                      capped_date, delivery_date, gap, service_contract, total_care,
                      row_type, seq,
                      gl_date, gl_count, hash)
                    select a.run_date, a.store_code, a.bopmast_id, a.deal_status,
                      a.deal_type, a.sale_type, a.sale_group, a.vehicle_type, a.stock_number, a.vin,
                      a.odometer_at_sale, a.buyer_bopname_id, a.cobuyer_bopname_id,
                      a.primary_sc, a.secondary_sc, a.fi_manager, a.origination_date, a.approved_date,
                      a.capped_date, a.delivery_date, a.gap, a.service_contract, a.total_care,
                      'new', 1,
                      coalesce(b.gl_date, '12/31/9999'), coalesce(b.unit_count, 0),
                      ( -- generate the hash
                        select md5(z::text) as hash
                        from (
                          select store_code, bopmast_id, deal_status,
                            deal_type, sale_type, sale_group, vehicle_type, stock_number, vin,
                            odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
                            primary_sc, secondary_sc, fi_manager, origination_date, approved_date,
                            capped_date, delivery_date, gap, service_contract,total_care,
                            coalesce(gl_date, '12/31/9999'), coalesce(unit_count, 0)
                          from sls.ext_deals x
                          left join sls.ext_accounting_deals y on x.stock_number = y.control
                            and y.gl_date = (
                              select max(gl_date)
                              from sls.ext_accounting_deals
                              where control = y.control)
                          where x.store_code = a.store_code
                            and x.bopmast_id = a.bopmast_id) z)
                    from sls.ext_deals a
                    left join sls.ext_accounting_deals b on a.stock_number = b.control
                      and b.gl_date = (
                        select max(gl_date)
                        from sls.ext_accounting_deals
                        where control = b.control)
                    where gl_date >= '02/01/2017'
                      and a.vin is not null
                      and not exists (
                        select *
                        from sls.xfm_deals
                        where store_code = a.store_code
                          and bopmast_id = a.bopmast_id) order by stock_number;
                """
                pg_cur.execute(sql)


class XfmDealsChangedRows(luigi.Task):
    """

    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    # -------------------------------- change nothing below this line -------------------------------------------------#
    def requires(self):
        yield ExtDeals()
        yield ExtAccountingDeals()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = """
                    insert into sls.xfm_deals (run_date, store_code, bopmast_id, deal_status,
                      deal_type, sale_type, sale_group, vehicle_type, stock_number, vin,
                      odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
                      primary_sc, secondary_sc, fi_manager, origination_Date, approved_date,
                      capped_date, delivery_date, gap, service_contract, total_care,
                      row_type, seq,
                      gl_date, gl_count, hash)
                    select current_date, e.store_code, e.bopmast_id, e.deal_status, e.deal_type,
                      e.sale_type, e.sale_group, e.vehicle_type,
                      -- e.stock_number, e.vin,
                      coalesce(e.stock_number, f.stock_number), coalesce(e.vin, f.vin),
                      e.odometer_at_sale, e.buyer_bopname_id, e.cobuyer_bopname_id,
                      e.primary_sc, e.secondary_sc, e.fi_manager, e.origination_date,
                      e.approved_date, e.capped_date, e.delivery_date, e.gap,
                      e.service_contract, e.total_care,
                      'update'::citext,
                      (
                        select max(seq) + 1
                        from sls.xfm_deals
                        where store_code = e.store_code
                          and bopmast_id = e.bopmast_id) as seq,
                       e.gl_date, e.gl_count, e.hash
                    from (
                      select z.*, md5(z::text) as hash
                      from (
                        select store_code, bopmast_id, deal_status,
                          deal_type, sale_type, sale_group, vehicle_type, stock_number, vin,
                          odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
                          primary_sc, secondary_sc, fi_manager, origination_date, approved_date,
                          capped_date, delivery_date, gap, service_contract,total_care,
                          coalesce(gl_date, '12/31/9999') as gl_date, coalesce(unit_count, 0) as gl_count
                        from sls.ext_deals x
                        left join sls.ext_accounting_deals y on x.stock_number = y.control
                          and y.gl_date = (
                            select max(gl_date)
                            from sls.ext_accounting_deals
                            where control = y.control)) z) e
                    inner join sls.xfm_deals f on e.store_code = f.store_code
                      and f.seq = (
                        select max(seq)
                        from sls.xfm_deals
                        where store_code = f.store_code
                          and bopmast_id = f.bopmast_id)
                      and e.bopmast_id = f.bopmast_id
                      and e.hash <> f.hash;
                """
                pg_cur.execute(sql)


class XfmDealsDeletedRows(luigi.Task):
    """

    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    # -------------------------------- change nothing below this line -------------------------------------------------#
    def requires(self):
        yield ExtDeals()
        yield ExtAccountingDeals()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = """
                    insert into sls.xfm_deals (run_date, store_code, bopmast_id, deal_status,
                      deal_type, sale_type, sale_group, vehicle_type, stock_number, vin,
                      odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
                      primary_sc, secondary_sc, fi_manager, origination_date, approved_date,
                      capped_date, delivery_date, gap, service_contract, total_care,
                      row_type, seq,
                      gl_date, gl_count, hash)
                    select current_date, a.store_code, a.bopmast_id, 'deleted'::citext as deal_status,
                      a.deal_type, a.sale_type, a.sale_group, a.vehicle_type, a.stock_number, a.vin,
                      a.odometer_at_sale, a.buyer_bopname_id, a.cobuyer_bopname_id,
                      a.primary_sc, a.secondary_sc, a.fi_manager, a.origination_date, a.approved_date,
                      a.capped_date, a.delivery_date, a.gap, a.service_contract, a.total_care,
                      'update'::citext as row_type,
                      (
                        select max(seq) + 1
                        from sls.xfm_deals
                         where store_code = a.store_code
                           and bopmast_id = a.bopmast_id) as seq,
                      c.gl_date, c.unit_count,
                      ( -- generate a new hash
                        select md5(z::text) as hash
                        from (
                          select store_code, bopmast_id, 'deleted',
                            deal_type, sale_type, sale_group, vehicle_type, stock_number, vin,
                            odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
                            primary_sc, secondary_sc, fi_manager, origination_date, approved_date,
                            capped_date, delivery_date, gap, service_contract,total_care,
                            gl_date, gl_count
                          from sls.xfm_deals k
                          where store_code = a.store_code
                            and bopmast_id = a.bopmast_id
                            and seq = (
                              select max(seq)
                              from sls.xfm_deals
                              where store_code = k.store_code
                                and bopmast_id = k.bopmast_id)) z)
                    from sls.xfm_deals a
                    left join sls.ext_deals b on a.store_code = b.store_code
                      and a.bopmast_id = b.bopmast_id
                    left join sls.ext_accounting_deals c on a.stock_number = c.control
                      and c.gl_date = (
                        select max(gl_date)
                        from sls.ext_accounting_deals
                        where control = c.control)
                    where b.store_code is null -- no longer in extract
                      and a.seq = (
                        select max(seq)
                        from sls.xfm_deals
                        where store_code = a.store_code
                          and bopmast_id = a.bopmast_id)
                      and a.deal_status <> 'deleted';
                """
                pg_cur.execute(sql)


class DealsNewRows(luigi.Task):
    """

    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    # -------------------------------- change nothing below this line -------------------------------------------------#
    def requires(self):
        yield XfmDealsNewRows()
        yield XfmDealsChangedRows()
        yield XfmDealsDeletedRows()

    def output(self):
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = """
                    insert into sls.deals (run_date, store_code, bopmast_id, deal_status_code, deal_type_code,
                      sale_type_code, sale_group_code, vehicle_type_code, stock_number, vin,
                      odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
                      primary_sc, secondary_sc, fi_manager, origination_date,
                      approved_date, capped_date, delivery_date, gap, service_contract,
                      total_care, seq, year_month, unit_count,
                      gl_date, deal_status, notes, hash)
                    select a.run_date, a.store_code, a.bopmast_id, a.deal_status, a.deal_type,
                      a.sale_type, a.sale_group, a.vehicle_type, a.stock_number, a.vin,
                      a.odometer_at_sale, a.buyer_bopname_id, a.cobuyer_bopname_id,
                      a.primary_sc, a.secondary_sc, a.fi_manager, a.origination_date,
                      a.approved_date, a.capped_date, a.delivery_date, a.gap, a.service_contract,
                      a.total_care,
                      1 as seq,
                      case
                        when a.gl_date is null then
                          (100* extract(year from a.capped_date) + extract(month from a.capped_date)):: integer
                        else
                          (100* extract(year from a.gl_date) + extract(month from a.gl_date)):: integer
                      end as year_month,
                      a.gl_count,
                      case -- delivery date may figure into this, eventually
                        when a.gl_date is null then a.capped_date
                        else a.gl_date
                      end as gl_date,
                      case
                        when a.deal_status = 'U' then 'capped'
                        when a.deal_status = 'A' then 'accepted'
                        else 'none'
                      end as deal_status, 'new row', a.hash
                    from sls.xfm_deals a
                    left join sls.deals b on a.store_code = b.store_code
                      and a.bopmast_id = b.bopmast_id
                    where a.deal_status = 'U'
                      and coalesce(a.gl_date, a.capped_date) > '01/31/2017' -- only feb and later deals
                      and b.store_code is null;
                """
                pg_cur.execute(sql)


class DealsChangedRows(luigi.Task):
    """
    compares hash of most recent xfm_deals record vs hash of most recent deals record
    6/12/18: stk H11080T/15088:
        5/15/18 uncapped  -> type_2_b, unit count = -1
        5/16/18 deleted -> type_2_c, unit_count -1 SHOULD BE unit count = 0
        if going from 2_b to 2_c in the same month and 2_b row unit count is - 1,
            new unit count s/b 0, don't know how to catch that,
        update the data and add to ignore
        see sales_pay_plan_201803/sql/troubleshooting/uncategorized_changes.sql
        2/15/19: same thing for H11735/16540
    11/22/19: for what looks like a clerical error, an accounting entry was made on an October deal,
        transaction detail in dealertrack shows in and out on a non existent stocknumber
        which generated an error due to changed gl_date, therefor, excluded bopmastid 57595
    12/23/19
        excluded bopmast_id 18539, H12810, solid november deal, kayle is just having trouble
            getting some t's crossed or i's dotted.  added this exclusion rather than accomodate
            a messy  series of irrelevant cap reset accept reset cap ...
    03/14/20
        added 60130 (G39164B) to ignore
    12/13/20
        added 64005 (G41107) to ignore
    1/14/21
        added 64182 (G38172R) to ignore, only diff is gl_date
    03/18/21
        added 65229 (G40805R) to ignore, diff is removal of cobuyer
    *** see sales_pay_play_201803/sql/troubleshooting/uncategorized_changes.sql for analyzing DANGER WILL ROB ***

    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    # -------------------------------- change nothing below this line -------------------------------------------------#
    def requires(self):
        yield DealsNewRows()

    def output(self):
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute("truncate sls.changed_deals")
                sql = """
                    truncate sls.changed_deals;
                    insert into sls.changed_deals
                    select c.hash as xfm_hash, d.hash as deal_hash,
                      case
                        -- 9/18/17: when deal status is none/none ignore
                        when d.deal_status_code = 'none' and c.deal_status = 'none' then 'ignore'                      
                        -- 8/8/17: exceptional anomalies to ignore, 31112A, see danger_will_robinson.sql
                        when c.bopmast_id in (43283,13634,43827,46610,50579,58799,60130,60157,64182) then 'ignore'
                        when c.bopmast_id in(15088, 16540, 17178) and c.store_code = 'RY2' then 'ignore'
                        when c.gl_date <> d.gl_date
                          and c.gl_count < 0 and d.unit_count > 0
                          and c.buyer_bopname_id = d.buyer_bopname_id
                          and c.deal_status = d.deal_status_code
                          and d.notes <> 'type_2_a' then 'type_2_a'  --30365 3/6
                        when c.deal_status in ('none', 'A') and d.deal_status_code = 'U' then 'type_2_b' -- 30745x 3/6
                        when (
                              c.deal_status = 'deleted'
                                and d.deal_status_code = 'U'
                                and c.gl_date <> d.gl_date
                                and d.notes <> 'type_2_a')
                          or ( -- curious possible anomaly 31135C, 31588XA, no change in gl_date
                              c.deal_status = 'deleted'
                                and d.deal_status_code in ('none','A')
                                and d.notes = 'type_2_b')
                          then 'type_2_c' -- 30157A
                        when d.notes = 'type_2_a' and c.deal_status = 'deleted' then 'type_2_d'
                        when d.notes = 'type_2_a' and c.gl_count > 0 and c.bopmast_id = d.bopmast_id then 'type_2_e'
                        when (d.notes = 'type_2_b' or d.notes = 'type_2_h') and c.gl_count > 0 and c.deal_status = 'U'
                          then 'type_2_f'
                        -- type_2_g: H10234G 5/23/17 the only thing changed was deal status: U -> deleted 
                        --      not sure why no accounting changes
                        -- except, this is an intramarket ws that was retailed at honda, that deal was deleted
                        when c.deal_status = 'deleted' and d.deal_status_code = 'U' then 'type_2_g'
                        -- 7/11/17 30838A: type_2_b with type_1 changes while still just accepted
                        when c.gl_date = d.gl_date and c.deal_status = 'A' and d.deal_status_code = 'A'
                          and d.notes = 'type_2_b' and c.gl_count = 1 and d.unit_count = -1 then 'type_2_h'
                        when d.notes = 'type_2_b' and d.deal_status_code = 'none' 
                            and c.deal_status = 'A' then 'do nothing'
                        -- temp handling of unwind anomalies, awaiting response from jeri: danger_will_robinson.sql
                        when c.deal_status <> d.deal_status_code and c.stock_number in ('31126A','31694','31493XX') then
                          'type_2_i'
                        when c.deal_status <> d.deal_status_code
                          or c.gl_count <> d.unit_count
                          or (c.gl_date <> d.gl_date
                            and -- 29164B: same year_month and no change in status and no change in count = type 1
                                (extract(year from c.gl_date) * 100) + extract(month from c.gl_date) <> 
                                    (extract(year from d.gl_date) * 100) + extract(month from d.gl_date)
                                OR c.deal_status <> d.deal_status_code
                                or c.gl_count <> d.unit_count)
                             then 'DANGER WILL ROBINSON'
                        else 'type_1'
                      end as change_type
                    from ( -- c: most recent xfm rows 1 row per store/bopmast_id
                      select *
                      from sls.xfm_deals a
                      where a.seq = (
                        select max(seq)
                        from sls.xfm_deals
                        where store_code = a.store_code
                          and bopmast_id = a.bopmast_id)) c
                    inner join (-- d: most recent deals rows 1 row per store/bopmast_id
                      select *
                      from sls.deals a
                      where a.deal_status <> 'deleted'
                        and a.seq = (
                          select max(seq)
                          from sls.deals
                          where store_code = a.store_code
                            and bopmast_id = a.bopmast_id)) d on c.store_code = d.store_code
                        and c.bopmast_id = d.bopmast_id
                      and c.hash <> d.hash
                      and c.bopmast_id not in (57595, 18539, 60897, 64005, 65229);
                """
                pg_cur.execute(sql)
                # test for questionable uncategorized changes
                sql = """
                    select count(*)
                    from sls.changed_deals
                    where change_type in('DANGER WILL ROBINSON',' do nothing')
                """
                pg_cur.execute(sql)
                if pg_cur.fetchone()[0] != 0:
                    raise ValueError('DANGER WILL ROBINSON')
                # type_1
                sql = """
                    update sls.deals w
                    set run_date = x.run_date,
                        store_code = x.store_code,
                        bopmast_id = x.bopmast_id,
                        deal_status_code = x.deal_status_code,
                        deal_type_code = x.deal_type,
                        sale_type_code = x.sale_type,
                        sale_group_code = x.sale_group,
                        vehicle_type_code = x.vehicle_type,
                        stock_number = x.stock_number,
                        vin = x.vin,
                        odometer_at_sale = x.odometer_at_sale,
                        buyer_bopname_id = x.buyer_bopname_id,
                        cobuyer_bopname_id = x.cobuyer_bopname_id,
                        primary_sc = x.primary_sc,
                        secondary_sc = x.secondary_sc,
                        fi_manager = x.fi_manager,
                        origination_date = x.origination_date,
                        approved_date = x.approved_date,
                        capped_date = x.capped_date,
                        delivery_date = x.delivery_date,
                        gap = x.gap,
                        service_contract = x.service_contract,
                        total_care = x.total_care,
                        seq = x.seq,
                        year_month = x.year_month,
                        unit_count = x.gl_count,
                        gl_date = x.gl_date,
                        deal_status = x.deal_status,
                        notes = x.notes,
                        hash = x.hash
                    from (
                      select a.run_date, a.store_code, a.bopmast_id, a.deal_status as deal_status_code, a.deal_type,
                        a.sale_type, a.sale_group, a.vehicle_type, a.stock_number, a.vin,
                        a.odometer_at_sale, a.buyer_bopname_id, a.cobuyer_bopname_id,
                        a.primary_sc, a.secondary_sc, a.fi_manager, a.origination_date,
                        a.approved_date, a.capped_date, a.delivery_date, a.gap, a.service_contract,
                        a.total_care,
                        b.seq,
                        -- 29164B: gl_date from xfm
                        b.year_month, a.gl_count, a.gl_date, b.deal_status, 'type_1' as notes, a.hash
                      from sls.changed_deals z
                      inner join  sls.xfm_deals a on z.xfm_hash = a.hash
                        and z.change_type = 'type_1'
                      inner join sls.deals b on z.deal_hash = b.hash
                        and z.change_type = 'type_1') x
                    where w.store_code = x.store_code
                      and w.bopmast_id = x.bopmast_id
                      and w.seq = x.seq;
                """
                pg_cur.execute(sql)
                # type_2_a
                sql = """
                    insert into sls.deals (run_date, store_code, bopmast_id, deal_status_code, deal_type_code,
                      sale_type_code, sale_group_code, vehicle_type_code, stock_number, vin,
                      odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
                      primary_sc, secondary_sc, fi_manager, origination_date,
                      approved_date, capped_date, delivery_date, gap, service_contract,
                      total_care, seq, year_month, unit_count,
                      gl_date, deal_status, notes, hash)
                    select a.run_date, a.store_code, a.bopmast_id, a.deal_status as deal_status_code, a.deal_type,
                      a.sale_type, a.sale_group, a.vehicle_type, a.stock_number, a.vin,
                      a.odometer_at_sale, a.buyer_bopname_id, a.cobuyer_bopname_id,
                      a.primary_sc, a.secondary_sc, a.fi_manager, a.origination_date,
                      a.approved_date, a.capped_date, a.delivery_date, a.gap, a.service_contract,
                      a.total_care,
                      (select max(seq) + 1 from sls.deals where store_code = b.store_code
                        and bopmast_id = b.bopmast_id) as seq,
                      (select year_month from dds.dim_date where the_date = a.gl_date) as year_month,
                      a.gl_count,
                      a.gl_date, a.deal_status, 'type_2_a' as notes,
                      a.hash
                    from sls.changed_deals z
                    inner join  sls.xfm_deals a on z.xfm_hash = a.hash
                      and z.change_type = 'type_2_a'
                    inner join sls.deals b on z.deal_hash = b.hash
                      and z.change_type = 'type_2_a';
                """
                pg_cur.execute(sql)
                # type_2_b
                sql = """
                    insert into sls.deals (run_date, store_code, bopmast_id, deal_status_code, deal_type_code,
                      sale_type_code, sale_group_code, vehicle_type_code, stock_number, vin,
                      odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
                      primary_sc, secondary_sc, fi_manager, origination_date,
                      approved_date, capped_date, delivery_date, gap, service_contract,
                      total_care, seq, year_month, unit_count,
                      gl_date, deal_status, notes, hash)
                    select a.run_date, a.store_code, a.bopmast_id, a.deal_status as deal_status_code, a.deal_type,
                      a.sale_type, a.sale_group, a.vehicle_type, a.stock_number, a.vin,
                      a.odometer_at_sale, a.buyer_bopname_id, a.cobuyer_bopname_id,
                      a.primary_sc, a.secondary_sc, a.fi_manager, a.origination_date,
                      a.approved_date, a.capped_date, a.delivery_date, a.gap, a.service_contract,
                      a.total_care,
                      (select max(seq) + 1 from sls.deals where store_code = b.store_code and
                        bopmast_id = b.bopmast_id) as seq,
                      -- (select year_month from dds.dim_date where the_date = a.gl_date),
                      (select year_month
                        from dds.dim_date
                        where the_date =
                          case
                            when a.gl_date = '12/31/9999' then a.run_date
                            else a.gl_date
                          end),
                      -1 as unit_count,
                      a.gl_date,
                      case
                        when a.deal_status = 'U' then 'capped'
                        when a.deal_status = 'A' then 'accepted'
                        else 'none'
                      end as deal_status, 'type_2_b' as notes,
                      a.hash
                    from sls.changed_deals z
                    inner join  sls.xfm_deals a on z.xfm_hash = a.hash
                      and z.change_type = 'type_2_b'
                    inner join sls.deals b on z.deal_hash = b.hash
                      and z.change_type = 'type_2_b';
                """
                pg_cur.execute(sql)
                # type_2_c
                sql = """
                    insert into sls.deals (run_date, store_code, bopmast_id, deal_status_code, deal_type_code,
                      sale_type_code, sale_group_code, vehicle_type_code, stock_number, vin,
                      odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
                      primary_sc, secondary_sc, fi_manager, origination_date,
                      approved_date, capped_date, delivery_date, gap, service_contract,
                      total_care, seq, year_month, unit_count,
                      gl_date, deal_status, notes, hash)
                    select a.run_date, a.store_code, a.bopmast_id, a.deal_status as deal_status_code, a.deal_type,
                      a.sale_type, a.sale_group, a.vehicle_type, a.stock_number, a.vin,
                      a.odometer_at_sale, a.buyer_bopname_id, a.cobuyer_bopname_id,
                      a.primary_sc, a.secondary_sc, a.fi_manager, a.origination_date,
                      a.approved_date, a.capped_date, a.delivery_date, a.gap, a.service_contract,
                      a.total_care,
                      (select max(seq) + 1 from sls.deals where store_code = b.store_code
                        and bopmast_id = b.bopmast_id) as seq,
                      (100* extract(year from a.gl_date) + extract(month from a.gl_date)):: integer as year_month,
                      -- a.gl_count,
                      -1 as unit_count,
                      a.gl_date, 'deleted' as deal_status, 'type_2_c' as notes,
                      a.hash
                    from sls.changed_deals z
                    inner join  sls.xfm_deals a on z.xfm_hash = a.hash
                      and z.change_type = 'type_2_c'
                    inner join sls.deals b on z.deal_hash = b.hash
                      and z.change_type = 'type_2_c';
                """
                pg_cur.execute(sql)
                # type_2_d
                sql = """
                    update sls.deals a
                    set run_date = x.run_date,
                        deal_status_code = 'deleted',
                        deal_status = 'deleted',
                        notes = 'type_2_d'
                    from (
                      select n.run_date, m.deal_hash
                      from sls.changed_deals m
                      inner join sls.xfm_deals n on m.xfm_hash = n.hash
                      where m.change_type = 'type_2_d') x
                    where a.hash = x.deal_hash;
                """
                pg_cur.execute(sql)
                # type_2_e
                sql = """
                    insert into sls.deals (run_date, store_code, bopmast_id, deal_status_code, deal_type_code,
                      sale_type_code, sale_group_code, vehicle_type_code, stock_number, vin,
                      odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
                      primary_sc, secondary_sc, fi_manager, origination_date,
                      approved_date, capped_date, delivery_date, gap, service_contract,
                      total_care, seq, year_month, unit_count,
                      gl_date, deal_status, notes, hash)
                    select a.run_date, a.store_code, a.bopmast_id, a.deal_status as deal_status_code, a.deal_type,
                      a.sale_type, a.sale_group, a.vehicle_type, a.stock_number, a.vin,
                      a.odometer_at_sale, a.buyer_bopname_id, a.cobuyer_bopname_id,
                      a.primary_sc, a.secondary_sc, a.fi_manager, a.origination_date,
                      a.approved_date, a.capped_date, a.delivery_date, a.gap, a.service_contract,
                      a.total_care,
                      (select max(seq) + 1 from sls.deals where store_code = b.store_code
                        and bopmast_id = b.bopmast_id) as seq,
                      (select year_month from dds.dim_date where the_date = a.gl_date) as year_month,
                      a.gl_count,
                      a.gl_date,
                      case
                        when a.deal_status = 'U' then 'capped'
                        when a.deal_status = 'A' then 'accepted'
                        else 'none'
                      end as deal_status,
                      'type_2_e' as notes,
                      a.hash
                    from sls.changed_deals z
                    inner join  sls.xfm_deals a on z.xfm_hash = a.hash
                      and z.change_type = 'type_2_e'
                    inner join sls.deals b on z.deal_hash = b.hash
                      and z.change_type = 'type_2_e';
                """
                pg_cur.execute(sql)
                # type_2_f
                sql = """
                    insert into sls.deals (run_date, store_code, bopmast_id, deal_status_code, deal_type_code,
                      sale_type_code, sale_group_code, vehicle_type_code, stock_number, vin,
                      odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
                      primary_sc, secondary_sc, fi_manager, origination_date,
                      approved_date, capped_date, delivery_date, gap, service_contract,
                      total_care, seq, year_month, unit_count,
                      gl_date, deal_status, notes, hash)
                    select a.run_date, a.store_code, a.bopmast_id, a.deal_status as deal_status_code, a.deal_type,
                      a.sale_type, a.sale_group, a.vehicle_type, a.stock_number, a.vin,
                      a.odometer_at_sale, a.buyer_bopname_id, a.cobuyer_bopname_id,
                      a.primary_sc, a.secondary_sc, a.fi_manager, a.origination_date,
                      a.approved_date, a.capped_date, a.delivery_date, a.gap, a.service_contract,
                      a.total_care,
                      (select max(seq) + 1 from sls.deals where store_code = b.store_code
                        and bopmast_id = b.bopmast_id) as seq,
                      (select year_month from dds.dim_date where the_date = a.gl_date) as year_month,
                      a.gl_count,
                      a.gl_date,
                      case
                        when a.deal_status = 'U' then 'capped'
                        when a.deal_status = 'A' then 'accepted'
                        else 'none'
                      end as deal_status,
                      'type_2_f' as notes,
                      a.hash
                    from sls.changed_deals z
                    inner join  sls.xfm_deals a on z.xfm_hash = a.hash
                      and z.change_type = 'type_2_f'
                      and a.run_date = (
                        select max(run_date)
                        from sls.xfm_deals
                        where store_code = a.store_code
                          and bopmast_id = a.bopmast_id)
                    inner join sls.deals b on z.deal_hash = b.hash
                      and z.change_type = 'type_2_f';
                """
                pg_cur.execute(sql)
                # type_2_g
                sql = """
                    insert into sls.deals (run_date, store_code, bopmast_id, deal_status_code, deal_type_code,
                      sale_type_code, sale_group_code, vehicle_type_code, stock_number, vin,
                      odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
                      primary_sc, secondary_sc, fi_manager, origination_date,
                      approved_date, capped_date, delivery_date, gap, service_contract,
                      total_care, seq, year_month, unit_count,
                      gl_date, deal_status, notes, hash)
                    select a.run_date, a.store_code, a.bopmast_id, a.deal_status as deal_status_code, a.deal_type,
                      a.sale_type, a.sale_group, a.vehicle_type, a.stock_number, a.vin,
                      a.odometer_at_sale, a.buyer_bopname_id, a.cobuyer_bopname_id,
                      a.primary_sc, a.secondary_sc, a.fi_manager, a.origination_date,
                      a.approved_date, a.capped_date, a.delivery_date, a.gap, a.service_contract,
                      a.total_care,
                      (select max(seq) + 1 from sls.deals where store_code = b.store_code
                        and bopmast_id = b.bopmast_id) as seq,
                      (select year_month from dds.dim_date where the_date = a.run_date),
                      -1 as unit_count,
                      a.gl_date, 'deleted' as deal_status,
                      'type_2_g' as notes,
                      a.hash
                    from sls.changed_deals z
                    inner join  sls.xfm_deals a on z.xfm_hash = a.hash
                      and z.change_type = 'type_2_g'
                      and a.run_date = (
                        select max(run_date)
                        from sls.xfm_deals
                        where store_code = a.store_code
                          and bopmast_id = a.bopmast_id)
                    inner join sls.deals b on z.deal_hash = b.hash
                      and z.change_type = 'type_2_g';
                """
                pg_cur.execute(sql)
                # type_2_h
                sql = """
                    insert into sls.deals (run_date, store_code, bopmast_id, deal_status_code, deal_type_code,
                      sale_type_code, sale_group_code, vehicle_type_code, stock_number, vin,
                      odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
                      primary_sc, secondary_sc, fi_manager, origination_date,
                      approved_date, capped_date, delivery_date, gap, service_contract,
                      total_care, seq, year_month, unit_count,
                      gl_date, deal_status, notes, hash)
                    select a.run_date, a.store_code, a.bopmast_id, b.deal_status_code, a.deal_type,
                      a.sale_type, a.sale_group, a.vehicle_type, a.stock_number, a.vin,
                      a.odometer_at_sale, a.buyer_bopname_id, a.cobuyer_bopname_id,
                      a.primary_sc, a.secondary_sc, a.fi_manager, a.origination_date,
                      a.approved_date, a.capped_date, a.delivery_date, a.gap, a.service_contract,
                      a.total_care,
                      (select max(seq) + 1 from sls.deals where store_code = b.store_code
                        and bopmast_id = b.bopmast_id) as seq,
                      b.year_month,
                      --b.unit_count,
                      0 as unit_count,
                      b.gl_date, b.deal_status,
                      'type_2_h' as notes,
                      a.hash
                    from sls.changed_deals z
                    inner join  sls.xfm_deals a on z.xfm_hash = a.hash
                      and z.change_type = 'type_2_h'
                      and a.run_date = (
                        select max(run_date)
                        from sls.xfm_deals
                        where store_code = a.store_code
                          and bopmast_id = a.bopmast_id)
                    inner join sls.deals b on z.deal_hash = b.hash
                      and z.change_type = 'type_2_h';
                """
                pg_cur.execute(sql)
                # type_2_i
                sql = """
                    insert into sls.deals (run_date, store_code, bopmast_id, deal_status_code, deal_type_code,
                      sale_type_code, sale_group_code, vehicle_type_code, stock_number, vin,
                      odometer_at_sale, buyer_bopname_id, cobuyer_bopname_id,
                      primary_sc, secondary_sc, fi_manager, origination_date,
                      approved_date, capped_date, delivery_date, gap, service_contract,
                      total_care, seq, year_month, unit_count,
                      gl_date, deal_status, notes, hash)
                    select a.run_date, a.store_code, a.bopmast_id, a.deal_status, a.deal_type,
                      a.sale_type, a.sale_group, a.vehicle_type, a.stock_number, a.vin,
                      a.odometer_at_sale, a.buyer_bopname_id, a.cobuyer_bopname_id,
                      a.primary_sc, a.secondary_sc, a.fi_manager, a.origination_date,
                      a.approved_date, a.capped_date, a.delivery_date, a.gap, a.service_contract,
                      a.total_care,
                      (select max(seq) + 1 from sls.deals where store_code = b.store_code
                        and bopmast_id = b.bopmast_id) as seq,
                      b.year_month,
                      1 as unit_count,
                      b.gl_date,
                      'capped' as deal_status,
                      'type_2_i' as notes,
                      a.hash
                    from sls.changed_deals z
                    inner join  sls.xfm_deals a on z.xfm_hash = a.hash
                      and z.change_type = 'type_2_i'
                      and a.run_date = (
                        select max(run_date)
                        from sls.xfm_deals
                        where store_code = a.store_code
                          and bopmast_id = a.bopmast_id)
                    inner join sls.deals b on z.deal_hash = b.hash
                      and z.change_type = 'type_2_i';
                """
                pg_cur.execute(sql)
