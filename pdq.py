# encoding=utf-8
"""
generate pdq writer stats for biweekly payroll
generate spreadsheet and send to the appropriate folks
everyday except monday at 4AM with previous day data, accumulated throughout the pay period
12/4
    per andrew's request added sales totals
02/25/20
    the weekly report to Ryan runs on Wednesdays at 4:55AM
"""
import luigi
import utilities
import csv
import datetime
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email import encoders
import os
import smtplib
from openpyxl import load_workbook
import openpyxl
import fact_repair_order

pipeline = 'pdq'
pg_server = '173'
ts_no_spaces = str(datetime.datetime.now()).replace(" ", "")
ts_with_minute = '{:%Y-%m-%d-%H-%M}'.format(datetime.datetime.now())

pdq_dir = 'pdq/'
file_name = pdq_dir + 'writer_stats.csv'
sos_file_name = pdq_dir + 'sos_writer_stats.csv'
spreadsheet = pdq_dir + 'pdq_writer_stats.xlsx'
sos_spreadsheet = pdq_dir + 'sos_pdq_writer_stats.xlsx'


# # if these task are being called via run_all, that task will handle all of this logging
@luigi.Task.event_handler(luigi.Event.START)
def task_start(self):
    utilities.luigi_log_start(pg_server, self.task_id + '__' + ts_no_spaces, self.pipeline, self.__class__.__name__)


@luigi.Task.event_handler(luigi.Event.SUCCESS)
def task_success(self):
    utilities.luigi_log_pass(pg_server, self.task_id + '__' + ts_no_spaces, self.pipeline, self.__class__.__name__)
    try:  # WrapperTest throws an AttributeError, see /Notes/issues
        with self.output().open("w") as out_file:
            # write to the output file
            out_file.write('pass')
    except AttributeError:
        pass


class GetPdqRos(luigi.Task):
    """
    10/14/2020: removed the ads dependency, modified all functions to use dds.fact_repair_order (pg)
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield fact_repair_order.FactRepairOrder()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute('select pdq.get_ros()')


class GetPdqRoSales(luigi.Task):
    """
    populate pdq.ro_sales
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield GetPdqRos()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute('select pdq.udpate_ro_sales()')


class CreatePdqWriterStats(luigi.Task):
    """
    create a spreadsheet of writer stats
    ads scrapes run after midnight, therefore this generates data for the
        previous day (current_date - 1)
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield GetPdqRoSales()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = """
                    select 'Pay period from '::text ||
                      to_char(biweekly_pay_period_start_date, 'MM/DD/YYYY') || ' thru '::text ||
                      to_char(biweekly_pay_period_end_date, 'MM/DD/YYYY') || '      ' ||
                      'Data thru '::text || to_char(a.the_date, 'MM/DD/YYYY')
                    from dds.dim_date a
                    join dds.working_days b on a.the_date = b.the_date
                      and b.department = 'pdq'
                    where a.the_Date = current_date - 1;                   
                """
                pg_cur.execute(sql)
                with open(file_name, 'w') as f:
                    writer = csv.writer(f)
                    writer.writerow([pg_cur.fetchone()[0]])
                    writer.writerow([])
                    pg_cur.execute("select pdq.get_ro_sales_for_pay_period('{}')".format('RY1'))
                    writer.writerow(["RY1 Sales pay period to date: $" + str(pg_cur.fetchone()[0])])
                    pg_cur.execute("select pdq.get_ro_sales_for_pay_period('{}')".format('RY2'))
                    writer.writerow(["RY2 Sales pay period to date: $" + str(pg_cur.fetchone()[0])])
                    writer.writerow([])
                    writer.writerow(["store", "writer #", "writer", "emp #", "lof", "rotate", "air filters",
                                     "cabin filters", "batteries", "bulbs", "wipers", "lof pacing"])
                    pg_cur.execute('select * from pdq.get_writer_stats();')
                    writer.writerows(pg_cur)
                wb = openpyxl.Workbook()
                wb.save(spreadsheet)
                wb = load_workbook(spreadsheet)
                ws = wb.active
                with open(file_name) as f:
                    reader = csv.reader(f, delimiter=',')
                    for row in reader:
                        ws.append(row)
                wb.save(spreadsheet)


class EmailFile(luigi.Task):
    """
    based on python projects/open_ros/email_test.py
    mods:
        a single attachment
        smtp throws: AttributeError: SMTP instance has no attribute '__exit__'
            when invoked in a context manager, maybe a python 2 issue
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield CreatePdqWriterStats()

    def output(self):
        return luigi.LocalTarget(self.local_target())

    def run(self):
        comma_space = ', '
        sender = 'jandrews@cartiva.com'
        # recipients = ['jandrews@cartiva.com', 'test@cartiva.com']
        recipients = ['jandrews@cartiva.com', 'jdangerfield@gfhonda.com', 'rshroyer@gfhonda.com',
                      'aneumann@rydellcars.com', 'wolson@rydellcars.com', 'nneumann@rydellcars.com',
                      'dmarek@rydellcars.com', 'rsattler@rydellcars.com']
        outer = MIMEMultipart()
        outer['Subject'] = 'PDQ Writer Stats'
        outer['To'] = comma_space.join(recipients)
        outer['From'] = sender
        attachments = [spreadsheet]
        for x_file in attachments:
            try:
                with open(x_file, 'rb') as fp:
                    msg = MIMEBase('application', "octet-stream")
                    msg.set_payload(fp.read())
                encoders.encode_base64(msg)
                msg.add_header('Content-Disposition', 'attachment', filename=os.path.basename(x_file))
                outer.attach(msg)
            except Exception:
                raise
        composed = outer.as_string()
        s = smtplib.SMTP('mail.cartiva.com')
        s.sendmail(sender, recipients, composed)
        s.close()


class WriterStatsSOS(luigi.Task):
    """
    create a spreadsheet of writer stats every Wednesday morning for the preceding week
    for Ryan only
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield EmailFile()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = """
                    select 'Week of '::text ||
                      (to_char(current_Date - 7, 'MM/DD/YYYY')) || ' thru '::text ||
                      (to_char(current_date - 1, 'MM/DD/YYYY'))                   
                """
                pg_cur.execute(sql)
                with open(sos_file_name, 'w') as f:
                    writer = csv.writer(f)
                    writer.writerow([pg_cur.fetchone()[0]])
                    writer.writerow([])
                    pg_cur.execute("select pdq.get_ro_sales_for_sos('{}')".format('RY2'))
                    writer.writerow(["RY2 Sales for the week : $" + str(pg_cur.fetchone()[0])])
                    writer.writerow([])
                    writer.writerow(["store", "writer #", "writer", "emp #", "lof", "rotate", "air filters",
                                     "cabin filters", "batteries", "bulbs", "wipers"])
                    pg_cur.execute('select * from pdq.get_writer_stats_sos();')
                    writer.writerows(pg_cur)
                wb = openpyxl.Workbook()
                wb.save(sos_spreadsheet)
                wb = load_workbook(sos_spreadsheet)
                ws = wb.active
                with open(sos_file_name) as f:
                    reader = csv.reader(f, delimiter=',')
                    for row in reader:
                        ws.append(row)
                wb.save(sos_spreadsheet)

        comma_space = ', '
        sender = 'jandrews@cartiva.com'
        # recipients = ['jandrews@cartiva.com', 'test@cartiva.com']
        recipients = ['jandrews@cartiva.com', 'rshroyer@gfhonda.com']
        outer = MIMEMultipart()
        outer['Subject'] = 'PDQ Writer Stats for SOS'
        outer['To'] = comma_space.join(recipients)
        outer['From'] = sender
        attachments = [sos_spreadsheet]
        for x_file in attachments:
            try:
                with open(x_file, 'rb') as fp:
                    msg = MIMEBase('application', "octet-stream")
                    msg.set_payload(fp.read())
                encoders.encode_base64(msg)
                msg.add_header('Content-Disposition', 'attachment', filename=os.path.basename(x_file))
                outer.attach(msg)
            except Exception:
                raise
        composed = outer.as_string()
        s = smtplib.SMTP('mail.cartiva.com')
        s.sendmail(sender, recipients, composed)
        s.close()


# if __name__ == '__main__':
#     luigi.run(["--workers=2"], main_task_cls=EmailFile)
