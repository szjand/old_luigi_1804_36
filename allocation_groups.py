# encoding=utf-8
"""

"""

import utilities
import luigi
import datetime
import csv
from selenium import webdriver
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.ui import Select, WebDriverWait
import requests

pipeline = 'global_connect'
pg_server = '173'
ts_no_spaces = str(datetime.datetime.now()).replace(" ", "")

user = '41d9ec15-5148-439d-938a-313a1fa91cc6'
passcode = '666'
username = None
password = None


def setup_driver():
    # # Chromedriver path
    chromedriver_path = '/usr/bin/chromedriver'
    options = webdriver.ChromeOptions()
    options.add_argument('--user-agent="Mozilla/5.0 ((Windows NT 10.0; Win64; x64) AppleWebKit/537.36 '
                         '(KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36"')
    driver = webdriver.Chrome(executable_path=chromedriver_path, options=options)
    return driver


class AllocationGroups(luigi.Task):
    """
    """
    pipeline = pipeline
    # # local
    csv_dir = 'global_connect/alloc_groups/'
    # # production
    # csv_dir = '/home/rydell/projects/luigi_1804_36/global_connect/alloc_groups/'

    file_name = csv_dir + 'ext_alloc_groups.csv'

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    @staticmethod
    def get_secret(category, platform):
        modified_platform = platform.replace(' ', '+')
        url = ("https://beta.rydellvision.com:8888/vision/secret?platform={0}&category={1}".format
               (modified_platform, category))
        response = requests.get(url, auth=(user, passcode))
        return response.json()

    @staticmethod
    def login(self, driver):
        credentials = self.get_secret('Cartiva', 'Global Connect')
        resp_dict = credentials
        username_box = driver.find_element_by_id('IDToken1')
        password_box = driver.find_element_by_id('IDToken2')
        login_btn = driver.find_element_by_name('Login.Submit')
        username_box.send_keys(resp_dict.get('username'))
        password_box.send_keys(resp_dict.get('secret'))
        login_btn.click()
        WebDriverWait(driver, 5).until(
            ec.presence_of_element_located(('name', 'modelYear')))

    def run(self):
        """
        iterate thru the dropdowns on the page to get a full list of, for each model_year, allocation groups,
            model names and model codes
        """
        driver = setup_driver()
        try:
            driver.get('https://www.autopartners.net/apps/naowb/naowb/ordervehicle/choosemodel.do?modName=ov')
            self.login(self, driver)
            with open(self.file_name, 'w') as f:
                writer = csv.writer(f)
                # initialize the select instance
                model_year_dd = Select(driver.find_element_by_name('modelYear'))
                # create a list of elements from the select instance
                model_years = [model_year.text for model_year in model_year_dd.options if int(model_year.text) > 2017]
                for year in model_years:
                    model_year_dd.select_by_value(year)
                    division_dd = Select(driver.find_element_by_name('division'))
                    divisions = [division.text for division in division_dd.options if division.text != 'Select Division'
                                 and division.text != 'CHEVROLET ALL']
                    for division in divisions:
                        division_dd.select_by_visible_text(division)
                        distribution_entity_dd = Select(driver.find_element_by_name('distEntity'))
                        distribution_entity_dd.select_by_visible_text('RET RETAIL')
                        alloc_group_dd = Select(driver.find_element_by_name('allocation'))
                        alloc_groups = [alloc_group.text for alloc_group in alloc_group_dd.options if
                                        alloc_group.text != 'Select Allocation Group']
                        for alloc_group in alloc_groups:
                            alloc_group_dd.select_by_visible_text(alloc_group)
                            model_dd = Select(driver.find_element_by_name('model'))
                            models = [model.text for model in model_dd.options if model.text != 'Select Model']
                            for model in models:
                                model_dd.select_by_visible_text(model)
                                writer.writerow([year, division, alloc_group, model])
        finally:
            driver.quit()
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute("truncate gmgl.ext_allocation_groups")
                with open(self.file_name, 'r') as io:
                    pg_cur.copy_expert("""copy gmgl.ext_allocation_groups from stdin    
                                       with csv encoding 'latin-1 '""", io)


if __name__ == '__main__':
    luigi.run(["--workers=1"], main_task_cls=AllocationGroups)
