# encoding=utf-8
"""
"""
import utilities
import luigi
import datetime
import ext_arkona
import xfm_arkona
import smtplib
from email.message import EmailMessage


pipeline = 'fact_repair_order'
db2_server = 'report'
pg_server = '173'
ts_no_spaces = str(datetime.datetime.now()).replace(" ", "")

# class
class TheClassName(luigi.Task):
    """

    """
    extract_file = '../../extract_files/sdplopc.csv'
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        return None
        # yield DimServiceWriterXfm()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        pass