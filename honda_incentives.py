# encoding=utf-8
"""
https://pypi.org/project/pdftotext/
requires sudo apt install build-essential libpoppler-cpp-dev pkg-config python3-dev
and pdftotext successfully installed
11/13/2020
    first production deploy
    convert to using secrets
cronjob 9:15 mon - sat

"""

import utilities
import luigi
import datetime
import os
from selenium import webdriver
import time
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.by import By
from pathlib import Path
import pdftotext
import requests
import smtplib
from email.message import EmailMessage

pipeline = 'honda_incentives'
pg_server = '173'
ts_no_spaces = str(datetime.datetime.now()).replace(" ", "")
# for running multiple times a day
ts_with_minute = '{:%Y-%m-%d-%H-%M}'.format(datetime.datetime.now())
dealer_number = '207818'
user = '41d9ec15-5148-439d-938a-313a1fa91cc6'
passcode = '666'
username = None
password = None

local_or_production = 'production'
# local_or_production = 'local'


# # if these task are being called via run_all, that task will handle all of this logging
@luigi.Task.event_handler(luigi.Event.START)
def task_start(self):
    utilities.luigi_log_start(pg_server, self.task_id + '__' + ts_no_spaces, self.pipeline, self.__class__.__name__)


@luigi.Task.event_handler(luigi.Event.SUCCESS)
def task_success(self):
    utilities.luigi_log_pass(pg_server, self.task_id + '__' + ts_no_spaces, self.pipeline, self.__class__.__name__)
    try:  # WrapperTest throws an AttributeError, see /Notes/issues
        with self.output().open("w") as out_file:
            # write to the output file
            out_file.write('pass')
    except AttributeError:
        pass


class IncentiveBulletinsDownload(luigi.Task):
    """
    don't yet know how i will get the actual programs of interest
    workflow will need to be something like
        ry2 checks compatablilty sheet daily
        adds relevant programs   where?
        i process each relevant program id, eg daily download the program and process for changes
        this may be trickier than it sounds
        so, we have program HP-Z30, i have the current version in the db, now i have to expose any differences
        between the current version and today's version, fine, but once that is done, when do i update the
        current version
        sounds like i need to maintain a full history,
        version and dating available in the header, lines 1-4

        !!! get some clarification on lines 37, 38: see latest available program bulletins for details  ...
        !!! ... This bulletin will not be revised

        given that some of these programs have a thru date of 09/08, get the compatablitiy sheet and do this
        on all current programs

        Z-17 hangs up
    09/07/20 that problem is not specific to that program, seems as thought after some
        number of downloads, the program gets stuck
    09/08
        empty the download_dir
        download the bulletin to  download_dir
        rename the downloaded file to program_id.pdf and move it to pdf_dir

        after only 3 downloads, the page hung, hangs hard, have to shut it down from services
        increased the wait to download to 10 and all the rest downloaded without issue
    09/09
        ran without locking up
    11/13/20
        time to deploy this
        workflow: in Vision, Honda Mgr enters the relevant programs on the Honda Incentives page, which populate
        table hn.hin_incentives. That then becomes the basis for scraping the relevant bulletins from HIN.
        at this time, all we do is test for new or changed programs, only the current version of the relevant
        bulletin(s) is stored
        as of this date, the only changes seen in a bulletin have been irrelevant spacing
        maintenace of the programs is still manual
        relevant queries are in /honda_sales/incentive_diffs.sql
    """
    pipeline = pipeline

    if local_or_production == 'local':
        download_dir = '/home/jon/projects/luigi_1804_36/honda/incentive_diff/download_dir/'
        pdf_dir = '/home/jon/projects/luigi_1804_36/honda/incentive_diff/pdf_dir/'
    elif local_or_production == 'production':
        download_dir = '/home/rydell/projects/luigi_1804_36/honda/incentive_diff/download_dir/'
        pdf_dir = '/home/rydell/projects/luigi_1804_36/honda/incentive_diff/pdf_dir/'

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                pipeline + '_' + self.__class__.__name__ + ".txt")

    # # in development, need to be able run it more than once a day
    # def local_target(self):
    #     return '../../luigi_output_files/' + ts_with_minute + '_' + self.__class__.__name__ + ".txt"

    def requires(self):
        return None

    def output(self):
        return luigi.LocalTarget(self.local_target())

    @staticmethod
    def get_secret(category, platform):
        modified_platform = platform.replace(' ', '+')
        url = ("https://beta.rydellvision.com:8888/vision/secret?platform={0}&category={1}".format
               (modified_platform, category))
        response = requests.get(url, auth=(user, passcode))
        return response.json()

    @staticmethod
    def login(self, driver):
        credentials = self.get_secret('Cartiva', 'Honda')
        resp_dict = credentials
        driver.find_element_by_xpath('//*[@id="txtDlrNo"]').send_keys(dealer_number)
        username_box = driver.find_element_by_xpath('//*[@id="txtLogonID"]')
        password_box = driver.find_element_by_xpath('//*[@id="txtPassword"]')
        login_btn = driver.find_element_by_xpath('//*[@id="btnLogon"]')
        username_box.send_keys(resp_dict.get('username'))
        password_box.send_keys(resp_dict.get('secret'))
        login_btn.click()

    def setup_driver(self):
        # # Chromedriver path
        chromedriver_path = '/usr/bin/chromedriver'
        options = webdriver.ChromeOptions()
        options.add_experimental_option("prefs", {
            "download.default_directory": self.download_dir,
            "download.prompt_for_download": False,
            "plugins.always_open_pdf_externally": True})
        options.add_argument('--user-agent="Mozilla/5.0 ((Windows NT 10.0; Win64; x64) AppleWebKit/537.36 '
                             '(KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36"')
        driver = webdriver.Chrome(executable_path=chromedriver_path, options=options)
        return driver

    def run(self):
        # empty all the processing directories
        for filename in os.listdir(self.download_dir):
            os.unlink(self.download_dir + filename)
        for filename in os.listdir(self.pdf_dir):
            os.unlink(self.pdf_dir + filename)
        driver = self.setup_driver()
        try:
            driver.get('https://www.in.honda.com/RRAAApps/login/asp/rraalog.asp')
            self.login(self, driver)
            # sales
            WebDriverWait(driver, 5).until(
                ec.element_to_be_clickable(('xpath', '//*[@id="tblTabs"]/tbody/tr/td[3]'))).click()
            # Bulletins
            WebDriverWait(driver, 5).until(
                ec.element_to_be_clickable(
                    ('xpath', '//*[@id="RDAASH030000"]/table/tbody/tr/td[1]'))).click()
            # Sales and Marketing Bulletins
            WebDriverWait(driver, 5).until(
                ec.element_to_be_clickable(
                    ('xpath', '//*[@id="RDAASH030100"]/table/tbody/tr/td[2]'))).click()
            # this page is in an iframe named Content
            WebDriverWait(driver, 5).until(
                ec.frame_to_be_available_and_switch_to_it((By.XPATH, "//iframe[@id='Content']")))
            with utilities.pg(pg_server) as pg_con:
                with pg_con.cursor() as pg_cur:
                    # get all active program_ids
                    sql = """
                        select program_id
                        from hn.hin_incentives
                        where thru_date >= current_date
                    """
                    pg_cur.execute(sql)
                    for row in pg_cur.fetchall():
                        program_id = row[0]
                        new_file_name = self.pdf_dir + program_id + '.pdf'
                        # production required a bit of a wait here
                        time.sleep(5)
                        # enter program into keyword text box
                        keyword_textbox = driver.find_element_by_id('ctl00_ctl00_M_EPSMainContent_txtInput')
                        keyword_textbox.clear()
                        keyword_textbox.send_keys(program_id)
                        # Search button
                        WebDriverWait(driver, 5).until(
                            ec.element_to_be_clickable(('xpath', '//*[@id="btnSearch"]'))).click()
                        # 10/19/20 honda now includes the rules,terms & conditions as the first row on this page
                        # that is not what we need
                        # 12/3/20 now they have removed that row, so back to the original
                        # download program bulletin
                        # WebDriverWait(driver, 5).until(
                        #     ec.element_to_be_clickable(('xpath', '//*[@id="myTable4"]/tbody/tr/td[1]/a'))).click()
                        # 12/04/20 back to the extra line
                        # download program bulletin
                        WebDriverWait(driver, 5).until(
                            ec.element_to_be_clickable(('xpath', '//*[@id="myTable4"]/tbody/tr[2]/td[1]/a'))).click()
                        # wait for file to download
                        time.sleep(10)
                        for filename in os.listdir(self.download_dir):
                            old_file_name = self.download_dir + filename
                        # rename the downloaded file and move it to the pdf_dir
                        os.rename(old_file_name, new_file_name)
                        for filename in os.listdir(self.download_dir):
                            os.unlink(self.download_dir + filename)
        finally:
            driver.quit()


class IncentiveBulletinsParse(luigi.Task):
    """
    """
    pipeline = pipeline

    if local_or_production == 'local':
        pdf_dir = '/home/jon/projects/luigi_1804_36/honda/incentive_diff/pdf_dir/'
        text_dir = '/home/jon/projects/luigi_1804_36/honda/incentive_diff/text_dir/'
    elif local_or_production == 'production':
        pdf_dir = '/home/rydell/projects/luigi_1804_36/honda/incentive_diff/pdf_dir/'
        text_dir = '/home/rydell/projects/luigi_1804_36/honda/incentive_diff/text_dir/'

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                pipeline + '_' + self.__class__.__name__ + ".txt")

    # # in development, need to be able run it more than once a day
    # def local_target(self):
    #     return '../../luigi_output_files/' + ts_with_minute + '_' + self.__class__.__name__ + ".txt"

    def requires(self):
        yield IncentiveBulletinsDownload()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        for filename in os.listdir(self.text_dir):
            os.unlink(self.text_dir + filename)
        # convert the pdfs to text
        for filename in os.listdir(self.pdf_dir):
            text_file = Path(filename).stem + '.txt'
            with open(self.pdf_dir + filename, 'rb') as f:
                pdf = pdftotext.PDF(f)
                with open(self.text_dir + text_file, 'w') as f1:
                    f1.write(''.join(pdf))
        # clean up the text file and put it in the database
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute('truncate hn.hin_incentive_bulletins_ext;')
                for filename in os.listdir(self.text_dir):
                    program_id = Path(filename).stem
                    with open(self.text_dir + filename, 'r') as f:
                        for line_number, line in enumerate(f):
                            line = line.replace('207818', '').replace('•', '').replace('*', '').replace("'", "''")
                            if line.strip():
                                sql = "insert into hn.hin_incentive_bulletins_ext values('{}',{},'{}','{}');".format(
                                    program_id, line_number + 1, line.strip(), datetime.date.today())
                                pg_cur.execute(sql)

                sql = "select * from hn.hin_incentives_check();"
                pg_cur.execute(sql)
                if pg_cur.fetchone()[0] != 0:
                    msg = EmailMessage()
                    msg.set_content('honda incentives requires attention')
                    msg['Subject'] = 'honda incentives requires attention'
                    msg['From'] = 'jandrews@cartiva.com'
                    msg['To'] = 'jandrews@cartiva.com'
                    s = smtplib.SMTP('mail.cartiva.com')
                    s.send_message(msg)
                    s.quit()


# if __name__ == '__main__':
#     luigi.run(["--workers=1"], main_task_cls=IncentiveBulletinsDownload)
