import utilities
import os

pg_server = '173'
clean_csv_dir = '/home/jon/projects/luigi_1804_36/nissan/vehicle_inventory_detail/clean_csv_files/'


def main():
    with utilities.pg(pg_server) as pg_con:
        with pg_con.cursor() as pg_cur:
            # pg_cur.execute("truncate hn.stg_nna_vehicle_options")
            # pg_con.commit()
            # pg_cur.execute("truncate hn.stg_nna_vehicle_details cascade")
            # pg_con.commit()
            # pg_cur.execute("truncate tem.nissan_vins")
            # pg_con.commit()
            # for filename in os.listdir(clean_csv_dir):
                # print(filename)
                # pg_cur.execute("insert into tem.nissan_vins values('{}')".format(filename))
                with open(clean_csv_dir + 'XL57143.csv', 'r') as io:
                    # pg_cur.execute("truncate hn.ext_nna_vehicle_details")
                    pg_cur.copy_expert("""copy hn.ext_nna_vehicle_details_1 from stdin with CSV
                                          encoding 'latin-1'""", io)
                    # pg_con.commit()
                    # pg_cur.execute("select hn.stg_nna_vehicles()")
                    # pg_con.commit()



if __name__ == '__main__':
    main()