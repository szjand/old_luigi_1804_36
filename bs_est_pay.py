# encoding=utf-8
"""

"""
import luigi
import utilities
import datetime
import fact_gl

pipeline = 'bs_est_pay'
db2_server = 'report'
pg_server = '173'


class ClockHours(luigi.Task):
    """

    """
    extract_file = '../../extract_files/edwClockHoursFact.csv'
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    # -------------------------------- change nothing below this line -------------------------------------------------#
    def requires(self):
        return None

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                # ads requirement
                sql = """
                    select count(*)
                    from ops.ads_extract
                    where the_date = current_date
                      and task = 'bspp_xfm_fact_clock_hours'
                      and complete = TRUE;
                """
                pg_cur.execute(sql)
                if pg_cur.fetchone()[0] != 1:
                    raise ValueError('The ads requirement is not there')
                # populate bspp.clock_hours
                sql = """
                    delete
                    from bspp.clock_hours
                    where the_date in (
                      select the_date
                      from bspp.xfm_fact_clock_hours);
                """
                pg_cur.execute(sql)
                sql = """
                    insert into bspp.clock_hours(the_date,user_name,reg_hours,ot_hours,pto_hours,hol_hours)
                    select a.the_date, b.user_name, sum(a.regularhours), sum(a.overtimehours),
                      sum(a.ptohours + a.vacationhours) , sum(a.holidayhours)
                    from bspp.xfm_fact_clock_hours a
                    inner join bspp.personnel b on a.employee_number = b.employee_number
                    group by a.the_date, b.user_name;
                """
                pg_cur.execute(sql)


class BsSalesDetail(luigi.Task):
    """
    new est/parts/foreman pay plans, written by jeri, implemented 3/8/18
    based on accounting data, much simpler
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    # -------------------------------- change nothing below this line -------------------------------------------------#
    def requires(self):
        # return fact_gl.FactGl()
        yield fact_gl.FactGl()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = "select bspp.update_bs_sales_detail()"
                pg_cur.execute(sql)


if __name__ == '__main__':
    luigi.run(["--workers=4"], main_task_cls=ClockHours)
