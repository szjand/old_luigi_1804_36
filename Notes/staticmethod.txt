https://stackoverflow.com/questions/23944657/typeerror-method-takes-1-positional-argument-but-2-were-given

If I have a class ...

class MyClass:

    def method(arg):
        print(arg)
... which I use to create an object ...

my_object = MyClass()
... on which I call method("foo") like so ...

>>> my_object.method("foo")
Traceback (most recent call last):
File "<stdin>", line 1, in <module>
TypeError: method() takes exactly 1 positional argument (2 given)
... why does Python tell me I gave it two arguments, when I only gave one?


In Python, this:

my_object.method("foo")
... is syntactic sugar, which the interpreter translates behind the scenes into:

MyClass.method(my_object, "foo")
... which, as you can see, does indeed have two arguments - it's just that the first one is implicit,
from the point of view of the caller.

This is because most methods do some work with the object they're called on,
so there needs to be some way for that object to be referred to inside the method.
By convention, this first argument is called self inside the method definition:

class MyNewClass:

    def method(self, arg):
        print(self)
        print(arg)
If you call method("foo") on an instance of MyNewClass, it works as expected:

>>> my_new_object = MyNewClass()
>>> my_new_object.method("foo")
<__main__.MyNewClass object at 0x29045d0>
foo

Occasionally (but not often), you really don't care about the object that your method is bound to,
and in that circumstance, you can decorate the method with the builtin staticmethod() function to say so:

class MyOtherClass:

    @staticmethod
    def method(arg):
        print(arg)
... in which case you don't need to add a self argument to the method definition, and it still works:

>>> my_other_object = MyOtherClass()
>>> my_other_object.method("foo")
foo


answered May 29 '14 at 23:27

Zero Piraeus