# encoding=utf-8
"""
file is labeld service, it iws actually sales
"""

import utilities
import luigi
import datetime
import time
from selenium import webdriver
import csv
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.by import By
import requests
import os


dealer_number = '207818'

user = '41d9ec15-5148-439d-938a-313a1fa91cc6'
passcode = '666'
username = None
password = None





def get_secret(category, platform):
    modified_platform = platform.replace(' ', '+')
    url = ("https://beta.rydellvision.com:8888/vision/secret?platform={0}&category={1}".format
           (modified_platform, category))
    response = requests.get(url, auth=(user, passcode))
    return response.json()

def setup_driver():
    # # Chromedriver path
    chromedriver_path = '/usr/bin/chromedriver'
    options = webdriver.ChromeOptions()
    options.add_argument('--user-agent="Mozilla/5.0 ((Windows NT 10.0; Win64; x64) AppleWebKit/537.36 '
                         '(KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36"')
    driver = webdriver.Chrome(executable_path=chromedriver_path, options=options)
    return driver

def main():
    driver = setup_driver()
    try:
        driver.get('https://www.in.honda.com/RRAAApps/login/asp/rraalog.asp')
        credentials = get_secret('Cartiva', 'Honda')
        resp_dict = credentials
        driver.find_element_by_xpath('//*[@id="txtDlrNo"]').send_keys(dealer_number)
        username_box = driver.find_element_by_xpath('//*[@id="txtLogonID"]')
        password_box = driver.find_element_by_xpath('//*[@id="txtPassword"]')
        login_btn = driver.find_element_by_xpath('//*[@id="btnLogon"]')
        username_box.send_keys(resp_dict.get('username'))
        password_box.send_keys(resp_dict.get('secret'))
        login_btn.click()


        # everything on the main page is in an iframe named Content
        WebDriverWait(driver, 30).until(
            ec.frame_to_be_available_and_switch_to_it((By.XPATH, '//*[@id="Content"]')))

        # Honda Sales Experience (HSE)
        WebDriverWait(driver, 30).until(ec.element_to_be_clickable(
            (By.XPATH, '//*[@id="SalesSatTelSurveySection1"]/table/tbody/tr[1]/th/a'))).click()
        driver.switch_to.window(driver.window_handles[1])
        # wait until page finishes loading
        WebDriverWait(driver, 20).until(ec.presence_of_element_located(
            ('xpath', '//*[@id="OrgGrid"]/tbody/tr[5]/td[4]/a')))
        # from this page
        rydell_dealer_nps_mtd = driver.find_element_by_xpath('//*[@id="OrgGrid"]/tbody/tr[5]/td[4]/a').text
        zone_dealer_nps_mtd = driver.find_element_by_xpath('//*[@id="OrgGrid"]/tbody/tr[4]/td[4]').text
        national_dealer_nps_mtd = driver.find_element_by_xpath('//*[@id="OrgGrid"]/tbody/tr[2]/td[4]').text
        rydell_dealer_rec_mtd = driver.find_element_by_xpath('//*[@id="OrgGrid"]/tbody/tr[5]/td[5]/a').text
        zone_dealer_rec_mtd = driver.find_element_by_xpath('//*[@id="OrgGrid"]/tbody/tr[3]/td[5]').text
        national_dealer_rec_mtd = driver.find_element_by_xpath('//*[@id="OrgGrid"]/tbody/tr[2]/td[5]').text
        sales_mtd = [rydell_dealer_nps_mtd, zone_dealer_nps_mtd, national_dealer_nps_mtd, rydell_dealer_rec_mtd,
                     zone_dealer_rec_mtd, national_dealer_rec_mtd]
        print(sales_mtd)

        ## beginning of the problem area
        ## here is the problem, i am unable to get the date range pop up to appear

        # tried the only 2 elements i see as available, neither one of them trigger the date range pop up

        # driver.find_element_by_xpath('//*[@id="ctl00_ContentPlaceHolder1_ctl00_pnlDateBox"]')
        # driver.find_element_by_xpath('//*[@id="ctl00_ContentPlaceHolder1_ctl00_lblDateRange"]')

        # WebDriverWait(driver, 10).until(ec.element_to_be_clickable(
        #     (By.XPATH, '//*[@id="ctl00_ContentPlaceHolder1_ctl00_lblDateRange"]'))).click()

        # change the time period to previous 90 days (R3M on the calendar)
        # expose the calendar
        driver.find_element_by_xpath('//*[@id="filterDesktop"]/li[5]/div[2]/div[1]/i').click()
        # click R3M
        driver.find_element_by_xpath('//*[@id="Filter-1_datePeriod"]/li[4]/a').click()
        # click Apply Filters
        # driver.find_element_by_xpath('//*[@id="Filter-1_refresh"]/a').click()
        driver.find_element_by_xpath('//*[@id="filterDesktop"]/li[5]/div[2]/div[1]/div[2]/span[2]').click()
        # wait for page to load
        WebDriverWait(driver, 20).until(ec.presence_of_element_located(
            ('xpath', '//*[@id="OrgGrid"]/tbody/tr[5]/td[4]')))
        # get the data
        rydell_dealer_nps_prev90 = driver.find_element_by_xpath('//*[@id="OrgGrid"]/tbody/tr[5]/td[4]').text
        zone_dealer_nps_prev90 = driver.find_element_by_xpath('//*[@id="OrgGrid"]/tbody/tr[4]/td[4]').text
        national_dealer_nps_prev90 = driver.find_element_by_xpath('//*[@id="OrgGrid"]/tbody/tr[2]/td[4]').text
        rydell_dealer_rec_prev90 = driver.find_element_by_xpath('//*[@id="OrgGrid"]/tbody/tr[5]/td[5]').text
        zone_dealer_rec_prev90 = driver.find_element_by_xpath('//*[@id="OrgGrid"]/tbody/tr[3]/td[5]').text
        national_dealer_rec_prev90 = driver.find_element_by_xpath('//*[@id="OrgGrid"]/tbody/tr[2]/td[5]').text
        sales_prev90 = [rydell_dealer_nps_prev90, zone_dealer_nps_prev90, national_dealer_nps_prev90,
                        rydell_dealer_rec_prev90, zone_dealer_rec_prev90, national_dealer_rec_prev90]
        print(sales_prev90)


    finally:
        driver.quit()


if __name__ == '__main__':
    main()
