# last line of a csv file is nothing but crlf
# attempting to import the file into postgresql throws this error:
#     psycopg2.errors.BadCopyFileFormat: missing data for column "first_name"
#     CONTEXT:  COPY tmp_compli_users, line 1200: ""
# solution: rewrite the file skipping blank lines:
import csv
pg_server = '173'
working_dir = '/home/jon/projects/luigi_1804_36/compli/'
file_name = working_dir + 'ExportUsers.csv'
new_file_name = working_dir + 'export_users.csv'
with open(file_name, 'r') as orig_file:
    with open(new_file_name, 'w') as mod_file:
        writer = csv.writer(mod_file)
        for row in csv.reader(orig_file):
            if len(row) > 6:  # eliminate that annoying last line
                writer.writerow(row)


# simple email message from https://docs.python.org/3/library/email.examples.html
# halleluiah, much simpler with python 3
import smtplib
from email.message import EmailMessage
msg = EmailMessage()
msg.set_content('dim tech requires attention')
msg['Subject'] = 'News from fact repair order'
msg['From'] = 'jandrews@cartiva.com'
msg['To'] = 'jandrews@cartiva.com'
s = smtplib.SMTP('mail.cartiva.com')
s.send_message(msg)
s.quit()


# this is what i am always looking for as edit in place
import os
csv_dir = '/home/jon/projects/luigi_1804_36/nissan/vehicle_inventory_detail/csv_files/'
clean_csv_dir = '/home/jon/projects/luigi_1804_36/nissan/vehicle_inventory_detail/clean_csv_files/'
for filename in os.listdir(csv_dir):
    with open(csv_dir + filename, 'r') as infile, open(
            clean_csv_dir + filename, 'w') as outfile:
        reader = csv.reader(infile)
        writer = csv.writer(outfile)
        for row in reader:
            writer.writerow(item.replace(",", "").replace('\n', ' ') for item in row)

# get rid of pesky apostrophe's (') in json response
import requests
url = 'https://beta.rydellvision.com:8888/chrome/describe-vehicle-show-available/'
response = requests.get(url)
response = response.text.replace("'", "''")

#convert xlsx to csv
import openpyxl
wb = openpyxl.load_workbook(working_dir + 'rptTimeOffTracker_TimeAllocationDetail.xlsx')
sh = wb.active
with open(working_dir + 'time_allocation_detail.csv', 'w', newline="") as f:
    c = csv.writer(f, quoting=csv.QUOTE_NONNUMERIC)
    for r in sh.rows:
        c.writerow([cell.value for cell in r])

# Convert the xls to csv
import xlrd
wb = xlrd.open_workbook(working_dir +
                        'Advisor Check-In - Usage Report_Rydell Chevrolet Buick GMC '
                        'Cadillac_2020.08.01-2020.08.31.xls')
sh = wb.sheet_by_index(0)
with open(working_dir + 'maintenance_menu_generation_percentage.csv', 'w', newline="") as f:
    col = csv.writer(f, quoting=csv.QUOTE_NONNUMERIC)
    for row in range(sh.nrows):
        col.writerow(sh.row_values(row))

# write a separate header line to csv file, followed by the output of a query
import utilities
pg_server = '173'
upload_file_path = 'batch_python/requests/'
upload_file_name = '20201130_request_1.csv'
with utilities.pg(pg_server) as pg_con:
    with pg_con.cursor() as pg_cur:
        sql = """ select current_date """
        pg_cur.execute(sql)
        with open(upload_file_path + upload_file_name, 'w') as batch_request:
            csv.writer(batch_request).writerow(['passthru_id','vin','reducing_style_id','style_id',
                                                'year','make','model','trim_name','manufacturer_model_code',
                                                'wheelbase','exterior_color','interior_color','options','equipment',
                                                'nonfactory_equipment','acode','reducing_acode','style_name',
                                                'primary_option_names','include_technical_specification_title_id'])
            csv.writer(batch_request).writerows(pg_cur.fetchall())

# --------------------------------------------------------------------------------------------------
# -< selenium
# --------------------------------------------------------------------------------------------------
# sometimes with selenium, a scraper will fail with an element not interactable error that only
# shows up when run in a terminal or from a cron job
# have had luck with using execute_script in dealerfx_reports.py
# #     1. in place of sendkeys
#         driver.execute_script("arguments[0].setAttribute('value', '" + the_password + "')", password_box)
# #     2. in place of .click()
#         driver.execute_script("arguments[0].click();", login_btn)
#
# python selenium combine xpath and link text
# 	https://stackoverflow.com/questions/41586008/finding-xpath-of-a-link-using-link-text
#
#
# python selenium combine xpath and link text with variables
# 	https://www.softwaretestinghelp.com/xpath-writing-cheat-sheet-tutorial-examples/
# 	https://www.softwaretestinghelp.com/selenium-find-element-by-text/
#
#
#
# how to use variable in xpath in selenium python
# 	https://stackoverflow.com/questions/54417784/how-to-pass-python-variable-to-xpath-expression
#
#
# python selenium combine by.xpath and by.link_text
# 	https://stackoverflow.com/questions/55159886/selenium-locating-elements-by-more-than-one-method

# --------------------------------------------------------------------------------------------------
# -/> selenium
# --------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------
# -< SQL
# --------------------------------------------------------------------------------------------------
    ## a comma separated list of all columns in a table
    # select string_agg(column_name, ',' order by ordinal_position)
    # from information_schema.columns
    # where table_schema = 'chr2'
    #   and table_name = 'tmp_vehicle_description'


    ## add/subtract a variable number of years to a date
        # select current_date, a.n, (current_date + interval '1 year' * a.n)::date
        # from dds.tally a
        # where n between 1 and 20
        
    ## generate an 8 character "password"
        # select string_agg(substr(characters, (random() * length(characters) + 1)::integer, 1), '') as random_word
        # from (values('ABCDEFGHIJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz23456789#$@&?')) as symbols(characters)
        # -- length of word
        # join generate_series(1, 8) on 1 = 1);

    ## index on an array attribute: CREATE INDEX on techs USING GIN (tech_key);

    ## join on an array attribute:
        # select *
        # from dds.fact_repair_order a
        # left join techs b on a.tech_key = any(b.tech_key)
        # where ro = '16245005'

    ## restore dim_ccc.ccc_key to serial data type
        # create sequence dds.dim_ccc_ccc_key_seq  ::  table_name + field_name + 'seq'
        #   owned by dds.dim_ccc.ccc_key;  :: sequence owned by attribute  table_name.attribute_name
        # alter table dds.dim_ccc
        # alter column ccc_key set default nextval('dds.dim_ccc_ccc_key_seq');
        # select setval('dds.dim_ccc_ccc_key_seq', (select max(ccc_key)from dds.dim_ccc));

# --------------------------------------------------------------------------------------------------
# -/> SQL
# --------------------------------------------------------------------------------------------------

# csv/excel
    # # this notation ensures leading zeros in when opening a csv in excel (payroll_reports/master_deduction_list)
        # select a.pymast_company_number as store, a.employee_name as name,
        #   '="' ||a.department_code || '"' as department
        # from arkona.xfm_pymast a

    # # a common Concatenate problem in Excel is that the function result shows a date or time as a number
        # the solution is to use the TEXT(value, format_text ) function
        #        A                 B
        # 1	02/02/2016	=CONCATENATE( "The Date is: ", TEXT( A1, "mm/dd/yyyy" ) )


# AG The Silver Searcher super fast file contents search
    # # usage
        # ag [options] pattern [path ...]
    # # doc or just ag -help
        # https://github.com/ggreer/the_silver_searcher/blob/master/doc/ag.1.md
    # # change directory to E
        # cd/mnt/hgfs/E/"python projects"
    # # search for all sql files that contain arkona.xfm_pypclockin
        # ag arkona.xfm_pypclockin -- sql
    # # -Q --literal: Do not parse PATTERN as a regular expression. Try to match it literally.
        # ag -Q "jon.configurations" --sql

# ---------------------------------------------------------------------------------------
# -< Linux
# ---------------------------------------------------------------------------------------
#
# file/directory sizes
# du -hsc *
# - shows private
# du -sch .[!.]* *
# You can easily check disk space status with df -h
#
#
# text in files
# grep -Ril "text-to-find-here" /
# i stands for ignore case (optional in your case).
# R stands for recursive.
# l stands for "show the file name, not the result itself".
# / stands for starting at the root of your machine.
#
# Ubuntu 16.04 Unity
# window opens off screen
# ALT + TAB : select the application
# ALT + SPACE : select move
# can now move the window onto the screen
#
# find file/direcory names
# https://www.linode.com/docs/tools-reference/tools/find-files-in-linux-using-the-command-line/
# find . -name testfile.txt				Find a file called testfile.txt in current and sub-directories.
# find /home -name *.jpg					Find all .jpg files in the /home and sub-directories.
# find . -type f -empty					Find an empty file within the current directory.
# find /home -user exampleuser -mtime 7 -iname ".db"	Find all .db files (ignoring text case) modified in
# 								the last 7 days by a user named exampleuser.
# jon@ubuntu:/mnt/hgfs/E/DDS2$ find . -name *adj*		from linux guest, find any filename like adj
#
# test connection to remote host via a specific port (21):
# jon@ubuntu-1804:~$ nc -zv grab.cartiva.com 21
# Connection to grab.cartiva.com 21 port [tcp/ftp] succeeded!
#
#
# --< display file permissions in octal -----------------------------------------------------
# stat -c '%A %a %n' *
# %A Access rights in human readable form
#
# %a Access rights in octal
#
# %n File name
#
# Octal numbers and permissions
#
# You can use octal number to represent mode/permission:
#
# r: 4
# w: 2
# x: 1
# For example, for file owner you can use octal mode as follows. Read, write and execute (full)
# permission on a file in octal is 0+r+w+x = 0+4+2+1 = 7
#
# Only Read and write permission on a file in octal is 0+r+w+x = 0+4+2+0 = 6
#
# Only read and execute permission on a file in octal is 0+r+w+x = 0+4+0+1 = 5
#
# Use above method to calculate permission for group and others. Let us say you wish to give
# full permission to owner, read & execute permission to group, and read only permission to others,
# then you need to calculate permission as follows:
# 	User = r+w+x = 0+4+2+1 = 7
# 	Group= r+w+x = 0+4+2+0 = 6
# 	Others = r+w+x = 0+0+0+1 = 1
#
# Effective permission is 761.
#
# --/> display file permissions in octal -----------------------------------------------------
#
#
#
# Number	Permission Type		Symbol
# 0	No Permission		---
# 1	Execute			--x
# 2	Write			-w-
# 3	Execute + Write		-wx
# 4	Read			r--
# 5	Read + Execute		r-x
# 6	Read + Write		rw-
# 7	Read + Write + Execute	rwx
#
#
# --< 64 or 32 bit python ------------------------------------------------------------
# import ctypes
# print(ctypes.sizeof(ctypes.c_voidp))
# 8 = 64 bit
# 4 = 32 bit
#
# import platform
# platform.architecture()[0]
# --/> 64 or 32 bit python ------------------------------------------------------------

# ---------------------------------------------------------------------------------------
# -/> Linux
# ---------------------------------------------------------------------------------------