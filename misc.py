# encoding=utf-8
"""
1/9/20
    Assortment of individual tasks that can be run under luigi on the RunAll schedule
    remove class ChromeVersion
    add class KeyperKeysCheckedOut
    add class AddUpdateNrvEmployees
    add class Menusys: inventory for menusys made available on download.cartiva.com
07/17/20
    add class TimeClockPtoData
09/02/20
    add class ServiceAdvisorDashboardDmsDataUpdate
09/06/20
    moved AddUpdateNrvEmployees to compli.py
10/20/20
    added UpdateNcSalesByZip
"""
import luigi
import utilities
import datetime
from datetime import timedelta
import ftplib
import zipfile
import os
import glob
import fact_gl
import xfm_arkona
import used_cars
import time
import ext_arkona
import csv
from openpyxl import load_workbook
import openpyxl
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email import encoders
import smtplib
import fact_repair_order


pipeline = 'misc'
db2_server = 'report'
pg_server = '173'


class Flightplan(luigi.Task):
    """

    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield fact_gl.FactGl()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute("select fp.update_flightplan()")


class AllInventory(luigi.Task):
    """

    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield fact_gl.FactGl()
        yield xfm_arkona.XfmInpmast()
        yield used_cars.DailySnapshotBeta1()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute("select nrv.update_all_inventory()")


class OzarkVehicles(luigi.Task):
    """
    3/30/19
        ozark chevrolet DMS feed
        automate sends us files with the date encoded, eg,  20190328_TRC65.zip
        so i have been making a separate download directory for each day
        don't need that history,
        want to deploy to luigi, so refactor to download the file and delete it daily
        puting the contents of _batch_vehc.txt only in db, don't have column headers for the other files
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        # server = 'grab.cartiva.com'
        server = '172.17.196.26'
        username = 'jandrews'
        password = 'Andrews1954'
        date_string = str((datetime.datetime.now() - timedelta(1)).strftime('%Y%m%d'))
        file_match = date_string + '*.zip'
        ftp_target = '../../extract_files/automate/'
        zip_file_name = ftp_target + date_string + '_TRC65.zip'
        vehicles_file_name = ftp_target + date_string + '_TRC65_batch_vehc.txt'
        ftp = ftplib.FTP(server)
        ftp.login(username, password)
        ftp.cwd('automate/')
        for filename in ftp.nlst(file_match):
            fhandle = open(ftp_target + filename, 'wb')
            ftp.retrbinary('RETR ' + filename, fhandle.write)
            fhandle.close()
        ftp.quit()
        zipfile.ZipFile(zip_file_name).extractall(ftp_target)
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute("truncate ftp_inv.ext_ozark_vehicles")
                with open(vehicles_file_name, 'r') as io:
                    pg_cur.copy_expert("""copy ftp_inv.ext_ozark_vehicles from stdin
                                          with csv HEADER DELIMITER as '|' encoding 'latin-1'""", io)
                sql = """
                    insert into ftp_inv.ozark_vehicles
                    select current_date, a.*
                    from ftp_inv.ext_ozark_vehicles a;
                """
                pg_cur.execute(sql)
        files = glob.glob(ftp_target + '*')
        for f in files:
            os.remove(f)


class OzarkVinSolutions(luigi.Task):
    """
    ozark chevrolet vinsolutions feed
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        # server = 'grab.cartiva.com'
        server = '172.17.196.26'
        username = 'jandrews'
        password = 'Andrews1954'
        file_name = 'ozarkchevrolet.csv'
        # directory to which the file will be downloaded
        ftp_target = '../../extract_files/'
        ftp = ftplib.FTP(server)
        ftp.login(username, password)
        ftp.cwd('OzarkChev/')
        with open(ftp_target + file_name, 'wb') as fhandle:
            ftp.retrbinary('RETR ' + file_name, fhandle.write)
        ftp.quit()
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute("truncate ftp_inv.ext_ozark_vinsolutions")
                with open(ftp_target + file_name, 'r') as io:
                    pg_cur.copy_expert("""copy ftp_inv.ext_ozark_vinsolutions from stdin 
                                          with csv HEADER encoding 'latin-1'""", io)
                sql = """
                    insert into ftp_inv.ozark_vinsolutions
                    select current_date, a.*
                    from ftp_inv.ext_ozark_vinsolutions a;
                """
                pg_cur.execute(sql)


class StpaulAutosWbl(luigi.Task):
    """
    st paul autos DMS inventory feed
    4/2/19 failed each night since deploying in luigi with error: [Errno 111] Connection refused
    try adding a dependency on the previous task, guessing at a connection limit ...
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        # server = 'grab.cartiva.com'
        server = '172.17.196.26'
        username = 'jandrews'
        password = 'Andrews1954'
        file_name = 'WBLInvent.CSV'
        # directory to which the file will be downloaded
        ftp_target = '../../extract_files/'
        ftp = ftplib.FTP(server)
        ftp.login(username, password)
        ftp.cwd('stpautos/')
        with open(ftp_target + file_name, 'wb') as fhandle:
            ftp.retrbinary('RETR ' + file_name, fhandle.write)
        ftp.quit()
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute("truncate ftp_inv.ext_stpautos_wbl")
                with open(ftp_target + file_name, 'r') as io:
                    pg_cur.copy_expert("""copy ftp_inv.ext_stpautos_wbl from stdin 
                                          with csv HEADER encoding 'latin-1'""", io)
                sql = """
                    insert into ftp_inv.stpautos_wbl
                    select current_date, a.*
                    from ftp_inv.ext_stpautos_wbl a;
                """
                pg_cur.execute(sql)


class FtpInventory(luigi.Task):
    """
    other dealers inventory FTP'd to cartiva
    this simply asserts that a "reasonable" feed (100 rows) for current date has been processed
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield OzarkVehicles()
        yield OzarkVinSolutions()
        yield StpaulAutosWbl()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = """
                    select ftp_inv.assert_current_date_inventory();
                """
                pg_cur.execute(sql)


class CleanUp(luigi.Task):
    """
    delete files from ~/luigi_output_files older than 7 days
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        current_time = time.time()
        the_path = '/home/rydell/luigi_output_files'
        for f in os.listdir(the_path):
            the_file = os.path.join(the_path, f)
            if os.stat(the_file).st_mtime < current_time - (7 * 86400):
                os.remove(the_file)


class KeyperKeysCheckedOut(luigi.Task):
    """
    generate a daily email for ben that includes a spreadsheet of all keys currently checked out
    from the keyper system
    02/10/20: added count to the email subject line
    04/07/20: all of a sudden, ws = wb.get_active_sheet() is throwing an error,
        AttributeError: 'Workbook' object has no attribute 'get_active_sheet', replaced with ws = wb.active
    """
    pipeline = pipeline
    # # local
    # csv_dir = '/home/jon/projects/luigi_1804_36/keyper/'
    # production
    csv_dir = '/home/rydell/projects/luigi_1804_36/keyper/'
    file_name = csv_dir + 'ext_keyper.csv'
    spreadsheet = csv_dir + 'keys_currently_checked_out.xlsx'

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield ext_arkona.ExtInpmast()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute("select keys.get_keys_currently_checked_out();")
                pg_cur.execute("select count(*) from keys.keys_currently_checked_out;")
                the_count = str(pg_cur.fetchone()[0])
                pg_cur.execute("select * from keys.keys_currently_checked_out;")
                with open(self.file_name, 'w') as f:
                    writer = csv.writer(f)
                    writer.writerow(["stock_number", "transaction_date", "out_from", "out_to", "issue_reason",
                                     "days_out"])
                    writer.writerows(pg_cur)
                wb = openpyxl.Workbook()
                wb.save(self.spreadsheet)
                wb = load_workbook(self.spreadsheet)
                ws = wb.active
                with open(self.file_name) as f:
                    reader = csv.reader(f, delimiter=',')
                    for row in reader:
                        ws.append(row)
                wb.save(self.spreadsheet)
        comma_space = ', '
        sender = 'jandrews@cartiva.com'
        # recipients = ['jandrews@cartiva.com', 'test@cartiva.com']
        recipients = ['jandrews@cartiva.com', 'bcahalan@rydellcars.com']
        outer = MIMEMultipart()
        outer['Subject'] = the_count + ' Keys checked out from Keyper System'
        outer['To'] = comma_space.join(recipients)
        outer['From'] = sender
        attachments = [self.spreadsheet]
        for x_file in attachments:
            try:
                with open(x_file, 'rb') as fp:
                    msg = MIMEBase('application', "octet-stream")
                    msg.set_payload(fp.read())
                encoders.encode_base64(msg)
                msg.add_header('Content-Disposition', 'attachment', filename=os.path.basename(x_file))
                outer.attach(msg)
            except Exception:
                raise
        composed = outer.as_string()
        s = smtplib.SMTP('mail.cartiva.com')
        s.sendmail(sender, recipients, composed)
        s.close()


class FpInterestCredits(luigi.Task):
    """
    generate a daily email with interest credits
    """
    pipeline = pipeline
    body = ''

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                pg_cur.execute("select nc.get_fp_interest_credits();")
                pg_cur.execute("select the_data from nc.tmp_interest_credits  ;")
                for t in pg_cur.fetchall():
                    self.body += t[0] + '\n'
                self.body += '\n'

        comma_space = ', '
        sender = 'jandrews@cartiva.com'
        # recipients = ['jandrews@cartiva.com', 'test@cartiva.com']
        recipients = ['jandrews@cartiva.com', 'gsorum@cartiva.com', 'bcahalan@rydellcars.com', 'abruggeman@cartiva.com',
                      'jschmiess@rydellcars.com', 'tmonson@rydellcars.com', 'myem@rydellcars.com']
        outer = MIMEText(self.body)
        outer['Subject'] = 'Floorplan Interest Credits'
        outer['To'] = comma_space.join(recipients)
        outer['From'] = sender
        composed = outer.as_string()
        s = smtplib.SMTP('mail.cartiva.com')
        s.sendmail(sender, recipients, composed)
        s.close()


class Menusys(luigi.Task):
    """
    replace the old utilities menusys scheduled task
    generates an inventory file and puts it on download.cartiva.com
    """
    pipeline = pipeline
    # local
    # path = '/home/jon/projects/luigi_1804_36/menusys/'
    # production
    path = '/home/rydell/projects/luigi_1804_36/menusys/'

    file_name = 'MenuSysRydInv.csv'

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield ext_arkona.ExtInpmast()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = "select 'DEALERKEY','STOCKNUMBER','VIN','YEAR','MAKE','MODEL','N/U','MILES','COST','PRICE'"
                pg_cur.execute(sql)
                with open(self.path + self.file_name, 'w') as f:
                    csv.writer(f).writerows(pg_cur)
                sql = """
                    select 'f3429528-9e83-40cb-81b1-070f305500f1' as dealerkey, inpmast_stock_number as stocknumber,
                        inpmast_vin as vin, year, make, model, type_n_u as "N/U",
                        odometer as miles, inpmast_vehicle_cost as cost, list_price as price
                    from arkona.ext_inpmast
                    where status = 'I'
                """
                pg_cur.execute(sql)
                with open(self.path + self.file_name, 'a') as f:
                    csv.writer(f).writerows(pg_cur)
        server = 'download.cartiva.com'
        username = 'download'
        password = 'th!s!sc@rt!v@ftp'
        ftp = ftplib.FTP(server)
        ftp.login(username, password)
        ftp.storbinary('STOR ' + self.file_name, open(self.path + self.file_name, 'rb'))
        ftp.quit()


class TimeClockPtoData(luigi.Task):
    """
    updates pto.used_pto with time clock data
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield xfm_arkona.XfmPypclockin()

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = """select pto.used_pto_update();"""
                pg_cur.execute(sql)


class ServiceAdvisorDashboardDmsDataUpdate(luigi.Task):
    """
    updates all sad tables
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield fact_gl.FactGl()
        yield fact_repair_order.FactRepairOrder()

    def output(self):
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = "select sad.dms_data_nightly_update();"
                pg_cur.execute(sql)


class UpdateNcSalesByZip(luigi.Task):
    """
    calls function nc.update_sales_by_zip() which updates tables nc.sales_zip_by_make_model &
        nc.sales_zip_by_make_model_by_day
    """
    pipeline = pipeline

    def local_target(self):
        return ('../../luigi_output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        yield ext_arkona.ExtBopmast()
        yield ext_arkona.ExtBopname()

    def output(self):
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.pg(pg_server) as pg_con:
            with pg_con.cursor() as pg_cur:
                sql = "select nc.update_sales_by_zip();"
                pg_cur.execute(sql)


class MiscWrapper(luigi.WrapperTask):
    """
    a wrapper task that invokes all the classes in this module
    this task is susequently listed as a requirement in run_all
    that way i don't need to construct artificial dependencies just to ensure everything gets run
    """
    pipeline = pipeline

    def requires(self):
        yield Flightplan()
        yield AllInventory()
        yield FtpInventory()
        yield KeyperKeysCheckedOut()
        yield CleanUp()
        yield FpInterestCredits()
        yield Menusys()
        yield TimeClockPtoData()
        yield ServiceAdvisorDashboardDmsDataUpdate()
        yield UpdateNcSalesByZip()


# if __name__ == '__main__':
#     luigi.run(["--workers=1"], main_task_cls=FpInterestCredits)
